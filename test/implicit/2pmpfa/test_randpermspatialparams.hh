// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
#ifndef DUMUX_RANDPERM_SPATIAL_PARAMS_HH
#define DUMUX_RANDPERM_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include "dumux/material/spatialparameters/gstatrandompermeability.hh"

namespace Dumux
{

//forward declaration
template<class TypeTag>
class RandPermSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(RandPermSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(RandPermSpatialParams, SpatialParams, Dumux::RandPermSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(RandPermSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}


template<class TypeTag>
class RandPermSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
        typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
        typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
        typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename Grid::ctype CoordScalar;

        typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;

        enum
        {
            dim = Grid::dimension, dimWorld = Grid::dimensionworld
        };

        typedef typename GridView::Traits::template Codim<0>::Entity Element;
        typedef typename GridView::template Codim<0>::Iterator ElementIterator;

        typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
        typedef typename GridView::IndexSet IndexSet;

        typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
        typedef Dune::FieldMatrix<Scalar, dim, dim> FieldMatrix;
        typedef std::vector<FieldMatrix> PermeabilityType;
        typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
    typedef std::vector<MaterialLawParams> MaterialLawParamsVector;

    void loadIntrinsicPermeability(Problem& problem)
    {
        int size = gridView_.size(0);
        materialLawParams_.resize(size);
        permeability_.resize(size);

        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        ScalarField logPerm(size);
        ScalarField perm(size);

        bool create = false;
        if (ParameterTree::tree().hasKey("SpatialParams.GenerateNewPermeability"))
            create = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool , SpatialParams, GenerateNewPermeability);

        std::string gStatControlFileName("gstatControl_2D.txt");

        std::string gStatInputFileName("gstatInput2D.txt");

        std::string permeabilityFileName("permeab2D.dat");


        GstatRandomPermeability<GridView, Scalar> randomPermeability(gridView_,
                create, permeabilityFileName.c_str(), gStatControlFileName.c_str(), gStatInputFileName.c_str());

        Scalar totalVolume = 0;
        Scalar meanPermeability = 0;

        PrimaryVariables source;

        Scalar sourcePerm = 0;
        if (ParameterTree::tree().hasKey("SpatialParams.SourcePerm"))
            sourcePerm = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SourcePerm);

        std::vector<FieldMatrix> permLexi(gridView_.size(0));
        //Iterate over elements
        ElementIterator eItEnd = gridView_.template end<0> ();
        for (ElementIterator eIt = gridView_.template begin<0> (); eIt
                != eItEnd; ++eIt)
        {
            int globalIdx = indexSet_.index(*eIt);

            permLexi[globalIdx] = randomPermeability.K(*eIt);
        }

        std::ofstream dataFile_;
        dataFile_.open("soildata");

        for (int i = 0; i < gridView_.size(0); i++)
        {
            dataFile_ << permLexi[i] << "\n";
        }

        dataFile_.close();


        for (ElementIterator eIt = gridView_.template begin<0> (); eIt
                != eItEnd; ++eIt)
        {
            int globalIdx = indexSet_.index(*eIt);

            permeability_[globalIdx] = randomPermeability.K(*eIt);

            problem.sourceAtPos(source, eIt->geometry().center());

            if ((source[0] != 0.0 || source[1] != 0.0) && sourcePerm != 0.0)
            {
                for (int i = 0; i < dim; i++)
                    for (int j = 0; j < dim; j++)
                        permeability_[globalIdx][i][j] = (i==j) ? sourcePerm : 0.0;
            }

            Scalar volume = eIt->geometry().volume();
            totalVolume += volume;

            meanPermeability += volume / permeability_[globalIdx][0][0];
        }

        meanPermeability /= totalVolume;
        meanPermeability = 1 / meanPermeability;

        Scalar referencePd = 1000;
        if (ParameterTree::tree().hasKey("SpatialParams.ReferenceEntryPressure"))
            referencePd = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, ReferenceEntryPressure);
        Scalar BCLambda = 2.0;
        if (ParameterTree::tree().hasKey("SpatialParams.BCLambda"))
            BCLambda = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, BCLambda);
        Scalar sWr = 0.0;
        if (ParameterTree::tree().hasKey("SpatialParams.Swr"))
            sWr = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Swr);
        Scalar sNr = 0.0;
        if (ParameterTree::tree().hasKey("SpatialParams.Snr"))
            sNr = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Snr);

        //Iterate over elements
        for (int globalIdx = 0; globalIdx < size; globalIdx++)
        {
            Scalar elementEntryPressure = referencePd * sqrt(
                    meanPermeability / permeability_[globalIdx][0][0]);
            materialLawParams_[globalIdx].setPe(elementEntryPressure);
            materialLawParams_[globalIdx].setLambda(BCLambda);
            materialLawParams_[globalIdx].setSwr(sWr);
            materialLawParams_[globalIdx].setSnr(sNr);
        }

        Dune::VTKWriter < GridView > vtkwriter(gridView_);

        for (int i = 0; i < size; i++)
        {
            logPerm[i] = log10(permeability_[i][0][0]);
            perm[i] = permeability_[i][0][0];
        }
        vtkwriter.addCellData(perm, "absolute permeability");
        vtkwriter.addCellData(logPerm, "logarithm of permeability");

        std::string permFileName("randomperm");
        if (ParameterTree::tree().hasKey("SpatialParams.PermeabilitVtkFileName"))
            permFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, SpatialParams,
                PermeabilitVtkFileName);
        vtkwriter.write(permFileName, Dune::VTK::ascii);
    }

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    RandPermSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView),
      indexSet_(gridView.indexSet()),
      materialLawParams_(gridView.size(0)),
      permeability_(gridView.size(0)), porosity_(0.2)
    {
        if (ParameterTree::tree().hasKey("SpatialParams.Porosity"))
            porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
    }

    /*!
     * \brief Returns the scalar intrinsic permeability \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    FieldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    {
        return permeability_[indexSet_.index(element)];
    }

    FieldMatrix intrinsicPermeability(const Element &element) const
    {
        return permeability_[indexSet_.index(element)];
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     *
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return porosity_; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
        return materialLawParams_[indexSet_.index(element)];
    }


private:
    const GridView& gridView_;
    const IndexSet& indexSet_;
    MaterialLawParamsVector materialLawParams_;
    PermeabilityType permeability_;
    Scalar porosity_;
};

} // end namespace
#endif

