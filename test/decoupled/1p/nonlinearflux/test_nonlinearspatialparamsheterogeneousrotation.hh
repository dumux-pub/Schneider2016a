// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef TEST_NONLINEAR_SPATIALPARAMS_HETEROGENEOUSROTATION_HH
#define TEST_NONLINEAR_SPATIALPARAMS_HETEROGENEOUSROTATION_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux
{

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for 1-p diffusion models.
 */
template<class TypeTag>
class TestNonlinearSpatialParamsHeterogeneousRotation: public FVSpatialParamsOneP<TypeTag>
{
    typedef FVSpatialParamsOneP<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        return permeability_[indexSet_.index(element)];
        //return permeability_;
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    void initialize(const double delta)
    {
        delta_ = 1.0e-3;


        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            setPermeability_(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }


    }

    TestNonlinearSpatialParamsHeterogeneousRotation(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet()) /*, permeability_(0)*/
    {

        double pi = 4.0*atan(1.0);

        //thetaOne_ = 0.6981317007977316802; //40 Grad
        //thetaOne_ = 0.872664625997165; // 50 Grad
        //thetaTwo_ = 0.523598775598299; // 30 Grad

        thetaOne_ = pi/6.0;
        thetaTwo_ = -thetaOne_;
        thetaThree_ = -thetaOne_;
        thetaFour_ = thetaOne_;

        k1_ = 1000.0;
        k2_ = 1.0;

        delta_ = 1.0e-3;

        //double cost = cos(thetaOne_);
        //double sint = sqrt(1.0 - cost*cost);

        //permeability_[0][0] = cost*cost + delta_*sint*sint;
        //permeability_[1][1] = sint*sint + delta_*cost*cost;
        //permeability_[0][1] = permeability_[1][0] = cost*sint*(1.0 - delta_);

    }

private:
    void setPermeability_(FieldMatrix& perm, const GlobalPosition& globalPos) const
    {


//        double rt = globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1];
//
//        perm[0][0] = (delta_*globalPos[0]*globalPos[0] + globalPos[1]*globalPos[1])/rt;
//        perm[0][1] = -(1.0 - delta_)*globalPos[0]*globalPos[1]/rt;
//        perm[1][0] = perm[0][1];
//        perm[1][1] = (globalPos[0]*globalPos[0] + delta_*globalPos[1]*globalPos[1])/rt;


        double cost = cos(thetaOne_);
        //double sint = sqrt(1.0 - cost*cost);
        double sint = sin(thetaOne_);

        if(globalPos[0] <=0.5 && globalPos[1] <=0.5){
            cost = cos(thetaOne_);
            sint = sin(thetaOne_);
        }else if(globalPos[0] >0.5 && globalPos[1] <=0.5){
            cost = cos(thetaTwo_);
            sint = sin(thetaTwo_);
        }else if(globalPos[0] <=0.5 && globalPos[1] >0.5){
            cost = cos(thetaThree_);
            sint = sin(thetaThree_);
        }else if(globalPos[0] >0.5 && globalPos[1] >0.5){
            cost = cos(thetaFour_);
            sint = sin(thetaFour_);
        }

        //permeability_[0][0] = cost*cost + delta_*sint*sint;
        //permeability_[1][1] = sint*sint + delta_*cost*cost;
        //permeability_[0][1] = permeability_[1][0] = cost*sint*(1.0 - delta_);

        perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
        perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
        perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);

    }

    const GridView gridView_;
    const IndexSet& indexSet_;
    std::vector<FieldMatrix> permeability_;
    //FieldMatrix permeability_;
    double delta_;
    double thetaOne_;
    double thetaTwo_;
    double thetaThree_;
    double thetaFour_;
    double k1_;
    double k2_;
};

} // end namespace
#endif
