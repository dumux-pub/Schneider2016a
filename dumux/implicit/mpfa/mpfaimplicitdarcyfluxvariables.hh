// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        volume fluxes of fluid phases over a face of a finite volume by means
 *        of the Darcy approximation.
 *
 */
#ifndef DUMUX_MPFA_IMPLICIT_DARCY_FLUX_VARIABLES_HH
#define DUMUX_MPFA_IMPLICIT_DARCY_FLUX_VARIABLES_HH

#include <dune/common/float_cmp.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>

//#include <dumux/implicit/common/implicitproperties.hh>


namespace Dumux
{

namespace Properties
{
// forward declaration of properties
NEW_PROP_TAG(ImplicitMobilityUpwindWeight);
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(NumPhases);
NEW_PROP_TAG(ProblemEnableGravity);
}

/*!
 * \ingroup ImplicitFluxVariables
 * \brief Evaluates the normal component of the Darcy velocity
 * on a (sub)control volume face.
 */
template <class TypeTag>
class MpfaImplicitDarcyFluxVariables
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaInteractionVolume) InteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaOInteractionVolume) MpfaOInteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaFluxCalculator) FluxCalculator;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaOFluxCalculator) MpfaOFluxCalculator;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum { dim = GridView::dimension} ;
    enum { dimWorld = GridView::dimensionworld} ;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<1>::Entity Facet;
    typedef typename GridView::Intersection Intersection;

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)};

    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;

public:
    /*!
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    MpfaImplicitDarcyFluxVariables(const Problem &problem,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int fIdx,
                 const ElementVolumeVariables &elemVolVars,
                 const bool onBoundary = false)
    : fvGeometry_(fvGeometry), faceIdx_(fIdx), onBoundary_(onBoundary)
    {
        mobilityUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MobilityUpwindWeight);

        massUpwindWeight_ = 1; // GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);

        calculateFlux(problem, element, elemVolVars);
    }

public:

        /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face. face().normal has already the
     *        magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
    Scalar volumeFlux(const unsigned int phaseIdx) const
    { return volumeFlux_[phaseIdx]; }

    /*!
     * \brief Return the local index of the downstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int downstreamIdx(const unsigned phaseIdx) const
    { return downstreamIdx_[phaseIdx]; }

    /*!
     * \brief Return the local index of the upstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int upstreamIdx(const unsigned phaseIdx) const
    { return upstreamIdx_[phaseIdx]; }

    /*!
     * \brief Return the SCV (sub-control-volume) face. This may be either
     *        a face within the element or a face on the element boundary,
     *        depending on the value of onBoundary_.
     */
    const SCVFace &face() const
    {
            return fvGeometry_.subContVolFace[faceIdx_];
    }



protected:

    /*!
     * \brief Calculation of the flux through an element face
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateFlux(const Problem &problem,
                       const Element &element,
                       const ElementVolumeVariables &elemVolVars)
    {
        // Reference element of the current element
        const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());

        // loop over the phases
        for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
        {
            volumeFlux_[phaseIdx] = 0;

            // loop over the nodes of the face
            for (int faceNode = 0; faceNode < element.template subEntity<1>(faceIdx_)->geometry().corners(); faceNode++)
            {
                // get the local node indices of the nodes with respect to element
                int localVertIdx = referenceElement.subEntity(faceIdx_, 1, faceNode, dim);

                // check whether we have to handle an MPFA-O interaction volume or not
                bool isMpfaO = problem.model().interactionVolumeContainer().isMpfaOVolume(element, faceIdx_, localVertIdx);

                // calculate flux
                if (!isMpfaO)
                {
                    InteractionVolume &interactionVolume = *(problem.model().getInteractionVolume(element, faceIdx_, localVertIdx));
                    int faceIndexInRegion = -1;
                    Scalar tempMassFlux = FluxCalculator::calculateScvFaceFlux(problem, element, faceIdx_, interactionVolume, elemVolVars, fvGeometry_, phaseIdx, faceIndexInRegion);

                    // determine the upwind direction
                    if (tempMassFlux > 0)
                    {
                        upstreamIdx_[phaseIdx] = face().i;
                        downstreamIdx_[phaseIdx] = face().j;
                    }
                    else
                    {
                        upstreamIdx_[phaseIdx] = face().j;
                        downstreamIdx_[phaseIdx] = face().i;
                    }
                    // obtain the upwind volume variables
                    const VolumeVariables& upVolVars = elemVolVars[ upstreamIdx(phaseIdx) ];
                    const VolumeVariables& downVolVars = elemVolVars[ downstreamIdx(phaseIdx) ];

                    tempMassFlux *= mobilityUpwindWeight_*upVolVars.mobility(phaseIdx)
                            + (1.0 - mobilityUpwindWeight_)*downVolVars.mobility(phaseIdx);

                    tempMassFlux *= massUpwindWeight_*upVolVars.fluidState().density(phaseIdx)
                            + (1.0 - massUpwindWeight_)*downVolVars.fluidState().density(phaseIdx);

                    volumeFlux_[phaseIdx] += tempMassFlux;
                }
                else
                {
                    MpfaOInteractionVolume &interactionVolume = *(problem.model().getMpfaOInteractionVolume(element, faceIdx_, localVertIdx));
                    int faceIndexInRegion = -1;
                    Scalar tempMassFlux = MpfaOFluxCalculator::calculateScvFaceFlux(problem, element, faceIdx_, interactionVolume, elemVolVars, fvGeometry_, phaseIdx, faceIndexInRegion);
                    const typename MpfaOInteractionVolume::SubVolumeFace& subFace = interactionVolume.getSubVolumeFace(faceIndexInRegion);

                    // set the flux
                    if (subFace.faceType == MpfaOInteractionVolume::FaceTypes::InternalDirichletFace)
                    {
                        PrimaryVariables interiorDirichletValues; VolumeVariables interiorDirichletVolVars;
                        problem.internalDirichletAtPos(interiorDirichletValues, subFace.center);
                        interiorDirichletVolVars.update(interiorDirichletValues, problem, element, fvGeometry_, 0, false);
                        // determine the upwind direction
                        if (tempMassFlux > 0)
                        {
                            upstreamIdx_[phaseIdx] = face().i;
                            downstreamIdx_[phaseIdx] = face().j;
                            // obtain the upwind volume variables
                            const VolumeVariables& upVolVars = elemVolVars[ upstreamIdx(phaseIdx) ];
                            const VolumeVariables& downVolVars = interiorDirichletVolVars;

                            tempMassFlux *= mobilityUpwindWeight_*upVolVars.mobility(phaseIdx)
                                    + (1.0 - mobilityUpwindWeight_)*downVolVars.mobility(phaseIdx);

                            tempMassFlux *= massUpwindWeight_*upVolVars.fluidState().density(phaseIdx)
                                    + (1.0 - massUpwindWeight_)*downVolVars.fluidState().density(phaseIdx);
                        }
                        else
                        {
                            upstreamIdx_[phaseIdx] = face().j;
                            downstreamIdx_[phaseIdx] = face().i;

                            // obtain the upwind volume variables
                            const VolumeVariables& upVolVars = interiorDirichletVolVars;
                            const VolumeVariables& downVolVars = elemVolVars[ downstreamIdx(phaseIdx) ];

                            tempMassFlux *= mobilityUpwindWeight_*upVolVars.mobility(phaseIdx)
                                    + (1.0 - mobilityUpwindWeight_)*downVolVars.mobility(phaseIdx);

                            tempMassFlux *= massUpwindWeight_*upVolVars.fluidState().density(phaseIdx)
                                    + (1.0 - massUpwindWeight_)*downVolVars.fluidState().density(phaseIdx);
                        }
                    }
                    else
                    {
                        // determine the upwind direction
                        if (tempMassFlux > 0)
                        {
                            upstreamIdx_[phaseIdx] = face().i;
                            downstreamIdx_[phaseIdx] = face().j;
                        }
                        else
                        {
                            upstreamIdx_[phaseIdx] = face().j;
                            downstreamIdx_[phaseIdx] = face().i;
                        }
                        // obtain the upwind volume variables
                        const VolumeVariables& upVolVars = elemVolVars[ upstreamIdx(phaseIdx) ];
                        const VolumeVariables& downVolVars = elemVolVars[ downstreamIdx(phaseIdx) ];

                        tempMassFlux *= mobilityUpwindWeight_*upVolVars.mobility(phaseIdx)
                                + (1.0 - mobilityUpwindWeight_)*downVolVars.mobility(phaseIdx);

                        tempMassFlux *= massUpwindWeight_*upVolVars.fluidState().density(phaseIdx)
                                + (1.0 - massUpwindWeight_)*downVolVars.fluidState().density(phaseIdx);
                    }
                    volumeFlux_[phaseIdx] += tempMassFlux;
                }
            }
        }
    }

    const FVElementGeometry &fvGeometry_;       //!< Information about the geometry of discretization
    const unsigned int faceIdx_;                //!< The index of the sub control volume face
    const bool      onBoundary_;                //!< Specifying whether we are currently on the boundary of the simulation domain
    unsigned int    upstreamIdx_[numPhases] , downstreamIdx_[numPhases]; //!< local index of the upstream / downstream vertex
    Scalar          volumeFlux_[numPhases] ;    //!< Velocity multiplied with normal (magnitude=area)
    Scalar          mobilityUpwindWeight_;      //!< Upwind weight for mobility. Set to one for full upstream weighting
    Scalar          massUpwindWeight_;
};

} // end namespace

#endif // DUMUX_MPFA_IMPLICIT_DARCY_FLUX_VARIABLES_HH
