// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
#ifndef DUMUX_OBLIQUEDRAIN_SPATIALPARAMS_HH
#define DUMUX_OBLIQUEDRAIN_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams1p.hh>

namespace Dumux
{

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
template<class TypeTag>
class ObliqueDrainSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    ObliqueDrainSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {

            double delta = 0.2;
            double theta = atan(delta);

            double cost = cos(theta);
            double sint = sin(theta);

            double k1_= 100;
            double k2_ = 10;

            permOne_[0][0] = cost*cost*k1_ + sint*sint*k2_;
            permOne_[1][1] = sint*sint*k1_ + cost*cost*k2_;
            permOne_[0][1] = permOne_[1][0] = cost*sint*(k1_ - k2_);

            k1_= 1;
            k2_ = 0.1;

            permTwo_[0][0] = cost*cost*k1_ + sint*sint*k2_;
            permTwo_[1][1] = sint*sint*k1_ + cost*cost*k2_;
            permTwo_[0][1] = permTwo_[1][0] = cost*sint*(k1_ - k2_);
    }

    /*!
     * \brief Return the intrinsic permeability for the current sub-control volume in [m^2].
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        GlobalPosition globalPos = element.geometry().center();
        double x = globalPos[0];
        double y = globalPos[1];


        double Phi_One = y - 0.2*(x-0.5) - 0.475;
        double Phi_Two = Phi_One - 0.05;

        if(Phi_One < 0 || Phi_Two >  0) return permTwo_;
        else return permOne_;

        //eturn permOne_;
    }

    DimWorldMatrix intrinsicPermeability(const Element& element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        double x = globalPos[0];
        double y = globalPos[1];


        double Phi_One = y - 0.2*(x-0.5) - 0.475;
        double Phi_Two = Phi_One - 0.05;

        if(Phi_One < 0 || Phi_Two >  0) return permTwo_;
        else return permOne_;
    }

//    DimWorldMatrix intrinsicPermeability() const
//    {
//
//        return permOne_;
//    }

    /*! \brief Define the porosity in [-].
   *
   * \param element The finite element
   * \param fvGeometry The finite volume geometry
   * \param scvIdx The local index of the sub-control volume where
   */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    { return 1.0; }

private:

    DimWorldMatrix permOne_;
    DimWorldMatrix permTwo_;
};
} // end namespace
#endif

