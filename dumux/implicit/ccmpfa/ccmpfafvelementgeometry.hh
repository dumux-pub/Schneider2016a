// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Represents the finite volume geometry of a single element in
 *        the cell-centered fv scheme.
 */
#ifndef DUMUX_CC_MPFA_FV_ELEMENTGEOMETRY_HH
#define DUMUX_CC_MPFA_FV_ELEMENTGEOMETRY_HH

#include <dune/common/version.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/intersectioniterator.hh>

#include <dumux/common/propertysystem.hh>
#include <dumux/implicit/common/normaldecompositioninteractionvolume.hh>

namespace Dumux
{
namespace Properties
{
NEW_PROP_TAG(GridView);
NEW_PROP_TAG(Scalar);
}

/*!
 * \ingroup CCModel
 * \brief Represents the finite volume geometry of a single element in
 *        the cell-centered fv scheme.
 */
template<class TypeTag>
class CCMpfaFVElementGeometry
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{dimWorld = GridView::dimensionworld};

    enum{maxNFAP = 2}; //! maximum number of flux approximation points
    //enum{maxNFAP = (dim == 2 ? 9 : 27)}; //! maximum number of flux approximation points
    enum{maxF = 2*dim*(1 << (dim - 1))}; //! maximum number of faces
    enum{maxBF = dim << 1}; //! maximum number of boundary faces
    //enum{maxNeigh = (dim == 2 ? 9 : 27)};
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename Element::Geometry Geometry;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;

    typedef Dumux::NormalDecompositionInteractionVolume<TypeTag> InteractionVolume;
    typedef Dumux::CoNormalDecompositionFaceStencil<TypeTag> FaceStencil;

public:
    struct SubControlVolume //! FV intersected with element
    {
        LocalPosition local; //!< local position
        GlobalPosition global; //!< global position
        Scalar volume; //!< volume of scv
        bool inner;
    };

    struct SubControlVolumeFace //! interior face of a sub control volume
    {
        int i,j; //!< scvf seperates corner i and j of elem
        //LocalPosition ipLocal; //!< integration point in local coords
        GlobalPosition ipGlobal; //!< integration point in global coords
        GlobalPosition normal; //!< normal on face pointing to CV j or outward of the domain with length equal to |scvf|
        GlobalPosition unitNormal;
        Scalar area; //!< area of face
        Dune::FieldVector<GlobalPosition, maxNFAP> grad; //!< derivatives of shape functions at ip
        Dune::FieldVector<Scalar, maxNFAP> shapeValue; //!< value of shape functions at ip
        Dune::FieldVector<int, maxNFAP> fapIndices; //!< indices w.r.t.neighbors of the flux approximation points
        //Dune::FieldVector<int, maxNFAP> fapIndicesNeigh; //!< indices w.r.t.neighbors of the flux approximation points if neighboring elements
        unsigned numFap; //!< number of flux approximation points
        //unsigned numFapNeigh; //!< number of flux approximation points neighboring element
        //unsigned fIdx; //!< the index (w.r.t. the element) of the face (codim 1 entity) that the scvf is part of
        //unsigned fNeighIdx; //!< the index (w.r.t. the neighboring element) of the face (codim 1 entity) that the scvf is part of
    };

    typedef SubControlVolumeFace BoundaryFace; //!< compatibility typedef

    LocalPosition elementLocal; //!< local coordinate of element center
    GlobalPosition elementGlobal; //!< global coordinate of element center
    Scalar elementVolume; //!< element volume
    SubControlVolume subContVol[1]; //!< data of the sub control volumes
    SubControlVolumeFace subContVolFace[maxF]; //!< data of the sub control volume faces
    BoundaryFace boundaryFace[maxBF]; //!< data of the boundary faces
    int numScv; //!< number of subcontrol volumes
    //int numScvf; //!< number of inner-domain subcontrolvolume faces
    int numNeighbors; //!< number of neighboring elements including the element itself
    int numBoundaryGhostNeighbors;
    std::vector<ElementPointer> neighbors; //!< stores pointers for the neighboring elements
    std::vector<IntersectionIterator> boundaryIntersections;
    std::vector<GlobalPosition> boundaryEvaluationPoints;
    std::map< std::pair<int,int>, int> globalIdxToLocalIdx;

    int getLocalIndex(std::pair<int,int> index) const
    {

        auto iter = globalIdxToLocalIdx.find(index);
        if (iter != globalIdxToLocalIdx.end())
        {
            return iter->second;
        }
        std::cout<<"ERROR!!!!!!!!!!!!!!! CANT FIND INDEX"<<std::endl;
        return -1;
    }

    void updateInner(const Element& element)
    {
        const Geometry& geometry = element.geometry();

        elementVolume = geometry.volume();
        elementGlobal = geometry.center();
        elementLocal = geometry.local(elementGlobal);

        numScv = 1;
        //numScvf = 0;

        subContVol[0].local = elementLocal;
        subContVol[0].global = elementGlobal;
        subContVol[0].inner = true;
        subContVol[0].volume = elementVolume;

        // initialize neighbors list with self:
        numNeighbors = 1;
        numBoundaryGhostNeighbors = 0;
        neighbors.clear();
        //neighbors.reserve(maxNeigh);
        ElementPointer elementPointer(element);
        neighbors.push_back(elementPointer);
        boundaryIntersections.clear();
        boundaryEvaluationPoints.clear();
        globalIdxToLocalIdx.clear();
    }

    void update(const GridView& gridView, const Element& element, const Problem &problem)
    {
        updateInner(element);

        bool complexBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                ComplexBoundary);

        std::vector<ElementPointer> boundaryGhostElements;
        std::vector<std::pair<int,int>> boundaryIndices;
        std::vector<IntersectionIterator> boundaryIntersect;
        std::vector<GlobalPosition> boundaryEvalPoints;

        const Geometry& geometry = element.geometry();

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalIdxI = problem.elementMapper().index(element);
#else
        int globalIdxI = problem.elementMapper().map(element);
#endif
        const InteractionVolume* interactionVolumeI = problem.model().getInteractionVolume(globalIdxI);

        bool onBoundary = false;

        std::pair<int, int> index(globalIdxI,-1);
        globalIdxToLocalIdx[index] = numNeighbors-1;

        int fIdx = -1;
        // fill neighbor information and control volume face data:
        IntersectionIterator isEndIt = gridView.iend(element);
        for (IntersectionIterator isIt = gridView.ibegin(element); isIt != isEndIt; ++isIt)
        {
            const auto isGeometry = isIt->geometry();

            // neighbor information and inner cvf data:
            if (isIt->neighbor())
            {
                fIdx++;
                //int faceIdxI = isIt->indexInInside();
                //int faceIdxJ = isIt->indexInOutside();

                std::set< std::pair<int,int> > indices;
                indices.insert(std::pair<int,int>(globalIdxI,-1));

                ElementPointer elementJ(isIt->outside());
                //int globalIdxJ = problem.variables().index(*elementJ);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int globalIdxJ = problem.elementMapper().index(*elementJ);
#else
                int globalIdxJ = problem.elementMapper().map(*elementJ);
#endif
                index = std::pair<int,int>(globalIdxJ,-1);
                if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end()){
                    numNeighbors++;
                    neighbors.push_back(elementJ);
                    globalIdxToLocalIdx[index] = numNeighbors -1;
                    indices.insert(index);
                }

                const FaceStencil* faceStencilI = interactionVolumeI->getFaceStencilVectorPlus(index);
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const std::vector<ElementPointer> elementVectorI = faceStencilI->getStencilElementVector();
                //const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<GlobalPosition> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();

                for(int i = 0; i<numFAPI; i++){

                    if(globalIdxVectorI[i] > -1)
                    {
                        //index = std::pair<int,int>(globalIdxVectorI[i], -1);
                        index = stencilFaceGlobalIdxI[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            numNeighbors++;
                            neighbors.push_back(elementVectorI[i]);
                            globalIdxToLocalIdx[index] = numNeighbors -1;
                        }
                    }
                    else
                    {
                        index = stencilFaceGlobalIdxI[i];
                        //index = std::pair<int,int>(globalIdxI, stencilFaceIdxI[i]);
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            //numBoundaryGhostNeighbors++;
                            boundaryGhostElements.push_back(elementVectorI[i]);
                            boundaryIndices.push_back(index);
                            boundaryIntersect.push_back(interactionVolumeI->getElementIntersection(index));
                            boundaryEvalPoints.push_back(stencilCenterPointsI[i]);
                        }
                    }


                }

                index = std::pair<int,int>(globalIdxI, -1);
                const InteractionVolume* interactionVolumeJ = problem.model().getInteractionVolume(globalIdxJ);
                const FaceStencil* faceStencilJ = interactionVolumeJ->getFaceStencilVectorPlus(index);
                const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                const std::vector<ElementPointer> elementVectorJ = faceStencilJ->getStencilElementVector();
                //const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                const std::vector<GlobalPosition> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                const int numFAPJ = faceStencilJ->getNumElements();
                indices.insert(std::pair<int,int>(globalIdxJ,-1));

                for(int i = 0; i< numFAPJ; i++){

                    if(globalIdxVectorJ[i] > -1)
                    {
                        //index = std::pair<int,int>(globalIdxVectorJ[i], -1);
                        index = stencilFaceGlobalIdxJ[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            numNeighbors++;
                            neighbors.push_back(elementVectorJ[i]);
                            globalIdxToLocalIdx[index] = numNeighbors -1;
                        }
                    }
                    else
                    {
                        index = stencilFaceGlobalIdxJ[i];
                        //index = std::pair<int,int>(globalIdxJ, stencilFaceIdxJ[i]);
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            //numBoundaryGhostNeighbors++;
                            boundaryGhostElements.push_back(elementVectorJ[i]);
                            boundaryIndices.push_back(index);
                            //neighbors.push_back(elementNeighbors[i]);
                            //globalIdxToLocalIdx[index] = numNeighbors -1;
                            boundaryIntersect.push_back(interactionVolumeJ->getElementIntersection(index));
                            boundaryEvalPoints.push_back(stencilCenterPointsJ[i]);
                        }
                    }

                }


                int numTotalStencilPoints = indices.size();
                //int numTotalStencilPoints = 2;

                SubControlVolumeFace& scvFace = subContVolFace[fIdx];

                scvFace.i = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI,-1)];
                scvFace.j = globalIdxToLocalIdx[std::pair<int,int>(globalIdxJ,-1)];

                scvFace.ipGlobal = isGeometry.center();
                //scvFace.ipLocal =  geometry.local(scvFace.ipGlobal);
                scvFace.unitNormal = isIt->centerUnitOuterNormal();
                scvFace.normal = scvFace.unitNormal;
                scvFace.normal *= isGeometry.volume();
                scvFace.area = isGeometry.volume();

                GlobalPosition distVec = elementGlobal
                                       - elementJ->geometry().center();
                distVec /= distVec.two_norm2();

                // gradients using a two-point flux approximation
                scvFace.numFap = 2;

//                int fapIndex = 0;
//                for(auto setIt = indices.begin(); setIt != indices.end(); ++setIt)
//                {
//                    scvFace.fapIndices[fapIndex] = globalIdxToLocalIdx[*setIt];
//                    scvFace.grad[fapIndex] = distVec;
//                    scvFace.shapeValue[fapIndex] = 0.5;
//                    fapIndex++;
//                }

                for (unsigned int fapIdx = 0; fapIdx < scvFace.numFap; fapIdx++)
                {
                    scvFace.grad[fapIdx] = distVec;
                    scvFace.shapeValue[fapIdx] = 0.5;
                }
                scvFace.grad[1] *= -1.0;

                scvFace.fapIndices[0] = scvFace.i;
                scvFace.fapIndices[1] = scvFace.j;

                //scvFace.fIdx = faceIdxI;
                //scvFace.fNeighIdx = faceIdxJ;

                //scvfIdx++;
            }

            // boundary cvf data
            if (isIt->boundary())
            {
                onBoundary = true;

                int faceIdxI = isIt->indexInInside();
//                ElementPointer elementPointer(element);
                index = std::pair<int,int>(globalIdxI, faceIdxI);
//                if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end()){
//                    numBoundaryGhostNeighbors++;
//                    boundaryGhostElements.push_back(elementPointer);
//                    boundaryIndices.push_back(index);
//                    boundaryIntersections.push_back(isIt);
//                    boundaryEvaluationPoints.push_back(stencilCenterPointsI[i]);
//                }

                std::set< std::pair<int,int> > indices;

                const FaceStencil* faceStencilI = interactionVolumeI->getFaceStencilVectorPlus(index);
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const std::vector<ElementPointer> elementVectorI = faceStencilI->getStencilElementVector();
                //const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<GlobalPosition> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const int numFAPI = faceStencilI->getNumElements();

                //indices.insert(index);

                for(int i = 0; i<numFAPI; i++){

                    if(globalIdxVectorI[i] > -1)
                    {
                        //index = std::pair<int,int>(globalIdxVectorI[i], -1);
                        index = stencilFaceGlobalIdxI[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            numNeighbors++;
                            neighbors.push_back(elementVectorI[i]);
                            globalIdxToLocalIdx[index] = numNeighbors -1;
                        }
                    }
                    else
                    {
                        //index = std::pair<int,int>(globalIdxI, stencilFaceIdxI[i]);
                        index = stencilFaceGlobalIdxI[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            //numBoundaryGhostNeighbors++;
                            boundaryGhostElements.push_back(elementVectorI[i]);
                            boundaryIndices.push_back(index);
                            boundaryIntersect.push_back(interactionVolumeI->getElementIntersection(index));
                            boundaryEvalPoints.push_back(stencilCenterPointsI[i]);
                        }
                    }


                }

                if(complexBoundary)
                {
                index = std::pair<int,int>(globalIdxI, faceIdxI);
                const FaceStencil* faceStencilJ =  interactionVolumeI->getFaceStencilVectorMinus(index);
                const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                const std::vector<ElementPointer> elementVectorJ = faceStencilJ->getStencilElementVector();
                const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                const std::vector<GlobalPosition> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                const int numFAPJ = faceStencilJ->getNumElements();
                indices.insert(index);

                for(int i = 0; i< numFAPJ; i++){

                    if(globalIdxVectorJ[i] > -1)
                    {
                        //index = std::pair<int,int>(globalIdxVectorJ[i], -1);
                        index = stencilFaceGlobalIdxJ[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            numNeighbors++;
                            neighbors.push_back(elementVectorJ[i]);
                            globalIdxToLocalIdx[index] = numNeighbors -1;
                        }
                    }
                    else
                    {
                        index = stencilFaceGlobalIdxJ[i];
                        indices.insert(index);
                        if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end())
                        {
                            //numBoundaryGhostNeighbors++;
                            boundaryGhostElements.push_back(elementVectorJ[i]);
                            boundaryIndices.push_back(index);
                            //neighbors.push_back(elementNeighbors[i]);
                            //globalIdxToLocalIdx[index] = numNeighbors -1;
                            boundaryIntersect.push_back(isIt);
                            boundaryEvalPoints.push_back(stencilCenterPointsJ[i]);
                        }
                    }

                }

                }

                int numTotalStencilPoints = indices.size();
                //int numTotalStencilPoints = indices.size();

                //int numTotalStencilPoints = 2;

                SubControlVolumeFace& bFace = boundaryFace[faceIdxI];

//                bFace.fIdx = faceIdxI;
//                bFace.fNeighIdx = faceIdxI;

                //bFace.ipGlobal = isGeometry.center();
                //bFace.ipLocal =  geometry.local(bFace.ipGlobal);
                bFace.unitNormal = isIt->centerUnitOuterNormal();
                bFace.normal = bFace.unitNormal;
                bFace.normal *= isGeometry.volume();
                bFace.area = isGeometry.volume();
                bFace.i = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI,-1)];
                bFace.j = bFace.i;
                //bFace.i = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI,-1)];
                //bFace.j = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI, faceIdxI)];

                GlobalPosition distVec = elementGlobal - isGeometry.center();;
                distVec /= distVec.two_norm2();

                // gradients using a two-point flux approximation
//                bFace.numFap = numTotalStencilPoints;
//                int fapIndex = 0;
//                for(auto setIt = indices.begin(); setIt != indices.end(); ++setIt)
//                {
//                    bFace.fapIndices[fapIndex] = globalIdxToLocalIdx[*setIt];
//                    bFace.grad[fapIndex] = distVec;
//                    bFace.shapeValue[fapIndex] = 0.5;
//                    fapIndex++;
//                }

                bFace.numFap = 2;
                for (unsigned int fapIdx = 0; fapIdx < bFace.numFap; fapIdx++)
                {
                    bFace.grad[fapIdx] = distVec;
                    bFace.shapeValue[fapIdx] = 0.5;
                }
                bFace.grad[1] *= -1.0;

                bFace.fapIndices[0] = bFace.i;
                bFace.fapIndices[1] = bFace.j;

                //bfIdx++;
            }
        }

        // set the number of inner-domain subcontrolvolume faces
        //numScvf = scvfIdx + 1;

        // treat elements on the boundary


        for (int bfIdx = 0; bfIdx < boundaryIndices.size()/*numBoundaryGhostNeighbors*/; bfIdx++)

        {
            //ElementPointer elementPointer(element);
            IntersectionIterator isIt = boundaryIntersect[bfIdx];
            //int globalIdx = problem.variables().index(*boundaryGhostElements[bfIdx]);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalIdx = problem.elementMapper().index(*boundaryGhostElements[bfIdx]);
#else
            int globalIdx = problem.elementMapper().map(*boundaryGhostElements[bfIdx]);
#endif

            int faceIdxI = isIt->indexInInside();
            std::pair<int,int> index(boundaryIndices[bfIdx]);
            if(globalIdxToLocalIdx.find(index) == globalIdxToLocalIdx.end()){
                globalIdxToLocalIdx[index] = numNeighbors + numBoundaryGhostNeighbors;
                neighbors.push_back(boundaryGhostElements[bfIdx]);
                numBoundaryGhostNeighbors++;

                boundaryEvaluationPoints.push_back(boundaryEvalPoints[bfIdx]);
                boundaryIntersections.push_back(boundaryIntersect[bfIdx]);

                if(globalIdx == globalIdxI){
                    SubControlVolumeFace& bFace = boundaryFace[faceIdxI];
                    bFace.i = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI,-1)];
                    bFace.j = globalIdxToLocalIdx[std::pair<int,int>(globalIdxI, faceIdxI)];
                    bFace.ipGlobal = boundaryEvalPoints[bfIdx];
                    //bFace.ipLocal =  geometry.local(bFace.ipGlobal);
                    //bFace.fapIndices[1] = bFace.j;
                }

            }


        }


        //std::cout << numNeighbors <<std::endl;
    }
};

}

#endif

