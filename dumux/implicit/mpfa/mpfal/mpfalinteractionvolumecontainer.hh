#ifndef DUMUX_MPFA_L_INTERACTIONVOLUMECONTAINER_HH
#define DUMUX_MPFA_L_INTERACTIONVOLUMECONTAINER_HH


#include <dumux/common/math.hh>
#include <dumux/implicit/mpfa/mpfaproperties.hh>

#include <dumux/implicit/mpfa/mpfao/2d/mpfao2dinteractionvolume.hh>
#include <dumux/implicit/mpfa/mpfao/2d/mpfao2dmanager.hh>

namespace Dumux{

template<class TypeTag>
class MpfaLInteractionVolumeContainer
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaInteractionVolume) InteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaOInteractionVolume) MpfaOInteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaInteractionVolumeManager) InteractionVolumeManager;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaOInteractionVolumeManager) MpfaOManager;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;


public:

    MpfaLInteractionVolumeContainer(): problemPtr_(0)
    {}

    void init(Problem &problem)
    {
        problemPtr_ = &problem;

        // For the Mpfa-L method we use O-Method interaction volumes for the sub faces
        // that are connected to the boundary and L-Method interaction volumes for interior faces
        int oVolumesCounter = 0;
        int lVolumesCounter = 0;

        // first of all, do only the elements, that have intersections with the boundary
        // in order to properly set up MPFA-O interaction volumes
        ElementIterator eIt = problem.gridView().template begin<0>();
        ElementIterator eEndIt = problem.gridView().template end<0>();
        for(; eIt != eEndIt; ++eIt)
        {
            if (!eIt->hasBoundaryIntersections())
                continue;

            // global element index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalElemIdx = problem_().elementMapper().index(*eIt);
#else
            int globalElemIdx = problem_().elementMapper().map(*eIt);
#endif

            // Reference element will be needed later on
            const ReferenceElement& referenceElement = ReferenceElements::general(eIt->geometry().type());

            IntersectionIterator isIt = problem.gridView().ibegin(*eIt);
            IntersectionIterator isEnd = problem.gridView().iend(*eIt);
            for(; isIt != isEnd; ++isIt)
            {
                // loop over the nodes of the facet
                for(int facetNode = 0; facetNode < isIt->geometry().corners(); facetNode++)
                {
                    // get node index w.r.t element
                    int localVertCorner = referenceElement.subEntity(isIt->indexInInside(), 1, facetNode, dim);

                    // get global index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    int globalVertIdx = problem_().vertexMapper().index(*(eIt->template subEntity<dim>(localVertCorner)));
#else
                    int globalVertIdx = problem_().vertexMapper().map(*(eIt->template subEntity<dim>(localVertCorner)));
#endif

                    // only make volume if does not already exist
                    if (!hasVolumeBeenStored(*eIt, isIt->indexInInside(), localVertCorner) )
                    {
                        // If node touches boundary, create an MPFA-O volume, otherwise create an L-volume
                        if (halfFaceTouchesBoundary(*eIt, isIt, localVertCorner))
                        {
                            MpfaOInteractionVolume* iaVolume = new MpfaOInteractionVolume;

                            MpfaOManager::fillInteractionVolume(iaVolume, *eIt, problem, isIt->indexInInside(), localVertCorner, globalVertIdx);

                            mpfaOInteractionVolumes_.push_back(iaVolume);
                            oVolumesMap_.insert( std::pair<int, int> (globalVertIdx, oVolumesCounter) );
                            oVolumesCounter++;
                        }
                        else
                        {
                            InteractionVolume* iaVolume1 = new InteractionVolume(false);
                            InteractionVolume* iaVolume2 = new InteractionVolume(true);

                            InteractionVolumeManager::fillInteractionVolume(iaVolume1, *eIt, problem, isIt->indexInInside(), localVertCorner, globalVertIdx);
                            InteractionVolumeManager::fillInteractionVolume(iaVolume2, *eIt, problem, isIt->indexInInside(), localVertCorner, globalVertIdx);

                            int faceIndexIn1 = iaVolume1->getFluxFaceIdx();
                            int faceIndexIn2 = iaVolume2->getFluxFaceIdx();

                            typename InteractionVolume::TransmissivityMatrix &T1 = iaVolume1->getTransMatrix();
                            typename InteractionVolume::TransmissivityMatrix &T2 = iaVolume2->getTransMatrix();

                            Scalar t1_0 = T1[faceIndexIn1][0];
                            Scalar t2_0 = T2[faceIndexIn2][0];

                            if ( fabs(t1_0) < fabs(t2_0) )
                                mpfaLInteractionVolumes_.push_back(iaVolume1);
                            else
                                mpfaLInteractionVolumes_.push_back(iaVolume2);

                            std::pair<int, int> iSectionAndNode = std::make_pair(isIt->indexInInside(), localVertCorner);
                            std::pair <int, std::pair<int, int>> elemToPair = std::make_pair(globalElemIdx, iSectionAndNode);
                            lVolumesMap_.insert(std::pair< std::pair<int, std::pair<int, int> >, int> (elemToPair, lVolumesCounter));
                            lVolumesCounter++;
                        }
                    }
                }
            }
        }

        // Now we handle the remaining elements, i.e. create the MPFA-L volumes
        eIt = problem.gridView().template begin<0>();
        eEndIt = problem.gridView().template end<0>();
        for(; eIt != eEndIt; ++eIt)
        {
            if (eIt->hasBoundaryIntersections())
                continue;
            // global element index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalElemIdx = problem_().elementMapper().index(*eIt);
#else
            int globalElemIdx = problem_().elementMapper().map(*eIt);
#endif

            // Reference element will be needed later on
            const ReferenceElement& referenceElement = ReferenceElements::general(eIt->geometry().type());

            IntersectionIterator isIt = problem.gridView().ibegin(*eIt);
            IntersectionIterator isEnd = problem.gridView().iend(*eIt);
            for(; isIt != isEnd; ++isIt)
            {
                // loop over the nodes of the facet
                for(int facetNode = 0; facetNode < isIt->geometry().corners(); facetNode++)
                {
                    // get node index w.r.t element
                    int localVertCorner = referenceElement.subEntity(isIt->indexInInside(), 1, facetNode, dim);

                    // get global index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    int globalVertIdx = problem_().vertexMapper().index(*(eIt->template subEntity<dim>(localVertCorner)));
#else
                    int globalVertIdx = problem_().vertexMapper().map(*(eIt->template subEntity<dim>(localVertCorner)));
#endif

                    // only make volume if does not already exist
                    if (!hasVolumeBeenStored(*eIt, isIt->indexInInside(), localVertCorner) )
                    {
                        InteractionVolume* iaVolume1 = new InteractionVolume(false);
                        InteractionVolume* iaVolume2 = new InteractionVolume(true);

                        InteractionVolumeManager::fillInteractionVolume(iaVolume1, *eIt, problem, isIt->indexInInside(), localVertCorner, globalVertIdx);
                        InteractionVolumeManager::fillInteractionVolume(iaVolume2, *eIt, problem, isIt->indexInInside(), localVertCorner, globalVertIdx);

                        int FaceIndexin1 = iaVolume1->getFluxFaceIdx();
                        int FaceIndexin2 = iaVolume2->getFluxFaceIdx();

                        typename InteractionVolume::TransmissivityMatrix &T1 = iaVolume1->getTransMatrix();
                        typename InteractionVolume::TransmissivityMatrix &T2 = iaVolume2->getTransMatrix();

                        Scalar t1_0 = T1[FaceIndexin1][0];
                        Scalar t2_0 = T2[FaceIndexin2][0];

                        if ( fabs(t1_0) < fabs(t2_0) )
                            mpfaLInteractionVolumes_.push_back(iaVolume1);
                        else
                            mpfaLInteractionVolumes_.push_back(iaVolume2);

                        std::pair<int, int> iSectionAndNode = std::make_pair(isIt->indexInInside(), localVertCorner);
                        std::pair <int, std::pair<int, int>> elemToPair = std::make_pair(globalElemIdx, iSectionAndNode);
                        lVolumesMap_.insert(std::pair< std::pair<int, std::pair<int, int> >, int> (elemToPair, lVolumesCounter));
                        lVolumesCounter++;
                    }
                }
            }
        }
    }

    bool halfFaceTouchesBoundary(const Element &element, IntersectionIterator &isIt, int localNodeIdx)
    {
        if ( !GET_PROP_VALUE(TypeTag, FullMpfaOBoundary) )
            return nodeTouchesBoundary(element, isIt, localNodeIdx);
        else
            return fullFaceTouchesBoundary(element, isIt, localNodeIdx);
    }

    bool fullFaceTouchesBoundary(const Element &element, IntersectionIterator &isIt, int localNodeIdx)
    {
        if (!isIt->neighbor())
            return true;
        else
        {
            // the reference element
            const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());

            // get the local indices of the nodes beloning to the facet
            int noCorners = isIt->geometry().corners();
            int localCornerIndices[noCorners];
            for (int corner = 0; corner < noCorners; corner++)
            {
                localCornerIndices[corner] = referenceElement.subEntity(isIt->indexInInside(), 1, corner, dim);
            }

            IntersectionIterator isIt2 = problem_().gridView().ibegin(element);
            IntersectionIterator isIt2End = problem_().gridView().iend(element);
            for(; isIt2 != isIt2End; ++isIt2)
            {
                if (isIt2 == isIt)
                    continue;
                else
                    for(int corner = 0; corner < isIt2->geometry().corners(); corner++)
                    {
                        // get node index w.r.t element
                        int localVertCorner = referenceElement.subEntity(isIt2->indexInInside(), 1, corner, dim);

                        bool foundCornerOnFacet = false;
                        for(int facetNode = 0; facetNode < noCorners; facetNode++)
                            if (localCornerIndices[facetNode] == localVertCorner)
                            {
                                foundCornerOnFacet = true; break;
                            }

                        if (foundCornerOnFacet && !isIt2->neighbor())
                            return true;
                    }
            }
        }
        return false;
    }


    bool nodeTouchesBoundary(const Element &element, IntersectionIterator &isIt, int localNodeIdx)
    {
        if (!isIt->neighbor())
            return true;
        else
        {
            // the reference element
            const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());

            IntersectionIterator isIt2 = problem_().gridView().ibegin(element);
            IntersectionIterator isIt2End = problem_().gridView().iend(element);
            for(; isIt2 != isIt2End; ++isIt2)
            {
                if (isIt2 == isIt)
                    continue;
                else
                    for(int corner = 0; corner < isIt2->geometry().corners(); corner++)
                    {
                        // get node index w.r.t element
                        int localVertCorner = referenceElement.subEntity(isIt2->indexInInside(), 1, corner, dim);

                        if (localVertCorner == localNodeIdx && !isIt2->neighbor())
                            return true;
                    }
            }
        }

        return false;
    }

    bool isMpfaOVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        // first check the container of the O-volumes
        // get global vertex index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        std::map<int, int>::const_iterator it2 = oVolumesMap_.find(globalVertIdx);
        if (it2 != oVolumesMap_.end())
            return true;
        else
            return false;
    }

    bool isMpfaOVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // first check the container of the O-volumes
        // get global vertex index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        std::map<int, int>::const_iterator it2 = oVolumesMap_.find(globalVertIdx);
        if (it2 != oVolumesMap_.end())
            return true;
        else
            return false;
    }

    InteractionVolume* getInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        // first check if a corresponding l-Volume exists
        // global Index of the element
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalElemIdx = problem_().elementMapper().index(element);
#else
        int globalElemIdx = problem_().elementMapper().map(element);
#endif
        std::pair<int, int> facetAndVertex = std::make_pair(facetIdxOnElement, localVertIdx);
        std::pair<int, std::pair<int, int> > key = std::make_pair(globalElemIdx, facetAndVertex);

        std::map<std::pair<int, std::pair<int,int> >, int>::iterator it = lVolumesMap_.find(key);
        if (it != lVolumesMap_.end())
            return mpfaLInteractionVolumes_[it->second];
        else
            DUNE_THROW(Dune::NotImplemented, "Interaction volume not found!!!");
    }

    InteractionVolume* getInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // first check if a corresponding l-Volume exists
        // global Index of the element
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalElemIdx = problem_().elementMapper().index(element);
#else
        int globalElemIdx = problem_().elementMapper().map(element);
#endif
        std::pair<int, int> facetAndVertex = std::make_pair(facetIdxOnElement, localVertIdx);
        std::pair<int, std::pair<int, int> > key = std::make_pair(globalElemIdx, facetAndVertex);

        std::map<std::pair<int, std::pair<int,int> >, int>::const_iterator it = lVolumesMap_.find(key);
        if (it != lVolumesMap_.end())
            return mpfaLInteractionVolumes_[it->second];
        else
            DUNE_THROW(Dune::NotImplemented, "Interaction volume not found for " << globalElemIdx << " - " << facetIdxOnElement << " - " << localVertIdx);
    }

    MpfaOInteractionVolume* getMpfaOInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif
        std::map<int, int>::const_iterator it2 = oVolumesMap_.find(globalVertIdx);
        if (it2 != oVolumesMap_.end())
            return mpfaOInteractionVolumes_[it2->second];
        else
            DUNE_THROW(Dune::NotImplemented, "Interaction volume not found!!!");
    }

    MpfaOInteractionVolume* getMpfaOInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif
        std::map<int, int>::const_iterator it2 = oVolumesMap_.find(globalVertIdx);
        if (it2 != oVolumesMap_.end())
            return mpfaOInteractionVolumes_[it2->second];
        else
            DUNE_THROW(Dune::NotImplemented, "Interaction volume not found!!!");
    }

    bool hasVolumeBeenStored(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // first check the container of the L-volumes
        // get global element index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalElemIdx = problem_().elementMapper().index(element);
#else
        int globalElemIdx = problem_().elementMapper().map(element);
#endif

        std::pair<int, int> facetAndVertex = std::make_pair(facetIdxOnElement, localVertIdx);
        std::pair<int, std::pair<int,int> > key = std::make_pair(globalElemIdx, facetAndVertex);

        std::map< std::pair<int, std::pair<int, int> >, int >::const_iterator it = lVolumesMap_.find(key);

        if (it != lVolumesMap_.end())
            return true;
        else
        {
            // check the container of the O-volumes

            // get global vertex index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
            int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

            std::map<int, int>::const_iterator it2 = oVolumesMap_.find(globalVertIdx);
            if (it2 != oVolumesMap_.end())
                return true;
            else
                return false;
        }
    }

protected:
    /*!
     * \brief A reference to the problem on which the model is applied.
     */
    Problem &problem_()
    { return *problemPtr_; }
    /*!
     * \copydoc problem_()
     */
    const Problem &problem_() const
    { return *problemPtr_; }

private:
    Problem *problemPtr_;
    std::vector<InteractionVolume*> mpfaLInteractionVolumes_;
    std::vector<MpfaOInteractionVolume*> mpfaOInteractionVolumes_;

    std::map< int, int> oVolumesMap_;
    std::map< std::pair< int, std::pair<int, int> >, int> lVolumesMap_;
};
}
#endif /* DUMUX_MPFA_O_INTERACTIONVOLUMECONTAINER_HH */
