// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_RANDPERM_PROBLEM_HH
#define DUMUX_RANDPERM_PROBLEM_HH

#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/dnapl.hh>

#if PROBLEM == 1
#include <dumux/implicit/2pmpfa/2pmodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#include <dumux/implicit/ccmpfa/ccmpfapropertydefaults.hh>
#elif PROBLEM == 2
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/cellcentered/ccpropertydefaults.hh>
#include <dumux/implicit/cornerpoint/cpelementvolumevariables.hh>
#include <dumux/implicit/cornerpoint/cpfvelementgeometry.hh>
#include <dumux/implicit/cornerpoint/cpdarcyfluxvariables.hh>
#elif PROBLEM == 3
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/box/boxpropertydefaults.hh>
#endif

//#include <dumux/linear/amgbackend.hh>

#include "test_randpermspatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class RandPermProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_TYPE_TAG(RandPermProblem, INHERITS_FROM(TwoP, RandPermSpatialParams));
#if PROBLEM == 1
NEW_TYPE_TAG(RandPermCCMpfaProblem, INHERITS_FROM(CCMpfaModel, RandPermProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(RandPermCCProblem, INHERITS_FROM(CCModel, RandPermProblem));
#elif PROBLEM == 3
NEW_TYPE_TAG(RandPermBoxProblem, INHERITS_FROM(BoxModel, RandPermProblem));
#endif

// Set the grid type
#if HAVE_UG
SET_TYPE_PROP(RandPermProblem, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(RandPermProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);
#endif

// Set the problem property
SET_TYPE_PROP(RandPermProblem, Problem, RandPermProblem<TypeTag>);

// Set the wetting phase
SET_PROP(RandPermProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(RandPermProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::DNAPL<Scalar> > type;
};

SET_BOOL_PROP(RandPermProblem, ProblemEnableGravity, false);

// Linear solver settings
SET_TYPE_PROP(RandPermProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag> );

SET_INT_PROP(RandPermProblem, Formulation, TwoPFormulation::pnsw);

NEW_PROP_TAG(BaseProblem);

#if PROBLEM == 1
SET_TYPE_PROP(RandPermCCMpfaProblem, BaseProblem, ImplicitMpfaPorousMediaProblem<TypeTag>);
#elif PROBLEM == 2
SET_TYPE_PROP(RandPermCCProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
SET_TYPE_PROP(RandPermCCProblem, ElementVolumeVariables, CpElementVolumeVariables<TypeTag>);
SET_TYPE_PROP(RandPermCCProblem, FVElementGeometry, CpFVElementGeometry<TypeTag>);
SET_TYPE_PROP(RandPermCCProblem, FluxVariables, CpDarcyFluxVariables<TypeTag>);
#elif PROBLEM == 3
SET_TYPE_PROP(RandPermBoxProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
#endif

}


template <class TypeTag >
class RandPermProblem : public GET_PROP_TYPE(TypeTag, BaseProblem)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseProblem) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonwettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;


    enum {

        // primary variable indices
        pnIdx = Indices::pnIdx,
        swIdx = Indices::swIdx,

        // equation indices
        contiNEqIdx = Indices::contiNEqIdx,
        contiWEqIdx = Indices::contiWEqIdx,

        // phase indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,


        // world dimension
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    RandPermProblem(TimeManager &timeManager,
                const GridView &gridView)
    : ParentType(timeManager, gridView)
    {

        injectionRadius_ = this->bBoxMax().two_norm() * 0.05;
        if (ParameterTree::tree().hasKey("Problem.InjectionRadius"))
            injectionRadius_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InjectionRadius);

        injectorPos_ = this->bBoxMax();
        producerPos_ = this->bBoxMax();

        injectorPos_[0] *= 0.0;
        injectorPos_[1] *= 1.0;

        producerPos_[0] *= 1.0;
        producerPos_[1] *= 0.0;

        injectionRate_ = 1.0e-2;
        if (ParameterTree::tree().hasKey("Problem.InjectionRate"))
            injectionRate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InjectionRate);

        eps_ = 3e-6;
        temperature_ = 273.15 + 20; // -> 20°C

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);
    }

    void init()
    {
        outputInterval_ = 1e6;
        if (ParameterTree::tree().hasKey("Problem.OutputTimeInterval"))
        {
            outputInterval_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OutputTimeInterval);
        }

        this->timeManager().startNextEpisode(outputInterval_);

        this->spatialParams().loadIntrinsicPermeability(*this);

        ParentType::init();
    }


    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    void episodeEnd()
    {
        if (!this->timeManager().willBeFinished())
        this->timeManager().startNextEpisode(outputInterval_);
    };

    /*!
     * \brief User defined output after the time integration
     *
     * Will be called diretly after the time integration.
     */
    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0) {
            std::cout<<"Storage: " << storage << std::endl;
        }
    }

    /*!
     * \brief Returns the temperature \f$ K \f$
     *
     * This problem assumes a uniform temperature of 20 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Returns the source term
     *
     * \param values Stores the source values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} / (m^\textrm{dim} \cdot s )] \f$
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0;
    }


    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        Scalar radiusI = (globalPos-injectorPos_).two_norm();
        Scalar radiusP = (globalPos-producerPos_).two_norm();

        if (radiusI < injectionRadius_ /*|| radiusP < injectionRadius_*/)
        {
            values.setAllDirichlet();
        }
        else
        {
            values.setAllNeumann();
        }
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values = 0;

        Scalar radiusI = (globalPos-injectorPos_).two_norm();
        Scalar radiusP = (globalPos-producerPos_).two_norm();

        if (radiusI < injectionRadius_)
        {
            values[pnIdx] = 2e5;
            values[swIdx] = 1.0;
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values = 0.0;

        Scalar radiusI = (globalPos-injectorPos_).two_norm();
        Scalar radiusP = (globalPos-producerPos_).two_norm();

        if(radiusP < injectionRadius_)
        {
            values[contiNEqIdx] = injectionRate_;
        }
    }

      /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values[pnIdx] = 2e5;
        values[swIdx] = 0.0;
    }

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    bool shouldWriteOutput() const
    {
        return
            this->timeManager().timeStepIndex() == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
    }

private:
    Scalar injectionRadius_;
    GlobalPosition injectorPos_;
    GlobalPosition producerPos_;
    Scalar injectionRate_;
    Scalar outputInterval_;

    Scalar temperature_;
    Scalar eps_;
    std::string name_;
};
} //end namespace

#endif
