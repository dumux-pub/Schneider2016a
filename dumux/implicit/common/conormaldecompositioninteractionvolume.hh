#ifndef DUMUX_CONORMALDECOMPOSITION_INTERACTIONVOLUME_HH
#define DUMUX_CONORMALDECOMPOSITION_INTERACTIONVOLUME_HH

#include "conormaldecompositionfacestencil.hh"
#include <dumux/common/math.hh>

namespace Dumux{

template<class TypeTag>
class CoNormalDecompositionInteractionVolume
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;


public:
    enum FaceTypes
    {
        homogeneous = 1,
        heterogeneous = 2,
        boundary = 0
    };


    //! Mark storage as completed
    void setStored()
    {
        stored_ = true;
    }

    //! Returns true if information has already been stored
    bool isStored() const
    {
        return stored_;
    }

    void setPermeability(DimMatrix permeability)
    {
        permeability_ = permeability;
    }

    const DimMatrix getPermeability() const
    {
        return permeability_;
    }


    void setNeighboringPermeabilities(VectorFieldMatrix neighborPermeabilities)
    {
        neighborPermeabilities_ = neighborPermeabilities;
    }

    const VectorFieldMatrix getNeighboringPermeabilities() const
    {
        return neighborPermeabilities_;
    }

    void setNeighboringElements(std::vector<ElementPointer> neighboringElements){
        neighboringElements_ = neighboringElements;
    }


    void setElementIntersections(std::vector<IntersectionIterator> intersections){
        elementIntersections_ = intersections;
    }

    const std::vector<IntersectionIterator> getElementIntersections() const
    {
        return elementIntersections_;
    }

    void insertElementIntersection(IntersectionIterator intersection)
    {
        elementIntersections_.push_back(intersection);
    }

    const IntersectionIterator getElementIntersection(std::pair<int,int> indexOfNeighbor) const
    {

        return elementIntersections_[getLocalIndex(indexOfNeighbor)];
    }


    const std::vector<ElementPointer> getNeighboringElements() const
    {
        return neighboringElements_;
    }

    void setGlobalIdxNeighboringElements(std::vector<int> globalIdxNeighbors)
    {
        globalIdxNeighboringElements_ = globalIdxNeighbors;
    }

    const std::vector<int> getGlobalIdxNeighboringElements() const
    {
        return globalIdxNeighboringElements_;
    }

    void insertGlobalIdxNeighboringElements(int globalIdx)
    {
        globalIdxNeighboringElements_.push_back(globalIdx);
    }

    void setNormalVectors(VectorFieldVector normalVectors){
        normalVectors_ = normalVectors;
    }

    const VectorFieldVector getNormalVectors() const
    {
        return normalVectors_;
    }

    void setCoNormalVectors(VectorFieldVector coNormalVectors)
    {
        coNormals_ = coNormalVectors;
    }

    void setCellCenters(VectorFieldVector cellCenters){
        cellCenters_ = cellCenters;
    }

    const VectorFieldVector getCellCenters() const
    {
        return cellCenters_;
    }

    void setCellCenter(DimVector cellCenter){
        cellCenter_ = cellCenter;
    }

    const VectorFieldVector getCellCenter() const
    {
        return cellCenter_;
    }

    void setFaceCenters(VectorFieldVector faceCenters){
        faceCenters_ = faceCenters;
    }

    const VectorFieldVector getFaceCenters() const
    {
        return faceCenters_;
    }

    const VectorFieldVector getCoNormalVectors() const
    {
        return coNormals_;
    }

    void setConnectionVectors(VectorFieldVector connectionVectors){
        connectionVectors_ = connectionVectors;
    }

    void setIndexGlobalToLocal(std::map< std::pair<int,int>, int> indexGlobalToLocal)
    {
        indexGlobalToLocal_= indexGlobalToLocal;
    }

    const std::map< std::pair<int,int>, int> getIndexGlobalToLocal() const
    {
        return indexGlobalToLocal_;
    }


    void setIndexItToLocalMinus(std::vector<int> indexItToLocalMinus)
    {
        indexItToLocalMinus_ = indexItToLocalMinus;
    }

    const std::vector<int>  getIndexItToLocalMinus() const
    {
        return indexItToLocalMinus_;
    }

    void setNumFaces(int numNeigh){
        numFaces_ = numNeigh;
    }

    const int getNumFaces() const
    {
        return numFaces_;
    }

    void setGlobalIdx(int globalIdx){
        globalIdx_ = globalIdx;
     }

    const int getGlobalIdx()const
    {
        return globalIdx_ ;
    }

    void setVolumeType(int type){
        volumeType_ = type;
    }

    const int getVolumeType() const
    {
        return volumeType_;
    }

    void setFaceTypes(std::vector<int> faceTypes){
        faceTypes_ = faceTypes;
    }

    const int getFaceType(std::pair<int,int> index) const
    {
        return faceTypes_[getLocalIndex(index)];
    }

    void setFaceIdx(std::vector<int> faceIdx){
        faceIdx_ = faceIdx;
    }

    const int getFaceIdx(std::pair<int,int> index) const
    {
        return faceIdx_[getLocalIndex(index)];
    }

    const bool isOnBoundary() const
    {
        if(volumeType_ == 0) return true;
        else return false;
    }

    const CoNormalDecompositionFaceStencil<TypeTag>* getFaceStencilVectorPlus(std::pair<int,int> index) const
    {
        return &faceStencilVectorPlus_[getLocalIndex(index)];
    }

    const CoNormalDecompositionFaceStencil<TypeTag>* getFaceStencilVectorMinus(std::pair<int,int> index) const
    {
        return &faceStencilVectorMinus_[getLocalIndex(index)];
    }



    const Scalar getHarmonicAveragingCoefficient(std::pair<int,int> index) const
    {

        int localIndex = getLocalIndex(index);
        Scalar lambdaK = unitCoNormalProjections_[localIndex];
        Scalar lambdaL = neighboringUnitCoNormalProjections_[localIndex];
        Scalar distK = cellCenterFaceDistances_[localIndex];
        Scalar distL = neighboringCellCenterFaceDistances_[localIndex];

        Scalar coeff = 0.0;

        if(faceTypes_[localIndex] == boundary)
        {
            coeff = distK/lambdaK;
        }else
        {
            coeff = distL*lambdaK/(distL*lambdaK + distK*lambdaL);
        }

        return coeff;
    }

    const Scalar getNeighboringHarmonicAveragingCoefficient(std::pair<int,int> index) const
    {

        int localIndex = getLocalIndex(index);
        Scalar lambdaK = unitCoNormalProjections_[localIndex];
        Scalar lambdaL = neighboringUnitCoNormalProjections_[localIndex];
        Scalar distK = cellCenterFaceDistances_[localIndex];
        Scalar distL = neighboringCellCenterFaceDistances_[localIndex];

        return distK*lambdaL/(distL*lambdaK + distK*lambdaL);
    }

    int getLocalIndex(std::pair<int,int> index) const
    {

        auto iter = indexGlobalToLocal_.find(index);
        if (iter != indexGlobalToLocal_.end())
        {
            return iter->second;
        }
        std::cout<<"ERROR!!!!!!!!!!!!!!! CANT FIND INDEX"<<std::endl;
        return -1;
    }

    /*
    void initializeInteractionVolume(){

        IntersectionIterator isEndIt = problem_.gridView().iend(*element_);
        IntersectionIterator isIt = problem_.gridView().ibegin(*element_);

        for (; isIt != isEndIt; ++isIt){

            int faceIdx = isIt->indexInInside();
            indexItToLocal_.push_back(faceIdx);

            ElementPointer neighboringElement = isIt->outside();

            DimVector connectionVector(0);
            connectionVector = neighboringElement->geometry().center() - element_->geometry.center();
            connectionVectors_.push_back(connectionVector);

            neigboringElements_.push_back(neigboringElement);
            CoNormalDecompositionFaceStencil<TypeTag> faceStencil(faceIdx, element_, neighboringElement);

            ++numNeighbors_;

        }

        inverseIndexItToLocal_.resize(numNeighbors_);
    }
    */


//    void calculateStencilsMinus(IntersectionIterator isIt, IntersectionIterator isEndIt){
//
//      int indexIt = 0;
//
//        int numNeighboringPoints = numFaces_;
//
//      IntersectionIterator isItTemp = isIt;
//      IntersectionIterator isEndItTemp = isEndIt;
//
//      if(isOnBoundary()){
//
//          int indexItTemp = 0;
//
//            const ReferenceElement& referenceElement = ReferenceElements::general(element_->geometry().type());
//
//          for (; isItTemp != isEndItTemp; ++isItTemp){
//
//              if(faceTypes_[indexItTemp] == boundary){
//                      for (int i = 0; i < isItTemp->geometry().corners(); ++i)
//                      {
//                          int faceIdx = isItTemp->indexInInside();
//                          int localIdxVertex = referenceElement.subEntity(faceIdx, 1, i, dim);
//                          DimVector corner = element_->geometry().corner(localIdxVertex);
//                          //cellCenters.push_back(corner);
//                          cellCenters_.push_back(corner);
//                          faceTypes_.push_back(boundary);
//                          indexItToLocal_.push_back(faceIdx);
//                          globalIdxNeighboringElements_.push_back(-1);
//                          neighboringElements_.push_back(element_);
//                          numNeighboringPoints++;
//                      }
//               }
//
//              indexItTemp++;
//
//          }
//
//      }
//
//
//        for (; isIt != isEndIt; ++isIt){
//
//            if(faceTypes_[indexIt] == boundary /*|| faceTypes_[indexIt] == heterogeneous*/){
//            //if(volumeType_ == 1){
//                int faceIdx = isIt->indexInInside();
//
//                CoNormalDecompositionFaceStencil<TypeTag>* faceStencilMinus = new CoNormalDecompositionFaceStencil<TypeTag>(faceIdx, element_);
//                DimVector faceCoNormalVectorMinus(0);
//                faceCoNormalVectorMinus -= coNormals_[indexIt];
//                faceStencilMinus->setPermeability(permeability_);
//                Scalar faceArea = isIt->geometry().volume();
//                faceStencilMinus->setFaceArea(faceArea);
//                faceStencilMinus->setCoNormal(faceCoNormalVectorMinus);
//
//                DimVector faceCenter = isIt->geometry().center();
//                faceStencilMinus->setCenterPoint(faceCenter);
//
//                int numInsertedVectors = 0;
//
//
//                if(dim == 2){
//
//                  //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<2 ;i++)
//                  //{
//                      DimVector faceConnectionVectorMinusOne(0);
//                      DimVector centerPointOne;
//                      int globalIdxOne = -1;
//                      ElementPointer elementOne(element_);
//                      int faceIdxOne = -1;
//                      bool isOnBoundaryOne = false;
//
//                      centerPointOne = cellCenter_;
//                      faceConnectionVectorMinusOne = cellCenter_;
//                      globalIdxOne = globalIdx_;
//                      faceIdxOne = faceIdx;
//                      isOnBoundaryOne = false;
//
//                      faceConnectionVectorMinusOne -= faceCenter;
//
//                      for(int j = 0; j<numNeighboringPoints; j++ ){
//                          //if(i!=j){
//                              DimVector faceConnectionVectorMinusTwo(0);
//                              DimVector centerPointTwo;
//                              int globalIdxTwo = -1;
//                              ElementPointer elementTwo(element_);
//                              int faceIdxTwo = -1;
//                              bool isOnBoundaryTwo = false;
//
//                              if(j == inverseIndexItToLocal_[faceIdx]){
//                                  centerPointTwo = cellCenter_;
//                                  faceConnectionVectorMinusTwo = cellCenter_;
//                                  globalIdxTwo = globalIdx_;
//                                  elementOne = element_;
//                                  faceIdxTwo = faceIdx;
//                                  isOnBoundaryTwo = false;
//
//                              }else{
//
//                                  if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
//                                  {
//                                      centerPointTwo = cellCenters_[j];
//                                      faceConnectionVectorMinusTwo = cellCenters_[j];
//                                      globalIdxTwo = globalIdxNeighboringElements_[j];
//                                      elementTwo = neighboringElements_[j];
//                                      faceIdxTwo = indexItToLocal_[j];
//                                      if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                      else isOnBoundaryTwo = false;
//
//                                  }else if(faceTypes_[j] == heterogeneous)
//                                  {
//                                      centerPointTwo = faceHarmonicAveragingPoints_[j];
//                                      faceConnectionVectorMinusTwo = centerPointTwo;
//                                      globalIdxTwo = globalIdxNeighboringElements_[j];
//                                      elementTwo = neighboringElements_[j];
//                                      faceIdxTwo = indexItToLocal_[j];
//                                      if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                      else isOnBoundaryTwo = false;
//
//                                  }
//                              }
//
//                              faceConnectionVectorMinusTwo -= faceCenter;
//
//                              if(faceStencilMinus->calculateCoefficients(faceCoNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
//                                  faceStencilMinus->insertGlobalIdxNeighbors(globalIdxOne);
//                                  faceStencilMinus->insertStencilElement(elementOne);
//                                  faceStencilMinus->insertStencilFaceIdx(faceIdxOne);
//                                  faceStencilMinus->insertSpanVector(faceConnectionVectorMinusOne);
//                                  faceStencilMinus->insertStencilCenterPoint(centerPointOne);
//                                  faceStencilMinus->insertIsPointOnBoundary(isOnBoundaryOne);
//
//                                  faceStencilMinus->insertGlobalIdxNeighbors(globalIdxTwo);
//                                  faceStencilMinus->insertStencilElement(elementTwo);
//                                  faceStencilMinus->insertStencilFaceIdx(faceIdxTwo);
//                                  faceStencilMinus->insertSpanVector(faceConnectionVectorMinusTwo);
//                                  faceStencilMinus->insertStencilCenterPoint(centerPointTwo);
//                                  faceStencilMinus->insertIsPointOnBoundary(isOnBoundaryTwo);
//
//                                  numInsertedVectors++;
//                                  numInsertedVectors++;
//
//                                  break;
//                              }
//                          //}
//                      }
//                  //}
//
//                }
//                else{
//
//                  //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<3 ;i++)
//                  //{
//
//                      DimVector faceConnectionVectorMinusOne(0);
//                      DimVector centerPointOne;
//                      int globalIdxOne = -1;
//                      ElementPointer elementOne(element_);
//                      int faceIdxOne = -1;
//                      bool isOnBoundaryOne = false;
//
//                      centerPointOne = cellCenter_;
//                      faceConnectionVectorMinusOne = cellCenter_;
//                      globalIdxOne = globalIdx_;
//                      elementOne = element_;
//                      faceIdxOne = faceIdx;
//                      isOnBoundaryOne = false;
//
//                      faceConnectionVectorMinusOne -= faceCenter;
//
//                      for(int j = 0; j<numNeighboringPoints && numInsertedVectors<3; j++ )
//                      {
//                          DimVector faceConnectionVectorMinusTwo(0);
//                          DimVector centerPointTwo;
//                          int globalIdxTwo = -1;
//                          ElementPointer elementTwo(element_);
//                          int faceIdxTwo = -1;
//                          bool isOnBoundaryTwo = false;
//
//                          if(j == inverseIndexItToLocal_[faceIdx]){
//                              centerPointTwo = cellCenter_;
//                              faceConnectionVectorMinusTwo = cellCenter_;
//                              globalIdxTwo = globalIdx_;
//                              elementTwo = element_;
//                              faceIdxTwo = faceIdx;
//                              isOnBoundaryTwo = false;
//
//                          }else{
//                              if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
//                              {
//                                  centerPointTwo = cellCenters_[j];
//                                  faceConnectionVectorMinusTwo = cellCenters_[j];
//                                  globalIdxTwo = globalIdxNeighboringElements_[j];
//                                  elementTwo = neighboringElements_[j];
//                                  faceIdxTwo = indexItToLocal_[j];
//                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                  else isOnBoundaryTwo = false;
//
//                              }else if(faceTypes_[j] == heterogeneous)
//                              {
//                                  centerPointTwo = faceHarmonicAveragingPoints_[j];
//                                  faceConnectionVectorMinusTwo = centerPointTwo;
//                                  globalIdxTwo = globalIdxNeighboringElements_[j];
//                                  elementTwo = neighboringElements_[j];
//                                  faceIdxTwo = indexItToLocal_[j];
//                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                  else isOnBoundaryTwo = false;
//
//                              }
//                          }
//                          faceConnectionVectorMinusTwo -= faceCenter;
//
//                          for(int k = 0; k < numNeighboringPoints; k++ )
//                          {
//                              if(j!=k)
//                              {
//                                  DimVector faceConnectionVectorMinusThree(0);
//                                  DimVector centerPointThree;
//                                  int globalIdxThree = -1;
//                                  ElementPointer elementThree(element_);
//                                  int faceIdxThree = -1;
//                                  bool isOnBoundaryThree = false;
//                                  if(k == inverseIndexItToLocal_[faceIdx]){
//                                      centerPointThree = cellCenter_;
//                                      faceConnectionVectorMinusThree= cellCenter_;
//                                      globalIdxThree = globalIdx_;
//                                      elementThree = element_;
//                                      faceIdxThree = faceIdx;
//                                      isOnBoundaryThree = false;
//
//                                  }else{
//
//                                      if(faceTypes_[k] == homogeneous || faceTypes_[k] == boundary)
//                                      {
//                                          centerPointThree = cellCenters_[k];
//                                          faceConnectionVectorMinusThree = cellCenters_[k];
//                                          globalIdxThree = globalIdxNeighboringElements_[k];
//                                          elementThree = neighboringElements_[k];
//                                          faceIdxThree = indexItToLocal_[k];
//                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
//                                          else isOnBoundaryThree = false;
//
//                                      }else if(faceTypes_[k] == heterogeneous)
//                                      {
//                                          centerPointThree = faceHarmonicAveragingPoints_[k];
//                                          faceConnectionVectorMinusThree = centerPointThree;
//                                          globalIdxThree = globalIdxNeighboringElements_[k];
//                                          elementThree = neighboringElements_[k];
//                                          faceIdxThree = indexItToLocal_[k];
//                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
//                                          else isOnBoundaryThree = false;
//
//                                      }
//                                  }
//                                  faceConnectionVectorMinusThree -= faceCenter;
//
//                                  if(faceStencilMinus->calculateCoefficients(faceCoNormalVectorMinus,faceConnectionVectorMinusOne, faceConnectionVectorMinusTwo ,faceConnectionVectorMinusThree)){
//                                      faceStencilMinus->insertGlobalIdxNeighbors(globalIdxOne);
//                                      faceStencilMinus->insertStencilElement(elementOne);
//                                      faceStencilMinus->insertStencilFaceIdx(faceIdxOne);
//                                      faceStencilMinus->insertSpanVector(faceConnectionVectorMinusOne);
//                                      faceStencilMinus->insertStencilCenterPoint(centerPointOne);
//                                      faceStencilMinus->insertIsPointOnBoundary(isOnBoundaryOne);
//
//                                      faceStencilMinus->insertGlobalIdxNeighbors(globalIdxTwo);
//                                      faceStencilMinus->insertStencilElement(elementTwo);
//                                      faceStencilMinus->insertStencilFaceIdx(faceIdxTwo);
//                                      faceStencilMinus->insertSpanVector(faceConnectionVectorMinusTwo);
//                                      faceStencilMinus->insertStencilCenterPoint(centerPointTwo);
//                                      faceStencilMinus->insertIsPointOnBoundary(isOnBoundaryTwo);
//
//                                      faceStencilMinus->insertGlobalIdxNeighbors(globalIdxThree);
//                                      faceStencilMinus->insertStencilElement(elementThree);
//                                      faceStencilMinus->insertStencilFaceIdx(faceIdxThree);
//                                      faceStencilMinus->insertSpanVector(faceConnectionVectorMinusThree);
//                                      faceStencilMinus->insertStencilCenterPoint(centerPointThree);
//                                      faceStencilMinus->insertIsPointOnBoundary(isOnBoundaryThree);
//
//                                      numInsertedVectors++;
//                                      numInsertedVectors++;
//                                      numInsertedVectors++;
//
//                                      break;
//                                  }
//                              }
//                          }
//                      }
//                  //}
//
//                }
//
//
//                if(numInsertedVectors != dim) DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");
//                //faceStencilMinus->calculateInversStencilFaceIdx();
//                faceStencilMinus->calculateCoefficients();
//                faceStencilMinus->setStored();
//
//                faceStencilVectorMinus_.push_back(faceStencilMinus);
//
//            }
//            ++indexIt;
//        }
//
//    }

    void setElement(ElementPointer element)
    {
        //element_ = element;
    }

    CoNormalDecompositionInteractionVolume(): stored_(false), numFaces_(0), volumeType_(1)/*, element_()*/
    {

    }


public:
    bool stored_;
    int numFaces_;
    int globalIdx_;
    //ElementPointer element_;
    std::vector<CoNormalDecompositionFaceStencil<TypeTag>> faceStencilVectorPlus_;
    std::vector<CoNormalDecompositionFaceStencil<TypeTag>> faceStencilVectorMinus_;
    std::vector<ElementPointer> neighboringElements_;
    std::vector<IntersectionIterator> elementIntersections_;
    std::vector<int> globalIdxNeighboringElements_;
    //std::vector<int> indexItToLocal_;
    std::map< std::pair<int,int>, int> indexGlobalToLocal_;
    //std::vector<int> inverseIndexItToLocal_;
    std::vector<int> indexItToLocalMinus_;
    std::vector<Scalar> cellCenterFaceDistances_;
    std::vector<Scalar> neighboringCellCenterFaceDistances_;
    std::vector<Scalar> unitCoNormalProjections_;
    std::vector<Scalar> neighboringUnitCoNormalProjections_;
    VectorFieldVector connectionVectors_;
    VectorFieldVector coNormals_;
    VectorFieldVector normalVectors_;
    VectorFieldVector cellCenters_;
    VectorFieldVector faceCenters_;
    VectorFieldVector faceHarmonicAveragingPoints_;
    DimMatrix permeability_;
    DimVector cellCenter_;
    VectorFieldMatrix neighborPermeabilities_;
    int volumeType_;
    std::vector<int> faceTypes_;
    std::vector<int> faceIdx_;
    //Problem& problem_;

};
}
#endif /* CoNormalDecompositionInteractionVolume_HH_ */
