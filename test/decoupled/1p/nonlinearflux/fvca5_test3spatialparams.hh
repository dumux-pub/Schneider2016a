// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef DUMUX_FVCA5_TEST3_SPATIALPARAMETERS_HH
#define DUMUX_FVCA5_TEST3_SPATIALPARAMETERS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class FVCAFiveSpatialParamsThree;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(FVCAFiveSpatialParamsThree);

// Set the spatial parameters
SET_TYPE_PROP(FVCAFiveSpatialParamsThree, SpatialParams, Dumux::FVCAFiveSpatialParamsThree<TypeTag>);

// Set the material law
SET_PROP(FVCAFiveSpatialParamsThree, MaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef LinearMaterial<Scalar> RawMaterialLaw;
public:
    typedef EffToAbsLaw<RawMaterialLaw> type;
};
}

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for diffusion models.
 */
template<class TypeTag>
class FVCAFiveSpatialParamsThree: public FVSpatialParams<TypeTag>
{
    typedef FVSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld, numEq=1};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        return permeability_[indexSet_.index(element)];
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    void initialize()
    {
        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            setPermeability_(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }

    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
    const MaterialLawParams& materialLawParams(const Element &element) const
    {
            return materialLawParams_;
    }

    FVCAFiveSpatialParamsThree(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet())
    {
        // residual saturations
        materialLawParams_.setSwr(0.0);
        materialLawParams_.setSnr(0.0);

        // parameters for the linear entry pressure function
        materialLawParams_.setEntryPc(0);
        materialLawParams_.setMaxPc(0);
    }

private:

    //  set permeability values depending on subdomain
    void setPermeability_(FieldMatrix& perm, const GlobalPosition& globalPos) const
    {
        if ((globalPos[1] <= 0.5) && (globalPos[2] <= 0.5)){
            Scalar ax = 1;
            Scalar ay = 10;
            Scalar az = 0.01;
            perm[0][1] = perm[1][0] = perm[0][2] = perm[2][0] = perm[1][2] = perm[2][1] = 0.;
            perm[0][0] = ax;
            perm[1][1] = ay;
            perm[2][2] = az;
        }

        //subdomain i=2
        if ((globalPos[1] > 0.5) && (globalPos[2] <= 0.5)){
            Scalar ax = 1;
            Scalar ay = 0.1;
            Scalar az = 100;
            perm[0][1] = perm[1][0] = perm[0][2] = perm[2][0] = perm[1][2] = perm[2][1] = 0.;
            perm[0][0] = ax;
            perm[1][1] = ay;
            perm[2][2] = az;
        }

        //subdomain i=3
        if ((globalPos[1] > 0.5) && (globalPos[2] > 0.5)){
            Scalar ax = 1;
            Scalar ay = 0.01;
            Scalar az = 10;
            perm[0][1] = perm[1][0] = perm[0][2] = perm[2][0] = perm[1][2] = perm[2][1] = 0.;
            perm[0][0] = ax;
            perm[1][1] = ay;
            perm[2][2] = az;
        }

        //subdomain i=4
        if ((globalPos[1] <= 0.5) && (globalPos[2] > 0.5)){
            Scalar ax = 1;
            Scalar ay = 100;
            Scalar az = 0.1;
            perm[0][1] = perm[1][0] = perm[0][2] = perm[2][0] = perm[1][2] = perm[2][1] = 0.;
            perm[0][0] = ax;
            perm[1][1] = ay;
            perm[2][2] = az;
        }
    }

    const GridView gridView_;
    MaterialLawParams materialLawParams_;
    std::vector<FieldMatrix> permeability_;
    const IndexSet& indexSet_;
};

} // end namespace
#endif
