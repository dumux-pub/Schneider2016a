#ifndef DUMUX_COMPOSITIONALINTERACTIONVOLUMECONTAINER_HH
#define DUMUX_COMPOSITIONALINTERACTIONVOLUMECONTAINER_HH

#include "normaldecompositioninteractionvolume.hh"
#include "conormaldecompositionfacestencil.hh"
#include <dumux/common/math.hh>

namespace Dumux{

template<class TypeTag>
class CompositionalInteractionVolumeContainer
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

    typedef Dumux::NormalDecompositionInteractionVolume<TypeTag> InteractionVolume;
    typedef Dumux::CoNormalDecompositionFaceStencil<TypeTag> FaceStencil;


public:
    enum FaceTypes
    {
        homogeneous = 1,
        heterogeneous = 2,
        boundary = 0
    };

    void init(Problem &problem, int numDofs)
    {
        problemPtr_ = &problem;
        interactionVolumes_.clear();
        interactionVolumes_.resize(numDofs);
        initializeInteractionVolumes();
    }

    void update(int numDofs)
    {
        interactionVolumes_.resize(numDofs);
        initializeInteractionVolumes();
    }

    const InteractionVolume* getInteractionVolume(int globalIdx) const{
        return &interactionVolumes_[globalIdx];
    }

    void initializeInteractionVolumes(){

        ElementIterator eEndIt = problemPtr_->gridView().template end<0>();
        for (ElementIterator eIt = problemPtr_->gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {

            int globalIdxI = -1;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            globalIdxI = problem_().elementMapper().index(*eIt);
#else
            globalIdxI = problem_().elementMapper().map(*eIt);
#endif

            std::map< std::pair<int,int>, int> indexGlobalToLocal;
            int numFaces = 0;
            int numFacesMinus = 0;
            std::vector<int> indexItToLocalMinus;

            std::vector<ElementPointer> neighboringElements;
            std::vector<IntersectionIterator>  elementIntersections;
            std::vector<int> globalIdxNeighbors;
            VectorFieldVector connectionVectors;
            VectorFieldVector coNormals;
            VectorFieldVector cellCenters;
            VectorFieldVector faceCenters;
            DimMatrix permeability;
            DimVector cellCenter;
            VectorFieldMatrix neighborPermeabilities;
            VectorFieldVector normalVectors;
            VectorFieldVector coNormalVectors;
            InteractionVolume interactionVolume;
            std::vector<int> faceTypes;
            std::vector<int> facesIdx;

            interactionVolume.setElement(*eIt);

            permeability = problemPtr_->spatialParams().intrinsicPermeability(*eIt);

            int volumeType = 1;

            cellCenter = eIt->geometry().center();

            IntersectionIterator isEndIt = problemPtr_->gridView().iend(*eIt);
            IntersectionIterator isIt = problemPtr_->gridView().ibegin(*eIt);

            for (; isIt != isEndIt; ++isIt){

                elementIntersections.push_back(isIt);

                if(isIt->neighbor()){

                    facesIdx.push_back(isIt->indexInInside());
                    ElementPointer neighboringElement = isIt->outside();

                    int globalIdxJ = -1;
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    globalIdxJ = problem_().elementMapper().index(*neighboringElement);
#else
                    globalIdxJ = problem_().elementMapper().map(*neighboringElement);
#endif
                    std::pair<int, int> index(globalIdxJ,-1);
                    indexGlobalToLocal[index] = numFaces;

                    neighboringElements.push_back(neighboringElement);
                    globalIdxNeighbors.push_back(globalIdxJ);

                    DimMatrix neighborPermeability = problemPtr_->spatialParams().intrinsicPermeability(*neighboringElement);
                    neighborPermeabilities.push_back(neighborPermeability);

                    DimMatrix permeabilityDiff = neighborPermeability;
                    permeabilityDiff -= permeability;

                    double normPermeabilityDiff = permeabilityDiff.frobenius_norm();
                    bool isHomogeneous = true;

                    if(normPermeabilityDiff < 1.0e-30)
                    {
                        faceTypes.push_back(homogeneous);
                        indexItToLocalMinus.push_back(-1);
                        isHomogeneous = true;
                    }else{
                        faceTypes.push_back(heterogeneous);
                        indexItToLocalMinus.push_back(-1);
                        isHomogeneous = false;
                    }

                    DimVector connectionVector(0);
                    cellCenters.push_back(neighboringElement->geometry().center());
                    faceCenters.push_back(isIt->geometry().center());

                    if(isHomogeneous)
                    {
                        connectionVector = neighboringElement->geometry().center();
                        connectionVector -= cellCenter;
                    }
                    else
                    {
                        connectionVector = isIt->geometry().center();
                        connectionVector -= cellCenter;
                    }

                    connectionVectors.push_back(connectionVector);

                    DimVector unitNormalVector = isIt->centerUnitOuterNormal();
                    normalVectors.push_back(unitNormalVector);

                    DimVector coNormal(0);
                    permeability.mv(unitNormalVector, coNormal);
                    Scalar faceArea = isIt->geometry().volume();
                    coNormal *= faceArea;
                    coNormalVectors.push_back(coNormal);

                    ++numFaces;
                }else{
                    int faceIdx = isIt->indexInInside();
                    std::pair<int, int> index(globalIdxI, faceIdx);
                    indexGlobalToLocal[index] = numFaces;
                    facesIdx.push_back(faceIdx);

                    faceTypes.push_back(boundary);
                    indexItToLocalMinus.push_back(numFacesMinus);
                    numFacesMinus++;

                    volumeType = 0;
                    int globalIdxJ = -1;

                    DimVector connectionVector(0);
                    connectionVector = isIt->geometry().center();
                    connectionVector -= cellCenter;
                    connectionVectors.push_back(connectionVector);

                    cellCenters.push_back(isIt->geometry().center());
                    faceCenters.push_back(isIt->geometry().center());

                    neighboringElements.push_back(*eIt);
                    globalIdxNeighbors.push_back(globalIdxJ);

                    neighborPermeabilities.push_back(problemPtr_->spatialParams().intrinsicPermeability(*eIt));
                    DimVector unitNormalVector = isIt->centerUnitOuterNormal();
                    normalVectors.push_back(unitNormalVector);

                    DimVector coNormal(0);
                    permeability.mv(unitNormalVector, coNormal);
                    Scalar faceArea = isIt->geometry().volume();
                    coNormal *= faceArea;
                    coNormalVectors.push_back(coNormal);

                    ++numFaces;
                }

            }


            interactionVolume.setPermeability(permeability);
            interactionVolume.setNeighboringPermeabilities(neighborPermeabilities);
            interactionVolume.setNeighboringElements(neighboringElements);
            interactionVolume.setElementIntersections(elementIntersections);
            interactionVolume.setNormalVectors(normalVectors);
            interactionVolume.setCoNormalVectors(coNormalVectors);
            interactionVolume.setConnectionVectors(connectionVectors);
            interactionVolume.setIndexGlobalToLocal(indexGlobalToLocal);
            interactionVolume.setNumFaces(numFaces);
            interactionVolume.setVolumeType(volumeType);
            interactionVolume.setGlobalIdxNeighboringElements(globalIdxNeighbors);
            interactionVolume.setFaceTypes(faceTypes);
            interactionVolume.setFaceIdx(facesIdx);
            interactionVolume.setIndexItToLocalMinus(indexItToLocalMinus);
            interactionVolume.setCellCenter(cellCenter);
            interactionVolume.setCellCenters(cellCenters);
            interactionVolume.setFaceCenters(faceCenters);
            interactionVolume.setGlobalIdx(globalIdxI);
            interactionVolume.setStored();

            interactionVolumes_[globalIdxI] = interactionVolume;

            calculateFaceInterpolationPoints(*eIt, globalIdxI);

            calculateStencilsPlus(*eIt, globalIdxI);
            calculateNormalStencilsPlus(*eIt, globalIdxI);

            bool complexBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    ComplexBoundary);

            if(interactionVolume.isOnBoundary() && complexBoundary){
                calculateStencilsMinus(*eIt, globalIdxI);
                calculateNormalStencilsMinus(*eIt, globalIdxI);
            }


        }
    }

    void calculateFaceInterpolationPoints(const Element& element, int globalIdx){

        int indexIt = 0;
        IntersectionIterator isEndIt = problemPtr_->gridView().iend(element);
        IntersectionIterator isIt = problemPtr_->gridView().ibegin(element);

        InteractionVolume& interactionVolume = interactionVolumes_[globalIdx];

         for (; isIt != isEndIt; ++isIt){

             Scalar distI = 1.0;
             Scalar distJ = 1.0;
             Scalar lambdaI = 1.0;
             Scalar lambdaJ = 1.0;
             DimVector faceCenter = isIt->geometry().center();
             DimVector unitOuterNormal = interactionVolume.normalVectors_[indexIt];
             DimVector xI = interactionVolume.cellCenter_;
             DimVector connectionVectorI = faceCenter;
             connectionVectorI -= xI;

             DimVector y(0);
             DimVector faceProjection(0);

             if(interactionVolume.faceTypes_[indexIt] == boundary)
             {
                 distI = connectionVectorI * unitOuterNormal;
                 DimVector unitCoNormalI(0);
                 interactionVolume.permeability_.mv(unitOuterNormal, unitCoNormalI);
                 lambdaI = unitCoNormalI * unitOuterNormal;

                 y = xI;
                 y.axpy(distI/lambdaI, unitCoNormalI);
             }else
             {

                 DimVector xJ = isIt->outside()->geometry().center();
                 DimVector connectionVectorJ = xJ;
                 connectionVectorJ -= faceCenter;

                 distI = connectionVectorI * unitOuterNormal;
                 distJ = connectionVectorJ * unitOuterNormal;

                 if(interactionVolume.faceTypes_[indexIt] == homogeneous)
                 {
                     y.axpy(distJ, xI);
                     y.axpy(distI, xJ);
                     y /= (distJ + distI);
                 }
                 else if(interactionVolume.faceTypes_[indexIt] == heterogeneous)
                 {

                     DimVector unitCoNormalI(0);
                     interactionVolume.permeability_.mv(unitOuterNormal, unitCoNormalI);
                     DimVector unitCoNormalJ(0);
                     interactionVolume.neighborPermeabilities_[indexIt].mv(unitOuterNormal, unitCoNormalJ);

                     lambdaI = unitCoNormalI * unitOuterNormal;
                     lambdaJ = unitCoNormalJ * unitOuterNormal;

                     y.axpy(distJ * lambdaI, xI);
                     y.axpy(distI * lambdaJ, xJ);
                     y.axpy(distJ * distI, unitCoNormalI - unitCoNormalJ );
                     y /= (distJ * lambdaI + distI * lambdaJ);

                 }

             }

             faceProjection = xI;
             faceProjection.axpy(distI, unitOuterNormal);


             interactionVolume.unitCoNormalProjections_.push_back(lambdaI);
             interactionVolume.neighboringUnitCoNormalProjections_.push_back(lambdaJ);
             interactionVolume.cellCenterFaceDistances_.push_back(distI);
             interactionVolume.neighboringCellCenterFaceDistances_.push_back(distJ);
             interactionVolume.faceHarmonicAveragingPoints_.push_back(y);

             interactionVolume.faceProjectionPoints_.push_back(faceProjection);

             indexIt++;
         }
     }

    void calculateStencilsPlus(const Element& element, int globalIdx){

        int indexIt = 0;
        IntersectionIterator isEndIt = problemPtr_->gridView().iend(element);
        IntersectionIterator isIt = problemPtr_->gridView().ibegin(element);

        InteractionVolume& interactionVolume = interactionVolumes_[globalIdx];

        bool newBoundaryApprox = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                NewBoundaryApprox);

        for (; isIt != isEndIt; ++isIt){

                CoNormalDecompositionFaceStencil<TypeTag> faceStencilPlus;
                DimVector faceConnectionVectorPlus = interactionVolume.connectionVectors_[indexIt];
                DimVector faceCoNormalVectorPlus = interactionVolume.coNormals_[indexIt];
                Scalar faceArea = isIt->geometry().volume();
                faceStencilPlus.setFaceArea(faceArea);
                faceStencilPlus.setCenterPoint(interactionVolume.cellCenter_);
                DimVector faceCenter = isIt->geometry().center();
                DimVector normal = interactionVolume.normalVectors_[indexIt];

                bool isTPFA = false;

                int numInsertedVectors = 0;

                if(dim == 2){

                    for(int i = -1; i < interactionVolume.numFaces_ && numInsertedVectors < 2; i++){

                        DimVector faceConnectionVectorPlusOne(0);
                        DimVector centerPointOne(0);
                        int globalIdxOne = -1;
                        int faceIdxOne = -1;
                        std::pair<int,int> faceIdxGlobalOne(-1,-1);
                        ElementPointer  elementOne(element);
                        bool isOnBoundaryOne = false;

                        int localIdx;

                        if(i == -1) localIdx = indexIt;
                        else localIdx = i;


                        if(interactionVolume.faceTypes_[localIdx] == homogeneous || (interactionVolume.faceTypes_[localIdx] == boundary && !newBoundaryApprox))
                        {
                            faceConnectionVectorPlusOne = interactionVolume.connectionVectors_[localIdx];
                            centerPointOne = interactionVolume.cellCenters_[localIdx];
                        }
                        else if(interactionVolume.faceTypes_[localIdx] == heterogeneous || (interactionVolume.faceTypes_[localIdx] == boundary && newBoundaryApprox) )
                        {
                            centerPointOne = interactionVolume.faceHarmonicAveragingPoints_[localIdx];
                            if(interactionVolume.faceTypes_[localIdx] == heterogeneous)
                            {
                                centerPointOne = interactionVolume.cellCenters_[localIdx];
                            }
                            faceConnectionVectorPlusOne = interactionVolume.faceHarmonicAveragingPoints_[localIdx];
                            faceConnectionVectorPlusOne -= interactionVolume.cellCenter_;
                        }

                        globalIdxOne = interactionVolume.globalIdxNeighboringElements_[localIdx];
                        elementOne = interactionVolume.neighboringElements_[localIdx];
                        faceIdxOne = interactionVolume.faceIdx_[localIdx];
                        if(interactionVolume.faceTypes_[localIdx] == boundary)
                        {
                            isOnBoundaryOne = true;
                            faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                        }
                        else
                        {
                            isOnBoundaryOne = false;
                            faceIdxGlobalOne = std::pair<int,int>(globalIdxOne,-1);
                        }

                        if(i == -1){
                            if(faceStencilPlus.checkForTwoPointFlux(faceCoNormalVectorPlus, faceConnectionVectorPlusOne))
                            {
                                faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                faceStencilPlus.insertStencilElement(elementOne);
                                faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);
                                isTPFA = true;

                                break;
                            }
                        }

                        for(int j = 0 ; j < interactionVolume.numFaces_ && numInsertedVectors<2; j++){
                            if(i != indexIt && j!=i){

                                DimVector faceConnectionVectorPlusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                ElementPointer  elementTwo(element);
                                int faceIdxTwo = -1;
                                std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                                bool isOnBoundaryTwo = false;

                                if(interactionVolume.faceTypes_[j] == homogeneous || (interactionVolume.faceTypes_[j] == boundary && !newBoundaryApprox))
                                {
                                    faceConnectionVectorPlusTwo = interactionVolume.connectionVectors_[j];
                                    centerPointTwo = interactionVolume.cellCenters_[j];
                                }
                                else if(interactionVolume.faceTypes_[j] == heterogeneous || (interactionVolume.faceTypes_[j] == boundary && newBoundaryApprox))
                                {
                                    centerPointTwo = interactionVolume.faceHarmonicAveragingPoints_[j];
                                    if(interactionVolume.faceTypes_[j] == heterogeneous)
                                    {
                                        centerPointTwo = interactionVolume.cellCenters_[j];
                                    }
                                    faceConnectionVectorPlusTwo = interactionVolume.faceHarmonicAveragingPoints_[j];;
                                    faceConnectionVectorPlusTwo -= interactionVolume.cellCenter_;

                                }

                                globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
                                elementTwo = interactionVolume.neighboringElements_[j];
                                faceIdxTwo = interactionVolume.faceIdx_[j];

                                if(interactionVolume.faceTypes_[j] == boundary)
                                {
                                    isOnBoundaryTwo = true;
                                    faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
                                }
                                else
                                {
                                    isOnBoundaryTwo = false;
                                    faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
                                }

                                if(faceStencilPlus.calculateCoefficients(faceCoNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo)){
                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilPlus.insertStencilElement(elementOne);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                    //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusOne);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                    faceStencilPlus.insertStencilElement(elementTwo);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                    faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                    //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusTwo);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                    numInsertedVectors++;
                                    numInsertedVectors++;

                                    break;
                                }
                            }
                        }
                    }
                }
                else{

                    for(int i = -1; i < interactionVolume.numFaces_ && numInsertedVectors < 3 ;i++)
                    {
                        DimVector faceConnectionVectorPlusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        ElementPointer  elementOne(element);
                        int faceIdxOne = -1;
                        std::pair<int,int> faceIdxGlobalOne(-1,-1);
                        bool isOnBoundaryOne = false;

                        int localIdx;

                        if(i == -1) localIdx = indexIt;
                        else localIdx = i;

                        if(interactionVolume.faceTypes_[localIdx] == homogeneous || (interactionVolume.faceTypes_[localIdx] == boundary && !newBoundaryApprox))
                        {
                            centerPointOne = interactionVolume.cellCenters_[localIdx];
                            faceConnectionVectorPlusOne = interactionVolume.connectionVectors_[localIdx];
                        }
                        else if(interactionVolume.faceTypes_[localIdx] == heterogeneous || (interactionVolume.faceTypes_[localIdx] == boundary && newBoundaryApprox))
                        {
                            centerPointOne = interactionVolume.faceHarmonicAveragingPoints_[localIdx];
                            if(interactionVolume.faceTypes_[localIdx] == heterogeneous)
                            {
                                centerPointOne = interactionVolume.cellCenters_[localIdx];
                            }
                            faceConnectionVectorPlusOne = interactionVolume.faceHarmonicAveragingPoints_[localIdx];
                            faceConnectionVectorPlusOne -= interactionVolume.cellCenter_;
                        }

                        globalIdxOne = interactionVolume.globalIdxNeighboringElements_[localIdx];
                        elementOne = interactionVolume.neighboringElements_[localIdx];
                        faceIdxOne = interactionVolume.faceIdx_[localIdx];
                        if(interactionVolume.faceTypes_[localIdx] == boundary)
                        {
                            isOnBoundaryOne = true;
                            faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                        }
                           else
                        {
                            isOnBoundaryOne = false;
                            faceIdxGlobalOne = std::pair<int,int>(globalIdxOne,-1);
                        }

                        if(i == -1){
                            if(faceStencilPlus.checkForTwoPointFlux(faceCoNormalVectorPlus, faceConnectionVectorPlusOne))
                            {
                                faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                faceStencilPlus.insertStencilElement(elementOne);
                                faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                //faceStencilPlus->insertSpanVector(faceConnectionVectorPlusOne);
                                faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);
                                isTPFA = true;

                                break;
                            }
                        }

                        for(int j = 0; j<interactionVolume.numFaces_ && numInsertedVectors<3; j++ )
                        {
                            if(i != indexIt && j!=i)
                            {

                                DimVector faceConnectionVectorPlusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                ElementPointer  elementTwo(element);
                                int faceIdxTwo = -1;
                                std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                                bool isOnBoundaryTwo = false;

                                if(interactionVolume.faceTypes_[j] == homogeneous || (interactionVolume.faceTypes_[j] == boundary && !newBoundaryApprox))
                                {
                                    centerPointTwo = interactionVolume.cellCenters_[j];
                                    faceConnectionVectorPlusTwo = interactionVolume.connectionVectors_[j];
                                }
                                else if(interactionVolume.faceTypes_[j] == heterogeneous || (interactionVolume.faceTypes_[j] == boundary && newBoundaryApprox))
                                {
                                    centerPointTwo = interactionVolume.faceHarmonicAveragingPoints_[j];
                                    if(interactionVolume.faceTypes_[j] == heterogeneous)
                                    {
                                        centerPointTwo = interactionVolume.cellCenters_[j];
                                    }
                                    faceConnectionVectorPlusTwo = interactionVolume.faceHarmonicAveragingPoints_[j];
                                    faceConnectionVectorPlusTwo -= interactionVolume.cellCenter_;

                                }

                                globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
                                elementTwo = interactionVolume.neighboringElements_[j];
                                faceIdxTwo = interactionVolume.faceIdx_[j];
                                if(interactionVolume.faceTypes_[j] == boundary)
                                {
                                    isOnBoundaryTwo = true;
                                    faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
                                }
                                else
                                {
                                    isOnBoundaryTwo = false;
                                    faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
                                }

                                for(int k = 0; k < interactionVolume.numFaces_; k++ )
                                {
                                    if(k!=j && k!=i)
                                    {
                                        DimVector faceConnectionVectorPlusThree(0);
                                        DimVector centerPointThree;
                                        int globalIdxThree = -1;
                                        ElementPointer  elementThree(element);
                                        int faceIdxThree = -1;
                                        std::pair<int,int> faceIdxGlobalThree(-1,-1);
                                        bool isOnBoundaryThree = false;

                                        if(interactionVolume.faceTypes_[k] == homogeneous || (interactionVolume.faceTypes_[k] == boundary && !newBoundaryApprox))
                                        {
                                            centerPointThree = interactionVolume.cellCenters_[k];
                                            faceConnectionVectorPlusThree = interactionVolume.connectionVectors_[k];
                                        }
                                        else if(interactionVolume.faceTypes_[k] == heterogeneous || (interactionVolume.faceTypes_[k] == boundary && newBoundaryApprox))
                                        {
                                            centerPointThree = interactionVolume.faceHarmonicAveragingPoints_[k];
                                            if(interactionVolume.faceTypes_[k] == heterogeneous)
                                            {
                                                centerPointThree = interactionVolume.cellCenters_[k];
                                            }
                                            faceConnectionVectorPlusThree = interactionVolume.faceHarmonicAveragingPoints_[k];
                                            faceConnectionVectorPlusThree -= interactionVolume.cellCenter_;

                                        }

                                        globalIdxThree = interactionVolume.globalIdxNeighboringElements_[k];
                                        elementThree = interactionVolume.neighboringElements_[k];
                                        faceIdxThree = interactionVolume.faceIdx_[k];
                                        if(interactionVolume.faceTypes_[k] == boundary)
                                        {
                                            isOnBoundaryThree = true;
                                            faceIdxGlobalThree = std::pair<int,int>(globalIdx,faceIdxThree);
                                        }
                                        else
                                        {
                                            isOnBoundaryThree = false;
                                            faceIdxGlobalThree = std::pair<int,int>(globalIdxThree,-1);
                                        }

                                        if(faceStencilPlus.calculateCoefficients(faceCoNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo, faceConnectionVectorPlusThree)){
                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                            faceStencilPlus.insertStencilElement(elementOne);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                            faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                            //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusOne);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                            faceStencilPlus.insertStencilElement(elementTwo);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                            faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                            //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusTwo);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxThree);
                                            faceStencilPlus.insertStencilElement(elementThree);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxThree);
                                            faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalThree);
                                            //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusThree);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointThree);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryThree);

                                            numInsertedVectors++;
                                            numInsertedVectors++;
                                            numInsertedVectors++;

                                            break;
                                        }

                                    }
                                }

                            }
                        }
                    }

                }


                if(numInsertedVectors != dim && !isTPFA) DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");
                //faceStencilPlus.calculateInversStencilFaceIdx();
                //std::cout<<"Calc coeff for globalIdx: "<<globalIdx_<<std::endl;
                //faceStencilPlus.calculateCoefficients();
                faceStencilPlus.setStored();

                interactionVolume.faceStencilVectorPlus_.push_back(faceStencilPlus);

                ++indexIt;
            //}
        }

    }

    void calculateStencilsMinus(const Element& element, int globalIdx){

        int indexIt = 0;
        IntersectionIterator isEndIt = problemPtr_->gridView().iend(element);
        IntersectionIterator isIt = problemPtr_->gridView().ibegin(element);

        InteractionVolume& interactionVolume = interactionVolumes_[globalIdx];

        //int numNeighboringPoints = interactionVolume.numFaces_;

        for (; isIt != isEndIt; ++isIt){

            if(interactionVolume.faceTypes_[indexIt] == boundary /*|| faceTypes_[indexIt] == heterogeneous*/){
                int faceIdx = isIt->indexInInside();

                CoNormalDecompositionFaceStencil<TypeTag> faceStencilMinus;
                DimVector faceCoNormalVectorMinus(0);
                faceCoNormalVectorMinus -= interactionVolume.coNormals_[indexIt];
                Scalar faceArea = isIt->geometry().volume();
                faceStencilMinus.setFaceArea(faceArea);

                DimVector faceCenter = isIt->geometry().center();
                faceStencilMinus.setCenterPoint(faceCenter);

                bool isTPFA = false;
                int numInsertedVectors = 0;

                if(dim == 2){

                    //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<2 ;i++)
                    //{

                        DimVector faceConnectionVectorMinusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        int faceIdxOne = -1;
                        std::pair<int,int> faceIdxGlobalOne(-1,-1);
                        ElementPointer  elementOne(element);
                        bool isOnBoundaryOne = true;

                        centerPointOne = interactionVolume.cellCenter_;
                        globalIdxOne = globalIdx;
                        faceIdxOne = -1;
                        faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                        faceConnectionVectorMinusOne = centerPointOne;
                        faceConnectionVectorMinusOne -= faceCenter;

                        if(faceStencilMinus.checkForTwoPointFlux(faceCoNormalVectorMinus, faceConnectionVectorMinusOne))
                        {
                            faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                            faceStencilMinus.insertStencilElement(elementOne);
                            faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                            faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                            //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                            faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                            faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
                            isTPFA = true;

                        }

                        for (int j = 0; j < isIt->geometry().corners() && !isTPFA; j++)
                        {
                            DimVector corner = isIt->geometry().corner(j);

                            DimVector faceConnectionVectorMinusTwo(0);
                            DimVector centerPointTwo;
                            int globalIdxTwo = -1;
                            int faceIdxTwo = -1;
                            std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                            ElementPointer  elementTwo(element);
                            bool isOnBoundaryTwo = true;

                            faceConnectionVectorMinusTwo = corner;
                            faceConnectionVectorMinusTwo -= faceCenter;

                            centerPointTwo = corner;
                            globalIdxTwo = -1;
                            elementTwo = element;
                            faceIdxTwo = faceIdx;
                            isOnBoundaryTwo = true;
                            /*
                             * For each boundary point, we need a unique index such that it is globally unique.
                             * Therefore, we just add some offset (faceIdxTwo+1)*100 to the local corner Idx.
                             */
                            faceIdxGlobalTwo = std::pair<int,int>(globalIdx,(faceIdxTwo+1)*100 + j);


                            if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                faceStencilMinus.insertStencilElement(elementOne);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                faceStencilMinus.insertStencilElement(elementTwo);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                numInsertedVectors++;
                                numInsertedVectors++;

                                break;
                            }
                        }


//                      for(int j = 0; j<numNeighboringPoints; j++ ){
//                          if(j!=indexIt){
//
//                              DimVector faceConnectionVectorMinusTwo(0);
//                              DimVector centerPointTwo;
//                              int globalIdxTwo = -1;
//                              int faceIdxTwo = -1;
//                              std::pair<int,int> faceIdxGlobalTwo(-1,-1);
//                              ElementPointer  elementTwo(element);
//                              bool isOnBoundaryTwo = true;
//
//                                if(interactionVolume.faceTypes_[j] == homogeneous || interactionVolume.faceTypes_[j] == boundary)
//                                {
//                                  faceConnectionVectorMinusTwo = interactionVolume.cellCenters_[j];
//                                  faceConnectionVectorMinusTwo -= faceCenter;
//                                }
//                                else if(interactionVolume.faceTypes_[j] == heterogeneous /*|| interactionVolume.faceTypes_[j] == boundary*/)
//                                {
//                                  faceConnectionVectorMinusTwo = interactionVolume.faceHarmonicAveragingPoints_[j];
//                                  faceConnectionVectorMinusTwo -= interactionVolume.cellCenter_;
//
//                                }
//                                centerPointTwo = interactionVolume.cellCenters_[j];
//                                globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
//                                elementTwo = interactionVolume.neighboringElements_[j];
//                              faceIdxTwo = interactionVolume.faceIdx_[j];
//
//                                if(interactionVolume.faceTypes_[j] == boundary)
//                              {
//                                  isOnBoundaryTwo = true;
//                                  faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
//                              }
//                                else
//                              {
//                                  isOnBoundaryTwo = false;
//                                  faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
//                              }
//
//
//                              if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
//                                  faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
//                                  faceStencilMinus.insertStencilElement(elementOne);
//                                  faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
//                                  faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
//                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
//                                  faceStencilMinus.insertStencilCenterPoint(centerPointOne);
//                                  faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
//
//                                  faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
//                                  faceStencilMinus.insertStencilElement(elementTwo);
//                                  faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
//                                  faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
//                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusTwo);
//                                  faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
//                                  faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);
//
//                                    numInsertedVectors++;
//                                    numInsertedVectors++;
//
//                                  break;
//                              }
//                          //}
//                          }
//                      }

                }
                else{

                    DimVector faceConnectionVectorMinusOne(0);
                    DimVector centerPointOne;
                    int globalIdxOne = -1;
                    int faceIdxOne = -1;
                    std::pair<int,int> faceIdxGlobalOne(-1,-1);
                    ElementPointer  elementOne(element);
                    bool isOnBoundaryOne = true;

                    centerPointOne = interactionVolume.cellCenter_;
                    globalIdxOne = globalIdx;
                    faceIdxOne = -1;
                    faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                    faceConnectionVectorMinusOne = centerPointOne;
                    faceConnectionVectorMinusOne -= faceCenter;

                    if(faceStencilMinus.checkForTwoPointFlux(faceCoNormalVectorMinus, faceConnectionVectorMinusOne))
                    {
                        faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                        faceStencilMinus.insertStencilElement(elementOne);
                        faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                        faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                        //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                        faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                        faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
                        isTPFA = true;

                    }

                    for (int j = 0; j < isIt->geometry().corners() && !isTPFA && numInsertedVectors < 3; j++)
                    {
                        DimVector cornerOne = isIt->geometry().corner(j);

                        DimVector faceConnectionVectorMinusTwo(0);
                        DimVector centerPointTwo;
                        int globalIdxTwo = -1;
                        int faceIdxTwo = -1;
                        std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                        ElementPointer  elementTwo(element);
                        bool isOnBoundaryTwo = true;

                        faceConnectionVectorMinusTwo = cornerOne;
                        faceConnectionVectorMinusTwo -= faceCenter;

                        centerPointTwo = cornerOne;
                        globalIdxTwo = -1;
                        elementTwo = element;
                        faceIdxTwo = faceIdx;
                        isOnBoundaryTwo = true;
                        /*
                         * For each boundary point, we need a unique index such that it is globally unique.
                         * Therefore, we just add some offset (faceIdxTwo+1)*100 to the local corner Idx.
                         */
                        faceIdxGlobalTwo = std::pair<int,int>(globalIdx,(faceIdxTwo+1)*100 + j);

                        for (int k = 0; k < isIt->geometry().corners() && !isTPFA && numInsertedVectors < 3; k++)
                        {
                            if(k!=j){
                            DimVector cornerTwo = isIt->geometry().corner(k);

                            DimVector faceConnectionVectorMinusThree(0);
                            DimVector centerPointThree;
                            int globalIdxThree = -1;
                            int faceIdxThree = -1;
                            std::pair<int,int> faceIdxGlobalThree(-1,-1);
                            ElementPointer  elementThree(element);
                            bool isOnBoundaryThree = true;

                            faceConnectionVectorMinusThree = cornerTwo;
                            faceConnectionVectorMinusThree -= faceCenter;

                            centerPointThree = cornerTwo;
                            globalIdxThree = -1;
                            elementThree = element;
                            faceIdxThree = faceIdx;
                            isOnBoundaryThree = true;
                            /*
                             * For each boundary point, we need a unique index such that it is globally unique.
                             * Therefore, we just add some offset (faceIdxThree+1)*100 to the local corner Idx.
                             */
                            faceIdxGlobalThree = std::pair<int,int>(globalIdx,(faceIdxThree+1)*100 + k);

                            if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo, faceConnectionVectorMinusThree)){
                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                faceStencilMinus.insertStencilElement(elementOne);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                                faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                faceStencilMinus.insertStencilElement(elementTwo);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusTwo);
                                faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxThree);
                                faceStencilMinus.insertStencilElement(elementThree);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxThree);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalThree);
                                //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusThree);
                                faceStencilMinus.insertStencilCenterPoint(centerPointThree);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryThree);

                                numInsertedVectors++;
                                numInsertedVectors++;
                                numInsertedVectors++;

                                break;
                            }
                            }
                        }
                    }

//
//                  //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<3 ;i++)
//                  //{
//
//                      DimVector faceConnectionVectorMinusOne(0);
//                      DimVector centerPointOne;
//                      int globalIdxOne = -1;
//                      ElementPointer elementOne(element_);
//                      int faceIdxOne = -1;
//                      bool isOnBoundaryOne = false;
//
//                      centerPointOne = cellCenter_;
//                      faceConnectionVectorMinusOne = cellCenter_;
//                      globalIdxOne = globalIdx_;
//                      elementOne = element_;
//                      faceIdxOne = faceIdx;
//                      isOnBoundaryOne = false;
//
//                      faceConnectionVectorMinusOne -= faceCenter;
//
//                      for(int j = 0; j<numNeighboringPoints && numInsertedVectors<3; j++ )
//                      {
//                          DimVector faceConnectionVectorMinusTwo(0);
//                          DimVector centerPointTwo;
//                          int globalIdxTwo = -1;
//                          ElementPointer elementTwo(element_);
//                          int faceIdxTwo = -1;
//                          bool isOnBoundaryTwo = false;
//
//                          if(j == inverseIndexItToLocal_[faceIdx]){
//                              centerPointTwo = cellCenter_;
//                              faceConnectionVectorMinusTwo = cellCenter_;
//                              globalIdxTwo = globalIdx_;
//                              elementTwo = element_;
//                              faceIdxTwo = faceIdx;
//                              isOnBoundaryTwo = false;
//
//                          }else{
//                              if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
//                              {
//                                  centerPointTwo = cellCenters_[j];
//                                  faceConnectionVectorMinusTwo = cellCenters_[j];
//                                  globalIdxTwo = globalIdxNeighboringElements_[j];
//                                  elementTwo = neighboringElements_[j];
//                                  faceIdxTwo = indexItToLocal_[j];
//                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                  else isOnBoundaryTwo = false;
//
//                              }else if(faceTypes_[j] == heterogeneous)
//                              {
//                                  centerPointTwo = faceHarmonicAveragingPoints_[j];
//                                  faceConnectionVectorMinusTwo = centerPointTwo;
//                                  globalIdxTwo = globalIdxNeighboringElements_[j];
//                                  elementTwo = neighboringElements_[j];
//                                  faceIdxTwo = indexItToLocal_[j];
//                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
//                                  else isOnBoundaryTwo = false;
//
//                              }
//                          }
//                          faceConnectionVectorMinusTwo -= faceCenter;
//
//                          for(int k = 0; k < numNeighboringPoints; k++ )
//                          {
//                              if(j!=k)
//                              {
//                                  DimVector faceConnectionVectorMinusThree(0);
//                                  DimVector centerPointThree;
//                                  int globalIdxThree = -1;
//                                  ElementPointer elementThree(element_);
//                                  int faceIdxThree = -1;
//                                  bool isOnBoundaryThree = false;
//                                  if(k == inverseIndexItToLocal_[faceIdx]){
//                                      centerPointThree = cellCenter_;
//                                      faceConnectionVectorMinusThree= cellCenter_;
//                                      globalIdxThree = globalIdx_;
//                                      elementThree = element_;
//                                      faceIdxThree = faceIdx;
//                                      isOnBoundaryThree = false;
//
//                                  }else{
//
//                                      if(faceTypes_[k] == homogeneous || faceTypes_[k] == boundary)
//                                      {
//                                          centerPointThree = cellCenters_[k];
//                                          faceConnectionVectorMinusThree = cellCenters_[k];
//                                          globalIdxThree = globalIdxNeighboringElements_[k];
//                                          elementThree = neighboringElements_[k];
//                                          faceIdxThree = indexItToLocal_[k];
//                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
//                                          else isOnBoundaryThree = false;
//
//                                      }else if(faceTypes_[k] == heterogeneous)
//                                      {
//                                          centerPointThree = faceHarmonicAveragingPoints_[k];
//                                          faceConnectionVectorMinusThree = centerPointThree;
//                                          globalIdxThree = globalIdxNeighboringElements_[k];
//                                          elementThree = neighboringElements_[k];
//                                          faceIdxThree = indexItToLocal_[k];
//                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
//                                          else isOnBoundaryThree = false;
//
//                                      }
//                                  }
//                                  faceConnectionVectorMinusThree -= faceCenter;
//
//                                  if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus,faceConnectionVectorMinusOne, faceConnectionVectorMinusTwo ,faceConnectionVectorMinusThree)){
//                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
//                                      faceStencilMinus.insertStencilElement(elementOne);
//                                      faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
//                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusOne);
//                                      faceStencilMinus.insertStencilCenterPoint(centerPointOne);
//                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
//
//                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
//                                      faceStencilMinus.insertStencilElement(elementTwo);
//                                      faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
//                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusTwo);
//                                      faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
//                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);
//
//                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxThree);
//                                      faceStencilMinus.insertStencilElement(elementThree);
//                                      faceStencilMinus.insertStencilFaceIdx(faceIdxThree);
//                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusThree);
//                                      faceStencilMinus.insertStencilCenterPoint(centerPointThree);
//                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryThree);
//
//                                      numInsertedVectors++;
//                                      numInsertedVectors++;
//                                      numInsertedVectors++;
//
//                                      break;
//                                  }
//                              }
//                          }
//                      }
//                  //}
//
                }


                if(numInsertedVectors != dim && !isTPFA )
                    DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");

                faceStencilMinus.setStored();

                interactionVolume.faceStencilVectorMinus_.push_back(faceStencilMinus);

            }
            else
            {
                CoNormalDecompositionFaceStencil<TypeTag> faceStencilMinus;
                interactionVolume.faceStencilVectorMinus_.push_back(faceStencilMinus);
            }
            ++indexIt;
        }

    }


       void calculateNormalStencilsPlus(const Element& element, int globalIdx){

            int indexIt = 0;
            IntersectionIterator isEndIt = problemPtr_->gridView().iend(element);
            IntersectionIterator isIt = problemPtr_->gridView().ibegin(element);

            InteractionVolume& interactionVolume = interactionVolumes_[globalIdx];

            bool newBoundaryApprox = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    NewBoundaryApprox);

            for (; isIt != isEndIt; ++isIt){

                    CoNormalDecompositionFaceStencil<TypeTag> faceStencilPlus;
                    DimVector faceConnectionVectorPlus = interactionVolume.connectionVectors_[indexIt];
                    DimVector faceNormalVectorPlus = interactionVolume.normalVectors_[indexIt];
                    Scalar faceArea = isIt->geometry().volume();
                    faceStencilPlus.setFaceArea(faceArea);
                    faceStencilPlus.setCenterPoint(interactionVolume.cellCenter_);
                    DimVector faceCenter = isIt->geometry().center();

                    bool isTPFA = false;

                    int numInsertedVectors = 0;

                    if(dim == 2){

                        for(int i = -1; i < interactionVolume.numFaces_ && numInsertedVectors < 2; i++){

                            DimVector faceConnectionVectorPlusOne(0);
                            DimVector centerPointOne(0);
                            int globalIdxOne = -1;
                            int faceIdxOne = -1;
                            std::pair<int,int> faceIdxGlobalOne(-1,-1);
                            ElementPointer  elementOne(element);
                            bool isOnBoundaryOne = false;

                            int localIdx;

                            if(i == -1) localIdx = indexIt;
                            else localIdx = i;


                            if(interactionVolume.faceTypes_[localIdx] != boundary || (interactionVolume.faceTypes_[localIdx] == boundary && !newBoundaryApprox))
                            {
                                faceConnectionVectorPlusOne = interactionVolume.connectionVectors_[localIdx];
                                centerPointOne = interactionVolume.cellCenters_[localIdx];
                            }
                            else if((interactionVolume.faceTypes_[localIdx] == boundary && newBoundaryApprox))
                            {
                                centerPointOne = interactionVolume.faceProjectionPoints_[localIdx];

                                faceConnectionVectorPlusOne = centerPointOne;
                                faceConnectionVectorPlusOne -= interactionVolume.cellCenter_;
                            }

                            globalIdxOne = interactionVolume.globalIdxNeighboringElements_[localIdx];
                            elementOne = interactionVolume.neighboringElements_[localIdx];
                            faceIdxOne = interactionVolume.faceIdx_[localIdx];
                            if(interactionVolume.faceTypes_[localIdx] == boundary)
                            {
                                isOnBoundaryOne = true;
                                faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                            }
                            else
                            {
                                isOnBoundaryOne = false;
                                faceIdxGlobalOne = std::pair<int,int>(globalIdxOne,-1);
                            }

                            if(i == -1){
                                if(faceStencilPlus.checkForTwoPointFlux(faceNormalVectorPlus, faceConnectionVectorPlusOne))
                                {
                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilPlus.insertStencilElement(elementOne);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                    //faceStencilPlus.insertSpanVector(faceConnectionVectorPlusOne);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);
                                    isTPFA = true;

                                    break;
                                }
                            }

                            for(int j = 0 ; j < interactionVolume.numFaces_ && numInsertedVectors<2; j++){
                                if(i != indexIt && j!=i){

                                    DimVector faceConnectionVectorPlusTwo(0);
                                    DimVector centerPointTwo;
                                    int globalIdxTwo = -1;
                                    ElementPointer  elementTwo(element);
                                    int faceIdxTwo = -1;
                                    std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                                    bool isOnBoundaryTwo = false;

                                    if(interactionVolume.faceTypes_[j] != boundary || (interactionVolume.faceTypes_[j] == boundary && !newBoundaryApprox))
                                    {
                                        faceConnectionVectorPlusTwo = interactionVolume.connectionVectors_[j];
                                        centerPointTwo = interactionVolume.cellCenters_[j];
                                    }
                                    else if((interactionVolume.faceTypes_[j] == boundary && newBoundaryApprox))
                                    {
                                        centerPointTwo = interactionVolume.faceProjectionPoints_[j];

                                        faceConnectionVectorPlusTwo = centerPointTwo;
                                        faceConnectionVectorPlusTwo -= interactionVolume.cellCenter_;

                                    }

                                    globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
                                    elementTwo = interactionVolume.neighboringElements_[j];
                                    faceIdxTwo = interactionVolume.faceIdx_[j];

                                    if(interactionVolume.faceTypes_[j] == boundary)
                                    {
                                        isOnBoundaryTwo = true;
                                        faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
                                    }
                                    else
                                    {
                                        isOnBoundaryTwo = false;
                                        faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
                                    }

                                    if(faceStencilPlus.calculateCoefficients(faceNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo)){
                                        faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                        faceStencilPlus.insertStencilElement(elementOne);
                                        faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                        faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                        faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                        faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                        faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                        faceStencilPlus.insertStencilElement(elementTwo);
                                        faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                        faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                        faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                        faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                        numInsertedVectors++;
                                        numInsertedVectors++;

                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else{

                        for(int i = -1; i < interactionVolume.numFaces_ && numInsertedVectors < 3 ;i++)
                        {
                            DimVector faceConnectionVectorPlusOne(0);
                            DimVector centerPointOne;
                            int globalIdxOne = -1;
                            ElementPointer  elementOne(element);
                            int faceIdxOne = -1;
                            std::pair<int,int> faceIdxGlobalOne(-1,-1);
                            bool isOnBoundaryOne = false;

                            int localIdx;

                            if(i == -1) localIdx = indexIt;
                            else localIdx = i;

                            if(interactionVolume.faceTypes_[localIdx] != boundary || (interactionVolume.faceTypes_[localIdx] == boundary && !newBoundaryApprox))
                            {
                                centerPointOne = interactionVolume.cellCenters_[localIdx];
                                faceConnectionVectorPlusOne = interactionVolume.connectionVectors_[localIdx];
                            }
                            else if((interactionVolume.faceTypes_[localIdx] == boundary && newBoundaryApprox))
                            {
                                centerPointOne = interactionVolume.faceProjectionPoints_[localIdx];

                                faceConnectionVectorPlusOne = centerPointOne;
                                faceConnectionVectorPlusOne -= interactionVolume.cellCenter_;
                            }

                            globalIdxOne = interactionVolume.globalIdxNeighboringElements_[localIdx];
                            elementOne = interactionVolume.neighboringElements_[localIdx];
                            faceIdxOne = interactionVolume.faceIdx_[localIdx];
                            if(interactionVolume.faceTypes_[localIdx] == boundary)
                            {
                                isOnBoundaryOne = true;
                                faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                            }
                               else
                            {
                                isOnBoundaryOne = false;
                                faceIdxGlobalOne = std::pair<int,int>(globalIdxOne,-1);
                            }

                            if(i == -1){
                                if(faceStencilPlus.checkForTwoPointFlux(faceNormalVectorPlus, faceConnectionVectorPlusOne))
                                {
                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilPlus.insertStencilElement(elementOne);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);
                                    isTPFA = true;

                                    break;
                                }
                            }

                            for(int j = 0; j<interactionVolume.numFaces_ && numInsertedVectors<3; j++ )
                            {
                                if(i != indexIt && j!=i)
                                {

                                    DimVector faceConnectionVectorPlusTwo(0);
                                    DimVector centerPointTwo;
                                    int globalIdxTwo = -1;
                                    ElementPointer  elementTwo(element);
                                    int faceIdxTwo = -1;
                                    std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                                    bool isOnBoundaryTwo = false;

                                    if(interactionVolume.faceTypes_[j] != boundary || (interactionVolume.faceTypes_[j] == boundary && !newBoundaryApprox))
                                    {
                                        centerPointTwo = interactionVolume.cellCenters_[j];
                                        faceConnectionVectorPlusTwo = interactionVolume.connectionVectors_[j];
                                    }
                                    else if((interactionVolume.faceTypes_[j] == boundary && newBoundaryApprox))
                                    {
                                        centerPointTwo = interactionVolume.faceProjectionPoints_[j];

                                        faceConnectionVectorPlusTwo = centerPointTwo;
                                        faceConnectionVectorPlusTwo -= interactionVolume.cellCenter_;

                                    }

                                    globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
                                    elementTwo = interactionVolume.neighboringElements_[j];
                                    faceIdxTwo = interactionVolume.faceIdx_[j];
                                    if(interactionVolume.faceTypes_[j] == boundary)
                                    {
                                        isOnBoundaryTwo = true;
                                        faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
                                    }
                                    else
                                    {
                                        isOnBoundaryTwo = false;
                                        faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
                                    }

                                    for(int k = 0; k < interactionVolume.numFaces_; k++ )
                                    {
                                        if(k!=j && k!=i)
                                        {
                                            DimVector faceConnectionVectorPlusThree(0);
                                            DimVector centerPointThree;
                                            int globalIdxThree = -1;
                                            ElementPointer  elementThree(element);
                                            int faceIdxThree = -1;
                                            std::pair<int,int> faceIdxGlobalThree(-1,-1);
                                            bool isOnBoundaryThree = false;

                                            if(interactionVolume.faceTypes_[k] != boundary || (interactionVolume.faceTypes_[k] == boundary && !newBoundaryApprox))
                                            {
                                                centerPointThree = interactionVolume.cellCenters_[k];
                                                faceConnectionVectorPlusThree = interactionVolume.connectionVectors_[k];
                                            }
                                            else if((interactionVolume.faceTypes_[k] == boundary && newBoundaryApprox))
                                            {
                                                centerPointThree = interactionVolume.faceProjectionPoints_[k];

                                                faceConnectionVectorPlusThree = centerPointThree;
                                                faceConnectionVectorPlusThree -= interactionVolume.cellCenter_;

                                            }

                                            globalIdxThree = interactionVolume.globalIdxNeighboringElements_[k];
                                            elementThree = interactionVolume.neighboringElements_[k];
                                            faceIdxThree = interactionVolume.faceIdx_[k];
                                            if(interactionVolume.faceTypes_[k] == boundary)
                                            {
                                                isOnBoundaryThree = true;
                                                faceIdxGlobalThree = std::pair<int,int>(globalIdx,faceIdxThree);
                                            }
                                            else
                                            {
                                                isOnBoundaryThree = false;
                                                faceIdxGlobalThree = std::pair<int,int>(globalIdxThree,-1);
                                            }

                                            if(faceStencilPlus.calculateCoefficients(faceNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo, faceConnectionVectorPlusThree)){
                                                faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                                faceStencilPlus.insertStencilElement(elementOne);
                                                faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                                faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                                faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                                faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                                faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                                faceStencilPlus.insertStencilElement(elementTwo);
                                                faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                                faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                                faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                                faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                                faceStencilPlus.insertGlobalIdxNeighbors(globalIdxThree);
                                                faceStencilPlus.insertStencilElement(elementThree);
                                                faceStencilPlus.insertStencilFaceIdx(faceIdxThree);
                                                faceStencilPlus.insertStencilGlobalIdx(faceIdxGlobalThree);
                                                faceStencilPlus.insertStencilCenterPoint(centerPointThree);
                                                faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryThree);

                                                numInsertedVectors++;
                                                numInsertedVectors++;
                                                numInsertedVectors++;

                                                break;
                                            }

                                        }
                                    }

                                }
                            }
                        }

                    }


                    if(numInsertedVectors != dim && !isTPFA) DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");
                    faceStencilPlus.setStored();

                    interactionVolume.faceNormalStencilVectorPlus_.push_back(faceStencilPlus);

                    ++indexIt;
                //}
            }

        }

        void calculateNormalStencilsMinus(const Element& element, int globalIdx){

            int indexIt = 0;
            IntersectionIterator isEndIt = problemPtr_->gridView().iend(element);
            IntersectionIterator isIt = problemPtr_->gridView().ibegin(element);

            InteractionVolume& interactionVolume = interactionVolumes_[globalIdx];

            //int numNeighboringPoints = interactionVolume.numFaces_;


    //      if(interactionVolume.isOnBoundary()){
    //
    //          int indexItTemp = 0;
    //
    //          const ReferenceElement& referenceElement = ReferenceElements::general(element->geometry().type());
    //
    //          for (; isIt != isEndIt; ++isIt){
    //
    //              if(faceTypes_[indexItTemp] == boundary){
    //                      for (int i = 0; i < isIt->geometry().corners(); ++i)
    //                      {
    //                          int faceIdx = isIt->indexInInside();
    //                          int localIdxVertex = referenceElement.subEntity(faceIdx, 1, i, dim);
    //                          DimVector corner = element->geometry().corner(localIdxVertex);
    //                          //cellCenters.push_back(corner);
    //                          cellCenters_.push_back(corner);
    //                          faceTypes_.push_back(boundary);
    //                          indexItToLocal_.push_back(faceIdx);
    //                          globalIdxNeighboringElements_.push_back(-1);
    //                          neighboringElements_.push_back(element);
    //                          numNeighboringPoints++;
    //                      }
    //               }
    //
    //              indexItTemp++;
    //
    //          }
    //
    //      }

            for (; isIt != isEndIt; ++isIt){

                if(interactionVolume.faceTypes_[indexIt] == boundary /*|| faceTypes_[indexIt] == heterogeneous*/){
                    int faceIdx = isIt->indexInInside();

                    CoNormalDecompositionFaceStencil<TypeTag> faceStencilMinus;
                    DimVector faceNormalVectorMinus(0);
                    faceNormalVectorMinus -= interactionVolume.normalVectors_[indexIt];
                    Scalar faceArea = isIt->geometry().volume();
                    faceStencilMinus.setFaceArea(faceArea);

                    DimVector faceCenter = isIt->geometry().center();
                    faceStencilMinus.setCenterPoint(faceCenter);

                    bool isTPFA = false;
                    int numInsertedVectors = 0;

                    if(dim == 2){

                        //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<2 ;i++)
                        //{

                            DimVector faceConnectionVectorMinusOne(0);
                            DimVector centerPointOne;
                            int globalIdxOne = -1;
                            int faceIdxOne = -1;
                            std::pair<int,int> faceIdxGlobalOne(-1,-1);
                            ElementPointer  elementOne(element);
                            bool isOnBoundaryOne = true;

                            centerPointOne = interactionVolume.cellCenter_;
                            globalIdxOne = globalIdx;
                            faceIdxOne = -1;
                            faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                            faceConnectionVectorMinusOne = centerPointOne;
                            faceConnectionVectorMinusOne -= faceCenter;

                            if(faceStencilMinus.checkForTwoPointFlux(faceNormalVectorMinus, faceConnectionVectorMinusOne))
                            {
                                faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                faceStencilMinus.insertStencilElement(elementOne);
                                faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                                faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
                                isTPFA = true;

                            }

                            for (int j = 0; j < isIt->geometry().corners() && !isTPFA; j++)
                            {
                                DimVector corner = isIt->geometry().corner(j);

                                DimVector faceConnectionVectorMinusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                int faceIdxTwo = -1;
                                std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                                ElementPointer  elementTwo(element);
                                bool isOnBoundaryTwo = true;

                                faceConnectionVectorMinusTwo = corner;
                                faceConnectionVectorMinusTwo -= faceCenter;

                                centerPointTwo = corner;
                                globalIdxTwo = -1;
                                elementTwo = element;
                                faceIdxTwo = faceIdx;
                                isOnBoundaryTwo = true;
                                /*
                                 * For each boundary point, we need a unique index such that it is globally unique.
                                 * Therefore, we just add some offset (faceIdxTwo+1)*100 to the local corner Idx.
                                 */
                                faceIdxGlobalTwo = std::pair<int,int>(globalIdx,(faceIdxTwo+1)*100 + j);


                                if(faceStencilMinus.calculateCoefficients(faceNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilMinus.insertStencilElement(elementOne);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);;
                                    faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                    faceStencilMinus.insertStencilElement(elementTwo);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                    faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                    numInsertedVectors++;
                                    numInsertedVectors++;

                                    break;
                                }
                            }


    //                      for(int j = 0; j<numNeighboringPoints; j++ ){
    //                          if(j!=indexIt){
    //
    //                              DimVector faceConnectionVectorMinusTwo(0);
    //                              DimVector centerPointTwo;
    //                              int globalIdxTwo = -1;
    //                              int faceIdxTwo = -1;
    //                              std::pair<int,int> faceIdxGlobalTwo(-1,-1);
    //                              ElementPointer  elementTwo(element);
    //                              bool isOnBoundaryTwo = true;
    //
    //                                if(interactionVolume.faceTypes_[j] == homogeneous || interactionVolume.faceTypes_[j] == boundary)
    //                                {
    //                                  faceConnectionVectorMinusTwo = interactionVolume.cellCenters_[j];
    //                                  faceConnectionVectorMinusTwo -= faceCenter;
    //                                }
    //                                else if(interactionVolume.faceTypes_[j] == heterogeneous /*|| interactionVolume.faceTypes_[j] == boundary*/)
    //                                {
    //                                  faceConnectionVectorMinusTwo = interactionVolume.faceHarmonicAveragingPoints_[j];
    //                                  faceConnectionVectorMinusTwo -= interactionVolume.cellCenter_;
    //
    //                                }
    //                                centerPointTwo = interactionVolume.cellCenters_[j];
    //                                globalIdxTwo = interactionVolume.globalIdxNeighboringElements_[j];
    //                                elementTwo = interactionVolume.neighboringElements_[j];
    //                              faceIdxTwo = interactionVolume.faceIdx_[j];
    //
    //                                if(interactionVolume.faceTypes_[j] == boundary)
    //                              {
    //                                  isOnBoundaryTwo = true;
    //                                  faceIdxGlobalTwo = std::pair<int,int>(globalIdx,faceIdxTwo);
    //                              }
    //                                else
    //                              {
    //                                  isOnBoundaryTwo = false;
    //                                  faceIdxGlobalTwo = std::pair<int,int>(globalIdxTwo,-1);
    //                              }
    //
    //
    //                              if(faceStencilMinus.calculateCoefficients(faceNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
    //                                  faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
    //                                  faceStencilMinus.insertStencilElement(elementOne);
    //                                  faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
    //                                  faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
    //                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
    //                                  faceStencilMinus.insertStencilCenterPoint(centerPointOne);
    //                                  faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
    //
    //                                  faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
    //                                  faceStencilMinus.insertStencilElement(elementTwo);
    //                                  faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
    //                                  faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
    //                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusTwo);
    //                                  faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
    //                                  faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);
    //
    //                                    numInsertedVectors++;
    //                                    numInsertedVectors++;
    //
    //                                  break;
    //                              }
    //                          //}
    //                          }
    //                      }

                    }
                    else{

                        DimVector faceConnectionVectorMinusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        int faceIdxOne = -1;
                        std::pair<int,int> faceIdxGlobalOne(-1,-1);
                        ElementPointer  elementOne(element);
                        bool isOnBoundaryOne = true;

                        centerPointOne = interactionVolume.cellCenter_;
                        globalIdxOne = globalIdx;
                        faceIdxOne = -1;
                        faceIdxGlobalOne = std::pair<int,int>(globalIdx,faceIdxOne);
                        faceConnectionVectorMinusOne = centerPointOne;
                        faceConnectionVectorMinusOne -= faceCenter;

                        if(faceStencilMinus.checkForTwoPointFlux(faceNormalVectorMinus, faceConnectionVectorMinusOne))
                        {
                            faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                            faceStencilMinus.insertStencilElement(elementOne);
                            faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                            faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                            //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                            faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                            faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
                            isTPFA = true;

                        }

                        for (int j = 0; j < isIt->geometry().corners() && !isTPFA && numInsertedVectors < 3; j++)
                        {
                            DimVector cornerOne = isIt->geometry().corner(j);

                            DimVector faceConnectionVectorMinusTwo(0);
                            DimVector centerPointTwo;
                            int globalIdxTwo = -1;
                            int faceIdxTwo = -1;
                            std::pair<int,int> faceIdxGlobalTwo(-1,-1);
                            ElementPointer  elementTwo(element);
                            bool isOnBoundaryTwo = true;

                            faceConnectionVectorMinusTwo = cornerOne;
                            faceConnectionVectorMinusTwo -= faceCenter;

                            centerPointTwo = cornerOne;
                            globalIdxTwo = -1;
                            elementTwo = element;
                            faceIdxTwo = faceIdx;
                            isOnBoundaryTwo = true;
                            /*
                             * For each boundary point, we need a unique index such that it is globally unique.
                             * Therefore, we just add some offset (faceIdxTwo+1)*100 to the local corner Idx.
                             */
                            faceIdxGlobalTwo = std::pair<int,int>(globalIdx,(faceIdxTwo+1)*100 + j);

                            for (int k = 0; k < isIt->geometry().corners() && !isTPFA && numInsertedVectors < 3; k++)
                            {
                                if(k!=j){
                                DimVector cornerTwo = isIt->geometry().corner(k);

                                DimVector faceConnectionVectorMinusThree(0);
                                DimVector centerPointThree;
                                int globalIdxThree = -1;
                                int faceIdxThree = -1;
                                std::pair<int,int> faceIdxGlobalThree(-1,-1);
                                ElementPointer  elementThree(element);
                                bool isOnBoundaryThree = true;

                                faceConnectionVectorMinusThree = cornerTwo;
                                faceConnectionVectorMinusThree -= faceCenter;

                                centerPointThree = cornerTwo;
                                globalIdxThree = -1;
                                elementThree = element;
                                faceIdxThree = faceIdx;
                                isOnBoundaryThree = true;
                                /*
                                 * For each boundary point, we need a unique index such that it is globally unique.
                                 * Therefore, we just add some offset (faceIdxThree+1)*100 to the local corner Idx.
                                 */
                                faceIdxGlobalThree = std::pair<int,int>(globalIdx,(faceIdxThree+1)*100 + k);

                                if(faceStencilMinus.calculateCoefficients(faceNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo, faceConnectionVectorMinusThree)){
                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilMinus.insertStencilElement(elementOne);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalOne);
                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusOne);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                    faceStencilMinus.insertStencilElement(elementTwo);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                    faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalTwo);
                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusTwo);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxThree);
                                    faceStencilMinus.insertStencilElement(elementThree);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxThree);
                                    faceStencilMinus.insertStencilGlobalIdx(faceIdxGlobalThree);
                                    //faceStencilMinus.insertSpanVector(faceConnectionVectorPlusThree);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointThree);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryThree);

                                    numInsertedVectors++;
                                    numInsertedVectors++;
                                    numInsertedVectors++;

                                    break;
                                }
                                }
                            }
                        }

    //
    //                  //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<3 ;i++)
    //                  //{
    //
    //                      DimVector faceConnectionVectorMinusOne(0);
    //                      DimVector centerPointOne;
    //                      int globalIdxOne = -1;
    //                      ElementPointer elementOne(element_);
    //                      int faceIdxOne = -1;
    //                      bool isOnBoundaryOne = false;
    //
    //                      centerPointOne = cellCenter_;
    //                      faceConnectionVectorMinusOne = cellCenter_;
    //                      globalIdxOne = globalIdx_;
    //                      elementOne = element_;
    //                      faceIdxOne = faceIdx;
    //                      isOnBoundaryOne = false;
    //
    //                      faceConnectionVectorMinusOne -= faceCenter;
    //
    //                      for(int j = 0; j<numNeighboringPoints && numInsertedVectors<3; j++ )
    //                      {
    //                          DimVector faceConnectionVectorMinusTwo(0);
    //                          DimVector centerPointTwo;
    //                          int globalIdxTwo = -1;
    //                          ElementPointer elementTwo(element_);
    //                          int faceIdxTwo = -1;
    //                          bool isOnBoundaryTwo = false;
    //
    //                          if(j == inverseIndexItToLocal_[faceIdx]){
    //                              centerPointTwo = cellCenter_;
    //                              faceConnectionVectorMinusTwo = cellCenter_;
    //                              globalIdxTwo = globalIdx_;
    //                              elementTwo = element_;
    //                              faceIdxTwo = faceIdx;
    //                              isOnBoundaryTwo = false;
    //
    //                          }else{
    //                              if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
    //                              {
    //                                  centerPointTwo = cellCenters_[j];
    //                                  faceConnectionVectorMinusTwo = cellCenters_[j];
    //                                  globalIdxTwo = globalIdxNeighboringElements_[j];
    //                                  elementTwo = neighboringElements_[j];
    //                                  faceIdxTwo = indexItToLocal_[j];
    //                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
    //                                  else isOnBoundaryTwo = false;
    //
    //                              }else if(faceTypes_[j] == heterogeneous)
    //                              {
    //                                  centerPointTwo = faceHarmonicAveragingPoints_[j];
    //                                  faceConnectionVectorMinusTwo = centerPointTwo;
    //                                  globalIdxTwo = globalIdxNeighboringElements_[j];
    //                                  elementTwo = neighboringElements_[j];
    //                                  faceIdxTwo = indexItToLocal_[j];
    //                                  if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
    //                                  else isOnBoundaryTwo = false;
    //
    //                              }
    //                          }
    //                          faceConnectionVectorMinusTwo -= faceCenter;
    //
    //                          for(int k = 0; k < numNeighboringPoints; k++ )
    //                          {
    //                              if(j!=k)
    //                              {
    //                                  DimVector faceConnectionVectorMinusThree(0);
    //                                  DimVector centerPointThree;
    //                                  int globalIdxThree = -1;
    //                                  ElementPointer elementThree(element_);
    //                                  int faceIdxThree = -1;
    //                                  bool isOnBoundaryThree = false;
    //                                  if(k == inverseIndexItToLocal_[faceIdx]){
    //                                      centerPointThree = cellCenter_;
    //                                      faceConnectionVectorMinusThree= cellCenter_;
    //                                      globalIdxThree = globalIdx_;
    //                                      elementThree = element_;
    //                                      faceIdxThree = faceIdx;
    //                                      isOnBoundaryThree = false;
    //
    //                                  }else{
    //
    //                                      if(faceTypes_[k] == homogeneous || faceTypes_[k] == boundary)
    //                                      {
    //                                          centerPointThree = cellCenters_[k];
    //                                          faceConnectionVectorMinusThree = cellCenters_[k];
    //                                          globalIdxThree = globalIdxNeighboringElements_[k];
    //                                          elementThree = neighboringElements_[k];
    //                                          faceIdxThree = indexItToLocal_[k];
    //                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
    //                                          else isOnBoundaryThree = false;
    //
    //                                      }else if(faceTypes_[k] == heterogeneous)
    //                                      {
    //                                          centerPointThree = faceHarmonicAveragingPoints_[k];
    //                                          faceConnectionVectorMinusThree = centerPointThree;
    //                                          globalIdxThree = globalIdxNeighboringElements_[k];
    //                                          elementThree = neighboringElements_[k];
    //                                          faceIdxThree = indexItToLocal_[k];
    //                                          if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
    //                                          else isOnBoundaryThree = false;
    //
    //                                      }
    //                                  }
    //                                  faceConnectionVectorMinusThree -= faceCenter;
    //
    //                                  if(faceStencilMinus.calculateCoefficients(faceNormalVectorMinus,faceConnectionVectorMinusOne, faceConnectionVectorMinusTwo ,faceConnectionVectorMinusThree)){
    //                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
    //                                      faceStencilMinus.insertStencilElement(elementOne);
    //                                      faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
    //                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusOne);
    //                                      faceStencilMinus.insertStencilCenterPoint(centerPointOne);
    //                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);
    //
    //                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
    //                                      faceStencilMinus.insertStencilElement(elementTwo);
    //                                      faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
    //                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusTwo);
    //                                      faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
    //                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);
    //
    //                                      faceStencilMinus.insertGlobalIdxNeighbors(globalIdxThree);
    //                                      faceStencilMinus.insertStencilElement(elementThree);
    //                                      faceStencilMinus.insertStencilFaceIdx(faceIdxThree);
    //                                      faceStencilMinus.insertSpanVector(faceConnectionVectorMinusThree);
    //                                      faceStencilMinus.insertStencilCenterPoint(centerPointThree);
    //                                      faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryThree);
    //
    //                                      numInsertedVectors++;
    //                                      numInsertedVectors++;
    //                                      numInsertedVectors++;
    //
    //                                      break;
    //                                  }
    //                              }
    //                          }
    //                      }
    //                  //}
    //
                    }


                    if(numInsertedVectors != dim && !isTPFA )
                        DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");

                    faceStencilMinus.setStored();

                    interactionVolume.faceNormalStencilVectorMinus_.push_back(faceStencilMinus);

                }
                else
                {
                    CoNormalDecompositionFaceStencil<TypeTag> faceStencilMinus;
                    interactionVolume.faceNormalStencilVectorMinus_.push_back(faceStencilMinus);
                }
                ++indexIt;
            }

        }


    CompositionalInteractionVolumeContainer(): problemPtr_(0)
    {

    }

protected:
    /*!
     * \brief A reference to the problem on which the model is applied.
     */
    Problem &problem_()
    { return *problemPtr_; }
    /*!
     * \copydoc problem_()
     */
    const Problem &problem_() const
    { return *problemPtr_; }

private:
    Problem *problemPtr_;
    std::vector<InteractionVolume> interactionVolumes_;

};
}
#endif /* CompositionalInteractionVolumeContainer_HH_ */
