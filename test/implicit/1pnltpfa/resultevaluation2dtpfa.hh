// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \ingroup IMPETtests
 * \brief Calculate errors for a FVCA6 benchmark problem.
 */
#ifndef DUMUX_RESULTEVALUATION_2D_TPFA_HH
#define DUMUX_RESULTEVALUATION_2D_TPFA_HH


namespace Dumux
{

/*!
 * \brief calculate errors for a FVCA6 benchmark problem
 */
template <class TypeTag>
struct ResultEvaluation
{

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > SolVector;


private:
    template<int dim>
    struct ElementLayout
    {
        bool contains (Dune::GeometryType gt)
        {
            return gt.dim() == dim;
        }
    };

    template<int dim>
    struct FaceLayout
    {
        bool contains (Dune::GeometryType gt)
        {
            return gt.dim() == dim-1;
        }
    };

public:
    double relativeL2Error;
    double absoluteL2Error;
    double relativeL2ErrorIn;
    double relativeL2ErrorOut;
    double realtiveL2ErrorFaceFluxes;
    double realtiveL2ErrorFaceFluxesBound;
    double realtiveL2ErrorFaceFluxesInner;
    double absoluteL2ErrorFaceFluxes;
    double realtiveL2ErrorFaceFluxes_withoutSingularity;
    double ergrad;
    double ervell2;
    double ervell2In;
    double uMinExact;
    double uMaxExact;
    double uMin;
    double uMax;
    double flux0;
    double flux1;
    double fluy0;
    double fluy1;
    double fluz0;
    double fluz1;
    double sumf;
    double sumflux;
    double exactflux0;
    double exactflux1;
    double exactfluy0;
    double exactfluy1;
    double exactfluz0;
    double exactfluz1;
    double errflx0;
    double errflx1;
    double errfly0;
    double errfly1;
    double erflm;
    double ener1;
    double hMax;
    double ngrad;
    SolVector localRelativeFaceFluxError;

    template<class Problem>
    void evaluate(Problem& problem, bool consecutiveNumbering = false)
    {
        typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
        typedef typename GridView::Grid Grid;
        enum {dim=Grid::dimension};
        typedef typename Grid::template Codim<0>::Entity Element;
        typedef typename Element::Geometry Geometry;
        typedef typename GridView::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename Geometry::JacobianTransposed JacobianTransposed;
        typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;

        typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
        typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

        int numDofs = problem.gridView().size(0);
        SolVector exactSol(numDofs);

        int refinement = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,int,Grid,Refinement);
        double h_ref = 0.1;
        //h_ref /= std::pow(2,refinement-3);
        Dune::FieldVector<double,dim>  x_singularity;
        x_singularity[0] = 0;
        x_singularity[1] = 0;

        localRelativeFaceFluxError.resize(numDofs);

        uMinExact = 1e100;
        uMaxExact = -1e100;
        uMin = 1e100;
        uMax = -1e100;
        flux0 = 0;
        flux1 = 0;
        fluy0 = 0;
        fluy1 = 0;
        fluz0 = 0;
        fluz1 = 0;
        sumf = 0;
        sumflux = 0;
        exactflux0 = 0;
        exactflux1 = 0;
        exactfluy0 = 0;
        exactfluy1 = 0;
        exactfluz0 = 0;
        exactfluz1 = 0;
        erflm = 0;
        ener1 = 0;
        hMax = 0;
        ngrad = 0;
        Scalar numerator = 0;
        Scalar denominator = 0;
        Scalar numerator_absolute = 0;
        Scalar numeratorIn = 0;
        Scalar denominatorIn = 0;
        Scalar numeratorOut = 0;
        Scalar denominatorOut = 0;
        Scalar totalFluxSum = 0;
        double numeratorGrad = 0;
        double denominatorGrad = 0;
        double numeratorFlux = 0;
        double denominatorFlux = 0;
        double numeratorFluxIn = 0;
        double denominatorFluxIn = 0;
        bool exactOutput = true;

        double numeratorFaceFlux = 0;
        double denominatorFaceFlux = 0;
        double numeratorFaceFluxBound = 0;
        double denominatorFaceFluxBound = 0;
        double numeratorFaceFluxInner = 0;
        double denominatorFaceFluxInner = 0;
        double numeratorFaceFlux_absolute = 0;
        double denominatorFaceFlux_absolute = 0;
        double numeratorFaceFlux_withoutSingularity = 0;
        double denominatorFaceFlux_withoutSingularity = 0;
        std::vector<bool> alreadyVisited(numDofs, false);

        ElementIterator endEIt = problem.gridView().template end<0>();
        for (ElementIterator eIt = problem.gridView().template begin<0>(); eIt != endEIt; ++eIt)
        {
            const Element& element = *eIt;

            //element geometry
            const Geometry& geometry = element.geometry();

            Dune::GeometryType geomType = geometry.type();

            //local and global coordinates of cell center
            const Dune::FieldVector<Scalar,dim>& local = ReferenceElements::general(geomType).position(0, 0);
            Dune::FieldVector<Scalar,dim> global = geometry.global(local);

            //volume of element
            Scalar volume = geometry.volume();

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int eIdx = problem.elementMapper().index(element);
#else
            int eIdx = problem.elementMapper().map(element);
#endif

            Scalar approxPressure = problem.model().curSol()[eIdx][0];
            Scalar exactPressure = problem.exact(global);

            //evaluate exact solution vector
            exactSol[eIdx] = exactPressure;
            localRelativeFaceFluxError[eIdx] = 0.0;

            //numerator and denominator of L2 error
            numerator += volume*(approxPressure - exactPressure)*(approxPressure - exactPressure);
            denominator += volume*exactPressure*exactPressure;

            numerator_absolute += volume*(approxPressure - exactPressure)*(approxPressure - exactPressure);

            // calculate the relative error for inner cells
            bool bndCell = false;
            IntersectionIterator beginis = problem.gridView().ibegin(element);
            IntersectionIterator endis = problem.gridView().iend(element);
            for (IntersectionIterator is = beginis; is!=endis; ++is)
            {
                if (is->boundary())
                {
                    bndCell = true;
                    break;
                }
            }

            //seperate error calculation for inner cells and outer cells
            if (bndCell)
            {
                numeratorOut += volume*(approxPressure - exactPressure)*(approxPressure - exactPressure);
                denominatorOut += volume*exactPressure*exactPressure;
            }
            else
            {
                numeratorIn += volume*(approxPressure - exactPressure)*(approxPressure - exactPressure);
                denominatorIn += volume*exactPressure*exactPressure;
            }

            //update uMinExact and uMaxExact
            uMinExact = std::min(uMinExact, exactPressure);
            uMaxExact = std::max(uMaxExact, exactPressure);

            //update uMin and uMax
            uMin = std::min(uMin, approxPressure);
            uMax = std::max(uMax, approxPressure);

            //evaluates source term at cell center
            //PrimaryVariables sourceVec;
            //problem.sourceAtPos(sourceVec, global);
            //sumf += volume*sourceVec[0];

            //get the absolute permeability
            Dune::FieldMatrix<double,dim,dim> K = problem.spatialParams().intrinsicPermeability(element);

            Dune::FieldVector<Scalar,2*dim> fluxVector;
            Dune::FieldVector<Scalar,dim> exactGradient;

            FVElementGeometry fvGeometry;
            fvGeometry.update(problem.gridView(), element);

            ElementVolumeVariables elemVolVars;

            elemVolVars.update(problem,
                               element,
                               fvGeometry,
                               false /* oldSol? */);

            //loops over faces of element
            int fIdx = -1;
            beginis = problem.gridView().ibegin(element);
            endis = problem.gridView().iend(element);
            for (IntersectionIterator is = beginis; is!=endis; ++is)
            {
                // local number of faces
                int faceIdx = 0.0;

                int eIdxJ = -1;

                if(is->neighbor()){
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                    eIdxJ = problem.elementMapper().index(*is->outside());
#else
                    eIdxJ = problem.elementMapper().map(*is->outside());
#endif
                    fIdx++;
                    faceIdx = fIdx;
                }else{
                    faceIdx = is->indexInInside();
                }

                Dune::FieldVector<double,dim> faceGlobal = is->geometry().center();
                double faceVol = is->geometry().volume();

                // get normal vector
                Dune::FieldVector<double,dim> unitOuterNormal = is->centerUnitOuterNormal();

                // get the exact gradient at the cell center: grad p_ex
                exactGradient = problem.exactGrad(faceGlobal);

                // get the negative exact velocity

                Dune::FieldVector<double,dim> KGrad(0);
                // umv: KGrad = K * exactGradient
                K.mv(exactGradient, KGrad);

                // calculate the exact normal velocity v = K grad p * n
                double exactFlux = KGrad*unitOuterNormal;

                // get the approximate normalvelocity

                FluxVariables fluxVars(problem,
                                        element,
                                        fvGeometry,
                                        faceIdx,
                                        elemVolVars,
                                        is->boundary());

                double approximateFlux = fluxVars.volumeFlux(0)/faceVol;

                // calculate the difference in the normal velocity
                double fluxDiff = exactFlux + approximateFlux;

                //std::cout << "GlobalIdxI: " << eIdx << "GlobalIdxJ: "<< eIdxJ << "fluxDiff: " << fluxDiff << std::endl;

                // update mean value error
                erflm = std::max(erflm, fabs(fluxDiff));

                numeratorFlux += volume*fluxDiff*fluxDiff;
                denominatorFlux += volume*exactFlux*exactFlux;

                localRelativeFaceFluxError[eIdx] += volume*fluxDiff*fluxDiff;
                totalFluxSum += volume*exactFlux*exactFlux;

                if (!bndCell)
                {
                    numeratorFluxIn += volume*fluxDiff*fluxDiff;
                    denominatorFluxIn += volume*exactFlux*exactFlux;
                }

                // calculate the fluxes through the element faces
                //exactFlux *= faceVol;
                //approximateFlux *= faceVol;
                fluxVector[faceIdx] = approximateFlux;
                //fluxDiff *= faceVol;

                if(!is->neighbor()){
                    numeratorFaceFlux +=  fluxDiff*fluxDiff*volume;
                    denominatorFaceFlux += exactFlux*exactFlux*volume;

                    numeratorFaceFluxBound +=  fluxDiff*fluxDiff*volume;
                    denominatorFaceFluxBound += exactFlux*exactFlux*volume;

                    numeratorFaceFlux_absolute +=  fluxDiff*fluxDiff*volume;
                    denominatorFaceFlux_absolute += volume;

                    if((faceGlobal-x_singularity).two_norm() > 2.0*h_ref)
                    {
                        numeratorFaceFlux_withoutSingularity +=  fluxDiff*fluxDiff*volume;
                        denominatorFaceFlux_withoutSingularity += exactFlux*exactFlux*volume;
                    }

                }else{
                    if(!alreadyVisited[eIdxJ]){
                        //get volume of neighbored element
                        Scalar volumeNeighbor = is->outside()->geometry().volume();
                        //calculate arithmetic average of cell volumes
                        Scalar volumeAverage = 0.5*(volumeNeighbor + volume);

                        numeratorFaceFlux +=  fluxDiff*fluxDiff*volumeAverage;
                        denominatorFaceFlux += exactFlux*exactFlux*volumeAverage;

                        numeratorFaceFluxInner +=  fluxDiff*fluxDiff*volumeAverage;
                        denominatorFaceFluxInner += exactFlux*exactFlux*volumeAverage;

                        numeratorFaceFlux_absolute +=  fluxDiff*fluxDiff*volumeAverage;
                        denominatorFaceFlux_absolute += volumeAverage;

                        if((faceGlobal-x_singularity).two_norm() > 2.0*h_ref)
                        {
                            numeratorFaceFlux_withoutSingularity +=  fluxDiff*fluxDiff*volumeAverage;
                            denominatorFaceFlux_withoutSingularity += exactFlux*exactFlux*volumeAverage;
                        }

                        if(!bndCell){

                        }else{
//                            bool bndCellJ = false;
//                            IntersectionIterator beginisIt = problem.gridView().ibegin(*is->outside());
//                            IntersectionIterator endisIt = problem.gridView().iend(*is->outside());
//                            for (IntersectionIterator isIt = beginisIt; isIt!=endisIt; ++isIt)
//                            {
//                                if (isIt->boundary())
//                                {
//                                    bndCellJ = true;
//                                    break;
//                                }
//                            }
//                            if(!bndCellJ){
//
//                            }
                        }

                    }
                }


                if (!is->neighbor()) {
                    if(dim>2)
                    {
                    if (fabs(faceGlobal[2]) < 1e-6) {
                        fluz0 += approximateFlux;
                        exactfluz0 += exactFlux;
                    }
                    else if (fabs(faceGlobal[2] - 1.0) < 1e-6) {
                        fluz1 += approximateFlux;
                        exactfluz1 += exactFlux;
                    }
                    }
                    if (fabs(faceGlobal[1]) < 1e-6) {
                        fluy0 += approximateFlux;
                        exactfluy0 += exactFlux;
                    }
                    else if (fabs(faceGlobal[1] - 1.0) < 1e-6) {
                        fluy1 += approximateFlux;
                        exactfluy1 += exactFlux;
                    }
                    else if (faceGlobal[0] < 1e-6) {
                        flux0 += approximateFlux;
                        exactflux0 += exactFlux;
                    }
                    else if (fabs(faceGlobal[0] - 1.0) < 1e-6) {
                        flux1 += approximateFlux;
                        exactflux1 += exactFlux;
                    }
                }
            }

            // calculate velocity on reference element
            Dune::FieldVector<Scalar,dim> refVelocity;
            if (geometry.corners() == 8) {
                refVelocity[0] = 0.5*(fluxVector[1] - fluxVector[0]);
                refVelocity[1] = 0.5*(fluxVector[3] - fluxVector[2]);
                refVelocity[2] = 0.5*(fluxVector[5] - fluxVector[4]);
            }
            else if(geometry.corners() == 4){
                refVelocity[0] = 0.5*(fluxVector[1] - fluxVector[0]);
                refVelocity[1] = 0.5*(fluxVector[3] - fluxVector[2]);
            }

            // get the transposed Jacobian of the element mapping
            const JacobianTransposed jacobianT =
                    geometry.jacobianTransposed(local);

            // calculate the element velocity by the Piola transformation
            Dune::FieldVector<Scalar,dim> elementVelocity(0);
            jacobianT.umtv(refVelocity, elementVelocity);
            elementVelocity /= geometry.integrationElement(local);

            // get the approximate gradient: solve v = - K grad p for grad p
            Dune::FieldVector<Scalar,dim> approximateGradient;
            K.solve(approximateGradient, elementVelocity);
            approximateGradient *= -1.0;
            // get the exact gradient
            exactGradient = problem.exactGrad(global);

            // the difference between exact and approximate gradient
            Dune::FieldVector<Scalar,dim> gradDiff(exactGradient);
            gradDiff -= approximateGradient;

            // add to energy
            ener1 += volume*(approximateGradient*elementVelocity);

            numeratorGrad += volume*(gradDiff*gradDiff);
            denominatorGrad += volume*(exactGradient*exactGradient);

            // calculate the maximum of the diagonal length of all elements on leaf grid
            for (int i = 0; i < eIt->geometry().corners(); ++i)
            {
                Dune::FieldVector<Scalar,dim> corner1 = eIt->geometry().corner(i);

                for (int j = 0; j < eIt->geometry().corners(); ++j)
                {
                    // get all corners of current element and compare the distances between them
                    Dune::FieldVector<Scalar,dim> corner2 = eIt->geometry().corner(j);

                    // distance vector between corners
                    Dune::FieldVector<Scalar,dim> distVec = corner1 - corner2;
                    Scalar dist = distVec.two_norm();

                    if (hMax < dist)
                        hMax = dist;
                }
            }

            // ngrad: L1 norm of the euclidian norm of the approximate gradient
            if(dim == 3)
            {
            ngrad += volume* sqrt(approximateGradient[0]*approximateGradient[0] + approximateGradient[1]*approximateGradient[1] +
                    approximateGradient[2]*approximateGradient[2]);
            }

            alreadyVisited[eIdx] = true;

        }

        relativeL2Error = sqrt(numerator/denominator);
        absoluteL2Error = sqrt(numerator_absolute);
        relativeL2ErrorIn = sqrt(numeratorIn/denominatorIn);
        relativeL2ErrorOut = sqrt(numeratorOut/denominatorOut);
        realtiveL2ErrorFaceFluxes = sqrt(numeratorFaceFlux/denominatorFaceFlux);
        realtiveL2ErrorFaceFluxesBound = sqrt(numeratorFaceFluxBound/denominatorFaceFluxBound);
        realtiveL2ErrorFaceFluxesInner = sqrt(numeratorFaceFluxInner/denominatorFaceFluxInner);
        absoluteL2ErrorFaceFluxes = sqrt(numeratorFaceFlux_absolute/denominatorFaceFlux_absolute);
        realtiveL2ErrorFaceFluxes_withoutSingularity = sqrt(numeratorFaceFlux_withoutSingularity/denominatorFaceFlux_withoutSingularity);
        ergrad = sqrt(numeratorGrad/denominatorGrad);
        ervell2 = sqrt(numeratorFlux/denominatorFlux);
        ervell2In = sqrt(numeratorFluxIn/denominatorFluxIn);
        sumflux = flux0 + flux1 + fluy0 + fluy1 - sumf;
        errflx0 = fabs((flux0 + exactflux0)/exactflux0);
        errflx1 = fabs((flux1 + exactflux1)/exactflux1);
        errfly0 = fabs((fluy0 + exactfluy0)/exactfluy0);
        errfly1 = fabs((fluy1 + exactfluy1)/exactfluy1);

        localRelativeFaceFluxError /= totalFluxSum;

        // generate a VTK file of exact solution
//        if (exactOutput)
//        {
//            Dune::VTKWriter<GridView> vtkwriter(gridView);
//            char fname[128];
//            sprintf(fname, "exactSol-numRefine%d", gridView.grid().maxLevel());
//            vtkwriter.addCellData(exactSol,"exact pressure solution~");
//            vtkwriter.write(fname,Dune::VTK::ascii);
//        }

        return;
    }

};

}

#endif
