// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_IMPLICIT_GRIDADAPTINDICATOR2P2C_HH
#define DUMUX_IMPLICIT_GRIDADAPTINDICATOR2P2C_HH

#include "2p2cproperties.hh"
#include <dune/localfunctions/lagrange/pqkfactory.hh>
//#include <dumux/linear/vectorexchange.hh>

/**
 * @file
 * @brief  Class defining a standard, saturation dependent indicator for grid adaptation
 */
namespace Dumux
{

template<class TypeTag>
class TwoPTwoCImplicitGridAdaptIndicator
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    // present phases
    enum {
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };

    enum {
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        pwsn = TwoPTwoCFormulation::pwsn,
        pnsw = TwoPTwoCFormulation::pnsw
    };

    // primary variable indices
    enum {
        switchIdx = Indices::switchIdx,
        pressureIdx = Indices::pressureIdx
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx
    };

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::ctype CoordScalar;

    typedef Dune::PQkLocalFiniteElementCache<CoordScalar, Scalar, dim, 1> LocalFiniteElementCache;
    typedef typename LocalFiniteElementCache::FiniteElementType LocalFiniteElement;

public:
    /*! \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * This standard indicator is based on the saturation gradient.
     */
    void calculateIndicator()
    {
        // prepare an indicator for refinement
        if(indicatorVector_.size() != problem_.gridView().size(0))
        {
            indicatorVector_.resize(problem_.gridView().size(0));
        }
        indicatorVector_ = -1e100;

        Scalar globalMax = -1e100;
        Scalar globalMin = 1e100;

        // 1) calculate Indicator -> min, maxvalues
        // loop over all leaf-elements
        for (const auto& element : Dune::elements(problem_.gridView()))
        {
            // calculate minimum and maximum saturation
            // index of the current leaf-elements
            int globalIdxI = problem_.elementMapper().index(element);

            int phasePresence = problem_.model().phasePresence(globalIdxI, false);
            Scalar satI = 0.0;

            if(!isBox)
            {
                Scalar sn = 0.0;
                if (phasePresence == nPhaseOnly)
                    sn = 1.0;
                else if (phasePresence == wPhaseOnly) {
                    sn = 0.0;
                }
                else if (phasePresence == bothPhases) {
                    if (formulation == pwsn)
                        sn = problem_.model().curSol()[globalIdxI][switchIdx];
                    else if (formulation == pnsw)
                        sn = 1.0 - problem_.model().curSol()[globalIdxI][switchIdx];
                    else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
                }
                satI = sn;
            }
            else
            {
                DUNE_THROW(Dune::NotImplemented,"Adaptivity not implemented for Box model");
            }

            globalMin = std::min(satI, globalMin);
            globalMax = std::max(satI, globalMax);

            // calculate refinement indicator in all cells
            for (const auto& intersection : Dune::intersections(problem_.gridView(), element))
            {
                // Only consider internal intersections
                if (intersection.neighbor())
                {
                    // Access neighbor
                    auto outside = intersection.outside();
                    int globalIdxJ = problem_.elementMapper().index(outside);

                    // Visit intersection only once
                    if (element.level() > outside.level() || (element.level() == outside.level() && globalIdxI < globalIdxJ))
                    {
                        int phasePresence = problem_.model().phasePresence(globalIdxJ, false);
                        Scalar satJ = 0.0;

                        if(!isBox)
                        {
                            Scalar sn = 0.0;
                            if (phasePresence == nPhaseOnly)
                                sn = 1.0;
                            else if (phasePresence == wPhaseOnly) {
                                sn = 0.0;
                            }
                            else if (phasePresence == bothPhases) {
                                if (formulation == pwsn)
                                    sn = problem_.model().curSol()[globalIdxJ][switchIdx];
                                else if (formulation == pnsw)
                                    sn = 1.0 - problem_.model().curSol()[globalIdxJ][switchIdx];
                                else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
                            }
                            satJ = sn;
                        }
                        else
                        {
                            DUNE_THROW(Dune::NotImplemented,"Adaptivity not implemented for Box model");
                        }



                        Scalar localdelta = std::abs(satI - satJ);
                        indicatorVector_[globalIdxI][0] = std::max(indicatorVector_[globalIdxI][0], localdelta);
                        indicatorVector_[globalIdxJ][0] = std::max(indicatorVector_[globalIdxJ][0], localdelta);
                    }
                }
            }

        }

        refineBound_ = refinetol_;
        coarsenBound_ = coarsentol_;

//#if HAVE_MPI
//    // communicate updated values
//    typedef VectorExchange<ElementMapper, ScalarSolutionType> DataHandle;
//    DataHandle dataHandle(problem_.elementMapper(), indicatorVector_);
//    problem_.gridView().template communicate<DataHandle>(dataHandle,
//                                                         Dune::InteriorBorder_All_Interface,
//                                                         Dune::ForwardCommunication);
//
//    refineBound_ = problem_.gridView().comm().max(refineBound_);
//    coarsenBound_ = problem_.gridView().comm().max(coarsenBound_);
//
//#endif
    }

    /*! \brief Indicator function for marking of grid cells for refinement
     *
     * Returns true if an element should be refined.
     *
     *  \param element A grid element
     */
    bool refine(const Element& element)
    {
        return (indicatorVector_[problem_.elementMapper().index(element)] > refineBound_);
    }

    /*! \brief Indicator function for marking of grid cells for coarsening
     *
     * Returns true if an element should be coarsened.
     *
     *  \param element A grid element
     */
    bool coarsen(const Element& element)
    {
        return (indicatorVector_[problem_.elementMapper().index(element)] < coarsenBound_);
    }

    /*! \brief Initializes the adaptation indicator class*/
    void init()
    {
        refineBound_ = 0.;
        coarsenBound_ = 0.;
        indicatorVector_.resize(problem_.gridView().size(0));
    };

    /*! @brief Constructs a GridAdaptIndicator instance
     *
     *  This standard indicator is based on the saturation gradient.
     *  It checks the local gradient compared to the maximum global gradient.
     *  The indicator is compared locally to a refinement/coarsening threshold to decide whether
     *  a cell should be marked for refinement or coarsening or should not be adapted.
     *
     * \param problem The problem object
     */
    TwoPTwoCImplicitGridAdaptIndicator(Problem& problem):
        problem_(problem)
    {
        refinetol_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, GridAdapt, RefineTolerance);
        coarsentol_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, GridAdapt, CoarsenTolerance);
    }

protected:
    Problem& problem_;
    Scalar refinetol_;
    Scalar coarsentol_;
    Scalar refineBound_;
    Scalar coarsenBound_;
    Dune::BlockVector<Dune::FieldVector<Scalar, 1> > indicatorVector_;
};
}

#endif
