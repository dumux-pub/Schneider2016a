// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        volume fluxes of fluid phases over a face of a finite volume by means
 *        of the Darcy approximation.
 *
 */
#ifndef DUMUX_NLTPFA_DARCY_FLUX_VARIABLES_HH
#define DUMUX_NLTPFA_DARCY_FLUX_VARIABLES_HH

#include <dumux/implicit/common/implicitproperties.hh>
#include <dune/common/float_cmp.hh>

#include <dumux/implicit/common/normaldecompositioninteractionvolume.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/math.hh>

namespace Dumux
{

namespace Properties
{
// forward declaration of properties
NEW_PROP_TAG(ImplicitMobilityUpwindWeight);
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(NumPhases);
NEW_PROP_TAG(ProblemEnableGravity);
}

/*!
 * \ingroup ImplicitFluxVariables
 * \brief Evaluates the normal component of the Darcy velocity
 * on a (sub)control volume face.
 */
template <class TypeTag>
class NLTPFADarcyFluxVariables
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;

    enum { dim = GridView::dimension} ;
    enum { dimWorld = GridView::dimensionworld} ;
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)} ;

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef Dumux::NormalDecompositionInteractionVolume<TypeTag> InteractionVolume;

    typedef Dumux::CoNormalDecompositionFaceStencil<TypeTag> FaceStencil;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    enum FaceTypes
    {
        homogeneous = 1,
        heterogeneous = 2,
        boundary = 0
    };

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    NLTPFADarcyFluxVariables(const Problem &problem,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int fIdx,
                 const ElementVolumeVariables &elemVolVars,
                 const bool onBoundary = false)
    : fvGeometry_(fvGeometry), faceIdx_(fIdx), onBoundary_(onBoundary)
    {
        mobilityUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MobilityUpwindWeight);
        calculateNormalVelocity_(problem, element, elemVolVars);
    }

public:
    /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face.
     *        face().normal
     *        has already the magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
    Scalar volumeFlux(const unsigned int phaseIdx) const
    { return volumeFlux_[phaseIdx]; }

    /*!
     * \brief Return the velocity of a given phase.
     *
     *        This is the full velocity vector on the
     *        face (without being multiplied with normal).
     *
     * \param phaseIdx index of the phase
     */
    GlobalPosition velocity(const unsigned int phaseIdx) const
    { return velocity_[phaseIdx] ; }

    /*!
     * \brief Return intrinsic permeability multiplied with potential
     *        gradient multiplied with normal.
     *        I.e. everything that does not need upwind decisions.
     *
     * \param phaseIdx index of the phase
     */
//    Scalar kGradPNormal(const unsigned int phaseIdx) const
//    { return kGradPNormal_[phaseIdx] ; }

    /*!
     * \brief Return the local index of the downstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int downstreamIdx(const unsigned phaseIdx) const
    { return downstreamIdx_[phaseIdx]; }

    /*!
     * \brief Return the local index of the upstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int upstreamIdx(const unsigned phaseIdx) const
    { return upstreamIdx_[phaseIdx]; }

    /*!
     * \brief Return the SCV (sub-control-volume) face. This may be either
     *        a face within the element or a face on the element boundary,
     *        depending on the value of onBoundary_.
     */
    const SCVFace &face() const
    {
        if (onBoundary_)
            return fvGeometry_.boundaryFace[faceIdx_];
        else
            return fvGeometry_.subContVolFace[faceIdx_];
    }

protected:

    void calculateNormalVelocity_(const Problem &problem,
                                  const Element &element,
                                  const ElementVolumeVariables &elemVolVars)
    {

        bool useNLTPFA = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                UseNlTpfa);

        bool recalculateDirichlet = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                RecalculateDirichletFromNeumann);

        bool addNeumannToFlux = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                AddNeumannFluxToFaceFlux);

        bool useTpfaAtBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                UseTpfaAtBoundary);

        bool useTpfaAtBoundaryCells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                bool,
                Mpfa,
                UseTpfaAtBoundaryCells);

        bool dependsOnNeumannBoundary = false;


        if(recalculateDirichlet && addNeumannToFlux)
        {
            std::cout << "Warning: Cannot use both RecalculateDirichletFromNeumann and AddNeumannFluxToFaceFlux" << std::endl;
        }


        for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
        {
            // get the global index of the cell
            //int globalIdxI = problem.variables().index(element);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalIdxI = problem.elementMapper().index(element);
#else
            int globalIdxI = problem.elementMapper().map(element);
#endif

            const InteractionVolume* interactionVolumeI = problem.model().getInteractionVolume(globalIdxI);

            Scalar faceFlux = 0.0;
            Scalar faceBoundaryFluxPlus = 0.0;
            Scalar faceBoundaryFluxMinus = 0.0;

            //int faceIdxI = faceIdx_;
            //int faceIdxJ = -1;

            GlobalPosition g(problem.gravityAtPos(face().ipGlobal));

            Scalar grav = 0;
            Scalar density = 0.0;
            if(GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity)){
                grav = g[dim-1];
                // calculate the phase density at the integration point. we
                // only do this if the wetting phase is present in both cells
                Scalar SI = elemVolVars[face().i].fluidState().saturation(phaseIdx);
                Scalar SJ = elemVolVars[face().j].fluidState().saturation(phaseIdx);
                Scalar rhoI = elemVolVars[face().i].fluidState().density(phaseIdx);
                Scalar rhoJ = elemVolVars[face().j].fluidState().density(phaseIdx);
                Scalar fI = std::max(0.0, std::min(SI/1e-5, 0.5));
                Scalar fJ = std::max(0.0, std::min(SJ/1e-5, 0.5));
                if (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(fI + fJ, 0.0, 1.0e-30))
                    // doesn't matter because no wetting phase is present in
                    // both cells!
                    fI = fJ = 0.5;
                density = (fI*rhoI + fJ*rhoJ)/(fI + fJ);
            }
            Scalar absHeight = problem.bBoxMax()[dim-1];


            /************* handle interior face *****************/

            if (!onBoundary_)
            {
                const Element& elementJ = *fvGeometry_.neighbors[face().j];
                //int faceIdxJ = face().fNeighIdx;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int globalIdxJ = problem.elementMapper().index(elementJ);
#else
                int globalIdxJ = problem.elementMapper().map(elementJ);
#endif
                const InteractionVolume* interactionVolumeJ = problem.model().getInteractionVolume(globalIdxJ);

                std::pair<int,int> indexI(globalIdxJ, -1);
                int faceType = interactionVolumeI->getFaceType(indexI);


                if(faceType == homogeneous || faceType == heterogeneous)
                {

                    const FaceStencil* faceStencilI = interactionVolumeI->getFaceStencilVectorPlus(indexI);
                    const int numFAPI = faceStencilI->getNumElements();
                    const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                    const DimVector coefficientsI = faceStencilI->getCoefficients();
                    const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                    const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                    const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                    const DimVector centerPointI = faceStencilI->getCenterPoint();
                    const bool isOnBoundaryI = interactionVolumeI->isOnBoundary();

                    std::pair<int,int> indexJ(globalIdxI, -1);
                    const FaceStencil* faceStencilJ = interactionVolumeJ->getFaceStencilVectorPlus(indexJ);
                    const int numFAPJ = faceStencilJ->getNumElements();
                    const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                    const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                    const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                    const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                    const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                    const DimVector centerPointJ = faceStencilJ->getCenterPoint();
                    const bool isOnBoundaryJ = interactionVolumeJ->isOnBoundary();

                    if(!(useTpfaAtBoundaryCells && (isOnBoundaryI || isOnBoundaryJ)))
                    {
                    Scalar d_plus = 0.0;
                    Scalar d_minus = 0.0;

                    Scalar M_plus = 0.0;
                    Scalar M_minus = 0.0;
                    Scalar M_plusminus = 0.0;
                    Scalar M_minusplus = 0.0;

                    Scalar mu_plus = 0.5;
                    Scalar mu_minus = 0.5;


                    for(int i=0 ; i < numFAPI ; i++){

                        if(globalIdxVectorI[i] > -1){

                            M_plus += coefficientsI[i];
                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                            int localFaceType = interactionVolumeI->getFaceType(index);

                            if(localFaceType == homogeneous){
                                if(globalIdxVectorI[i] == globalIdxJ){
                                    M_plusminus += coefficientsI[i];
                                }else{
                                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                                    d_plus += coefficientsI[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                            density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
                                }
                            }
                            else if(localFaceType == heterogeneous)
                            {
                                Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                                Scalar neighHarmonicCoeff = interactionVolumeI->getNeighboringHarmonicAveragingCoefficient(index);

                                if(globalIdxVectorI[i] == globalIdxJ){
                                    M_plus -= coefficientsI[i]*harmonicCoeff;
                                    M_plusminus += coefficientsI[i]*neighHarmonicCoeff;
                                }else{
                                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                                    d_plus += coefficientsI[i]*neighHarmonicCoeff*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                            density*(absHeight-stencilCenterPointsI[i][dim-1])*grav ) ;
                                    M_plus -= coefficientsI[i]*harmonicCoeff;
                                }


                            }
                        }
                        else
                        {
                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                            int volVarsIdx = fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);
                            Scalar potentialVal = 0.0;

                            if(bcTypes.hasNeumann())
                            {
                                dependsOnNeumannBoundary = true;

                                PrimaryVariables values;
                                //problem.neumannAtPos(values,stencilCenterPointsI[i]);
                                //problem.neumannAtPos(values,isIt->geometry().center());
                                problem.solDependentNeumann(values,
                                                              element,
                                                              fvGeometry_,
                                                              *isIt,
                                                              /*scvIdx=*/0,
                                                              stencilFaceIdxI[i],
                                                              elemVolVars);

                                if(recalculateDirichlet)
                                {
                                    potentialVal = calculateNeumannPressure(problem, globalIdxI, elemVolVars,
                                            interactionVolumeI, stencilFaceIdxI[i], values[phaseIdx], phaseIdx, isIt);


                                    M_plus += coefficientsI[i];
                                    d_plus += coefficientsI[i]*potentialVal;
                                }

                                Scalar visko = elemVolVars[volVarsIdx].fluidState().viscosity(phaseIdx);
                                Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                                visko = 1.0;
                                faceBoundaryFluxPlus += coefficientsI[i]*harmonicCoeff*visko*values[phaseIdx];

                            }else if(bcTypes.hasDirichlet())
                            {
                                //PrimaryVariables values;
                                //problem.dirichletAtPos(values, stencilCenterPointsI[i]);

                                potentialVal = (/*values[phaseIdx]*/ elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                        density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );

                                M_plus += coefficientsI[i];
                                d_plus += coefficientsI[i]*potentialVal;
                            }

                        }
                    }

                    for(int i=0 ; i < numFAPJ ; i++){

                        if(globalIdxVectorJ[i] > -1){

                            M_minus += coefficientsJ[i];

                            std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                            int localFaceType = interactionVolumeJ->getFaceType(index);

                            if(localFaceType == homogeneous){
                                if(globalIdxVectorJ[i] == globalIdxI){
                                    M_minusplus += coefficientsJ[i];
                                }else{
                                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                                    d_minus += coefficientsJ[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                            density*(absHeight-stencilCenterPointsJ[i][dim-1])*grav );
                                }
                            }
                            else if(localFaceType == heterogeneous)
                            {
                                Scalar harmonicCoeff = interactionVolumeJ->getHarmonicAveragingCoefficient(index);
                                Scalar neighHarmonicCoeff = interactionVolumeJ->getNeighboringHarmonicAveragingCoefficient(index);

                                if(globalIdxVectorJ[i] == globalIdxI){
                                    M_minus -= coefficientsJ[i]*harmonicCoeff;
                                    M_minusplus += coefficientsJ[i]*neighHarmonicCoeff;
                                }else{
                                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                                    d_minus += coefficientsJ[i]*neighHarmonicCoeff*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                            density*(absHeight-stencilCenterPointsJ[i][dim-1])*grav );
                                    M_minus -= coefficientsJ[i]*harmonicCoeff;
                                }


                            }

                        }
                        else
                        {
                            std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                            int volVarsIdx = fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeJ->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);
                            Scalar potentialVal = 0.0;

                            if(bcTypes.hasNeumann())
                            {
                                dependsOnNeumannBoundary = true;
                                PrimaryVariables values;
                                //problem.neumannAtPos(values,stencilCenterPointsJ[i]);
                                //problem.neumannAtPos(values,isIt->geometry().center());
                                problem.solDependentNeumann(values,
                                                              elementJ,
                                                              fvGeometry_,
                                                              *isIt,
                                                              /*scvIdx=*/0,
                                                              stencilFaceIdxJ[i],
                                                              elemVolVars);

                                if(recalculateDirichlet)
                                {
                                    potentialVal = calculateNeumannPressure(problem, globalIdxJ, elemVolVars,
                                            interactionVolumeJ, stencilFaceIdxJ[i], values[phaseIdx], phaseIdx, isIt);

                                    M_minus += coefficientsJ[i];
                                    d_minus += coefficientsJ[i]*potentialVal;
                                }

                                Scalar visko = elemVolVars[volVarsIdx].fluidState().viscosity(phaseIdx);
                                Scalar harmonicCoeff = interactionVolumeJ->getHarmonicAveragingCoefficient(index);
                                visko = 1.0;
                                faceBoundaryFluxMinus += coefficientsJ[i]*harmonicCoeff*visko*values[phaseIdx];
                                //std::cout<<"Val: "<<values<<std::endl;

                            }else if(bcTypes.hasDirichlet())
                            {
                                //PrimaryVariables values;
                                //problem.dirichletAtPos(values, stencilCenterPointsJ[i]);

                                potentialVal = (/*values[phaseIdx]*/ elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                        density*(absHeight-stencilCenterPointsJ[i][dim-1])*grav );

                                M_minus += coefficientsJ[i];
                                d_minus += coefficientsJ[i]*potentialVal;

                            }


                        }

                    }

                        mu_plus = 0.5;
                        mu_minus = 0.5;

                        if(useNLTPFA && (d_plus > 1.0e-30 && d_minus > 1.0e-30))
                        {
                            mu_plus = d_minus/(d_minus + d_plus);
                            mu_minus = d_plus/(d_minus + d_plus);
                        }else if(useNLTPFA){
                            //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                        }

                        M_plus *= mu_plus;
                        M_plus += mu_minus*M_minusplus;

                        M_minus *= mu_minus;
                        M_minus += mu_plus*M_plusminus;

                        int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                        int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));
                        faceFlux = M_plus*(elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx)  +
                                density*(absHeight-centerPointI[dim-1])*grav );
                        faceFlux -= M_minus*(elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                                density*(absHeight-centerPointJ[dim-1])*grav );
                        faceFlux -= mu_plus*d_plus;
                        faceFlux += mu_minus*d_minus;

                        faceBoundaryFluxPlus *= mu_plus;
                        faceBoundaryFluxMinus *= mu_minus;
                        if(addNeumannToFlux && !recalculateDirichlet)
                        {
                            faceFlux += faceBoundaryFluxPlus;
                            faceFlux -= faceBoundaryFluxMinus;
                        }
                        faceStencilI->setFaceFlux(faceFlux /*+ faceBoundaryFluxPlus - faceBoundaryFluxMinus*/);

                        if(dependsOnNeumannBoundary && useTpfaAtBoundary){
                            const SpatialParams &spatialParams = problem.spatialParams();
                            DimWorldMatrix KI;
                            DimWorldMatrix KJ;

                            KI = problem.spatialParams().intrinsicPermeability(element);
                            KJ = problem.spatialParams().intrinsicPermeability(elementJ);

                            DimWorldMatrix K;
                            spatialParams.meanK(K,KI,KJ);

                            std::pair<int,int> index(globalIdxJ,-1);

                            IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);

                            DimVector unitNormal = isIt->centerUnitOuterNormal();

                            DimVector centerPointI = element.geometry().center();
                            DimVector centerPointJ = elementJ.geometry().center();

                            DimVector distVec = centerPointJ - centerPointI;
                            distVec /= distVec.two_norm2();

                            K.mv(distVec,kGradP_[phaseIdx]);

                            kGradPNormal_[phaseIdx] = kGradP_[phaseIdx] * unitNormal;

                            int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                            int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));

                            Scalar pI = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                                    density*(absHeight-centerPointI[dim-1])*grav );

                            Scalar pJ = (elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                                    density*(absHeight-centerPointJ[dim-1])*grav );

                            faceFlux = face().area*kGradPNormal_[phaseIdx]*(pI-pJ);
                        }
                    }
                    else{
                        const SpatialParams &spatialParams = problem.spatialParams();
                        DimWorldMatrix KI;
                        DimWorldMatrix KJ;

                        KI = problem.spatialParams().intrinsicPermeability(element);
                        KJ = problem.spatialParams().intrinsicPermeability(elementJ);

                        DimWorldMatrix K;
                        spatialParams.meanK(K,KI,KJ);

                        std::pair<int,int> index(globalIdxJ,-1);

                        IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);

                        DimVector unitNormal = isIt->centerUnitOuterNormal();

                        DimVector centerPointI = element.geometry().center();
                        DimVector centerPointJ = elementJ.geometry().center();

                        DimVector distVec = centerPointJ - centerPointI;
                        distVec /= distVec.two_norm2();

                        K.mv(distVec,kGradP_[phaseIdx]);

                        kGradPNormal_[phaseIdx] = kGradP_[phaseIdx] * unitNormal;

                        int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                        int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));

                        Scalar pI = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                                density*(absHeight-centerPointI[dim-1])*grav );

                        Scalar pJ = (elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                                density*(absHeight-centerPointJ[dim-1])*grav );

                        faceFlux = face().area*kGradPNormal_[phaseIdx]*(pI-pJ);
                    }
                }
                else
                {
                    DUNE_THROW(Dune::NotImplemented, "Unknown face type!!!");
                }

            }
            /************* boundary face ************************/

            else
            {
                std::pair<int,int> indexI(globalIdxI, faceIdx_);
                const FaceStencil* faceStencilI = interactionVolumeI->getFaceStencilVectorPlus(indexI);
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const DimVector coefficientsI = faceStencilI->getCoefficients();
                const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const DimVector centerPointI = faceStencilI->getCenterPoint();

                bool complexBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        ComplexBoundary);

                bool newBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        NewBoundaryApprox);

                if(complexBoundary || newBoundary)
                {

                Scalar d_plus = 0.0;
                Scalar d_minus = 0.0;

                Scalar M_plus = 0.0;
                Scalar M_minus = 0.0;
                Scalar M_plusminus = 0.0;
                Scalar M_minusplus = 0.0;

                Scalar f_plus = 0.0;
                Scalar f_minus = 0.0;

                Scalar mu_plus = 0.5;
                Scalar mu_minus = 0.5;


                for(int i=0 ; i < numFAPI ; i++){

                    if(globalIdxVectorI[i] > -1){

                        M_plus += coefficientsI[i];

                        std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                        int localFaceType = interactionVolumeI->getFaceType(index);

                        if(localFaceType == homogeneous){
                            int volVarsIdx = fvGeometry_.getLocalIndex(index);
                            d_plus += coefficientsI[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                    density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
                        }
                        else if(localFaceType == heterogeneous){
                            Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                            Scalar neighHarmonicCoeff = interactionVolumeI->getNeighboringHarmonicAveragingCoefficient(index);

                            int volVarsIdx = fvGeometry_.getLocalIndex(index);
                            d_plus += coefficientsI[i]*(neighHarmonicCoeff*elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                    density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
                            M_plus -= coefficientsI[i]*harmonicCoeff;
                        }


                    }else{

                        std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                        int volVarsIdx = fvGeometry_.getLocalIndex(index);

                        BoundaryTypes bcTypes;
                        IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                        problem.boundaryTypes(bcTypes, *isIt);
                        Scalar potentialVal = 0.0;

                        if(bcTypes.hasNeumann())
                        {
                            dependsOnNeumannBoundary = true;
                            PrimaryVariables values;
                            //problem.neumannAtPos(values,stencilCenterPointsI[i]);
                            //problem.neumannAtPos(values,isIt->geometry().center());
                            problem.solDependentNeumann(values,
                                                          element,
                                                          fvGeometry_,
                                                          *isIt,
                                                          /*scvIdx=*/0,
                                                          stencilFaceIdxI[i],
                                                          elemVolVars);
                            if(recalculateDirichlet)
                            {
                                potentialVal = calculateNeumannPressure(problem, globalIdxI, elemVolVars,
                                        interactionVolumeI, stencilFaceIdxI[i], values[phaseIdx], phaseIdx, isIt);

                                M_plus += coefficientsI[i];

                                if(complexBoundary){
                                   if(/*stencilFaceIdxI[i]*/ stencilFaceGlobalIdxI[i].second == faceIdx_) std::cout<<"ERROR in FluxCalc!!!"<<std::endl;
                                   else d_plus += coefficientsI[i]*potentialVal;
                                }else
                                {
                                   d_plus += coefficientsI[i]*potentialVal;
                                }

                            }
                           Scalar visko = elemVolVars[volVarsIdx].fluidState().viscosity(phaseIdx);
                           Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                           visko = 1.0;
                           faceBoundaryFluxPlus += coefficientsI[i]*harmonicCoeff*visko*values[phaseIdx];

                        }else if(bcTypes.hasDirichlet())
                        {

                        //PrimaryVariables values;
                        //problem.dirichletAtPos(values, stencilCenterPointsI[i]);

                            potentialVal = (/*values[phaseIdx]*/ elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                    density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );

                            M_plus += coefficientsI[i];

                            if(complexBoundary){
                               if(/*stencilFaceIdxI[i]*/ stencilFaceGlobalIdxI[i].second == faceIdx_) M_plusminus += coefficientsI[i];
                               else d_plus += coefficientsI[i]*potentialVal;
                            }else
                            {
                               d_plus += coefficientsI[i]*potentialVal;
                            }

                        }


                    }
                }

                if(complexBoundary)
                {
                std::pair<int,int> indexJ(globalIdxI, faceIdx_);
                const FaceStencil* faceStencilJ = interactionVolumeI->getFaceStencilVectorMinus(indexJ);
                const int numFAPJ = faceStencilJ->getNumElements();
                const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                const DimVector centerPointJ = faceStencilJ->getCenterPoint();
                //const bool isOnBoundaryJ = interactionVolumeI->isOnBoundary();

                for(int i=0 ; i < numFAPJ ; i++){

                    if(globalIdxVectorJ[i] > -1){

                       M_minus += coefficientsJ[i];

                       std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);

                       if(globalIdxVectorJ[i] == globalIdxI){
                           M_minusplus += coefficientsJ[i];
                       }else{
                           int volVarsIdx = fvGeometry_.getLocalIndex(index);
                           d_minus += coefficientsJ[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                   density*(absHeight-stencilCenterPointsJ[i][dim-1])*grav );
                       }


                    }
                    else
                    {

                       std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                       int volVarsIdx = fvGeometry_.getLocalIndex(index);

                       BoundaryTypes bcTypes;
                       IntersectionIterator isIt = interactionVolumeI->getElementIntersection(std::pair<int,int>(globalIdxI,stencilFaceIdxJ[i]));
                       problem.boundaryTypes(bcTypes, *isIt);
                       Scalar potentialVal = 0.0;

                       if(bcTypes.hasNeumann())
                       {
                           dependsOnNeumannBoundary = true;
                           PrimaryVariables values;
                           //problem.neumannAtPos(values,stencilCenterPointsJ[i]);
                           //problem.neumannAtPos(values,isIt->geometry().center());
                           problem.solDependentNeumann(values,
                                                         element,
                                                         fvGeometry_,
                                                         *isIt,
                                                         /*scvIdx=*/0,
                                                         stencilFaceIdxJ[i],
                                                         elemVolVars);

                           if(recalculateDirichlet)
                           {
                               potentialVal = calculateNeumannPressure(problem, globalIdxI, elemVolVars,
                                       interactionVolumeI, stencilFaceIdxJ[i], values[phaseIdx], phaseIdx, isIt);

                               M_minus += coefficientsJ[i];
                               d_minus += coefficientsJ[i]*potentialVal;

                           }

                           Scalar visko = elemVolVars[volVarsIdx].fluidState().viscosity(phaseIdx);
                           Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                           visko = 1.0;
                           faceBoundaryFluxMinus += coefficientsJ[i]*harmonicCoeff*visko*values[phaseIdx];
                           //std::cout<<"Val: "<<values<<std::endl;

                       }else if(bcTypes.hasDirichlet())
                       {
                           //PrimaryVariables values;
                           //problem.dirichletAtPos(values, stencilCenterPointsJ[i]);

                           potentialVal = (/*values[phaseIdx]*/ elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                                   density*(absHeight-stencilCenterPointsJ[i][dim-1])*grav );

                           M_minus += coefficientsJ[i];
                           d_minus += coefficientsJ[i]*potentialVal;

                       }


                    }

                }


                  if(useNLTPFA && (d_plus > 1.0e-30 && d_minus > 1.0e-30))
                  {
                      mu_plus = d_minus/(d_minus + d_plus);
                      mu_minus = d_plus/(d_minus + d_plus);
                  }
                  else if(useNLTPFA){
                    //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                  }

                  M_plus *= mu_plus;
                  M_plus += mu_minus*M_minusplus;

                  M_minus *= mu_minus;
                  M_minus += mu_plus*M_plusminus;

                  int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                  int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI, faceIdx_));
                  faceFlux = M_plus*(elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                          density*(absHeight-centerPointI[dim-1])*grav );
                  faceFlux -= M_minus*(elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                          density*(absHeight-centerPointJ[dim-1])*grav );
                  faceFlux -= mu_plus*d_plus;
                  faceFlux += mu_minus*d_minus;

                  faceBoundaryFluxPlus *= mu_plus;
                  faceBoundaryFluxMinus *= mu_minus;

                }else{
                    int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    //int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,faceIdx_));
                    faceFlux = M_plus*(elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                            density*(absHeight-centerPointI[dim-1])*grav );
                    faceFlux -= d_plus;
                }


                if(addNeumannToFlux && !recalculateDirichlet)
                {
                    faceFlux += faceBoundaryFluxPlus;
                    faceFlux -= faceBoundaryFluxMinus;
                }

                if(dependsOnNeumannBoundary && useTpfaAtBoundary){
                    DimWorldMatrix K = problem.spatialParams().intrinsicPermeability(element);

                    std::pair<int,int> index(globalIdxI,faceIdx_);

                    IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);

                    DimVector unitNormal = isIt->centerUnitOuterNormal();

                    DimVector centerPointI = element.geometry().center();
                    DimVector centerPointJ = isIt->geometry().center();

                    DimVector distVec = centerPointJ - centerPointI;
                    distVec /= distVec.two_norm2();

                    K.mv(distVec,kGradP_[phaseIdx]);

                    kGradPNormal_[phaseIdx] = kGradP_[phaseIdx] * unitNormal;

                    int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,faceIdx_));

                    Scalar pI = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                            density*(absHeight-centerPointI[dim-1])*grav );

                    Scalar pJ = (elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                            density*(absHeight-centerPointJ[dim-1])*grav );

                    faceFlux = face().area*kGradPNormal_[phaseIdx]*(pI-pJ);

                }

                faceStencilI->setFaceFlux(faceFlux /*+ faceBoundaryFluxPlus - faceBoundaryFluxMinus*/);
                }
                else
                {
                    DimWorldMatrix K = problem.spatialParams().intrinsicPermeability(element);

                    std::pair<int,int> index(globalIdxI,faceIdx_);

                    IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);

                    DimVector unitNormal = isIt->centerUnitOuterNormal();

                    DimVector centerPointI = element.geometry().center();
                    DimVector centerPointJ = isIt->geometry().center();

                    DimVector distVec = centerPointJ - centerPointI;
                    distVec /= distVec.two_norm2();

                    K.mv(distVec,kGradP_[phaseIdx]);

                    kGradPNormal_[phaseIdx] = kGradP_[phaseIdx] * unitNormal;

                    int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,faceIdx_));

                    Scalar pI = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                            density*(absHeight-centerPointI[dim-1])*grav );

                    Scalar pJ = (elemVolVars[volVarsIdxJ].fluidState().pressure(phaseIdx) +
                            density*(absHeight-centerPointJ[dim-1])*grav );

                    faceFlux = face().area*kGradPNormal_[phaseIdx]*(pI-pJ);
                }

            }

                DimVector velocity = face().unitNormal;
                velocity *= faceFlux;
                velocity /= face().area;

                DimVector faceBoundaryVelocity = face().unitNormal;
                faceBoundaryVelocity *= (faceBoundaryFluxPlus - faceBoundaryFluxMinus);
                faceBoundaryVelocity /= face().area;

                // determine the upwind direction
                if (faceFlux >= 0)
                {
                    upstreamIdx_[phaseIdx] = face().i;
                    downstreamIdx_[phaseIdx] = face().j;
                }
                else
                {
                    upstreamIdx_[phaseIdx] = face().j;
                    downstreamIdx_[phaseIdx] = face().i;
                }

                // obtain the upwind volume variables
                const VolumeVariables& upVolVars = elemVolVars[ upstreamIdx(phaseIdx) ];
                const VolumeVariables& downVolVars = elemVolVars[ downstreamIdx(phaseIdx) ];

                // the minus comes from the Darcy relation which states that
                // the flux is from high to low potentials.
                // set the velocity

                velocity_[phaseIdx] = velocity;
                velocity_[phaseIdx] *= ( mobilityUpwindWeight_*upVolVars.mobility(phaseIdx)
                        + (1.0 - mobilityUpwindWeight_)*downVolVars.mobility(phaseIdx)) ;

                //velocity_[phaseIdx] += faceBoundaryVelocity;

                // set the volume flux
                volumeFlux_[phaseIdx] = velocity_[phaseIdx] * face().normal;

        }// loop all phases

    }

    Scalar calculateNeumannPressure(const Problem &problem, int globalIdxI,
            const ElementVolumeVariables &elemVolVars, const InteractionVolume* interactionVolumeI,
            int faceIdxI, Scalar fluxVal, int phaseIdx, IntersectionIterator isIt )
    {

        GlobalPosition g(problem.gravityAtPos(face().ipGlobal));

        Scalar grav = 0;
        Scalar density = 0.0;
        if(GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
        {
            grav = g[dim-1];
            // calculate the phase density at the integration point. we
            // only do this if the wetting phase is present in both cells
            Scalar SI = elemVolVars[face().i].fluidState().saturation(phaseIdx);
            Scalar SJ = elemVolVars[face().j].fluidState().saturation(phaseIdx);
            Scalar rhoI = elemVolVars[face().i].fluidState().density(phaseIdx);
            Scalar rhoJ = elemVolVars[face().j].fluidState().density(phaseIdx);
            Scalar fI = std::max(0.0, std::min(SI/1e-5, 0.5));
            Scalar fJ = std::max(0.0, std::min(SJ/1e-5, 0.5));
            if(Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(fI + fJ, 0.0, 1.0e-30))
            {
                // doesn't matter because no wetting phase is present in
                // both cells!
                fI = fJ = 0.5;
            }
                density = (fI*rhoI + fJ*rhoJ)/(fI + fJ);
        }
        Scalar absHeight = problem.bBoxMax()[dim-1];

        Scalar pressVal = 0.0;

        std::pair<int,int> indexI(globalIdxI, faceIdxI);
        const FaceStencil* faceStencilI = interactionVolumeI->getFaceStencilVectorPlus(indexI);
        const int numFAPI = faceStencilI->getNumElements();
        const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
        const DimVector coefficientsI = faceStencilI->getCoefficients();
        const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
        const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
        const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
        const DimVector centerPointI = faceStencilI->getCenterPoint();

        Scalar d_plus = 0.0;

        Scalar M_plus = 0.0;

        Scalar coeff_plus = 0.0;

        for(int i=0 ; i < numFAPI ; i++){

            if(globalIdxVectorI[i] > -1){

                //std::cout << globalIdxVectorI[i] << std::endl;

                M_plus += coefficientsI[i];

                std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                int localFaceType = interactionVolumeI->getFaceType(index);

                if(localFaceType == homogeneous){
                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                    d_plus += coefficientsI[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                            density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
                }
                else if(localFaceType == heterogeneous){
                    Scalar harmonicCoeff = interactionVolumeI->getHarmonicAveragingCoefficient(index);
                    Scalar neighHarmonicCoeff = interactionVolumeI->getNeighboringHarmonicAveragingCoefficient(index);

                    int volVarsIdx = fvGeometry_.getLocalIndex(index);
                    d_plus += coefficientsI[i]*(neighHarmonicCoeff*elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                            density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
                    M_plus -= coefficientsI[i]*harmonicCoeff;
                }

            }else{
                std::pair<int,int> index(globalIdxI,stencilFaceIdxI[i]);
                    int volVarsIdx = fvGeometry_.getLocalIndex(index);

                    M_plus += coefficientsI[i];
                    //stencilFaceIdxI[i] Problem mit complex Stencil, da stencilFaceIdxI mehrfach gleicher Index
                    if(stencilFaceIdxI[i] == faceIdxI) coeff_plus += coefficientsI[i];
                    else d_plus += coefficientsI[i]*(elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx) +
                           density*(absHeight-stencilCenterPointsI[i][dim-1])*grav );
            }
        }
        int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));

        Scalar pressElement = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
                density*(absHeight-centerPointI[dim-1])*grav );
        M_plus *= pressElement;

        Scalar visko = elemVolVars[volVarsIdxI].fluidState().viscosity(phaseIdx);
        Scalar mobility = elemVolVars[volVarsIdxI].mobility(phaseIdx);

//        if(fluxVal == 0)
//        {
//          pressVal = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
//                    density*(absHeight - isIt->geometry().center()[dim-1])*grav);
//        }
//        else
//        {
            pressVal = -(isIt->geometry().volume()*visko*fluxVal - M_plus + d_plus)/coeff_plus;
//        }

//          std::cout << "PotElement:" << pressElement<< std::endl;
//          std::cout << "PotBoundary:" << pressVal<< std::endl;

        //use Two-Point Flux reconstruction

//        const SpatialParams &spatialParams = problem.spatialParams();
//        DimWorldMatrix K;
//
//        //const Element& element = *isIt->inside();
//
//        K = problem.spatialParams().intrinsicPermeability(*isIt->inside());
//
//        DimVector Knormal(0);
//        DimVector unitNormal = isIt->centerUnitOuterNormal();
//
//        K.mv(unitNormal, Knormal);
//        Scalar k = Knormal*unitNormal;
//
//
//        int volVarsIdxI = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
//        int volVarsIdxJ = fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,faceIdxI));
//        //faceFlux = M_plus*elemVolVars[volVarsIdx].fluidState().pressure(phaseIdx);
//        //faceFlux -= M_minus*elemVolVars[volVarsIdxBound].fluidState().pressure(phaseIdx);
//
//        DimVector centerPointI = isIt->inside()->geometry().center();
//
//        DimVector distVec = isIt->geometry().center() - centerPointI;
//
//        Scalar dist = distVec.two_norm();
//        Scalar transmisibility = k/dist;
//
//        Scalar pressElement = (elemVolVars[volVarsIdxI].fluidState().pressure(phaseIdx) +
//                density*(absHeight-centerPointI[dim-1])*grav );
//
//        pressVal = -(fluxVal/transmisibility -  pressElement);

//        std::cout<<"fluxVal: "<<fluxVal<<std::endl;
//        std::cout<<"PressureElement: "<<pressElement<<std::endl;
//        std::cout<<"PressureVal: "<<pressVal<<std::endl;

        return pressVal;


    }

protected:
    const FVElementGeometry &fvGeometry_;       //!< Information about the geometry of discretization
    const unsigned int faceIdx_;                //!< The index of the sub control volume face
    const bool      onBoundary_;                //!< Specifying whether we are currently on the boundary of the simulation domain
    unsigned int    upstreamIdx_[numPhases] , downstreamIdx_[numPhases]; //!< local index of the upstream / downstream vertex
    Scalar          volumeFlux_[numPhases] ;    //!< Velocity multiplied with normal (magnitude=area)
    GlobalPosition  velocity_[numPhases] ;      //!< The velocity as determined by Darcy's law or by the Forchheimer relation
    Scalar          kGradPNormal_[numPhases] ;  //!< Permeability multiplied with gradient in potential, multiplied with normal (magnitude=area)
    GlobalPosition  kGradP_[numPhases] ; //!< Permeability multiplied with gradient in potential
    GlobalPosition  potentialGrad_[numPhases] ; //!< Gradient of potential, which drives flow
    Scalar          mobilityUpwindWeight_;      //!< Upwind weight for mobility. Set to one for full upstream weighting
    //const std::map<std::pair<int,int>,int> &map;

};

} // end namespace

#endif
