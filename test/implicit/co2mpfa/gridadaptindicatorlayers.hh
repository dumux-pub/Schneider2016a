// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_IMPLICIT_GRIDADAPTINDICATORLAYERS_HH
#define DUMUX_IMPLICIT_GRIDADAPTINDICATORLAYERS_HH


#include <dumux/implicit/2p2cmpfa/2p2cgridadaptindicator.hh>

/**
 * @file
 * @brief  Class defining a standard, saturation dependent indicator for grid adaptation
 */
namespace Dumux
{

template<class TypeTag>
class TwoPTwoCGridAdaptIndicatorLayers : public TwoPTwoCImplicitGridAdaptIndicator<TypeTag>
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef TwoPTwoCImplicitGridAdaptIndicator<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*! \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * This standard indicator is based on the saturation gradient.
     */
    void calculateIndicator()
    {
        ParentType::calculateIndicator();
        // loop over all leaf-elements
        for (const auto& element : Dune::elements(this->problem_.gridView()))
        {
            int globalIdxI = this->problem_.elementMapper().index(element);
            const GlobalPosition& globalPos = element.geometry().center();
            Scalar x = globalPos[0];
            Scalar y = globalPos[1];

            double pi = 4.0*atan(1.0);
            Scalar val1 = 50 + 10.0*sin(pi*x/200);
            Scalar val2 = 60 + 10.0*sin(pi*x/200);
            Scalar offset = 2.0;

            if( std::abs(y-val1) < offset  || std::abs(y-val2) < offset || std::abs(y-93) < offset)
                this->indicatorVector_[globalIdxI][0] = 1.0;
        }

//#if HAVE_MPI
//    // communicate updated values
//    typedef VectorExchange<ElementMapper, ScalarSolutionType> DataHandle;
//    DataHandle dataHandle(problem_.elementMapper(), indicatorVector_);
//    problem_.gridView().template communicate<DataHandle>(dataHandle,
//                                                         Dune::InteriorBorder_All_Interface,
//                                                         Dune::ForwardCommunication);
//
//    refineBound_ = problem_.gridView().comm().max(refineBound_);
//    coarsenBound_ = problem_.gridView().comm().max(coarsenBound_);
//
//#endif
    }


    TwoPTwoCGridAdaptIndicatorLayers(Problem& problem):
        ParentType(problem)
    {

    }

};
}

#endif
