// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FVNONLINEARVELOCITY1P_HH
#define DUMUX_FVNONLINEARVELOCITY1P_HH

#include <dumux/decoupled/1p/1pproperties.hh>

namespace Dumux
{

template<class TypeTag>
class FVNonlinearVelocity1P
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, CellData) CellData;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        pressEqIdx = Indices::pressureEqIdx // only one equation!
    };

    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> DimMatrix;

public:
    FVNonlinearVelocity1P(Problem& problem)
    : problem_(problem), gravity_(problem.gravity())
      {
        ElementIterator element = problem_.gridView().template begin<0> ();
        Scalar temperature = problem_.temperature(*element);
        Scalar referencePress = problem_.referencePressure(*element);

        density_ = Fluid::density(temperature, referencePress);
        viscosity_ = Fluid::viscosity(temperature, referencePress);
      }

    // Calculates the velocity at a cell-cell interface.
    void calculateVelocity(const Intersection&, CellData&);

    //Calculates the velocity at a boundary.
    void calculateVelocityOnBoundary(const Intersection&, CellData&);

    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {
        Dune::BlockVector<Dune::FieldVector<Scalar, dim> > &velocity = *(writer.template allocateManagedBuffer<Scalar, dim> (
                problem_.gridView().size(0)));

        // compute update vector
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            // cell index
            int globalIdx = problem_.variables().index(*eIt);

            CellData& cellData = problem_.variables().cellData(globalIdx);

            const typename Element::Geometry& geometry = eIt->geometry();
            // get corresponding reference element
            typedef Dune::ReferenceElements<Scalar, dim> ReferenceElements;
            const Dune::ReferenceElement< Scalar , dim > & refElement =
                    ReferenceElements::general( geometry.type() );
            const int numberOfFaces=refElement.size(1);

            std::vector<Scalar> flux(numberOfFaces,0);

            // run through all intersections with neighbors and boundary
            IntersectionIterator
            isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator
                    isIt = problem_.gridView().ibegin(*eIt); isIt
                    !=isEndIt; ++isIt)
            {
                int isIndex = isIt->indexInInside();

                flux[isIndex] = isIt->geometry().volume()
                        * (isIt->centerUnitOuterNormal() * cellData.fluxData().velocity(isIndex));
            }

            // calculate velocity on reference element as the Raviart-Thomas-0
            // interpolant of the fluxes
            Dune::FieldVector<Scalar, dim> refVelocity;
            // simplices
            if (refElement.type().isSimplex()) {
                for (int dimIdx = 0; dimIdx < dim; dimIdx++)
                {
                    refVelocity[dimIdx] = -flux[dim - 1 - dimIdx];
                    for (int faceIdx = 0; faceIdx < dim + 1; faceIdx++)
                    {
                        refVelocity[dimIdx] += flux[faceIdx]/(dim + 1);
                    }
                }
            }
            // cubes
            else if (refElement.type().isCube()){
                for (int i = 0; i < dim; i++)
                    refVelocity[i] = 0.5 * (flux[2*i + 1] - flux[2*i]);
            }
            // 3D prism and pyramids
            else {
                DUNE_THROW(Dune::NotImplemented, "velocity output for prism/pyramid not implemented");
            }

            const Dune::FieldVector<Scalar, dim>& localPos
              = refElement.position(0, 0);

            // get the transposed Jacobian of the element mapping
            const typename Element::Geometry::JacobianTransposed& jacobianT =
                geometry.jacobianTransposed(localPos);

            // calculate the element velocity by the Piola transformation
            Dune::FieldVector<Scalar, dim> elementVelocity(0);
            jacobianT.umtv(refVelocity, elementVelocity);
            elementVelocity /= geometry.integrationElement(localPos);

            velocity[globalIdx] = elementVelocity;
        }

        writer.attachCellData(velocity, "velocity", dim);

        return;
    }
private:
    Problem &problem_;
    const GlobalPosition& gravity_; //!< vector including the gravity constant
    Scalar density_;
    Scalar viscosity_;
};

template<class TypeTag>
void FVNonlinearVelocity1P<TypeTag>::calculateVelocity(const Intersection& intersection, CellData& cellData)
{
    problem_.model().calculateVelocity(intersection, cellData);

    return;
}

template<class TypeTag>
void FVNonlinearVelocity1P<TypeTag>::calculateVelocityOnBoundary(const Intersection& intersection, CellData& cellData)
{
    problem_.model().calculateVelocity(intersection, cellData);
    return;
}
}
#endif
