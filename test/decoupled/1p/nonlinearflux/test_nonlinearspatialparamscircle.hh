// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef TEST_NONLINEAR_SPATIALPARAMS_CIRCLE_HH
#define TEST_NONLINEAR_SPATIALPARAMS_CIRCLE_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux
{

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for 1-p diffusion models.
 */
template<class TypeTag>
class TestNonlinearSpatialParamsCircle: public FVSpatialParamsOneP<TypeTag>
{
    typedef FVSpatialParamsOneP<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;


    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        return permeability_[indexSet_.index(element)];
        //return permeability_;
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    void initialize(const double delta)
    {
        delta_ = delta;

        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            setPermeability_(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }


    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *permXX = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permXY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permYY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *volume = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = indexSet_.index(*eIt);
            (*permXX)[eIdxGlobal][0] = permeability_[eIdxGlobal][0][0];
            (*permXY)[eIdxGlobal][0] = permeability_[eIdxGlobal][0][1];
            (*permYY)[eIdxGlobal][0] = permeability_[eIdxGlobal][1][1];
        }

        writer.attachCellData(*permXX, "permeability-X");
        writer.attachCellData(*permYY, "permeability-Y");
        writer.attachCellData(*permXY, "permeability-Offdiagonal");

        return;
    }

    TestNonlinearSpatialParamsCircle(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet()), permeability_(0)
    {

//        permeability_[0][0] = permeability_[1][1] = 1.0;
//        permeability_[0][1] = permeability_[1][0] = 0.5;
    }

private:
    void setPermeability_(FieldMatrix& perm, const GlobalPosition& globalPos) const
    {

        double rt = globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1];

        perm[0][0] = (delta_*globalPos[0]*globalPos[0] + globalPos[1]*globalPos[1])/rt;
        perm[0][1] = -(1.0 - delta_)*globalPos[0]*globalPos[1]/rt;
        perm[1][0] = perm[0][1];
        perm[1][1] = (globalPos[0]*globalPos[0] + delta_*globalPos[1]*globalPos[1])/rt;

//
//        permeability_[0][0] = permeability_[1][1] = 1.0;
//        permeability_[0][1] = permeability_[1][0] = 0.5;

//        Scalar x = globalPos[0];
//        Scalar y = globalPos[1];
//
//        if(x + y <= 1.02 && x + y >= 0.98 ){
//            double pi = 4.0*atan(1.0);
//
//            double theta = -pi/4.0;
//
//            double cost = cos(theta);
//            double sint = sin(theta);
//
//            double k1_ = 100.0;
//            double k2_ = 0.01;
//
//            perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
//            perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
//            perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);
//
//        }else{
//            perm[0][0] = 1.0;
//            perm[1][1] = 1.0;
//        }

//      if(globalPos[0]<=0.52 && globalPos[0]>=0.48){
//            perm[0][0] = 0.01;
//            perm[1][1] = 100.0;
//
//            if(globalPos[1]<=0.52 && globalPos[1]>=0.48){
//                perm[0][0] = 100.0;
//                perm[1][1] = 100.0;
//            }
//
//            //perm[0][1] = perm[1][0] = 10.0;
//      }else if(globalPos[1]<=0.52 && globalPos[1]>=0.48){
//            perm[0][0] = 100.0;
//            perm[1][1] = 0.01;
//        }else{
//            perm[0][0] = 1.0;
//            perm[1][1] = 1.0;
////            perm[0][1] = 0.5;
////            perm[1][0] = 0.5;
//        }


//      perm[0][0] = perm[1][1] = 1.0;
//      perm[0][1] = perm[1][0] = 0.5;

    }

    const GridView gridView_;
    const IndexSet& indexSet_;
    std::vector<FieldMatrix> permeability_;
    //FieldMatrix permeability_;
    double delta_;
};

} // end namespace
#endif
