// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*****************************************************************************/
/*!
 * \file
 *
 * \brief test problem for the decoupled one-phase model.
 */
#ifndef DUMUX_TEST_NONLINEAR_PROBLEM_HETEROGENEOUSROTATION_HH
#define DUMUX_TEST_NONLINAER_PROBLEM_HETEROGENEOUSROTATION_HH

#if HAVE_ALUGRID
#include <dune/grid/alugrid/2d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#include <dune/grid/uggrid.hh>

#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/unit.hh>

#include <dumux/porousmediumflow/1p/sequential/diffusion/problem.hh>
#include <dumux/decoupled/1p/diffusion/fvnonlinear/fvnonlinearpressureproperties1p.hh>
#include <dumux/porousmediumflow/sequential/cellcentered/velocity.hh>

#include "test_nonlinearspatialparamsheterogeneousrotation.hh"

#include <dumux/io/vtkmultiwriter.hh>

namespace Dumux
{

template<class TypeTag>
class TestProblemNonlinearHeterogeneousRotation;

//////////
// Specify the properties
//////////
namespace Properties
{
NEW_TYPE_TAG(TestProblemNonlinearHeterogeneousRotation, INHERITS_FROM(FVNonlinearPressureOneP));

// Set the grid type
SET_PROP(TestProblemNonlinearHeterogeneousRotation, Grid)
{
    typedef Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming> type;
};

// Set the wetting phase
SET_PROP(TestProblemNonlinearHeterogeneousRotation, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the spatial parameters
SET_TYPE_PROP(TestProblemNonlinearHeterogeneousRotation, SpatialParams, Dumux::TestNonlinearSpatialParamsHeterogeneousRotation<TypeTag>);

// Enable gravity
SET_BOOL_PROP(TestProblemNonlinearHeterogeneousRotation, ProblemEnableGravity, false);

//Set the problem
SET_TYPE_PROP(TestProblemNonlinearHeterogeneousRotation, Problem, Dumux::TestProblemNonlinearHeterogeneousRotation<TypeTag>);


SET_INT_PROP(TestProblemNonlinearHeterogeneousRotation, LinearSolverVerbosity, 1);
}

/*!
 * \ingroup IMPETtests
 *
 * \brief test problem for the decoupled one-phase model.
 */
template<class TypeTag>
class TestProblemNonlinearHeterogeneousRotation: public DiffusionProblem1P<TypeTag >
{
    typedef DiffusionProblem1P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;


public:
    TestProblemNonlinearHeterogeneousRotation(TimeManager &timeManager, const GridView &gridView) :
        ParentType(gridView), velocity_(*this)
    {
        delta_ = 1e-3 ;
        delta_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Delta);


        this->spatialParams().initialize(delta_);
    }

    /*!
    * \name Problem parameters
    */
    // \{

    /*!
    * \brief The problem name.
    *
    * This is used as a prefix for files generated by the simulation.
    */
    const char *name() const
    {
        if (ParameterTree::tree().hasKey("Problem.OutputFileName"))
        {
            std::string fileName(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, OutputFileName));
            return fileName.c_str();
        }
        else
        {
            return "test_1pheterogeneousrotation";;
        }
    }

    bool shouldWriteRestartFile() const
    { return false; }

    bool shouldWriteOutput() const
    { return false; }

    /*
    void addOutputVtkFields()
    {
        velocity_.calculateVelocity();
        velocity_.addOutputVtkFields(this->resultWriter());
    }
    */

    void addOutputVtkFields()
    {
        ScalarSolution *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *globalIdx = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *volume = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            (*exactPressure)[this->elementMapper().map(*eIt)][0] = exact(eIt->geometry().center());
            (*globalIdx)[this->elementMapper().map(*eIt)][0] = this->elementMapper().map(*eIt);
            (*volume)[this->elementMapper().map(*eIt)][0] = eIt->geometry().volume();
        }

        this->resultWriter().attachCellData(*exactPressure, "exactPressure");
        this->resultWriter().attachCellData(*globalIdx, "globalIdx");
        this->resultWriter().attachCellData(*volume, "volume");

        return;
    }

    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {
        ScalarSolution *exactPressure = writer.allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *globalIdx = writer.allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *volume = writer.allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            (*exactPressure)[this->elementMapper().map(*eIt)][0] = exact(eIt->geometry().center());
            (*globalIdx)[this->elementMapper().map(*eIt)][0] = this->elementMapper().map(*eIt);
            (*volume)[this->elementMapper().map(*eIt)][0] = eIt->geometry().volume();
        }

        writer.attachCellData(*exactPressure, "exactPressure");
        writer.attachCellData(*volume, "volume");
        writer.attachCellData(*globalIdx, "globalIdx");

        velocity_.calculateVelocity();
        velocity_.addOutputVtkFields(writer);

        return;
    }

    /*!
    * \brief Returns the temperature within the domain.
    *
    * This problem assumes a temperature of 10 degrees Celsius.
    */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 273.15 + 10; // -> 10°C
    }

    // \}

    //! Returns the reference pressure for evaluation of constitutive relations
    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1e5; // -> 10°C
    }


    void sourceAtPos(PrimaryVariables &values,const GlobalPosition& globalPos) const
    {

            Scalar x = globalPos[0];
            Scalar y = globalPos[1];

            values = 0;


            double eps = 1.0e-7;

//            //double shift = 0.2;
//
//            if((x-0.2)>= 8.5/18.0 && (x-0.2)<= 9.5/18.0 && (y-0.2)>= 8.5/18.0 && (y-0.2)<= 9.5/18.0){
//              values = 810000.0/4.0;
//            }else if((x+0.2)>= 8.5/18.0 && (x+0.2)<= 9.5/18.0 && (y+0.2)>= 8.5/18.0 && (y+0.2)<= 9.5/18.0){
//              values = 810000.0/4.0;
//            }


            if(x>= 7.0/18.0 +eps && x<= 11.0/18.0 -eps && y>= 7.0/18.0 +eps && y<= 11.0/18.0 - eps){
                values = 81000000.0/4.0;
            }



    }

    /*!
    * \brief Returns the type of boundary condition.
    *
    * BC can be dirichlet (pressure) or neumann (flux).
    */
    void boundaryTypes(BoundaryTypes &bcType,
            const Intersection& intersection) const
    {
        bcType.setAllDirichlet();
    }

    //! return dirichlet condition  (pressure, [Pa])
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values = 0.0;

//        if (fabs(globalPos[1]) < 1e-6) {
//            if (globalPos[0] < 0.2)
//                values = 1.0;
//            else if (globalPos[0] < 0.3)
//                values = -5.0*globalPos[0] + 2.0;
//            else
//                values = 0.5;
//        }
//        else if (fabs(1.0 - globalPos[1]) < 1e-6) {
//            if (globalPos[0] < 0.7)
//                values = 0.5;
//            else if (globalPos[0] < 0.8)
//                values = -5.0*globalPos[0] + 4.0;
//            else
//                values = 0.0;
//        }
//        else if (fabs(globalPos[0]) < 1e-6) {
//            if (globalPos[1] < 0.2)
//                values = 1.0;
//            else if (globalPos[1] < 0.3)
//                values = -5.0*globalPos[1] + 2.0;
//            else
//                values = 0.5;
//        }
//        else {
//            if (globalPos[1] < 0.7)
//                values = 0.5;
//            else if (globalPos[1] < 0.8)
//                values = -5.0*globalPos[1] + 4.0;
//            else
//                values = 0.0;
//        }

    }


    //! return neumann condition  (flux, [kg/(m^2 s)])
    void neumann(PrimaryVariables &values, const Intersection& intersection) const
    {
    values = 0;
    }

private:
    Scalar exact (const GlobalPosition& globalPos) const
    {
        double pi = 4.0*atan(1.0);

        return 0.0;
    }

    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
        {
        Dune::FieldVector<Scalar,dim> grad(0);

        return grad;
        }

    double delta_;
    Dumux::FVVelocity<TypeTag, typename GET_PROP_TYPE(TypeTag, Velocity) > velocity_;
};
} //end namespace

#endif

