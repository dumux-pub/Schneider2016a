// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Contains the data which is required to calculate
 *        all fluxes of components over a face of a finite volume for
 *        the two-phase two-component model fully implicit model.
 */
#ifndef DUMUX_2P2C_NLTPFA_FLUX_VARIABLES_HH
#define DUMUX_2P2C_NLTPFA_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/spline.hh>

#include "2p2cproperties.hh"

namespace Dumux
{

/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitFluxVariables
 * \brief Contains the data which is required to calculate
 *        all fluxes of components over a face of a finite volume for
 *        the two-phase two-component model fully implicit model.
 *
 * This means pressure and concentration gradients, phase densities at
 * the integration point, etc.
 */
template <class TypeTag>
class TwoPTwoCNlTpfaFluxVariables : public GET_PROP_TYPE(TypeTag, BaseFluxVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseFluxVariables) BaseFluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, EffectiveDiffusivityModel) EffectiveDiffusivityModel;
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases) };

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        wCompIdx = Indices::wCompIdx,
        nCompIdx = Indices::nCompIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld} ;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dumux::NormalDecompositionInteractionVolume<TypeTag> InteractionVolume;

    typedef Dumux::CoNormalDecompositionFaceStencil<TypeTag> FaceStencil;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

 public:
    /*!
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the fully implicit scheme
     * \param fIdx The local index of the sub-control-volume face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary Evaluate flux at inner sub-control-volume face or on a boundary face
     */
    TwoPTwoCNlTpfaFluxVariables(const Problem &problem,
                          const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int fIdx,
                          const ElementVolumeVariables &elemVolVars,
                          const bool onBoundary = false)
        : BaseFluxVariables(problem, element, fvGeometry, fIdx, elemVolVars, onBoundary)
    {
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
            density_[phaseIdx] = Scalar(0);
            molarDensity_[phaseIdx] = Scalar(0);
            moleFractionGrad_[phaseIdx] = Scalar(0);
        }

        calculateValues_(problem, element, elemVolVars);
    }

 protected:
    void calculateValues_(const Problem &problem,
                          const Element &element,
                          const ElementVolumeVariables &elemVolVars)
    {
        // calculate densities at the integration points of the face
        GlobalPosition tmp(0.0);
        for (unsigned int idx = 0;
             idx < this->face().numFap;
             idx++) // loop over adjacent vertices
        {
            // index for the element volume variables
            int volVarsIdx = this->face().fapIndices[idx];

            for (int phaseIdx = 0; phaseIdx < numPhases; phaseIdx++)
            {
                density_[phaseIdx] += elemVolVars[volVarsIdx].density(phaseIdx)*
                    this->face().shapeValue[idx];
                molarDensity_[phaseIdx] += elemVolVars[volVarsIdx].molarDensity(phaseIdx)*
                    this->face().shapeValue[idx];
            }
        }

        calculateGradients_(problem, element, elemVolVars);
        calculatePorousDiffCoeff_(problem, element, elemVolVars);
    }

    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
           bool useNLTPFA = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        UseNlTpfa);

            bool recalculateDirichlet = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    RecalculateDirichletFromNeumann);

            bool addNeumannToFlux = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    AddNeumannFluxToFaceFlux);

//            bool useTpfaAtBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
//                    bool,
//                    Mpfa,
//                    UseTpfaAtBoundary);

            bool useTpfaAtBoundaryCells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    UseTpfaAtBoundaryCells);

            //bool dependsOnNeumannBoundary = false;


            if(recalculateDirichlet && addNeumannToFlux)
            {
                std::cout << "Warning: Cannot use both RecalculateDirichletFromNeumann and AddNeumannFluxToFaceFlux" << std::endl;
            }

            // get the global index of the cell
            //int globalIdxI = problem.variables().index(element);
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalIdxI = problem.elementMapper().index(element);
#else
            int globalIdxI = problem.elementMapper().map(element);
#endif

            const InteractionVolume* interactionVolumeI = problem.model().getInteractionVolume(globalIdxI);

            Scalar faceFlux_wPhase = 0.0;
            Scalar faceBoundaryFluxPlus_wPhase = 0.0;
            Scalar faceBoundaryFluxMinus_wPhase = 0.0;

            Scalar faceFlux_nPhase = 0.0;

            GlobalPosition tmp(0.0);
            moleFractionGrad_[wPhaseIdx] = tmp;
            moleFractionGrad_[nPhaseIdx] = tmp;
            /************* handle interior face *****************/

            if (!this->onBoundary_)
            {
                const Element& elementJ = *this->fvGeometry_.neighbors[this->face().j];
                //int faceIdxJ = this->face().fNeighIdx;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int globalIdxJ = problem.elementMapper().index(elementJ);
#else
                int globalIdxJ = problem.elementMapper().map(elementJ);
#endif
                const InteractionVolume* interactionVolumeJ = problem.model().getInteractionVolume(globalIdxJ);

                std::pair<int,int> indexI(globalIdxJ, -1);

                const FaceStencil* faceStencilI = interactionVolumeI->getNormalFaceStencilVectorPlus(indexI);
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const DimVector coefficientsI = faceStencilI->getCoefficients();
                const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const DimVector centerPointI = faceStencilI->getCenterPoint();
                const bool isOnBoundaryI = interactionVolumeI->isOnBoundary();

                std::pair<int,int> indexJ(globalIdxI, -1);
                const FaceStencil* faceStencilJ = interactionVolumeJ->getNormalFaceStencilVectorPlus(indexJ);
                const int numFAPJ = faceStencilJ->getNumElements();
                const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                const DimVector centerPointJ = faceStencilJ->getCenterPoint();
                const bool isOnBoundaryJ = interactionVolumeJ->isOnBoundary();

                if(!(useTpfaAtBoundaryCells && (isOnBoundaryI || isOnBoundaryJ)))
                {
                Scalar d_plus_wPhase = 0.0;
                Scalar d_minus_wPhase = 0.0;

                Scalar d_plus_nPhase = 0.0;
                Scalar d_minus_nPhase = 0.0;

                Scalar M_plus = 0.0;
                Scalar M_minus = 0.0;
                Scalar M_plusminus = 0.0;
                Scalar M_minusplus = 0.0;

                Scalar mu_plus_wPhase = 0.5;
                Scalar mu_minus_wPhase = 0.5;

                Scalar mu_plus_nPhase = 0.5;
                Scalar mu_minus_nPhase = 0.5;


                for(int i=0 ; i < numFAPI ; i++){

                    if(globalIdxVectorI[i] > -1){

                        M_plus += coefficientsI[i];
                        std::pair<int,int> index(stencilFaceGlobalIdxI[i]);

                        if(globalIdxVectorI[i] == globalIdxJ){
                            M_plusminus += coefficientsI[i];
                        }else{
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                            d_plus_wPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                            d_plus_nPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                        }

                    }
                    else
                    {
                        std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                        int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                        BoundaryTypes bcTypes;
                        IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                        problem.boundaryTypes(bcTypes, *isIt);

                        if(bcTypes.hasNeumann())
                        {
                            //dependsOnNeumannBoundary = true;
                        }
                        else if(bcTypes.hasDirichlet())
                        {
                            M_plus += coefficientsI[i];
                            d_plus_wPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                            d_plus_nPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                        }

                    }
                }

                for(int i=0 ; i < numFAPJ ; i++){

                    if(globalIdxVectorJ[i] > -1){

                        M_minus += coefficientsJ[i];

                        std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);

                        if(globalIdxVectorJ[i] == globalIdxI){
                            M_minusplus += coefficientsJ[i];
                        }else{
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                            d_minus_wPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                            d_minus_nPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                        }


                    }
                    else
                    {
                        std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                        BoundaryTypes bcTypes;
                        IntersectionIterator isIt = interactionVolumeJ->getElementIntersection(index);
                        problem.boundaryTypes(bcTypes, *isIt);
                        Scalar potentialVal = 0.0;

                        if(bcTypes.hasNeumann())
                        {
                            //dependsOnNeumannBoundary = true;
                        }
                        else if(bcTypes.hasDirichlet())
                        {
                            M_minus += coefficientsJ[i];
                            d_minus_wPhase += coefficientsJ[i]*elemVolVars[0].moleFraction(wPhaseIdx, nCompIdx);
                            d_minus_nPhase += coefficientsJ[i]*elemVolVars[0].moleFraction(nPhaseIdx, wCompIdx);

                        }


                    }

                }

                    mu_plus_wPhase = 0.5;
                    mu_minus_wPhase = 0.5;

                    mu_plus_nPhase = 0.5;
                    mu_minus_nPhase = 0.5;

                    if(useNLTPFA && (d_plus_wPhase > 1.0e-30 && d_minus_wPhase > 1.0e-30))
                    {
                        mu_plus_wPhase = d_minus_wPhase/(d_minus_wPhase + d_plus_wPhase);
                        mu_minus_wPhase = d_plus_wPhase/(d_minus_wPhase + d_plus_wPhase);
                    }else if(useNLTPFA){
                        //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                    }

                    if(useNLTPFA && (d_plus_nPhase > 1.0e-30 && d_minus_nPhase > 1.0e-30))
                    {
                        mu_plus_nPhase = d_minus_nPhase/(d_minus_nPhase + d_plus_nPhase);
                        mu_minus_nPhase = d_plus_nPhase/(d_minus_nPhase + d_plus_nPhase);
                    }else if(useNLTPFA){
                        //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                    }

                    //M_plus *= mu_plus;
                    //M_plus += mu_minus*M_minusplus;


                    //M_minus *= mu_minus;
                    //M_minus += mu_plus*M_plusminus;

                    Scalar M_plus_wPhase = M_plus*mu_plus_wPhase + mu_minus_wPhase*M_minusplus;
                    Scalar M_minus_wPhase = M_minus*mu_minus_wPhase + mu_plus_wPhase*M_plusminus;

                    Scalar M_plus_nPhase = M_plus*mu_plus_nPhase + mu_minus_nPhase*M_minusplus;
                    Scalar M_minus_nPhase = M_minus*mu_minus_nPhase + mu_plus_nPhase*M_plusminus;

                    int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));

                    faceFlux_wPhase = M_plus_wPhase*elemVolVars[volVarsIdxI].moleFraction(wPhaseIdx, nCompIdx);
                    faceFlux_wPhase -= M_minus_wPhase*elemVolVars[volVarsIdxJ].moleFraction(wPhaseIdx, nCompIdx);
                    faceFlux_wPhase -= mu_plus_wPhase*d_plus_wPhase;
                    faceFlux_wPhase += mu_minus_wPhase*d_minus_wPhase;

                    faceFlux_nPhase = M_plus_nPhase*elemVolVars[volVarsIdxI].moleFraction(nPhaseIdx, wCompIdx);
                    faceFlux_nPhase -= M_minus_nPhase*elemVolVars[volVarsIdxJ].moleFraction(nPhaseIdx, wCompIdx);
                    faceFlux_nPhase -= mu_plus_nPhase*d_plus_nPhase;
                    faceFlux_nPhase += mu_minus_nPhase*d_minus_nPhase;
                }
                else
                {
                    GlobalPosition tmp(0.0);
                    for (unsigned int idx = 0;
                         idx < this->face().numFap;
                         idx++) // loop over adjacent vertices
                    {
                        // FE gradient at vertex idx
                        const GlobalPosition &feGrad = this->face().grad[idx];

                        // index for the element volume variables
                        int volVarsIdx = this->face().fapIndices[idx];

                        // the mole fraction gradient of the wetting phase
                        tmp = feGrad;
                        tmp *= elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                        //moleFractionGrad_[wPhaseIdx] += tmp;
                        faceFlux_wPhase = tmp * this->face().unitNormal;
                        faceFlux_wPhase *= -1;

                        // the mole fraction gradient of the non-wetting phase
                        tmp = feGrad;
                        tmp *= elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                        //moleFractionGrad_[nPhaseIdx] += tmp;
                        faceFlux_nPhase = tmp * this->face().unitNormal;
                        faceFlux_nPhase *= -1;
                    }
                }


            }
            /************* boundary face ************************/

            else
            {
                std::pair<int,int> indexI(globalIdxI, this->faceIdx_);
                const FaceStencil* faceStencilI = interactionVolumeI->getNormalFaceStencilVectorPlus(indexI);
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const DimVector coefficientsI = faceStencilI->getCoefficients();
                const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const DimVector centerPointI = faceStencilI->getCenterPoint();

                Scalar d_plus_wPhase = 0.0;
                Scalar d_minus_wPhase = 0.0;

                Scalar d_plus_nPhase = 0.0;
                Scalar d_minus_nPhase = 0.0;

                Scalar M_plus = 0.0;
                Scalar M_minus = 0.0;
                Scalar M_plusminus = 0.0;
                Scalar M_minusplus = 0.0;

                Scalar mu_plus_wPhase = 0.5;
                Scalar mu_minus_wPhase = 0.5;

                Scalar mu_plus_nPhase = 0.5;
                Scalar mu_minus_nPhase = 0.5;

                bool complexBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        ComplexBoundary);

                bool useTpfaAtBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        UseTpfaAtBoundary);

                if(!useTpfaAtBoundary)
                {

                    for(int i=0 ; i < numFAPI ; i++){

                        if(globalIdxVectorI[i] > -1){

                            M_plus += coefficientsI[i];

                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);

                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                            d_plus_wPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                            d_plus_nPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);

                        }else{

                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);
                            Scalar potentialVal = 0.0;

                            if(bcTypes.hasNeumann())
                            {
                                //dependsOnNeumannBoundary = true;

                            }else if(bcTypes.hasDirichlet())
                            {

                                M_plus += coefficientsI[i];

                                if(complexBoundary){
                                   if(stencilFaceGlobalIdxI[i].second == this->faceIdx_) M_plusminus += coefficientsI[i];
                                   else{
                                       d_plus_wPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                                       d_plus_nPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                                   }
                                }else
                                {
                                    d_plus_wPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                                    d_plus_nPhase += coefficientsI[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                                }

                            }


                        }
                    }

                    if(complexBoundary)
                    {
                    std::pair<int,int> indexJ(globalIdxI, this->faceIdx_);
                    const FaceStencil* faceStencilJ = interactionVolumeI->getNormalFaceStencilVectorMinus(indexJ);
                    const int numFAPJ = faceStencilJ->getNumElements();
                    const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                    const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                    const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                    const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                    const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                    const DimVector centerPointJ = faceStencilJ->getCenterPoint();


                    for(int i=0 ; i < numFAPJ ; i++){

                        if(globalIdxVectorJ[i] > -1){

                           M_minus += coefficientsJ[i];

                           std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);

                           if(globalIdxVectorJ[i] == globalIdxI){
                               M_minusplus += coefficientsJ[i];
                           }else{
                               int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                               d_minus_wPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                               d_minus_nPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                           }


                        }
                        else
                        {

                           std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                           int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                           BoundaryTypes bcTypes;
                           IntersectionIterator isIt = interactionVolumeI->getElementIntersection(std::pair<int,int>(globalIdxI,stencilFaceIdxJ[i]));
                           problem.boundaryTypes(bcTypes, *isIt);
                           Scalar potentialVal = 0.0;

                           if(bcTypes.hasNeumann())
                           {
                               //dependsOnNeumannBoundary = true;

                           }else if(bcTypes.hasDirichlet())
                           {

                               M_minus += coefficientsJ[i];
                               d_minus_wPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                               d_minus_nPhase += coefficientsJ[i]*elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);

                           }


                        }

                    }

                    mu_plus_wPhase = 0.5;
                    mu_minus_wPhase = 0.5;

                    mu_plus_nPhase = 0.5;
                    mu_minus_nPhase = 0.5;

                    if(useNLTPFA && (d_plus_wPhase > 1.0e-30 && d_minus_wPhase > 1.0e-30))
                    {
                        mu_plus_wPhase = d_minus_wPhase/(d_minus_wPhase + d_plus_wPhase);
                        mu_minus_wPhase = d_plus_wPhase/(d_minus_wPhase + d_plus_wPhase);
                    }else if(useNLTPFA){
                        //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                    }

                    if(useNLTPFA && (d_plus_nPhase > 1.0e-30 && d_minus_nPhase > 1.0e-30))
                    {
                        mu_plus_nPhase = d_minus_nPhase/(d_minus_nPhase + d_plus_nPhase);
                        mu_minus_nPhase = d_plus_nPhase/(d_minus_nPhase + d_plus_nPhase);
                    }else if(useNLTPFA){
                        //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                    }


                    Scalar M_plus_wPhase = M_plus*mu_plus_wPhase + mu_minus_wPhase*M_minusplus;
                    Scalar M_minus_wPhase = M_minus*mu_minus_wPhase + mu_plus_wPhase*M_plusminus;

                    Scalar M_plus_nPhase = M_plus*mu_plus_nPhase + mu_minus_nPhase*M_minusplus;
                    Scalar M_minus_nPhase = M_minus*mu_minus_nPhase + mu_plus_nPhase*M_plusminus;

                    int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI, this->faceIdx_));

                    faceFlux_wPhase = M_plus_wPhase*elemVolVars[volVarsIdxI].moleFraction(wPhaseIdx, nCompIdx);
                    faceFlux_wPhase -= M_minus_wPhase*elemVolVars[volVarsIdxJ].moleFraction(wPhaseIdx, nCompIdx);
                    faceFlux_wPhase -= mu_plus_wPhase*d_plus_wPhase;
                    faceFlux_wPhase += mu_minus_wPhase*d_minus_wPhase;

                    faceFlux_nPhase = M_plus_nPhase*elemVolVars[volVarsIdxI].moleFraction(nPhaseIdx, wCompIdx);
                    faceFlux_nPhase -= M_minus_nPhase*elemVolVars[volVarsIdxJ].moleFraction(nPhaseIdx, wCompIdx);
                    faceFlux_nPhase -= mu_plus_nPhase*d_plus_nPhase;
                    faceFlux_nPhase += mu_minus_nPhase*d_minus_nPhase;


                    }else{
                        int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                        faceFlux_wPhase = M_plus*elemVolVars[volVarsIdxI].moleFraction(wPhaseIdx, nCompIdx);
                        faceFlux_wPhase -= d_plus_wPhase;

                        faceFlux_nPhase = M_plus*elemVolVars[volVarsIdxI].moleFraction(nPhaseIdx, wCompIdx);
                        faceFlux_nPhase -= d_plus_nPhase;
                    }


                }
                else{
                    GlobalPosition tmp(0.0);
                    for (unsigned int idx = 0;
                         idx < this->face().numFap;
                         idx++) // loop over adjacent vertices
                    {
                        // FE gradient at vertex idx
                        const GlobalPosition &feGrad = this->face().grad[idx];

                        // index for the element volume variables
                        int volVarsIdx = this->face().fapIndices[idx];

                        // the mole fraction gradient of the wetting phase
                        tmp = feGrad;
                        tmp *= elemVolVars[volVarsIdx].moleFraction(wPhaseIdx, nCompIdx);
                        //moleFractionGrad_[wPhaseIdx] += tmp;
                        faceFlux_wPhase = tmp * this->face().unitNormal;
                        faceFlux_wPhase *= -1;

                        // the mole fraction gradient of the non-wetting phase
                        tmp = feGrad;
                        tmp *= elemVolVars[volVarsIdx].moleFraction(nPhaseIdx, wCompIdx);
                        //moleFractionGrad_[nPhaseIdx] += tmp;
                        faceFlux_nPhase = tmp * this->face().unitNormal;
                        faceFlux_nPhase *= -1;
                    }
                }
            }

            moleFractionGrad_[wPhaseIdx] = this->face().unitNormal;
            moleFractionGrad_[wPhaseIdx] *= -faceFlux_wPhase;

            moleFractionGrad_[nPhaseIdx] = this->face().unitNormal;
            moleFractionGrad_[nPhaseIdx] *= -faceFlux_nPhase;

    }

    Scalar rhoFactor_(int phaseIdx, int scvIdx, const ElementVolumeVariables &vDat)
    {
        static const Scalar eps = 1e-2;
        const Scalar sat = vDat[scvIdx].density(phaseIdx);
        if (sat > eps)
            return 0.5;
        if (sat <= 0)
            return 0;

        static const Dumux::Spline<Scalar> sp(0, eps, // x0, x1
                                              0, 0.5, // y0, y1
                                              0, 0); // m0, m1
        return sp.eval(sat);
    }

    void calculatePorousDiffCoeff_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars)
    {
        const VolumeVariables &volVarsI = elemVolVars[this->face().i];
        const VolumeVariables &volVarsJ = elemVolVars[this->face().j];

        // the effective diffusion coefficients at vertex i and j
        Scalar diffCoeffI;
        Scalar diffCoeffJ;

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            // make sure to only calculate diffusion coefficients
            // for phases which exist in both finite volumes
            if (volVarsI.saturation(phaseIdx) <= 0 || volVarsJ.saturation(phaseIdx) <= 0)
            {
                porousDiffCoeff_[phaseIdx] = 0.0;
                continue;
            }

            diffCoeffI = EffectiveDiffusivityModel::effectiveDiffusivity(volVarsI.porosity(),
                                                                         volVarsI.saturation(phaseIdx),
                                                                         volVarsI.diffCoeff(phaseIdx));

            diffCoeffJ = EffectiveDiffusivityModel::effectiveDiffusivity(volVarsJ.porosity(),
                                                                         volVarsJ.saturation(phaseIdx),
                                                                         volVarsJ.diffCoeff(phaseIdx));

            // -> harmonic mean
            porousDiffCoeff_[phaseIdx] = harmonicMean(diffCoeffI, diffCoeffJ);
        }
    }

 public:
    /*!
     * \brief Returns the effective diffusion coefficient \f$\mathrm{[m^2/s]}\f$
     *        for each fluid phase in the porous medium.
     *
     * \param phaseIdx The phase index
     */
    Scalar porousDiffCoeff(int phaseIdx) const
    { return porousDiffCoeff_[phaseIdx]; };

    /*!
     * \brief Returns the density \f$\mathrm{[kg/m^3]}\f$ of a phase.
     *
     * \param phaseIdx The phase index
     */
    Scalar density(int phaseIdx) const
    { return density_[phaseIdx]; }

    /*!
     * \brief Returns the molar density \f$\mathrm{[mol/m^3]}\f$ of a phase.
     *
     * \param phaseIdx The phase index
     */
    Scalar molarDensity(int phaseIdx) const
    { return molarDensity_[phaseIdx]; }

    /*!
     * \brief Returns the mole fraction gradient \f$\mathrm{[1/m]}\f$
     *        of the dissolved component in a phase.
     *
     * \param phaseIdx The phase index
     */
    const GlobalPosition &moleFractionGrad(int phaseIdx) const
    { return moleFractionGrad_[phaseIdx]; };

 protected:
    // mole fraction gradients
    GlobalPosition moleFractionGrad_[numPhases];

    // density of each face at the integration point
    Scalar density_[numPhases], molarDensity_[numPhases];

    // the diffusion coefficient for the porous medium
    Scalar porousDiffCoeff_[numPhases];
};

} // end namespace

#endif
