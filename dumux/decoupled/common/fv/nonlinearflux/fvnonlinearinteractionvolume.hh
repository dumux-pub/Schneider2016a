#ifndef DUMUX_FVNONLINEARINTERACTIONVOLUME_HH
#define DUMUX_FVNONLINEARINTERACTIONVOLUME_HH

//#include "fvnonlinearpressureproperties1p.hh"
#include "fvnonlinearfacestencil.hh"
#include <dumux/decoupled/1p/1pproperties.hh>
#include <dumux/common/math.hh>

namespace Dumux{

template<class TypeTag>
class FVNonlinearInteractionVolume
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;


public:
    enum FaceTypes
    {
        homogeneous = 1,
        heterogeneous = 2,
        boundary = 0
    };


    //! Mark storage as completed
    void setStored()
    {
        stored_ = true;
    }

    //! Returns true if information has already been stored
    bool isStored() const
    {
        return stored_;
    }

    void setPermeability(DimMatrix permeability)
    {
        permeability_ = permeability;
    }

    DimMatrix getPermeability()
    {
        return permeability_;
    }


    void setNeighboringPermeabilities(VectorFieldMatrix neighborPermeabilities)
    {
        neighborPermeabilities_ = neighborPermeabilities;
    }

    VectorFieldMatrix getNeighboringPermeabilities()
    {
        return neighborPermeabilities_;
    }

    void setNeighboringElements(std::vector<ElementPointer> neighboringElements){
        neighboringElements_ = neighboringElements;
    }


    void setElementIntersections(std::vector<IntersectionIterator> intersections){
        elementIntersections_ = intersections;
    }

    std::vector<IntersectionIterator> getElementIntersections()
    {
        return elementIntersections_;
    }

    void insertElementIntersection(IntersectionIterator intersection)
    {
        elementIntersections_.push_back(intersection);
    }

    IntersectionIterator getElementIntersection(int faceIdx)
    {
        return elementIntersections_[inverseIndexItToLocal_[faceIdx]];
    }

    std::vector<ElementPointer> getNeighboringElements()
    {
        return neighboringElements_;
    }

    void setGlobalIdxNeighboringElements(std::vector<int> globalIdxNeighbors)
    {
        globalIdxNeighboringElements_ = globalIdxNeighbors;
    }

    std::vector<int> getGlobalIdxNeighboringElements()
    {
        return globalIdxNeighboringElements_;
    }

    void insertGlobalIdxNeighboringElements(int globalIdx)
    {
        globalIdxNeighboringElements_.push_back(globalIdx);
    }

    void setNormalVectors(VectorFieldVector normalVectors){
        normalVectors_ = normalVectors;
    }

    VectorFieldVector getNormalVectors()
    {
        return normalVectors_;
    }

    void setCoNormalVectors(VectorFieldVector coNormalVectors)
    {
        coNormals_ = coNormalVectors;
    }

    void setCellCenters(VectorFieldVector cellCenters){
        cellCenters_ = cellCenters;
    }

    VectorFieldVector getCellCenters()
    {
        return cellCenters_;
    }

    void setCellCenter(DimVector cellCenter){
        cellCenter_ = cellCenter;
    }

    VectorFieldVector getCellCenter()
    {
        return cellCenter_;
    }

    void setFaceCenters(VectorFieldVector faceCenters){
        faceCenters_ = faceCenters;
    }

    VectorFieldVector getFaceCenters()
    {
        return faceCenters_;
    }

    VectorFieldVector getCoNormalVectors()
    {
        return coNormals_;
    }

    void setConnectionVectors(VectorFieldVector connectionVectors){
        connectionVectors_ = connectionVectors;
    }

    void setIndexItLocal(std::vector<int> indexItToLocal)
    {
        indexItToLocal_= indexItToLocal;
    }

    std::vector<int> getIndexItLocal()
    {
        return indexItToLocal_;
    }

    void setIndexItToLocalMinus(std::vector<int> indexItToLocalMinus){
        indexItToLocalMinus_ = indexItToLocalMinus;
    }

    std::vector<int>  getIndexItToLocalMinus(){
        return indexItToLocalMinus_;
    }

    void setInverseIndexItLocal(std::vector<int> inverseIndexItToLocal)
    {
        inverseIndexItToLocal_ = inverseIndexItToLocal;
    }

    std::vector<int> getInverseIndexItLocal()
    {
        return inverseIndexItToLocal_;
    }


    void setNumFaces(int numNeigh){
        numFaces_ = numNeigh;
    }

    int getNumFaces(){
        return numFaces_;
    }

    void setGlobalIdx(int globalIdx){
        globalIdx_ = globalIdx;
     }

    int getGlobalIdx(){
        return globalIdx_ ;
     }

    void setVolumeType(int type){
        volumeType_ = type;
    }

    int getVolumeType(){
        return volumeType_;
    }

    void setFaceTypes(std::vector<int> faceTypes){
        faceTypes_ = faceTypes;
    }

    int getFaceType(int faceIdx){
        return faceTypes_[inverseIndexItToLocal_[faceIdx]];
    }

    bool isOnBounday(){
        if(volumeType_ == 0) return true;
        else return false;
    }

    void calculateInverseIndexItLocal()
    {
        inverseIndexItToLocal_.resize(numFaces_);
        for(int i=0; i<numFaces_; i++){
            inverseIndexItToLocal_[indexItToLocal_[i]] = i;
        }
    }

    FVNonlinearFaceStencil<TypeTag> getFaceStencilVectorPlus(int faceIdx)
    {
        return faceStencilVectorPlus_[inverseIndexItToLocal_[faceIdx]];
    }

    FVNonlinearFaceStencil<TypeTag> getFaceStencilVectorMinus(int faceIdx)
    {

        int index = indexItToLocalMinus_[inverseIndexItToLocal_[faceIdx]];
        if(index == -1) std::cout<<"ERROR: can't find heterogeneous index!!!!"<<std::endl;

        return faceStencilVectorMinus_[index];

    }

    void calculateFaceInterpolationPoints(IntersectionIterator isIt, IntersectionIterator isEndIt){

        int indexIt = 0;

        for (; isIt != isEndIt; ++isIt){

            Scalar distK = 1.0;
            Scalar distL = 1.0;
            Scalar lambdaK = 1.0;
            Scalar lambdaL = 1.00;
            DimVector faceCenter = isIt->geometry().center();
            DimVector unitOuterNormal = normalVectors_[indexIt];
            DimVector connectionVectorK = faceCenter;
            connectionVectorK -= cellCenter_;

            DimVector y(0);

            if(faceTypes_[indexIt] == boundary){
                y = faceCenter;
            }
            else
            {
                DimVector xL = isIt->outside()->geometry().center();
                DimVector connectionVectorL = xL;
                connectionVectorL -= faceCenter;

                distK = connectionVectorK * unitOuterNormal;
                distL = connectionVectorL * unitOuterNormal;

                if(faceTypes_[indexIt] == homogeneous)
                {
                    y.axpy(distL, cellCenter_);
                    y.axpy(distK, xL);
                    y /= (distL + distK);
                }
                else if(faceTypes_[indexIt] == heterogeneous)
                {

                    DimVector unitCoNormalK(0);
                    permeability_.mv(unitOuterNormal, unitCoNormalK);
                    DimVector unitCoNormalL(0);
                    neighborPermeabilities_[indexIt].mv(unitOuterNormal, unitCoNormalL);

                    lambdaK = unitCoNormalK * unitOuterNormal;
                    lambdaL = unitCoNormalL * unitOuterNormal;

                    y.axpy(distL * lambdaK, cellCenter_);
                    y.axpy(distK * lambdaL, xL);
                    y.axpy(distL * distK, unitCoNormalK - unitCoNormalL );
                    y /= (distL * lambdaK + distK * lambdaL);

                }

            }

            unitCoNormalProjections_.push_back(lambdaK);
            neighboringUnitCoNormalProjections_.push_back(lambdaL);
            cellCenterFaceDistances_.push_back(distK);
            neighboringCellCenterFaceDistances_.push_back(distL);
            faceHarmonicAveragingPoints_.push_back(y);

            indexIt++;
        }
    }

    Scalar getHarmonicAveragingCoefficient(int faceIdx){

        int index = inverseIndexItToLocal_[faceIdx];
        Scalar lambdaK = unitCoNormalProjections_[index];
        Scalar lambdaL = neighboringUnitCoNormalProjections_[index];
        Scalar distK = cellCenterFaceDistances_[index];
        Scalar distL = neighboringCellCenterFaceDistances_[index];

        return distL*lambdaK/(distL*lambdaK + distK*lambdaL);
    }

    Scalar getNeighboringHarmonicAveragingCoefficient(int faceIdx){

        int index = inverseIndexItToLocal_[faceIdx];
        Scalar lambdaK = unitCoNormalProjections_[index];
        Scalar lambdaL = neighboringUnitCoNormalProjections_[index];
        Scalar distK = cellCenterFaceDistances_[index];
        Scalar distL = neighboringCellCenterFaceDistances_[index];

        return distK*lambdaL/(distL*lambdaK + distK*lambdaL);
    }

    void calculateStencilsPlus(IntersectionIterator isIt, IntersectionIterator isEndIt){

        int indexIt = 0;

        for (; isIt != isEndIt; ++isIt){
            //if(isIt->neighbor()){
            //if(volumeType_ == 1){
                int faceIdx = isIt->indexInInside();

                //FVNonlinearFaceStencil<TypeTag> faceStencil(faceIdx, element_, neighboringElements_[indexIt]);
                FVNonlinearFaceStencil<TypeTag> faceStencilPlus(faceIdx, element_);
                DimVector faceConnectionVectorPlus = connectionVectors_[indexIt];
                DimVector faceCoNormalVectorPlus = coNormals_[indexIt];
                faceStencilPlus.setPermeability(permeability_);
                Scalar faceArea = isIt->geometry().volume();
                faceStencilPlus.setFaceArea(faceArea);
                faceStencilPlus.setCoNormal(faceCoNormalVectorPlus);
                faceStencilPlus.setCenterPoint(cellCenter_);
                faceStencilPlus.setNormal(normalVectors_[indexIt]);
                DimVector faceCenter = isIt->geometry().center();
                DimVector normal = normalVectors_[indexIt];
                //faceStencilPlus.setCenterPoint(faceCenter);

                int numInsertedVectors = 0;

                if(dim == 2){

                    for(int i = -1; i<numFaces_ && numInsertedVectors<2; i++){

                        DimVector faceConnectionVectorPlusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        ElementPointer  elementOne(element_);
                        int faceIdxOne = -1;
                        bool isOnBoundaryOne = false;

                        int localIdx;

                        if(i == -1) localIdx = indexIt;
                        else localIdx = i;


                        if(faceTypes_[localIdx] == homogeneous || faceTypes_[localIdx] == boundary)
                        {
                            faceConnectionVectorPlusOne = connectionVectors_[localIdx];
                        }
                        else if(faceTypes_[localIdx] == heterogeneous)
                        {
                            faceConnectionVectorPlusOne = faceHarmonicAveragingPoints_[localIdx];
                            faceConnectionVectorPlusOne -= cellCenter_;

                        }
                        centerPointOne = cellCenters_[localIdx];
                        globalIdxOne = globalIdxNeighboringElements_[localIdx];
                        elementOne = neighboringElements_[localIdx];
                        faceIdxOne = indexItToLocal_[localIdx];
                        if(faceTypes_[localIdx] == boundary) isOnBoundaryOne = true;
                        else isOnBoundaryOne = false;


                        for(int j = 0 ; j<numFaces_ && numInsertedVectors<2; j++){
                            if(i != indexIt && j!=i){

                                DimVector faceConnectionVectorPlusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                ElementPointer  elementTwo(element_);
                                int faceIdxTwo = -1;
                                bool isOnBoundaryTwo = false;

                                int localIdxJ;

                                //if(j == -1) localIdxJ = indexIt;
                                //else localIdxJ = j;
                                localIdxJ = j;

                                if(faceTypes_[localIdxJ] == homogeneous || faceTypes_[localIdxJ] == boundary)
                                {
                                    faceConnectionVectorPlusTwo = connectionVectors_[j];
                                }
                                else if(faceTypes_[localIdxJ] == heterogeneous)
                                {
                                    faceConnectionVectorPlusTwo = faceHarmonicAveragingPoints_[j];
                                    faceConnectionVectorPlusTwo -= cellCenter_;

                                }
                                centerPointTwo = cellCenters_[localIdxJ];
                                globalIdxTwo = globalIdxNeighboringElements_[localIdxJ];
                                elementTwo = neighboringElements_[localIdxJ];
                                faceIdxTwo = indexItToLocal_[localIdxJ];
                                if(faceTypes_[localIdxJ] == boundary) isOnBoundaryTwo = true;
                                else isOnBoundaryTwo = false;

                                if(faceStencilPlus.calculateCoefficients(faceCoNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo)){
                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilPlus.insertStencilElement(elementOne);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilPlus.insertSpanVector(faceConnectionVectorPlusOne);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                    faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                    faceStencilPlus.insertStencilElement(elementTwo);
                                    faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                    faceStencilPlus.insertSpanVector(faceConnectionVectorPlusTwo);
                                    faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                    faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                    numInsertedVectors++;
                                    numInsertedVectors++;

                                    break;
                                }
                            }
                        }
                    }
                }
                else{

                    for(int i = -1; i<numFaces_ && numInsertedVectors<3 ;i++)
                    {
                        DimVector faceConnectionVectorPlusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        ElementPointer  elementOne(element_);
                        int faceIdxOne = -1;
                        bool isOnBoundaryOne = false;

                        int localIdx;

                        if(i == -1) localIdx = indexIt;
                        else localIdx = i;


                        if(faceTypes_[localIdx] == homogeneous || faceTypes_[localIdx] == boundary)
                        {
                            faceConnectionVectorPlusOne = connectionVectors_[localIdx];
                        }
                        else if(faceTypes_[localIdx] == heterogeneous)
                        {
                            faceConnectionVectorPlusOne = faceHarmonicAveragingPoints_[localIdx];
                            faceConnectionVectorPlusOne -= cellCenter_;
                        }
                        centerPointOne = cellCenters_[localIdx];
                        globalIdxOne = globalIdxNeighboringElements_[localIdx];
                        elementOne = neighboringElements_[localIdx];
                        faceIdxOne = indexItToLocal_[localIdx];
                        if(faceTypes_[localIdx] == boundary) isOnBoundaryOne = true;
                        else isOnBoundaryOne = false;

                        for(int j = 0; j<numFaces_ && numInsertedVectors<3; j++ )
                        {
                            if(i != indexIt && j!=i)
                            {

                                DimVector faceConnectionVectorPlusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                ElementPointer  elementTwo(element_);
                                int faceIdxTwo = -1;
                                bool isOnBoundaryTwo = false;

                                if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
                                {
                                    faceConnectionVectorPlusTwo = connectionVectors_[j];
                                }
                                else if(faceTypes_[j] == heterogeneous)
                                {
                                    faceConnectionVectorPlusTwo = faceHarmonicAveragingPoints_[j];
                                    faceConnectionVectorPlusTwo -= cellCenter_;

                                }
                                centerPointTwo = cellCenters_[j];
                                globalIdxTwo = globalIdxNeighboringElements_[j];
                                elementTwo = neighboringElements_[j];
                                faceIdxTwo = indexItToLocal_[j];
                                if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
                                else isOnBoundaryTwo = false;

                                for(int k = 0; k < numFaces_; k++ )
                                {
                                    if(k!=j)
                                    {
                                        DimVector faceConnectionVectorPlusThree(0);
                                        DimVector centerPointThree;
                                        int globalIdxThree = -1;
                                        ElementPointer  elementThree(element_);
                                        int faceIdxThree = -1;
                                        bool isOnBoundaryThree = false;

                                        if(faceTypes_[k] == homogeneous || faceTypes_[k] == boundary)
                                        {
                                            faceConnectionVectorPlusThree = connectionVectors_[k];
                                        }
                                        else if(faceTypes_[k] == heterogeneous)
                                        {
                                            faceConnectionVectorPlusThree = faceHarmonicAveragingPoints_[k];
                                            faceConnectionVectorPlusThree -= cellCenter_;

                                        }
                                        centerPointThree = cellCenters_[k];
                                        globalIdxThree = globalIdxNeighboringElements_[k];
                                        elementThree = neighboringElements_[k];
                                        faceIdxThree = indexItToLocal_[k];
                                        if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
                                        else isOnBoundaryThree = false;

                                        if(faceStencilPlus.calculateCoefficients(faceCoNormalVectorPlus, faceConnectionVectorPlusOne ,faceConnectionVectorPlusTwo, faceConnectionVectorPlusThree)){
                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxOne);
                                            faceStencilPlus.insertStencilElement(elementOne);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxOne);
                                            faceStencilPlus.insertSpanVector(faceConnectionVectorPlusOne);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointOne);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryOne);

                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxTwo);
                                            faceStencilPlus.insertStencilElement(elementTwo);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxTwo);
                                            faceStencilPlus.insertSpanVector(faceConnectionVectorPlusTwo);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointTwo);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                            faceStencilPlus.insertGlobalIdxNeighbors(globalIdxThree);
                                            faceStencilPlus.insertStencilElement(elementThree);
                                            faceStencilPlus.insertStencilFaceIdx(faceIdxThree);
                                            faceStencilPlus.insertSpanVector(faceConnectionVectorPlusThree);
                                            faceStencilPlus.insertStencilCenterPoint(centerPointThree);
                                            faceStencilPlus.insertIsPointOnBoundary(isOnBoundaryThree);

                                            numInsertedVectors++;
                                            numInsertedVectors++;
                                            numInsertedVectors++;

                                            break;
                                        }

                                    }
                                }

                            }
                        }
                    }

                }


                if(numInsertedVectors != dim) DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");
                //faceStencilPlus.calculateInversStencilFaceIdx();
                faceStencilPlus.calculateCoefficients();
                faceStencilPlus.setStored();

                faceStencilVectorPlus_.push_back(faceStencilPlus);

                ++indexIt;
            //}
        }

    }

    void calculateStencilsMinus(IntersectionIterator isIt, IntersectionIterator isEndIt){

        int indexIt = 0;

        int numNeighboringPoints = numFaces_;

        IntersectionIterator isItTemp = isIt;
        IntersectionIterator isEndItTemp = isEndIt;

        if(isOnBounday()){

            int indexItTemp = 0;

            const ReferenceElement& referenceElement = ReferenceElements::general(element_->geometry().type());

            for (; isItTemp != isEndItTemp; ++isItTemp){

                if(faceTypes_[indexItTemp] == boundary){
                        for (int i = 0; i < isItTemp->geometry().corners(); ++i)
                        {
                            int faceIdx = isItTemp->indexInInside();
                            int localIdxVertex = referenceElement.subEntity(faceIdx, 1, i, dim);
                            DimVector corner = element_->geometry().corner(localIdxVertex);
                            //cellCenters.push_back(corner);
                            cellCenters_.push_back(corner);
                            faceTypes_.push_back(boundary);
                            indexItToLocal_.push_back(faceIdx);
                            globalIdxNeighboringElements_.push_back(-1);
                            neighboringElements_.push_back(element_);
                            numNeighboringPoints++;
                        }
                 }

                indexItTemp++;

            }

        }


        for (; isIt != isEndIt; ++isIt){

            if(faceTypes_[indexIt] == boundary /*|| faceTypes_[indexIt] == heterogeneous*/){
            //if(volumeType_ == 1){
                int faceIdx = isIt->indexInInside();

                FVNonlinearFaceStencil<TypeTag> faceStencilMinus(faceIdx, element_);
                DimVector faceCoNormalVectorMinus(0);
                faceCoNormalVectorMinus -= coNormals_[indexIt];
                faceStencilMinus.setPermeability(permeability_);
                Scalar faceArea = isIt->geometry().volume();
                faceStencilMinus.setFaceArea(faceArea);
                faceStencilMinus.setCoNormal(faceCoNormalVectorMinus);

                DimVector faceCenter = isIt->geometry().center();
                faceStencilMinus.setCenterPoint(faceCenter);

                int numInsertedVectors = 0;


                if(dim == 2){

                    //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<2 ;i++)
                    //{
                        DimVector faceConnectionVectorMinusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        ElementPointer elementOne(element_);
                        int faceIdxOne = -1;
                        bool isOnBoundaryOne = false;

                        centerPointOne = cellCenter_;
                        faceConnectionVectorMinusOne = cellCenter_;
                        globalIdxOne = globalIdx_;
                        faceIdxOne = faceIdx;
                        isOnBoundaryOne = false;

                        faceConnectionVectorMinusOne -= faceCenter;

                        for(int j = 0; j<numNeighboringPoints; j++ ){
                            //if(i!=j){
                                DimVector faceConnectionVectorMinusTwo(0);
                                DimVector centerPointTwo;
                                int globalIdxTwo = -1;
                                ElementPointer elementTwo(element_);
                                int faceIdxTwo = -1;
                                bool isOnBoundaryTwo = false;

                                if(j == inverseIndexItToLocal_[faceIdx]){
                                    centerPointTwo = cellCenter_;
                                    faceConnectionVectorMinusTwo = cellCenter_;
                                    globalIdxTwo = globalIdx_;
                                    elementOne = element_;
                                    faceIdxTwo = faceIdx;
                                    isOnBoundaryTwo = false;

                                }else{

                                    if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
                                    {
                                        centerPointTwo = cellCenters_[j];
                                        faceConnectionVectorMinusTwo = cellCenters_[j];
                                        globalIdxTwo = globalIdxNeighboringElements_[j];
                                        elementTwo = neighboringElements_[j];
                                        faceIdxTwo = indexItToLocal_[j];
                                        if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
                                        else isOnBoundaryTwo = false;

                                    }else if(faceTypes_[j] == heterogeneous)
                                    {
                                        centerPointTwo = faceHarmonicAveragingPoints_[j];
                                        faceConnectionVectorMinusTwo = centerPointTwo;
                                        globalIdxTwo = globalIdxNeighboringElements_[j];
                                        elementTwo = neighboringElements_[j];
                                        faceIdxTwo = indexItToLocal_[j];
                                        if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
                                        else isOnBoundaryTwo = false;

                                    }
                                }

                                faceConnectionVectorMinusTwo -= faceCenter;

                                if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus, faceConnectionVectorMinusOne ,faceConnectionVectorMinusTwo)){
                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                    faceStencilMinus.insertStencilElement(elementOne);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                    faceStencilMinus.insertSpanVector(faceConnectionVectorMinusOne);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                    faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                    faceStencilMinus.insertStencilElement(elementTwo);
                                    faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                    faceStencilMinus.insertSpanVector(faceConnectionVectorMinusTwo);
                                    faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                    faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                    numInsertedVectors++;
                                    numInsertedVectors++;

                                    break;
                                }
                            //}
                        }
                    //}

                }
                else{

                    //for(int i = 0; i<numNeighboringPoints && numInsertedVectors<3 ;i++)
                    //{

                        DimVector faceConnectionVectorMinusOne(0);
                        DimVector centerPointOne;
                        int globalIdxOne = -1;
                        ElementPointer elementOne(element_);
                        int faceIdxOne = -1;
                        bool isOnBoundaryOne = false;

                        centerPointOne = cellCenter_;
                        faceConnectionVectorMinusOne = cellCenter_;
                        globalIdxOne = globalIdx_;
                        elementOne = element_;
                        faceIdxOne = faceIdx;
                        isOnBoundaryOne = false;

                        faceConnectionVectorMinusOne -= faceCenter;

                        for(int j = 0; j<numNeighboringPoints && numInsertedVectors<3; j++ )
                        {
                            DimVector faceConnectionVectorMinusTwo(0);
                            DimVector centerPointTwo;
                            int globalIdxTwo = -1;
                            ElementPointer elementTwo(element_);
                            int faceIdxTwo = -1;
                            bool isOnBoundaryTwo = false;

                            if(j == inverseIndexItToLocal_[faceIdx]){
                                centerPointTwo = cellCenter_;
                                faceConnectionVectorMinusTwo = cellCenter_;
                                globalIdxTwo = globalIdx_;
                                elementTwo = element_;
                                faceIdxTwo = faceIdx;
                                isOnBoundaryTwo = false;

                            }else{
                                if(faceTypes_[j] == homogeneous || faceTypes_[j] == boundary)
                                {
                                    centerPointTwo = cellCenters_[j];
                                    faceConnectionVectorMinusTwo = cellCenters_[j];
                                    globalIdxTwo = globalIdxNeighboringElements_[j];
                                    elementTwo = neighboringElements_[j];
                                    faceIdxTwo = indexItToLocal_[j];
                                    if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
                                    else isOnBoundaryTwo = false;

                                }else if(faceTypes_[j] == heterogeneous)
                                {
                                    centerPointTwo = faceHarmonicAveragingPoints_[j];
                                    faceConnectionVectorMinusTwo = centerPointTwo;
                                    globalIdxTwo = globalIdxNeighboringElements_[j];
                                    elementTwo = neighboringElements_[j];
                                    faceIdxTwo = indexItToLocal_[j];
                                    if(faceTypes_[j] == boundary) isOnBoundaryTwo = true;
                                    else isOnBoundaryTwo = false;

                                }
                            }
                            faceConnectionVectorMinusTwo -= faceCenter;

                            for(int k = 0; k < numNeighboringPoints; k++ )
                            {
                                if(j!=k)
                                {
                                    DimVector faceConnectionVectorMinusThree(0);
                                    DimVector centerPointThree;
                                    int globalIdxThree = -1;
                                    ElementPointer elementThree(element_);
                                    int faceIdxThree = -1;
                                    bool isOnBoundaryThree = false;
                                    if(k == inverseIndexItToLocal_[faceIdx]){
                                        centerPointThree = cellCenter_;
                                        faceConnectionVectorMinusThree= cellCenter_;
                                        globalIdxThree = globalIdx_;
                                        elementThree = element_;
                                        faceIdxThree = faceIdx;
                                        isOnBoundaryThree = false;

                                    }else{

                                        if(faceTypes_[k] == homogeneous || faceTypes_[k] == boundary)
                                        {
                                            centerPointThree = cellCenters_[k];
                                            faceConnectionVectorMinusThree = cellCenters_[k];
                                            globalIdxThree = globalIdxNeighboringElements_[k];
                                            elementThree = neighboringElements_[k];
                                            faceIdxThree = indexItToLocal_[k];
                                            if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
                                            else isOnBoundaryThree = false;

                                        }else if(faceTypes_[k] == heterogeneous)
                                        {
                                            centerPointThree = faceHarmonicAveragingPoints_[k];
                                            faceConnectionVectorMinusThree = centerPointThree;
                                            globalIdxThree = globalIdxNeighboringElements_[k];
                                            elementThree = neighboringElements_[k];
                                            faceIdxThree = indexItToLocal_[k];
                                            if(faceTypes_[k] == boundary) isOnBoundaryThree = true;
                                            else isOnBoundaryThree = false;

                                        }
                                    }
                                    faceConnectionVectorMinusThree -= faceCenter;

                                    if(faceStencilMinus.calculateCoefficients(faceCoNormalVectorMinus,faceConnectionVectorMinusOne, faceConnectionVectorMinusTwo ,faceConnectionVectorMinusThree)){
                                        faceStencilMinus.insertGlobalIdxNeighbors(globalIdxOne);
                                        faceStencilMinus.insertStencilElement(elementOne);
                                        faceStencilMinus.insertStencilFaceIdx(faceIdxOne);
                                        faceStencilMinus.insertSpanVector(faceConnectionVectorMinusOne);
                                        faceStencilMinus.insertStencilCenterPoint(centerPointOne);
                                        faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryOne);

                                        faceStencilMinus.insertGlobalIdxNeighbors(globalIdxTwo);
                                        faceStencilMinus.insertStencilElement(elementTwo);
                                        faceStencilMinus.insertStencilFaceIdx(faceIdxTwo);
                                        faceStencilMinus.insertSpanVector(faceConnectionVectorMinusTwo);
                                        faceStencilMinus.insertStencilCenterPoint(centerPointTwo);
                                        faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryTwo);

                                        faceStencilMinus.insertGlobalIdxNeighbors(globalIdxThree);
                                        faceStencilMinus.insertStencilElement(elementThree);
                                        faceStencilMinus.insertStencilFaceIdx(faceIdxThree);
                                        faceStencilMinus.insertSpanVector(faceConnectionVectorMinusThree);
                                        faceStencilMinus.insertStencilCenterPoint(centerPointThree);
                                        faceStencilMinus.insertIsPointOnBoundary(isOnBoundaryThree);

                                        numInsertedVectors++;
                                        numInsertedVectors++;
                                        numInsertedVectors++;

                                        break;
                                    }
                                }
                            }
                        }
                    //}

                }


                if(numInsertedVectors != dim) DUNE_THROW(Dune::NotImplemented, "Not enough vectors!!!");
                //faceStencilMinus.calculateInversStencilFaceIdx();
                faceStencilMinus.calculateCoefficients();
                faceStencilMinus.setStored();

                faceStencilVectorMinus_.push_back(faceStencilMinus);

            }
            ++indexIt;
        }

    }

    FVNonlinearInteractionVolume(ElementPointer element): element_(*element), stored_(false), numFaces_(0), volumeType_(1)
    {

    }


private:
    bool stored_;
    int numFaces_;
    int globalIdx_;
    ElementPointer element_;
    std::vector<FVNonlinearFaceStencil<TypeTag> > faceStencilVectorPlus_;
    std::vector<FVNonlinearFaceStencil<TypeTag> > faceStencilVectorMinus_;
    std::vector<ElementPointer> neighboringElements_;
    std::vector<IntersectionIterator> elementIntersections_;
    std::vector<int> globalIdxNeighboringElements_;
    std::vector<int> indexItToLocal_;
    std::vector<int> inverseIndexItToLocal_;
    std::vector<int> indexItToLocalMinus_;
    std::vector<Scalar> cellCenterFaceDistances_;
    std::vector<Scalar> neighboringCellCenterFaceDistances_;
    std::vector<Scalar> unitCoNormalProjections_;
    std::vector<Scalar> neighboringUnitCoNormalProjections_;
    VectorFieldVector connectionVectors_;
    VectorFieldVector coNormals_;
    VectorFieldVector normalVectors_;
    VectorFieldVector cellCenters_;
    VectorFieldVector faceCenters_;
    VectorFieldVector faceHarmonicAveragingPoints_;
    DimMatrix permeability_;
    DimVector cellCenter_;
    VectorFieldMatrix neighborPermeabilities_;
    int volumeType_;
    std::vector<int> faceTypes_;

};
}
#endif /* FVNONLINEARINTERACTIONVOLUME_HH_ */
