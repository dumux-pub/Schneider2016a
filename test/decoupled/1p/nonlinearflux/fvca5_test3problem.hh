// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*****************************************************************************/
/*!
 * \file
 * \brief test problem for diffusion models from the FVCA6 benchmark.
 */
#ifndef DUMUX_FVCA5_TEST3_PROBLEM_HH
#define DUMUX_FVCA5_TEST3_PROBLEM_HH

#if HAVE_ALUGRID
#include <dune/grid/alugrid/3d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#include <dune/grid/uggrid.hh>

#include <dumux/material/components/unit.hh>
//#include <dumux/io/cubegridcreator.hh>

#include <dumux/porousmediumflow/2p/sequential/diffusion/cellcentered/pressureproperties.hh>
#include <dumux/porousmediumflow/2p/sequential/diffusion/mpfa/lmethod/3dpressurepropertiesadaptive.hh>
#include <dumux/porousmediumflow/2p/sequential/diffusion/mimetic/pressureproperties.hh>

#include <dumux/porousmediumflow/2p/sequential/transport/cellcentered/properties.hh>
#include <dumux/porousmediumflow/2p/sequential/impes/problem.hh>
#include <dumux/porousmediumflow/2p/sequential/diffusion/problem.hh>
#include <dumux/porousmediumflow/sequential/cellcentered/velocity.hh>

#include "fvca5_test3spatialparams.hh"

namespace Dumux
{
/*!
 * \ingroup IMPETtests
 */
template<class TypeTag>
class FVCAFiveProblemThree;

//////////
// Specify the properties
//////////
namespace Properties
{
NEW_TYPE_TAG(DiffusionTestProblem, INHERITS_FROM(FVTransportTwoP, IMPESTwoPAdaptive, FVCAFiveSpatialParamsThree));
//NEW_TYPE_TAG(DiffusionTestProblem, INHERITS_FROM(DecoupledTwoP, FVCAFiveSpatialParamsThree));

// Set the grid type
#if HAVE_ALUGRID || HAVE_DUNE_ALUGRID
SET_TYPE_PROP(DiffusionTestProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);
#else
SET_TYPE_PROP(DiffusionTestProblem, Grid, Dune::UGGrid<3>);
#endif

SET_TYPE_PROP(DiffusionTestProblem, Problem, Dumux::FVCAFiveProblemThree<TypeTag>);

// Set the wetting phase
SET_PROP(DiffusionTestProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(DiffusionTestProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(DiffusionTestProblem, ProblemEnableGravity, false);

#if HAVE_SUPERLU
SET_TYPE_PROP(DiffusionTestProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag>);
#else
SET_TYPE_PROP(DiffusionTestProblem, LinearSolver, Dumux::ILUnRestartedGMResBackend<TypeTag>);
SET_INT_PROP(DiffusionTestProblem, LinearSolverGMResRestart, 80);
SET_INT_PROP(DiffusionTestProblem, LinearSolverMaxIterations, 1000);
SET_SCALAR_PROP(DiffusionTestProblem, LinearSolverResidualReduction, 1e-8);
SET_SCALAR_PROP(DiffusionTestProblem, LinearSolverPreconditionerIterations, 1);
#endif

//SET_TYPE_PROP(DiffusionTestProblem, GridCreator, CubeGridCreator<TypeTag>);

// set the types for the 2PFA FV method
//NEW_TYPE_TAG(FVCAFiveProblemThreeFV, INHERITS_FROM(FVPressureTwoP, DiffusionTestProblem));

// set the types for the MPFA-L FV method
NEW_TYPE_TAG(FVCAFiveProblemThreeMpfaL, INHERITS_FROM(FvMpfaL3dPressureTwoPAdaptive, DiffusionTestProblem));
SET_INT_PROP(FVCAFiveProblemThreeMpfaL, MPFATransmissibilityCriterion, 1);

// set the types for the mimetic FD method
//NEW_TYPE_TAG(FVCAFiveProblemThreeMimetic, INHERITS_FROM(MimeticPressureTwoP, DiffusionTestProblem));
}

/*!
 * \ingroup DecoupledProblems
 *
 * \brief test problem for diffusion models from the FVCA6 benchmark.
 */
template<class TypeTag>
class FVCAFiveProblemThree: public IMPESProblem2P<TypeTag>
//class FVCAFiveProblemThree: public DiffusionProblem2P<TypeTag>
{
    //typedef DiffusionProblem2P<TypeTag> ParentType;
    typedef IMPESProblem2P<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,
        pWIdx = Indices::pressureIdx,
        swIdx = Indices::swIdx,
        pressureEqIdx = Indices::pressureEqIdx,
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;

    FVCAFiveProblemThree(TimeManager &timeManager, const GridView &gridView) :
        ParentType(timeManager, gridView), velocity_(*this)
    {
        try
        {
            int numRefine;
            numRefine = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumRefine);
            Grid& grid = GridCreator::grid();

            grid.globalRefine(numRefine);
            ElementIterator eIt = grid.leafGridView().template begin<0>();
            ElementIterator eEndIt = grid.leafGridView().template end<0>();
            for(;eIt != eEndIt; ++eIt)
            {
                GlobalPosition center = eIt->geometry().center();
                //if(center[0]<0.5 && center[1]<0.5 && center[2]<0.5)
                if(center[1]<0.5 && center[2]<0.5)
                {
                    grid.mark(1,*eIt);
                }

            }

            grid.adapt();
            // delete marks
            grid.postAdapt();
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

    }

    //!for this specific problem: initialize the saturation and afterwards the model
    void init()
    {
        this->variables().initialize();
        this->spatialParams().initialize();
        for (int i = 0; i < this->gridView().size(0); i++)
        {
            this->variables().cellData(i).setSaturation(wPhaseIdx, 1.0);
            this->variables().cellData(i).setSaturation(nPhaseIdx, 0.0);
        }
        this->model().initialize();
        velocity_.initialize();
    }

    const char *name() const
    {
        if (ParameterTree::tree().hasKey("Problem.OutputFileName"))
        {
            std::string fileName(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, OutputFileName));
            return fileName.c_str();
        }
        else
        {
            return "fvca5_test3";
        }
    }

    void calculateFVVelocity()
    {
        velocity_.calculateVelocity();
//        velocity_.addOutputVtkFields(this->resultWriter());
    }

    //! \copydoc ParentType::addOutputVtkFields()
    void addOutputVtkFields()
    {
        ScalarSolution *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *globalIdx = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarSolution *volume = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            (*exactPressure)[this->elementMapper().map(*eIt)][0] = exact(eIt->geometry().center());
            (*globalIdx)[this->elementMapper().map(*eIt)][0] = this->elementMapper().map(*eIt);
            (*volume)[this->elementMapper().map(*eIt)][0] = eIt->geometry().volume();
        }

        this->resultWriter().attachCellData(*exactPressure, "exactPressure");
        //this->resultWriter().attachCellData(*globalIdx, "globalIdx");
        this->resultWriter().attachCellData(*volume, "volume");

        return;
    }

    /*!
    * \name Problem parameters
    */
    // \{

    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 273.15 + 10; // -> 10°C
    }

    // \}


    //! Returns the reference pressure for evaluation of constitutive relations
    Scalar referencePressureAtPos(const GlobalPosition& globalPos) const
    {
        return 1e5; // -> 10°C
    }

    void sourceAtPos(PrimaryVariables &values,const GlobalPosition& globalPos) const
    {
        values = 0;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar z = globalPos[2];

        double pi = 4.0*atan(1.0);

        if ((globalPos[1] <= 0.5) && (globalPos[2] <= 0.5)){
            values[wPhaseIdx] = (1101.0*pi*pi*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z))/250.0;
        }
        if ((globalPos[1] > 0.5) && (globalPos[2] <= 0.5)){
            values[wPhaseIdx] = 4044.0*pi*pi*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z);
        }
        if ((globalPos[1] > 0.5) && (globalPos[2] > 0.5)){
            values[wPhaseIdx] = 4404.0*pi*pi*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z);
        }
        if ((globalPos[1] <= 0.5) && (globalPos[2] > 0.5)){
            values[wPhaseIdx] = (1011.0*pi*pi*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z))/250.0;
        }
    }

    /*!
    * \brief Returns the type of boundary condition.
    *
    *
    * BC for saturation equation can be dirichlet (saturation), neumann (flux), or outflow.
    */
    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition& globalPos) const
    {
        bcTypes.setAllDirichlet();
    }

    //! set dirichlet condition  (saturation [-])
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        // für die Dirichlet-RB (Druck) werden die exakten Werte der gegebenen Lösung eingesetzt
        values[pWIdx] = exact(globalPos);
        values[swIdx] = 1.0;
    }

    //! set neumann condition for phases (flux, [kg/(m^2 s)])
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition& globalPos) const
    {
        values = 0;
    }

    Scalar exact (const GlobalPosition& globalPos) const
    {
        double pi = 4.0*atan(1.0);
        double alpha = 0.;
        if ((globalPos[1] <= 0.5) && (globalPos[2] <= 0.5)){
            alpha = 0.1;
        }
        if ((globalPos[1] > 0.5) && (globalPos[2] <= 0.5)){
            alpha = 10.0;
        }
        if ((globalPos[1] > 0.5) && (globalPos[2] > 0.5)){
            alpha = 100.0;
        }
        if ((globalPos[1] <= 0.5) && (globalPos[2] > 0.5)){
            alpha = 0.01;
        }

        return alpha*sin(2*pi*globalPos[0])*sin(2*pi*globalPos[1])*sin(2*pi*globalPos[2]);
    }

    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
        {
        Dune::FieldVector<Scalar,dim> grad(0);

        return grad;
        }
private:
    Dumux::FVVelocity<TypeTag, typename GET_PROP_TYPE(TypeTag, Velocity) > velocity_;
    static constexpr Scalar eps_ = 1e-4;

};
} //end namespace

#endif
