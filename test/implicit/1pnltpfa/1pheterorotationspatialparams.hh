// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
#ifndef DUMUX_1P_HETEROROTATION_SPATIALPARAMS_HH
#define DUMUX_1P_HETEROROTATION_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams1p.hh>

namespace Dumux
{

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
template<class TypeTag>
class OnePHeteroRotationSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    OnePHeteroRotationSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {

        double pi = 4.0*atan(1.0);

        thetaOne_ = 1.0*pi/6.0;
        thetaTwo_ = thetaOne_;
        thetaThree_ = thetaOne_;
        thetaFour_ = thetaOne_;
        //thetaOne_ = pi/6.0;
        //thetaTwo_ = -thetaOne_;
        //thetaThree_ = -thetaOne_;
        //thetaFour_ = thetaOne_;

        k1_ = 1000.0;
        k2_ = 1.0;
    }

    /*!
     * \brief Return the intrinsic permeability for the current sub-control volume in [m^2].
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        double cost = cos(thetaOne_);
        double sint = sin(thetaOne_);

        GlobalPosition globalPos = element.geometry().center();

        if(globalPos[0] <=0.5 && globalPos[1] <=0.5){
            cost = cos(thetaOne_);
            sint = sin(thetaOne_);
        }else if(globalPos[0] >0.5 && globalPos[1] <=0.5){
            cost = cos(thetaTwo_);
            sint = sin(thetaTwo_);
        }else if(globalPos[0] <=0.5 && globalPos[1] >0.5){
            cost = cos(thetaThree_);
            sint = sin(thetaThree_);
        }else if(globalPos[0] >0.5 && globalPos[1] >0.5){
            cost = cos(thetaFour_);
            sint = sin(thetaFour_);
        }

        DimWorldMatrix perm;

        perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
        perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
        perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);

        return perm;
    }

    DimWorldMatrix intrinsicPermeability(const Element& element) const
    {
        double cost = cos(thetaOne_);
        double sint = sin(thetaOne_);

        GlobalPosition globalPos = element.geometry().center();

        if(globalPos[0] <=0.5 && globalPos[1] <=0.5){
            cost = cos(thetaOne_);
            sint = sin(thetaOne_);
        }else if(globalPos[0] >0.5 && globalPos[1] <=0.5){
            cost = cos(thetaTwo_);
            sint = sin(thetaTwo_);
        }else if(globalPos[0] <=0.5 && globalPos[1] >0.5){
            cost = cos(thetaThree_);
            sint = sin(thetaThree_);
        }else if(globalPos[0] >0.5 && globalPos[1] >0.5){
            cost = cos(thetaFour_);
            sint = sin(thetaFour_);
        }

        DimWorldMatrix perm;

        perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
        perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
        perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);

        return perm;
    }

    DimWorldMatrix intrinsicPermeabilityAtPos(const GlobalPosition &globalPos) const
    {
        double cost = cos(thetaOne_);
        double sint = sin(thetaOne_);

        if(globalPos[0] <=0.5 && globalPos[1] <=0.5){
            cost = cos(thetaOne_);
            sint = sin(thetaOne_);
        }else if(globalPos[0] >0.5 && globalPos[1] <=0.5){
            cost = cos(thetaTwo_);
            sint = sin(thetaTwo_);
        }else if(globalPos[0] <=0.5 && globalPos[1] >0.5){
            cost = cos(thetaThree_);
            sint = sin(thetaThree_);
        }else if(globalPos[0] >0.5 && globalPos[1] >0.5){
            cost = cos(thetaFour_);
            sint = sin(thetaFour_);
        }

        DimWorldMatrix perm;

        perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
        perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
        perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);

        return perm;;
    }

    /*! \brief Define the porosity in [-].
   *
   * \param element The finite element
   * \param fvGeometry The finite volume geometry
   * \param scvIdx The local index of the sub-control volume where
   */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    { return 1.0; }

private:

    double delta_;
    double thetaOne_;
    double thetaTwo_;
    double thetaThree_;
    double thetaFour_;
    double k1_;
    double k2_;
};
} // end namespace
#endif

