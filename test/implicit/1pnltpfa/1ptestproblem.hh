// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_1PTEST_PROBLEM_HH
#define DUMUX_1PTEST_PROBLEM_HH

#if PROBLEM == 1
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#elif PROBLEM == 2
#include <dumux/implicit/1pmpfa/1pmpfamodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#elif PROBLEM == 3
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#endif
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/unit.hh>

//#include <dumux/linear/amgbackend.hh>

#include <dumux/nonlinear/newtoncontrollerabs.hh>

#include "1ptestspatialparams.hh"

namespace Dumux
{
template <class TypeTag>
class OnePTestProblem;

namespace Properties
{

NEW_TYPE_TAG(OnePTestProblem, INHERITS_FROM(OneP));
#if PROBLEM == 1
NEW_TYPE_TAG(OnePTestCCProblem, INHERITS_FROM(CCModel, OnePTestProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(OnePTestCCMpfaProblem, INHERITS_FROM(CCMpfaModel, OnePTestProblem));
#elif PROBLEM == 3
NEW_TYPE_TAG(OnePTestBoxProblem, INHERITS_FROM(BoxModel, OnePTestProblem));
#endif

SET_PROP(OnePTestProblem, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};


// Set the grid type
SET_TYPE_PROP(OnePTestProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_TYPE_PROP(OnePTestProblem, Problem, Dumux::OnePTestProblem<TypeTag> );

// Set the spatial parameters
SET_TYPE_PROP(OnePTestProblem, SpatialParams, Dumux::OnePTestSpatialParams<TypeTag> );

// Linear solver settings
SET_TYPE_PROP(OnePTestProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag> );

// Enable gravity
SET_BOOL_PROP(OnePTestProblem, ProblemEnableGravity, false);

// Set ewton controller
SET_TYPE_PROP(OnePTestProblem, NewtonController, Dumux::NewtonControllerAbs<TypeTag> );
}

#if PROBLEM == 1
template <class TypeTag>
class OnePTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 2
template <class TypeTag>
class OnePTestProblem : public ImplicitMpfaPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 3
template <class TypeTag>
class OnePTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
#endif

#if PROBLEM == 1
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 2
    typedef ImplicitMpfaPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 3
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
public:
    OnePTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);
    }


    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param values Stores the source values, acts as return value
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0;
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
            values.setAllDirichlet();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        //values[pressureIdx] = 1.0e+5*(2.0 - globalPos[dimWorld-1]);
        //values = 0.0;

        values[pressureIdx] = 0.0;

        if (fabs(globalPos[1]) < 1e-6) {
            if (globalPos[0] < 0.2)
                values[pressureIdx] = 1.0;
            else if (globalPos[0] < 0.3)
                values[pressureIdx] = -5.0*globalPos[0] + 2.0;
            else
                values[pressureIdx] = 0.5;
        }
        else if (fabs(1.0 - globalPos[1]) < 1e-6) {
            if (globalPos[0] < 0.7)
                values[pressureIdx] = 0.5;
            else if (globalPos[0] < 0.8)
                values[pressureIdx] = -5.0*globalPos[0] + 4.0;
            else
                values[pressureIdx] = 0.0;
        }
        else if (fabs(globalPos[0]) < 1e-6) {
            if (globalPos[1] < 0.2)
                values[pressureIdx] = 1.0;
            else if (globalPos[1] < 0.3)
                values[pressureIdx] = -5.0*globalPos[1] + 2.0;
            else
                values[pressureIdx] = 0.5;
        }
        else {
            if (globalPos[1] < 0.7)
                values[pressureIdx] = 0.5;
            else if (globalPos[1] < 0.8)
                values[pressureIdx] = -5.0*globalPos[1] + 4.0;
            else
                values[pressureIdx] = 0.0;
        }

        values[pressureIdx] += 1.0;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        priVars[conti0EqIdx] = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        priVars[pressureIdx] = 1.0;
    }
    // \}

private:
    std::string name_;
};
} //end namespace

#endif
