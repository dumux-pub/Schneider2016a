#ifndef DUMUX_BENCHMARK3_2PNIPROBLEM_HH
#define DUMUX_BENCHMARK3_2PNIPROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#if HAVE_ALUGRID
#include <dune/grid/alugrid/3d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#if PROBLEM == 1
#include <dumux/implicit/2pmpfa/2pmodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#include <dumux/linearmpfa/amgbackend.hh>
#elif PROBLEM == 2
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/linear/amgbackend.hh>
#endif

#include <dumux/material/fluidsystems/brineco2fluidsystem.hh>

#include "benchmark3spatialparameters.hh"
#include "benchmark3co2tables.hh"

#define ISOTHERMAL 0

namespace Dumux {

template<class TypeTag>
class Benchmark3_2PNIProblem;

namespace Properties {
NEW_TYPE_TAG(Benchmark3_2PNIProblem, INHERITS_FROM(TwoPNI, Benchmark3SpatialParameters));
#if PROBLEM == 1
NEW_TYPE_TAG(Benchmark3_2PNIProblem_CCMpfa, INHERITS_FROM(CCMpfaModel, Benchmark3_2PNIProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(Benchmark3_2PNIProblem_CC, INHERITS_FROM(CCModel, Benchmark3_2PNIProblem));
#endif

// Set the grid type
SET_PROP(Benchmark3_2PNIProblem, Grid)
{
    typedef Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming> type;
    //typedef Dune::UGGrid<3> type;
};

// Set the problem property
SET_PROP(Benchmark3_2PNIProblem, Problem)
{
    typedef Dumux::Benchmark3_2PNIProblem<TypeTag> type;
};

// set the fluid system
SET_PROP(Benchmark3_2PNIProblem, FluidSystem)
{
    typedef Dumux::FluidSystems::BrineCO2<typename GET_PROP_TYPE(TypeTag, Scalar), Dumux::Benchmark3::CO2Tables> type;
};

// Set the spatial parameters
SET_PROP(Benchmark3_2PNIProblem, SpatialParams)
{ typedef Dumux::Benchmark3SpatialParameters<TypeTag> type; };

// Enable gravity
SET_BOOL_PROP(Benchmark3_2PNIProblem, ProblemEnableGravity, true);

// enable jacobian matrix recycling by default
SET_BOOL_PROP(Benchmark3_2PNIProblem, ImplicitEnableJacobianRecycling, false);
// enable partial reassembling by default
SET_BOOL_PROP(Benchmark3_2PNIProblem, ImplicitEnablePartialReassemble, false);

//SET_TYPE_PROP(Benchmark3_2PNIProblem, LinearSolver, Dumux::SORBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(Benchmark3_2PNIProblem, LinearSolver, Dumux::ILUnBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(Benchmark3_2PNIProblem, LinearSolver, SuperLUBackend<TypeTag> );
SET_TYPE_PROP(Benchmark3_2PNIProblem, LinearSolver, Dumux::AMGBackend<TypeTag> );

NEW_PROP_TAG(BaseProblem);

#if PROBLEM == 1
SET_TYPE_PROP(Benchmark3_2PNIProblem_CCMpfa, BaseProblem, ImplicitMpfaPorousMediaProblem<TypeTag>);
#elif PROBLEM == 2
SET_TYPE_PROP(Benchmark3_2PNIProblem_CC, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
#endif

}

/*!
 * \ingroup TwoPNIBoxProblems
 * \brief Benchmark given for the CO2 Workshop in Svalbard August 2009
 */
template<class TypeTag>
class Benchmark3_2PNIProblem : public GET_PROP_TYPE(TypeTag, BaseProblem)
{
    typedef Benchmark3_2PNIProblem<TypeTag> ThisType;
    typedef typename GET_PROP_TYPE(TypeTag, BaseProblem) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx,

        contiNEqIdx = Indices::contiNEqIdx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

public:
    Benchmark3_2PNIProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        this->spatialParams().setSpatialParametersProperties();

       // initialize the fluid system
        std::cout << "Initializing the fluid system for the 2pni model\n";
        FluidSystem::init(0.1);
        this->timeManager().startNextEpisode(7.884e8);
    }


    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     *
     * The default behaviour is to write one restart file every 5 time
     * steps. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     *
     * The default behaviour is to write out every the solution for
     * very time step. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteOutput() const
    {
        return
            this->timeManager().timeStepIndex() % 1 == 0 ||
            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished();
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    void boundaryTypes(BoundaryTypes &values, const Intersection &is) const
    {
        const GlobalPosition normal = is.centerUnitOuterNormal();
        Scalar normalZComp, absNormalZComp;
        normalZComp = normal[dim-1];
        absNormalZComp = std::fabs(normalZComp);

        if(absNormalZComp> 0.2)
            values.setAllNeumann();
        else
            values.setAllDirichlet();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex representing the "half volume on the boundary"
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        Scalar densityB = 1037.24; // Brine density
        values[pressureIdx] = 1.013e5 - globalPos[2]* densityB*9.81;
        values[saturationIdx] = 0.0;
        values[temperatureIdx] = 283.15 - globalPos[2] * 0.03;

    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvElemGeom,
                 const Intersection &is,
                 int scvIdx,
                 int boundaryFaceIdx) const
    {
        //const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        values = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvElemGeom,
                int scvIdx) const
    {
        const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
        Scalar m;
        values = Scalar(0.0);
        if(this->timeManager().episodeIndex() == 1){
            m = 2.573325096E-5;
            // m = 2.058660077e-5;   // 60 m injection area
            // m = 3.431100128e-5;    // 40 m injection area
            if (globalPos[0] > 5399. && globalPos[0] < 5508. && globalPos[1] > 3285. && globalPos[1] < 3393. && globalPos[2] <  -2951.96) {
                // mass flux
                values[contiNEqIdx] = m;
                // energy flux due to the mass flux. TODO: solution
                // dependend source terms
                values[energyEqIdx] = m*FluidSystem::CO2::gasEnthalpy(273.15+80, 30e6);
            }
        }
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvElemGeom,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        Scalar densityB = 1037.24; // Brine density
        values[pressureIdx] = 1.013e5 - globalPos[2]* densityB*9.81;
        values[saturationIdx] = 0.0;
        values[temperatureIdx] = 283.15 - globalPos[2] * 0.03;

    }

    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables mass;

        this->model().globalStorage(mass);
        double time = this->timeManager().time();

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0) {
            std::cout<< "MassCO2: "<< mass[1] << " kg,"
                     << " MassBrine: " << mass[0] << " kg,"
                     << " Internal Energy: " << mass[2] << " J,"
                     << " Time: "<<time<<std::endl;
        }
    }

    void episodeEnd()
    {
        // Start new episode if episode is over and assign new boundary conditions
        if(this->timeManager().episodeIndex() ==1 )
            this->timeManager().startNextEpisode(7.884e8);
        // this->timeManager().startNextEpisode(7.56864e8);
        // this->timeManager().setTimeStepSize(1000);

        // notify the model that the type of the boundary has been
        // changed for some vertices
        this->model().resetJacobianAssembler();
        std::cout<<"Jacobian Assembler reseted, episode index is: "<<this->timeManager().episodeIndex()<<std::endl;
    }

    /*!
     * \brief Append all quantities of interest which can be derived
     *        from the solution of the current time step to the VTK
     *        writer.
     */
    void addOutputVtkFields()
    {
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

     // get the number of degrees of freedom
     unsigned numDofs = this->model().numDofs();
     unsigned numElements = this->gridView().size(0);

     //create required scalar fields
     ScalarField *Kxx = this->resultWriter().allocateManagedBuffer(numElements);

     ElementIterator eIt = this->gridView().template begin<0>();
     ElementIterator eEndIt = this->gridView().template end<0>();
     for (; eIt != eEndIt; ++eIt)
     {
         int eIdx = this->elementMapper().map(*eIt);
         (*Kxx)[eIdx] = this->spatialParams().intrinsicPermeability(*eIt)[0][0];
     }

     //pass the scalar fields to the vtkwriter
     this->resultWriter().attachDofData(*Kxx, "Kxx", false); //element data
    }

private:
    static constexpr Scalar eps_ = 1e-8;

    std::vector<int> elementsAtWell_;

    std::string name_;

};
} //end namespace

#endif
