// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BUCKLEYLEVERETT_ANALYTICAL_HH
#define DUMUX_BUCKLEYLEVERETT_ANALYTICAL_HH

#include <dumux/implicit/2pmpfa/2pproperties.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dune/geometry/quadraturerules.hh>

/**
 * @file
 * @brief  Analytical solution of the buckley-leverett problem
 * @author Markus Wolff
 */

namespace Dumux
{
/**
 * \ingroup fracflow
 * @brief IMplicit Pressure Explicit Saturation (IMPES) scheme for the solution of
 * the Buckley-Leverett problem
 */

template<typename Scalar, typename Law>
struct CheckMaterialLaw
{
    static bool isLinear()
    {
        return false;
    }
};

template<typename Scalar>
struct CheckMaterialLaw<Scalar, LinearMaterial<Scalar> >
{
    static bool isLinear()
    {
        return true;
    }
};

template<typename Scalar>
struct CheckMaterialLaw<Scalar, EffToAbsLaw< LinearMaterial<Scalar> > >
{
    static bool isLinear()
    {
        return true;
    }
};

template<class TypeTag> class BuckleyLeverettAnalyticGlobal
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename SpatialParams::MaterialLaw MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum
    {
        dimworld = GridView::dimensionworld,
        dim = GridView::dimension
    };
    enum
    {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx
    };

    enum
    {
        saturationIdx = Indices::saturationIdx,
        pressureIdx = Indices::pressureIdx
    };

    typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > BlockVector;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef Dune::FieldVector<Scalar, dimworld> GlobalPosition;

    typedef Dune::QuadratureRule<Scalar, dim> Quad;
    typedef typename Quad::iterator QuadIterator;
    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

private:

    // functions needed for analytical solution

    void initializeAnalytic()
    {
        int size = problem_.gridView().size(0);
        analyticSolution_.resize(size);
        analyticSolution_ = 0;
        errorGlobal_.resize(size);
        errorGlobal_ = 0;
        errorLocal_.resize(size);
        errorLocal_ = 0;
        volumes_.resize(size);
        volumes_ = 0;

        return;
    }
    /*!
     * \brief DOC ME!
     * \param materialLawParams DOC ME!
     */
    void prepareAnalytic()
    {
        ElementIterator dummyElement = problem_.gridView().template begin<0>();
        const MaterialLawParams& materialLawParams(problem_.spatialParams().materialLawParams(*dummyElement));

        swr_ = materialLawParams.swr();
        snr_ = materialLawParams.snr();
        Scalar porosity = problem_.spatialParams().porosity(*dummyElement);

        FluidState fluidState;
        fluidState.setTemperature(problem_.temperature());
        fluidState.setPressure(wPhaseIdx, problem_.referencePressure());
        fluidState.setPressure(nPhaseIdx, problem_.referencePressure());
        Scalar viscosityW = FluidSystem::viscosity(fluidState, wPhaseIdx);
        Scalar viscosityNW = FluidSystem::viscosity(fluidState, nPhaseIdx);


        if (CheckMaterialLaw<Scalar, MaterialLaw>::isLinear() && viscosityW == viscosityNW)
        {
            std::pair<Scalar, Scalar> entry;
            entry.first = 1 - snr_;

            entry.second = vTot_  / (porosity * (1 - swr_ - snr_));

            frontParams_.push_back(entry);
        }
        else
        {
        Scalar sw0 = swr_;
        //Scalar sw0 = 0.74;
        Scalar fw0 = MaterialLaw::krw(materialLawParams, sw0)/viscosityW;
        fw0 /= (fw0 + MaterialLaw::krn(materialLawParams, sw0)/viscosityNW);
        Scalar sw1 = sw0 + deltaS_;
        Scalar fw1 = MaterialLaw::krw(materialLawParams, sw1)/viscosityW;
        fw1 /= (fw1 + MaterialLaw::krn(materialLawParams, sw1)/viscosityNW);
        Scalar tangentSlopeOld = (fw1 - fw0)/(sw1 - sw0);
        sw1 += deltaS_;
        fw1 = MaterialLaw::krw(materialLawParams, sw1)/viscosityW;
        fw1 /= (fw1 + MaterialLaw::krn(materialLawParams, sw1)/viscosityNW);
        Scalar tangentSlopeNew = (fw1 - fw0)/(sw1 - sw0);

        while (tangentSlopeNew > tangentSlopeOld && sw1 < (1.0 - snr_))
        {
            tangentSlopeOld = tangentSlopeNew;
            sw1 += deltaS_;
            fw1 = MaterialLaw::krw(materialLawParams, sw1)/viscosityW;
            fw1 /= (fw1 + MaterialLaw::krn(materialLawParams, sw1)/viscosityNW);
            tangentSlopeNew = (fw1 - fw0)/(sw1 - sw0);
        }
        //sw1 -= deltaS_;
        sw0 = sw1 - deltaS_;
        fw0 = MaterialLaw::krw(materialLawParams, sw0)/viscosityW;
                fw0 /= (fw0 + MaterialLaw::krn(materialLawParams, sw0)/viscosityNW);
        Scalar sw2 = sw1 + deltaS_;
        Scalar fw2 = MaterialLaw::krw(materialLawParams, sw2)/viscosityW;
        fw2 /= (fw2 + MaterialLaw::krn(materialLawParams, sw2)/viscosityNW);
        while (sw1 <= (1.0 - snr_))
        {
            std::pair<Scalar, Scalar> entry;
            entry.first = sw1;

            Scalar dfwdsw = (fw2 - fw0)/(sw2 - sw0);

            entry.second = vTot_  / porosity * dfwdsw;

            frontParams_.push_back(entry);

            sw0 = sw1;
            sw1 = sw2;
            fw0 = fw1;
            fw1 = fw2;

            sw2 += deltaS_;
            fw2 = MaterialLaw::krw(materialLawParams, sw2)/viscosityW;
            fw2 /= (fw2 + MaterialLaw::krn(materialLawParams, sw2)/viscosityNW);
        }
        }

        return;
    }

//    void calcSatError()
//    {
//        Scalar globalVolume = 0;
//        Scalar errorNorm = 0.0;
//
//        ElementIterator eEndIt = problem_.gridView().template end<0> ();
//        for (ElementIterator eIt = problem_.gridView().template begin<0> (); eIt != eEndIt; ++eIt)
//        {
//
//#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
//            int index = problem_.elementMapper().index(*eIt);
//#else
//            int index = problem_.elementMapper().map(*eIt);
//#endif
//            Scalar sat = problem_.model().curSol()[globalIdxI][saturationIdx];
//
//            Scalar volume = eIt->geometry().volume();
//
//            Scalar error = analyticSolution_[index] - sat;
//
//            errorLocal_[index] = error;
//
//            globalVolume += volume;
//            errorNorm += std::abs(volume*error);
//        }
//
//      errorGlobalGlobal_ += errorNorm * problem_.timeManager().timeStepSize();
//
//        if (globalVolume > 0.0 && errorNorm > 0.0)
//        {
//          errorNorm = std::sqrt(errorNorm)/globalVolume;
//          errorGlobal_ = errorNorm;
//        }
//        else
//        {
//          errorGlobal_ = 0;
//        }
//
//        std::cout<<"--------------------------------------------\n";
//        std::cout<<"GlobalError analytic\n";
//        std::cout<<"is  "<<  errorGlobalGlobal_ <<"\n";
//
//        return;
//
//    }


    void updateExSol()
    {
        Scalar time = problem_.timeManager().time() + problem_.timeManager().timeStepSize();
        //std::cout<< "TEND" << time << std::endl;

        // position of the fluid front

        Scalar delta_x = 0.05;

        Scalar xMax =  frontParams_[0].second * time;

        // iterate over vertices and get analytic saturation solution
        ElementIterator eEndIt = problem_.gridView().template end<0> ();
        for (ElementIterator eIt = problem_.gridView().template begin<0> (); eIt != eEndIt; ++eIt)
        {
            // get global coordinate of cell center
            const Element &element = *eIt;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int index = problem_.elementMapper().index(element);
#else
            int index = problem_.elementMapper().map(element);
#endif

            analyticSolution_[index] = 0.0;
            errorLocal_[index] = 0.0;

            Scalar sat = 1.0 - problem_.model().curSol()[index][saturationIdx];

            //get the Gaussian quadrature rule for intervals
            const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());
            const Quad &quad = Dune::QuadratureRules<Scalar, dim>::rule(referenceElement.type(), 4);
            //std::cout << "Using quadrature rule of order: " << quad.order() << std::endl;
            const QuadIterator qend = quad.end();

            QuadIterator qp = quad.begin();
            for(; qp != qend; ++qp)
            {
                //std::cout << qp->position() << std::endl;
                GlobalPosition globalPos = element.geometry().global(qp->position());

                Scalar integrationElement = element.geometry().integrationElement(qp->position());

                if (globalPos[0] > xMax)
                {
                    analyticSolution_[index] += swr_*qp->weight()*integrationElement;
                    if(std::abs(xMax-globalPos[0])>delta_x)
                    {
                    errorLocal_[index] += std::abs(swr_ - sat)* std::abs(swr_-sat) *qp->weight()* integrationElement;
                    }
                }
                else
                {
                    int size = frontParams_.size();
                    if (size == 1)
                    {
                        analyticSolution_[index] += frontParams_[0].first*qp->weight()*integrationElement;
                        if(std::abs(xMax-globalPos[0])>delta_x)
                        {
                            errorLocal_[index] += std::abs(frontParams_[0].first - sat)* std::abs(frontParams_[0].first - sat) *qp->weight()* integrationElement;
                        }

                    }
                    else
                    {
                        // find x_f next to global coordinate of the vertex
                        for (int i = 0; i < size-1; i++)
                        {
                            Scalar x = frontParams_[i].second * time;
                            Scalar xMinus = frontParams_[i+1].second * time;
                            if (globalPos[0] <= x && globalPos[0] > xMinus)
                            {
                                Scalar satApprox = (frontParams_[i].first + ((frontParams_[i].first - frontParams_[i+1].first)/(x - xMinus)) * (globalPos[0] - x));
                                if(satApprox < 0){
                                    std::cout << "SatApprox: " <<satApprox <<std::endl;
                                }
                                analyticSolution_[index] += satApprox*qp->weight()*integrationElement;
                                if(std::abs(xMax-globalPos[0])>delta_x)
                                {
                                    errorLocal_[index] += std::abs(satApprox - sat)*std::abs(satApprox - sat)*qp->weight()* integrationElement;
                                }

                                break;
                            }
                        }

                    }
                }

            }
            volumes_[index] = element.geometry().volume();
            analyticSolution_[index] /= element.geometry().volume();
        }

        // call function to calculate the saturation error
        //calcSatError();

        return;
    }

public:
    void calculateAnalyticSolution()
    {
        Scalar time = problem_.timeManager().time() + problem_.timeManager().timeStepSize();
        Scalar TEnd = problem_.timeManager().endTime();

        initializeAnalytic();

        if(time > TEnd - 10.0)
        {
        updateExSol();
        }
    }

    BlockVector AnalyticSolution() const
    {
        return analyticSolution_;
    }

    //Write saturation and pressure into file
    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {
        int size = problem_.gridView().size(0);
        BlockVector *analyticSolution = writer.allocateManagedBuffer(size);
        //BlockVector *errorGlobal = writer.allocateManagedBuffer (size);
        BlockVector *errorLocal = writer.allocateManagedBuffer (size);
        BlockVector *volumes = writer.allocateManagedBuffer(size);

        *analyticSolution = analyticSolution_;
        //*errorGlobal = errorGlobal_;
        *errorLocal = errorLocal_;
        *volumes = volumes_;

        writer.attachCellData(*analyticSolution, "Sexact");
        //writer.attachCellData(*errorGlobal, "global error");
        writer.attachCellData(*errorLocal, "local error");
        writer.attachCellData(*volumes, "volumes");

        return;
    }

    //! Construct an IMPES object.
    BuckleyLeverettAnalyticGlobal(Problem& problem) :
        problem_(problem), analyticSolution_(0), errorGlobal_(0), errorLocal_(0), frontParams_(0), deltaS_(1e-6), errorGlobalGlobal_(0), volumes_(0)
    {}

    void initialize(Scalar vTot)
    {
        vTot_ = vTot;
        initializeAnalytic();
        prepareAnalytic();
    }


private:
    Problem& problem_;

    BlockVector analyticSolution_;
    BlockVector volumes_;
    BlockVector errorGlobal_;
    BlockVector errorLocal_;

    std::vector<std::pair<Scalar, Scalar> > frontParams_;
    const Scalar deltaS_;

    Scalar swr_;
    Scalar snr_;
    Scalar vTot_;

    Scalar errorGlobalGlobal_;



    int tangentIdx_;
};
}
#endif
