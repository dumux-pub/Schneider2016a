
install(FILES
        2pindices.hh
        2plocalresidual.hh
        2pmodel.hh
        2pproperties.hh
        2ppropertydefaults.hh
        2pvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/2pmpfatest)
