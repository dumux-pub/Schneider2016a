// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_OBLIQUEDRAIN_PROBLEM_HH
#define DUMUX_OBLIQUEDRAIN_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif
#if HAVE_ALUGRID
#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/gmshreader.hh>

#if PROBLEM == 1
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#elif PROBLEM == 2
#include <dumux/implicit/1pmpfa/1pmpfamodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#elif PROBLEM == 3
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#endif
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/unit.hh>

//#include <dumux/linear/amgbackend.hh>

#include "test_obliquedrainspatialparams.hh"

#if PROBLEM == 2
#include "resultevaluation2d.hh"
#endif
#include <dune/geometry/quadraturerules.hh>

namespace Dumux
{
template <class TypeTag>
class ObliqueDrainProblem;

namespace Properties
{

NEW_TYPE_TAG(ObliqueDrainProblem, INHERITS_FROM(OneP));
#if PROBLEM == 1
NEW_TYPE_TAG(ObliqueDrainCCProblem, INHERITS_FROM(CCModel, ObliqueDrainProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(ObliqueDrainCCMpfaProblem, INHERITS_FROM(CCMpfaModel, ObliqueDrainProblem));
#elif PROBLEM == 3
NEW_TYPE_TAG(ObliqueDrainBoxProblem, INHERITS_FROM(BoxModel, ObliqueDrainProblem));
#endif

SET_PROP(ObliqueDrainProblem, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the grid type
//SET_TYPE_PROP(ObliqueDrainProblem, Grid, Dune::YaspGrid<2>);
SET_TYPE_PROP(ObliqueDrainProblem, Grid, Dune::UGGrid<2>);
//SET_TYPE_PROP(ObliqueDrainProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_TYPE_PROP(ObliqueDrainProblem, Problem, Dumux::ObliqueDrainProblem<TypeTag> );

// Set the spatial parameters
SET_TYPE_PROP(ObliqueDrainProblem, SpatialParams, Dumux::ObliqueDrainSpatialParams<TypeTag> );

// Linear solver settings
SET_TYPE_PROP(ObliqueDrainProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag> );

//NEW_TYPE_TAG(ObliqueDrainBoxProblemWithAMG, INHERITS_FROM(ObliqueDrainBoxProblem));
//NEW_TYPE_TAG(ObliqueDrainCCProblemWithAMG, INHERITS_FROM(ObliqueDrainCCProblem));
//// Solver settings for the tests using AMG
//SET_TYPE_PROP(ObliqueDrainBoxProblemWithAMG, LinearSolver, Dumux::AMGBackend<TypeTag> );
//SET_TYPE_PROP(ObliqueDrainCCProblemWithAMG, LinearSolver, Dumux::AMGBackend<TypeTag> );


// Enable gravity
SET_BOOL_PROP(ObliqueDrainProblem, ProblemEnableGravity, false);
}

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::YaspGrid<2> type;</tt> to
 * <tt>typedef Dune::YaspGrid<3> type;</tt> in the problem file
 * and use <tt>test_1p_3d.dgf</tt> in the parameter file.
 */

#if PROBLEM == 1
template <class TypeTag>
class ObliqueDrainProblem : public ImplicitPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 2
template <class TypeTag>
class ObliqueDrainProblem : public ImplicitMpfaPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 3
template <class TypeTag>
class ObliqueDrainProblem : public ImplicitPorousMediaProblem<TypeTag>
{
#endif

#if PROBLEM == 1
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 2
    typedef ImplicitMpfaPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 3
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#endif



    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };


    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    //quadrature Rule
    typedef Dune::QuadratureRule<Scalar, dim> Quad;
    typedef typename Quad::iterator QuadIterator;
    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

public:
    ObliqueDrainProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        delta_ = 0.2;
        theta_ = atan(delta_);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param values Stores the source values, acts as return value
     * \param globalPos The global position
     */
//    void sourceAtPos(PrimaryVariables &values,
//                const GlobalPosition &globalPos) const
//    {
//      Scalar x = globalPos[0];
//      Scalar y = globalPos[1];
//
//
//      Scalar sx = 16*x*y*(y - 1) + 16*y*(x - 1)*(y - 1);
//      Scalar sy = 16*x*y*(x - 1) + 16*x*(x - 1)*(y - 1);
//
//      Scalar sxx = 32*y*(y - 1);
//      Scalar sxy = 16*(x - 1)*(y - 1) + 16*x*y + 16*x*(y - 1) + 16*y*(x - 1);
//      Scalar syy = 32*x*(x - 1);
//
//      DimWorldMatrix K = this->spatialParams().intrinsicPermeability();
//
//      values = -(K[0][0]*sxx + K[0][1]*sxy + K[1][0]*sxy + K[1][1]*syy);
//        //values = 48*x*(1-x) - 16*(1-2*x)*(1-2*y) + 48*y*(1-y);
//    }

    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {

//      DimWorldMatrix K = this->spatialParams().intrinsicPermeability(element);
//      values = 0;
//
//        //get the Gaussian quadrature rule for intervals
//      const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());
//        const Quad &quad = Dune::QuadratureRules<Scalar, dim>::rule(referenceElement.type(), 5);
//        //std::cout << "Using quadrature rule of order: " << quad.order() << std::endl;
//        const QuadIterator qend = quad.end();
//
//      QuadIterator qp = quad.begin();
//      for(; qp != qend; ++qp)
//      {
//          //std::cout << qp->position() << std::endl;
//          GlobalPosition globalPos = element.geometry().global(qp->position());
//
//          Scalar x = globalPos[0];
//          Scalar y = globalPos[1];
//
//
//          Scalar sx = 16*x*y*(y - 1) + 16*y*(x - 1)*(y - 1);
//          Scalar sy = 16*x*y*(x - 1) + 16*x*(x - 1)*(y - 1);
//
//          Scalar sxx = 32*y*(y - 1);
//          Scalar sxy = 16*(x - 1)*(y - 1) + 16*x*y + 16*x*(y - 1) + 16*y*(x - 1);
//          Scalar syy = 32*x*(x - 1);
//
//
//          Scalar integrationElement = element.geometry().integrationElement(qp->position());
//          values += -(K[0][0]*sxx + K[0][1]*sxy + K[1][0]*sxy + K[1][1]*syy)*qp->weight()*integrationElement;
//      }
//
//      values /=element.geometry().volume();

        values = 0.0;

    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
            values.setAllDirichlet();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = exact(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    using ParentType::neumann;
    void neumann(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        priVars[conti0EqIdx] = 0;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        priVars[pressureIdx] = 1.0;
    }

#if PROBLEM == 2
    void resultEvaluation()
    {
        result.evaluate(this->grid().leafGridView(), *this, true);

        std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
        std::cout.precision(2);
        std::cout
                << "\t pErrorL2 \t pInnerErrorL2 \t vErrorL2 \t vInnerErrorL2 \t faceFluxErrorL2 \t faceFluxBoundErrorL2 \t faceFluxInnerErrorL2 \t faceFluxInnerNoIsErrorL2 \t hMax \t\t pMin\t\t pMax\t\t pMinExact\t pMaxExact\t nGrad"
                << std::endl;
        std::cout << "\t " << result.relativeL2Error << "\t " << result.relativeL2ErrorIn << "\t "
                << result.ervell2 << "\t " << result.ervell2In << "\t " << result.realtiveL2ErrorFaceFluxes << "\t\t " << result.realtiveL2ErrorFaceFluxesBound << "\t\t " << result.realtiveL2ErrorFaceFluxesInner << "\t\t " << result.realtiveL2ErrorFaceFluxesInnerNoIntersect
                << "\t" << "\t\t " << result.hMax << "\t " << result.uMin << "\t " << result.uMax << "\t " << result.uMinExact << "\t " << result.uMaxExact << "\t "
                << result.ngrad << "\t " << std::endl;
    }
#endif

    void addOutputVtkFields()
    {

        //resultEvaluation();
        ScalarField *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        //ScalarField *localRelativeFaceFluxError = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int gIdx = this->elementMapper().index(*eIt);
#else
            int gIdx = this->elementMapper().map(*eIt);
#endif

            (*exactPressure)[gIdx][0] = exact(eIt->geometry().center());
        }

        //*localRelativeFaceFluxError = result.localRelativeFaceFluxError;

        this->resultWriter().attachCellData(*exactPressure, "exact pressure");
        //this->resultWriter().attachCellData(*localRelativeFaceFluxError, "localFluxError");

        return;
    }

    //! exakte Lösung für Fehlerberechnung in resultevaluation3d.hh und das Setzen der RB
    Scalar exact (const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar press = -x - delta_*y;

        return press;

    }
    //! Berechnung des exakten Gradienten für Fehlerberechung in resultevaluation3d.hh
    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
    {

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Dune::FieldVector<Scalar,dim> grad(0);

        grad[0] = -1;
        grad[1] = -delta_;

        return grad;
    }

    // \}

private:
    std::string name_;
    //Dumux::ResultEvaluation<TypeTag> result;
    double delta_;
    double theta_;
};
} //end namespace

#endif
