# dune-common
# releases/2.4 # e1a9b914d0a3b133641647a6987c61c9e2a5423a # 2016-06-15 07:05:39 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
git reset --hard e1a9b914d0a3b133641647a6987c61c9e2a5423a
cd ..

# dune-geometry
# releases/2.4 # ac1fca4ff249ccdc7fb035fa069853d84b93fb73 # 2016-05-28 07:30:48 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
git reset --hard ac1fca4ff249ccdc7fb035fa069853d84b93fb73
cd ..

# dune-grid
# releases/2.4 # 5aeced8b0a64d46ff12afd3a252f99c19a8575d8 # 2016-10-11 09:17:15 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
git reset --hard 5aeced8b0a64d46ff12afd3a252f99c19a8575d8
cd ..

# dune-localfunctions
# releases/2.4 # b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a # 2016-05-28 07:35:51 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
git reset --hard b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
cd ..

# dune-istl
# releases/2.4 # ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff # 2016-07-30 12:35:06 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
git reset --hard ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff
cd ..

# dune-alugrid
# releases/2.4 # 1f25886228a2050e13b657914ddde9d3d31009e6 # 2016-04-23 12:41:36 +0200 # Robert K
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout releases/2.4
git reset --hard 1f25886228a2050e13b657914ddde9d3d31009e6
cd ..

# dumux
# releases/2.8 # 2521d491a1ac5a7a41c4a3a53737eccf523610c8 # 2016-02-02 09:56:32 +0100 # Thomas Fetzer
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/2.8
git reset --hard 2521d491a1ac5a7a41c4a3a53737eccf523610c8
patch -p1 < ../patches/dumux_9999-uncommitted-changes.patch
cd ..

