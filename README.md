This file has been created automatically. Please adapt it to your needs.

===============================

The content of this DUNE module was extracted from the module dumux-devel-old.
In particular, the following subfolders of dumux-devel-old have been extracted:

  test/implicit/1pnltpfa,
  test/decoupled/1p/nonlinearflux,
  test/implicit/2pmpfa,
  test/implicit/co2mpfa,

Additionally, all headers in dumux-devel-old that are required to build the
executables from the sources

  test/implicit/1pnltpfa/FVCA6test1/test1_fvca6.cc,
  test/implicit/1pnltpfa/FVCA6test2/test2_fvca6.cc,
  test/implicit/1pnltpfa/FVCA6test3/test3_fvca6.cc,
  test/implicit/1pnltpfa/test_1pharmonicavg.cc,
  test/implicit/1pnltpfa/test_1pweakconv.cc,
  test/implicit/1pnltpfa/test_1pweakconv_mpfal.cc,
  test/implicit/1pnltpfa/test_1pweakconv_nltpfa.cc,
  test/implicit/1pnltpfa/test_cc1p.cc,
  test/implicit/1pnltpfa/test_cc1pheterorotation.cc,
  test/implicit/1pnltpfa/test_ccboundary1p.cc,
  test/implicit/1pnltpfa/test_fvca5_nltpfa.cc,
  test/implicit/1pnltpfa/test_obliquedrain.cc,
  test/decoupled/1p/nonlinearflux/fvca5_test3.cc,
  test/decoupled/1p/nonlinearflux/test_fvca5_picard.cc,
  test/decoupled/1p/nonlinearflux/test_nonlinear2D.cc,
  test/decoupled/1p/nonlinearflux/test_nonlinear.cc,
  test/decoupled/1p/nonlinearflux/test_nonlinearcircle.cc,
  test/decoupled/1p/nonlinearflux/test_nonlinearheterogeneousrotation.cc,
  test/decoupled/1p/nonlinearflux/test_nonlineartest3.cc,
  test/implicit/2pmpfa/test_ccmpfa2p.cc,
  test/implicit/2pmpfa/test_heteroinjection.cc,
  test/implicit/2pmpfa/test_layeredinjection.cc,
  test/implicit/2pmpfa/test_randperm_box.cc,
  test/implicit/2pmpfa/test_randperm.cc,
  test/implicit/2pmpfa/test_randperm_mpfa.cc,
  test/implicit/2pmpfa/test_randperm_tpfa.cc,
  test/implicit/2pmpfa/test_wellinjection.cc,
  test/implicit/co2mpfa/benchmark3_2p2cni_box.cc,
  test/implicit/co2mpfa/benchmark3_2p2cni.cc,
  test/implicit/co2mpfa/benchmark3_2p2cni_mpfa.cc,
  test/implicit/co2mpfa/benchmark3_2p2cni_tpfa.cc,
  test/implicit/co2mpfa/benchmark3_2pni.cc,
  test/implicit/co2mpfa/test_co2layer.cc,
  test/implicit/co2mpfa/test_co2nilayer_box.cc,
  test/implicit/co2mpfa/test_co2nilayer.cc,
  test/implicit/co2mpfa/test_co2nilayer_mpfa_adaptive.cc,
  test/implicit/co2mpfa/test_co2nilayer_mpfa.cc,
  test/implicit/co2mpfa/test_co2nilayer_tpfa_adaptive.cc,
  test/implicit/co2mpfa/test_co2nilayer_tpfa.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.




dune-common               releases/2.4    e1a9b914d0a3b133641647a6987c61c9e2a5423a
dune-geometry             releases/2.4    ac1fca4ff249ccdc7fb035fa069853d84b93fb73
dune-grid                 releases/2.4    5aeced8b0a64d46ff12afd3a252f99c19a8575d8
dune-localfunctions       releases/2.4    b3a11b4a446ddafc31d51bd6695b8a8a6a1ba30a
dune-istl                 releases/2.4    ac276f16a04d9ec11bf4ef1a7c76f45f967fdaff
dune-alugrid              releases/2.4    1f25886228a2050e13b657914ddde9d3d31009e6
dumux                     releases/2.8    2521d491a1ac5a7a41c4a3a53737eccf523610c8
Created patch dumux_9999-uncommitted-changes.patch
