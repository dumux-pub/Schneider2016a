// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup CCProperties
 * \ingroup CCModel
 * \file
 *
 * \brief Default properties for cell centered models
 */
#ifndef DUMUX_MPFA_PROPERTY_DEFAULTS_HH
#define DUMUX_MPFA_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/cellcentered/ccelementboundarytypes.hh>
#include "mpfaelementvolumevariables.hh"
#include "mpfafvelementgeometry.hh"
#include "mpfaassembler.hh"
#include "mpfahelper.hh"
#include "mpfalocalresidual.hh"
#include "mpfaproperties.hh"
#include "mpfaimplicitdarcyfluxvariables.hh"
#include "mpfaimplicitlocaljacobian.hh"



namespace Dumux {

struct MpfaMethods
{
public:
    // MPFA O-method
    static const unsigned int oMethod = 0;
    // MPFA L-method (Not yet implemented)
    static const unsigned int lMethod = 1;
};

// forward declarations
template<class TypeTag> class MpfaLocalResidual;
template<class TypeTag> class CCElementBoundaryTypes;
template<class TypeTag> class MpfaElementVolumeVariables;
template<class TypeTag> class MpfaFVElementGeometry;
template<class TypeTag> class MpfaAssembler;
template<class TypeTag, int method, int dimension> class MPFAHelper;

namespace Properties {
//! Set the default for the FVElementGeometry
SET_TYPE_PROP(MpfaModel, FVElementGeometry, Dumux::MpfaFVElementGeometry<TypeTag>);

//! Set the default for the ElementBoundaryTypes
SET_TYPE_PROP(MpfaModel, ElementBoundaryTypes, Dumux::CCElementBoundaryTypes<TypeTag>);

//! Mapper for the degrees of freedoms.
SET_TYPE_PROP(MpfaModel, DofMapper, typename GET_PROP_TYPE(TypeTag, ElementMapper));

//! Set the BaseLocalResidual to CCLocalResidual
SET_TYPE_PROP(MpfaModel, BaseLocalResidual, Dumux::MpfaLocalResidual<TypeTag>);

//! An array of secondary variable containers
SET_TYPE_PROP(MpfaModel, ElementVolumeVariables, Dumux::MpfaElementVolumeVariables<TypeTag>);

//! Assembler for the global jacobian matrix
SET_TYPE_PROP(MpfaModel, JacobianAssembler, Dumux::MpfaAssembler<TypeTag>);

//! The local jacobian operator
SET_TYPE_PROP(MpfaModel, LocalJacobian, Dumux::MpfaImplicitLocalJacobian<TypeTag>);

//! indicate that this is no box discretization
SET_BOOL_PROP(MpfaModel, ImplicitIsBox, false);

//! by default only calculate the flux over the half faces with the o method
SET_BOOL_PROP(MpfaModel, FullMpfaOBoundary, false);

//! a type to extract the indices for the different mpfa methods
SET_TYPE_PROP(MpfaModel, MpfaMethodIndices, Dumux::MpfaMethods);

//! set the mpfa-o method by default
SET_INT_PROP(MpfaModel, MpfaMethod, Dumux::MpfaMethods::oMethod);

//! extract interactionvolume type from the helper
SET_PROP(MpfaModel, MpfaInteractionVolume)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = GET_PROP_VALUE(TypeTag, MpfaMethod)};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::InteractionVolume type;
};

//! extract interactionvolume type for the o-method from the helper
SET_PROP(MpfaModel, MpfaOInteractionVolume)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = Dumux::MpfaMethods::oMethod};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::InteractionVolume type;
};

//! extract interactionvolume manager type from the helper
SET_PROP(MpfaModel, MpfaInteractionVolumeManager)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = GET_PROP_VALUE(TypeTag, MpfaMethod)};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::InteractionVolumeManager type;
};

//! extract interactionvolume manager type of the o method  from the helper
SET_PROP(MpfaModel, MpfaOInteractionVolumeManager)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = Dumux::MpfaMethods::oMethod};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::InteractionVolumeManager type;
};

//! extract interactionvolume container type from the helper
SET_PROP(MpfaModel, MpfaInteractionVolumeContainer)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = GET_PROP_VALUE(TypeTag, MpfaMethod)};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::InteractionVolumeContainer type;
};

//! extract flux calculator type from the helper
SET_PROP(MpfaModel, MpfaFluxCalculator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = GET_PROP_VALUE(TypeTag, MpfaMethod)};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::FluxCalculator type;
};

//! extract flux calculator type from the helper
SET_PROP(MpfaModel, MpfaOFluxCalculator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum{dim = GridView::dimension};
    enum{MpfaMethod = Dumux::MpfaMethods::oMethod};
    typedef Dumux::MPFAHelper<TypeTag, MpfaMethod, dim> MPFAHelper;

public:
    typedef typename MPFAHelper::FluxCalculator type;
};

//! by default, continuity in the interactionvolumes is enforced on the intersection centers
SET_SCALAR_PROP(MpfaModel, MpfaContinuityPoint, 0.);



} // namespace Properties
} // namespace Dumux

#endif
