// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
#ifndef DUMUX_1P_HARMONICAVG_SPATIALPARAMS_HH
#define DUMUX_1P_HARMONICAVG_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams1p.hh>

namespace Dumux
{

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p box model
 */
template<class TypeTag>
class OnePHarmonicAvgSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    OnePHarmonicAvgSpatialParams(const GridView& gridView)
        : ParentType(gridView), coeff_(0)
    {

        Scalar klxx = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, klxx);
        Scalar klyy = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, klyy);
        Scalar kuxx = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, kuxx);
        Scalar kuyy = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, kuyy);

        coeff_[0] = 1.0;
        coeff_[1] = 1.0;
        coeff_[2] = 0.0;
        coeff_[3] = 1.0;

        coeff_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, c0);
        coeff_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, c1);
        coeff_[2] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, c2);
        coeff_[3] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, c3);

        permLower_[0][0] = klxx;
        permLower_[1][1] = klyy;
        permLower_[0][1] = permLower_[1][0] = 0.0;


        permUpper_[0][0] = kuxx;
        permUpper_[1][1] = kuyy;
        permUpper_[0][1] = permUpper_[1][0] = 0.0;
    }

    /*!
     * \brief Return the intrinsic permeability for the current sub-control volume in [m^2].
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        const GlobalPosition globalPos = element.geometry().center();
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar val = (coeff_[0]*x- coeff_[1]*y - coeff_[2]);

        if(val < 0) return permUpper_;

        return permLower_;

    }

    DimWorldMatrix intrinsicPermeability(const Element& element) const
    {
        const GlobalPosition globalPos = element.geometry().center();
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar val = (coeff_[0]*x- coeff_[1]*y - coeff_[2]);

        if(val < 0) return permUpper_;

        return permLower_;
    }

    DimWorldMatrix intrinsicPermeabilityAtPos(GlobalPosition globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar val = (coeff_[0]*x- coeff_[1]*y - coeff_[2]);

        if(val < 0) return permUpper_;

        return permLower_;
    }


    /*! \brief Define the porosity in [-].
   *
   * \param element The finite element
   * \param fvGeometry The finite volume geometry
   * \param scvIdx The local index of the sub-control volume where
   */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    { return 1.0; }

private:

    Dune::FieldVector<Scalar,4> coeff_;
    DimWorldMatrix permLower_;
    DimWorldMatrix permUpper_;
};
} // end namespace
#endif

