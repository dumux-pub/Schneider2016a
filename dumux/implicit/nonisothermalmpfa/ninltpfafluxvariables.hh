// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief flux variables for the implicit non-isothermal models
 */
#ifndef DUMUX_NI_NLTPFAFLUX_VARIABLES_HH
#define DUMUX_NI_NLTPFAFLUX_VARIABLES_HH

#include <dumux/common/math.hh>

#include "niproperties.hh"
#include <dumux/implicit/common/normaldecompositioninteractionvolume.hh>

namespace Dumux
{

/*!
 * \ingroup NIModel
 * \ingroup ImplicitFluxVariables
 * \brief This class contains data which is required to
 *        calculate the heat fluxes over a face of a finite
 *        volume for the implicit non-isothermal models.
 *        The mass fluxes are computed in the parent class.
 */
template <class TypeTag>
class NINlTpfaFluxVariables : public GET_PROP_TYPE(TypeTag, IsothermalFluxVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, IsothermalFluxVariables) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel) ThermalConductivityModel;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum { dimWorld = GridView::dimensionworld };
    enum { dim = GridView::dimension };
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef NormalDecompositionInteractionVolume<TypeTag> InteractionVolume;

    typedef CoNormalDecompositionFaceStencil<TypeTag> FaceStencil;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

public:

    /*!
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the fully implicit scheme
     * \param fIdx The local index of the sub-control-volume face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary Distinguishes if we are on a sub-control-volume face or on a boundary face
     */
    NINlTpfaFluxVariables(const Problem &problem,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int fIdx,
                            const ElementVolumeVariables &elemVolVars,
                            bool onBoundary = false)
    : ParentType(problem, element, fvGeometry, fIdx, elemVolVars, onBoundary)
    {
        calculateValues_(problem, element, elemVolVars);
    }

    /*!
     * \brief The total heat flux \f$\mathrm{[J/s]}\f$ due to heat conduction
     *        of the rock matrix over the sub-control volume face in
     *        direction of the face normal.
     */
    Scalar normalMatrixHeatFlux() const
    { return normalMatrixHeatFlux_; }

    /*!
     * \brief The harmonically averaged effective thermal conductivity.
     */
    Scalar effThermalConductivity() const
    { return lambdaEff_; }

protected:
    void calculateValues_(const Problem &problem,
                          const Element &element,
                          const ElementVolumeVariables &elemVolVars)
    {
        // calculate temperature gradient using finite element
        // gradients
        temperatureGrad_ = 0;

       bool useNLTPFA = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    UseNlTpfa);

            bool recalculateDirichlet = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    RecalculateDirichletFromNeumann);

            bool addNeumannToFlux = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    AddNeumannFluxToFaceFlux);

//            bool useTpfaAtBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
//                    bool,
//                    Mpfa,
//                    UseTpfaAtBoundary);

            bool useTpfaAtBoundaryCells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                    bool,
                    Mpfa,
                    UseTpfaAtBoundaryCells);

//            bool dependsOnNeumannBoundary = false;


            if(recalculateDirichlet && addNeumannToFlux)
            {
                std::cout << "Warning: Cannot use both RecalculateDirichletFromNeumann and AddNeumannFluxToFaceFlux" << std::endl;
            }

            // get the global index of the cell
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int globalIdxI = problem.elementMapper().index(element);
#else
            int globalIdxI = problem.elementMapper().map(element);
#endif

            const InteractionVolume* interactionVolumeI = problem.model().getInteractionVolume(globalIdxI);

            Scalar faceFlux = 0.0;

            /************* handle interior face *****************/

            if (!this->onBoundary_)
            {
                const Element& elementJ = *this->fvGeometry_.neighbors[this->face().j];
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
                int globalIdxJ = problem.elementMapper().index(elementJ);
#else
                int globalIdxJ = problem.elementMapper().map(elementJ);
#endif
                const InteractionVolume* interactionVolumeJ = problem.model().getInteractionVolume(globalIdxJ);

                std::pair<int,int> indexI(globalIdxJ, -1);
                //int faceType = interactionVolumeI->getFaceType(indexI);

                const FaceStencil* faceStencilI = interactionVolumeI->getNormalFaceStencilVectorPlus(indexI);
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const DimVector coefficientsI = faceStencilI->getCoefficients();
                const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const DimVector centerPointI = faceStencilI->getCenterPoint();
                const bool isOnBoundaryI = interactionVolumeI->isOnBoundary();

                std::pair<int,int> indexJ(globalIdxI, -1);
                const FaceStencil* faceStencilJ = interactionVolumeJ->getNormalFaceStencilVectorPlus(indexJ);
                const int numFAPJ = faceStencilJ->getNumElements();
                const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                const DimVector centerPointJ = faceStencilJ->getCenterPoint();
                const bool isOnBoundaryJ = interactionVolumeJ->isOnBoundary();

                if(!(useTpfaAtBoundaryCells && (isOnBoundaryI || isOnBoundaryJ)))
                {

                    Scalar d_plus = 0.0;
                    Scalar d_minus = 0.0;

                    Scalar M_plus = 0.0;
                    Scalar M_minus = 0.0;
                    Scalar M_plusminus = 0.0;
                    Scalar M_minusplus = 0.0;

                    Scalar mu_plus = 0.5;
                    Scalar mu_minus = 0.5;

                    for(int i=0 ; i < numFAPI ; i++){

                        if(globalIdxVectorI[i] > -1){

                            M_plus += coefficientsI[i];
                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);

                            if(globalIdxVectorI[i] == globalIdxJ){
                                M_plusminus += coefficientsI[i];
                            }else{
                                int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                                d_plus += coefficientsI[i]*elemVolVars[volVarsIdx].temperature();
                            }

                        }
                        else
                        {
                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);

                            if(bcTypes.hasNeumann())
                            {
                                //dependsOnNeumannBoundary = true;
                                int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                                M_plus += coefficientsI[i];
                                d_plus += coefficientsI[i]*elemVolVars[volVarsIdxI].temperature();
                            }
                            else if(bcTypes.hasDirichlet())
                            {
                                M_plus += coefficientsI[i];
                                d_plus += coefficientsI[i]*elemVolVars[volVarsIdx].temperature();
                            }

                        }
                    }

                    for(int i=0 ; i < numFAPJ ; i++){

                        if(globalIdxVectorJ[i] > -1){

                            M_minus += coefficientsJ[i];

                            std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);

                            if(globalIdxVectorJ[i] == globalIdxI){
                                M_minusplus += coefficientsJ[i];
                            }else{
                                int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                                d_minus += coefficientsJ[i]*elemVolVars[volVarsIdx].temperature();
                            }


                        }
                        else
                        {
                            std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeJ->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);
                            Scalar potentialVal = 0.0;

                            if(bcTypes.hasNeumann())
                            {
                                //dependsOnNeumannBoundary = true;
                                int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));
                                M_minus += coefficientsJ[i];
                                d_minus += coefficientsJ[i]*elemVolVars[volVarsIdxJ].temperature();
                            }
                            else if(bcTypes.hasDirichlet())
                            {

                                M_minus += coefficientsJ[i];
                                d_minus += coefficientsJ[i]*elemVolVars[volVarsIdx].temperature();

                            }


                        }

                    }

                        mu_plus = 0.5;
                        mu_minus = 0.5;

                        if(useNLTPFA && (d_plus > 1.0e-30 && d_minus > 1.0e-30))
                        {
                            mu_plus = d_minus/(d_minus + d_plus);
                            mu_minus = d_plus/(d_minus + d_plus);
                        }else if(useNLTPFA){
                            //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                        }

                        M_plus = M_plus*mu_plus + mu_minus*M_minusplus;
                        M_minus = M_minus*mu_minus + mu_plus*M_plusminus;

                        int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                        int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxJ,-1));

                        faceFlux = M_plus*elemVolVars[volVarsIdxI].temperature();
                        faceFlux -= M_minus*elemVolVars[volVarsIdxJ].temperature();
                        faceFlux -= mu_plus*d_plus;
                        faceFlux += mu_minus*d_minus;

                }
                else
                {
                    GlobalPosition tmp(0.0);
                    for (unsigned int idx = 0; idx < this->face().numFap; idx++)
                    {
                        tmp = this->face().grad[idx];

                        // index for the element volume variables
                        int volVarsIdx = this->face().fapIndices[idx];

                        tmp *= elemVolVars[volVarsIdx].temperature();
                        temperatureGrad_ += tmp;
                    }

                    faceFlux = temperatureGrad_*this->face().unitNormal;
                    faceFlux *= -1;
                }
            }
            /************* boundary face ************************/

            else
            {
                std::pair<int,int> indexI(globalIdxI, this->faceIdx_);
                const FaceStencil* faceStencilI = interactionVolumeI->getNormalFaceStencilVectorPlus(indexI);
                const int numFAPI = faceStencilI->getNumElements();
                const std::vector<int> globalIdxVectorI = faceStencilI->getGlobalIdxNeighbors();
                const DimVector coefficientsI = faceStencilI->getCoefficients();
                const std::vector<int> stencilFaceIdxI = faceStencilI->getStencilFaceIdxVector();
                const std::vector< std::pair<int,int> > stencilFaceGlobalIdxI = faceStencilI->getStencilGlobalIdxVector();
                const std::vector<DimVector> stencilCenterPointsI = faceStencilI->getStencilCenterPoints();
                const DimVector centerPointI = faceStencilI->getCenterPoint();

                Scalar d_plus = 0.0;
                Scalar d_minus = 0.0;

                Scalar M_plus = 0.0;
                Scalar M_minus = 0.0;
                Scalar M_plusminus = 0.0;
                Scalar M_minusplus = 0.0;

                Scalar mu_plus = 0.5;
                Scalar mu_minus = 0.5;

                bool complexBoundary = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        ComplexBoundary);

                bool useTpfaAtBoundaryCells = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                        bool,
                        Mpfa,
                        UseTpfaAtBoundaryCells);

                if(!useTpfaAtBoundaryCells)
                {
                    for(int i=0 ; i < numFAPI ; i++){

                        if(globalIdxVectorI[i] > -1){

                            M_plus += coefficientsI[i];

                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);

                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                            d_plus += coefficientsI[i]*elemVolVars[volVarsIdx].temperature();

                        }else{

                            std::pair<int,int> index(stencilFaceGlobalIdxI[i]);
                            int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                            BoundaryTypes bcTypes;
                            IntersectionIterator isIt = interactionVolumeI->getElementIntersection(index);
                            problem.boundaryTypes(bcTypes, *isIt);
                            Scalar potentialVal = 0.0;

                            if(bcTypes.hasNeumann())
                            {
                                //dependsOnNeumannBoundary = true;
                                int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                                M_plus += coefficientsI[i];
                                d_plus += coefficientsI[i]*elemVolVars[volVarsIdxI].temperature();

                            }else if(bcTypes.hasDirichlet())
                            {

                                M_plus += coefficientsI[i];

                                if(complexBoundary){
                                   if(stencilFaceGlobalIdxI[i].second == this->faceIdx_) M_plusminus += coefficientsI[i];
                                   else{
                                       d_plus += coefficientsI[i]*elemVolVars[volVarsIdx].temperature();
                                   }
                                }else
                                {
                                    d_plus += coefficientsI[i]*elemVolVars[volVarsIdx].temperature();
                                }

                            }


                        }
                    }

                    if(complexBoundary)
                    {
                    std::pair<int,int> indexJ(globalIdxI, this->faceIdx_);
                    const FaceStencil* faceStencilJ = interactionVolumeI->getNormalFaceStencilVectorMinus(indexJ);
                    const int numFAPJ = faceStencilJ->getNumElements();
                    const std::vector<int> globalIdxVectorJ = faceStencilJ->getGlobalIdxNeighbors();
                    const DimVector coefficientsJ = faceStencilJ->getCoefficients();
                    const std::vector<int> stencilFaceIdxJ = faceStencilJ->getStencilFaceIdxVector();
                    const std::vector< std::pair<int,int> > stencilFaceGlobalIdxJ = faceStencilJ->getStencilGlobalIdxVector();
                    const std::vector<DimVector> stencilCenterPointsJ = faceStencilJ->getStencilCenterPoints();
                    const DimVector centerPointJ = faceStencilJ->getCenterPoint();
                    //const bool isOnBoundaryJ = interactionVolumeI->isOnBoundary();


                    for(int i=0 ; i < numFAPJ ; i++){

                        if(globalIdxVectorJ[i] > -1){

                           M_minus += coefficientsJ[i];

                           std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);

                           if(globalIdxVectorJ[i] == globalIdxI){
                               M_minusplus += coefficientsJ[i];
                           }else{
                               int volVarsIdx = this->fvGeometry_.getLocalIndex(index);
                               d_minus += coefficientsJ[i]*elemVolVars[volVarsIdx].temperature();
                           }


                        }
                        else
                        {

                           std::pair<int,int> index(stencilFaceGlobalIdxJ[i]);
                           int volVarsIdx = this->fvGeometry_.getLocalIndex(index);

                           BoundaryTypes bcTypes;
                           IntersectionIterator isIt = interactionVolumeI->getElementIntersection(std::pair<int,int>(globalIdxI,stencilFaceIdxJ[i]));
                           problem.boundaryTypes(bcTypes, *isIt);
                           Scalar potentialVal = 0.0;

                           if(bcTypes.hasNeumann())
                           {
                                //dependsOnNeumannBoundary = true;
                                int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,this->faceIdx_));
                                M_minus += coefficientsJ[i];
                                d_minus += coefficientsJ[i]*elemVolVars[volVarsIdxJ].temperature();

                           }else if(bcTypes.hasDirichlet())
                           {

                               M_minus += coefficientsJ[i];
                               d_minus += coefficientsJ[i]*elemVolVars[volVarsIdx].temperature();

                           }


                        }

                    }

                    mu_plus = 0.5;
                    mu_minus = 0.5;

                    if(useNLTPFA && (d_plus > 1.0e-30 && d_minus > 1.0e-30))
                    {
                        mu_plus = d_minus/(d_minus + d_plus);
                        mu_minus = d_plus/(d_minus + d_plus);
                    }else if(useNLTPFA){
                        //std::cout<<"Negative values!!!!!!!!!!!!!!" << std::endl;
                    }

                    M_plus = M_plus*mu_plus + mu_minus*M_minusplus;
                    M_minus = M_minus*mu_minus + mu_plus*M_plusminus;

                    int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                    int volVarsIdxJ = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI, this->faceIdx_));

                    faceFlux = M_plus*elemVolVars[volVarsIdxI].temperature();
                    faceFlux -= M_minus*elemVolVars[volVarsIdxJ].temperature();
                    faceFlux -= mu_plus*d_plus;
                    faceFlux += mu_minus*d_minus;

                    }else{
                        int volVarsIdxI = this->fvGeometry_.getLocalIndex(std::pair<int,int>(globalIdxI,-1));
                        faceFlux = M_plus*elemVolVars[volVarsIdxI].temperature();
                        faceFlux -= d_plus;
                    }
                }
                else
                {
                    GlobalPosition tmp(0.0);
                    for (unsigned int idx = 0; idx < this->face().numFap; idx++)
                    {
                        tmp = this->face().grad[idx];

                        // index for the element volume variables
                        int volVarsIdx = this->face().fapIndices[idx];

                        tmp *= elemVolVars[volVarsIdx].temperature();
                        temperatureGrad_ += tmp;
                    }

                    faceFlux = temperatureGrad_*this->face().unitNormal;
                    faceFlux *= -1;
                }


            }

        lambdaEff_ = 0;
        calculateEffThermalConductivity_(problem, element, elemVolVars);

        temperatureGrad_ = this->face().unitNormal;
        temperatureGrad_ *= faceFlux;
        // project the heat flux vector on the face's normal vector
        normalMatrixHeatFlux_ = this->face().area*faceFlux;
        //normalMatrixHeatFlux_ = temperatureGrad_* this->face().normal;
        normalMatrixHeatFlux_ *= lambdaEff_;
    }

    void calculateEffThermalConductivity_(const Problem &problem,
                                          const Element &element,
                                          const ElementVolumeVariables &elemVolVars)
    {
        const unsigned i = this->face().i;
        const unsigned j = this->face().j;
        Scalar lambdaI, lambdaJ;

        if (GET_PROP_VALUE(TypeTag, ImplicitIsBox))
        {
            lambdaI =
              ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[i],
                                                                     problem.spatialParams(),
                                                                     element, this->fvGeometry_, i);

            lambdaJ =
              ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[j],
                                                                     problem.spatialParams(),
                                                                     element, this->fvGeometry_, j);
        }
        else
        {
            const Element& elementI = *this->fvGeometry_.neighbors[i];
            FVElementGeometry fvGeometryI;
            fvGeometryI.subContVol[0].global = elementI.geometry().center();

            lambdaI =
              ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[i],
                                                                     problem.spatialParams(),
                                                                     elementI, fvGeometryI, 0);

            const Element& elementJ = *this->fvGeometry_.neighbors[j];
            FVElementGeometry fvGeometryJ;
            fvGeometryJ.subContVol[0].global = elementJ.geometry().center();

            lambdaJ =
              ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[j],
                                                                     problem.spatialParams(),
                                                                     elementJ, fvGeometryJ, 0);
        }

        lambdaEff_ = harmonicMean(lambdaI, lambdaJ);
    }

private:
    Scalar lambdaEff_;
    Scalar normalMatrixHeatFlux_;
    GlobalPosition temperatureGrad_;
};

} // end namespace

#endif
