// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
#ifndef DUMUX_LAYEREDINJECTION_SPATIAL_PARAMS_HH
#define DUMUX_LAYEREDINJECTION_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

//#include <dumux/implicit/2pmpfa/2pmodel.hh>

#if PROBLEM == 1
#include <dumux/implicit/2pmpfa/2pmodel.hh>
#elif PROBLEM == 2
#include <dumux/implicit/2p/2pmodel.hh>
#elif PROBLEM == 3
#include <dumux/implicit/2p/2pmodel.hh>
#elif PROBLEM == 4
#include <dumux/implicit/2pmpfatest/2ppropertydefaults.hh>
#endif

namespace Dumux
{

//forward declaration
template<class TypeTag>
class LayeredInjectionSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(LayeredInjectionSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(LayeredInjectionSpatialParams, SpatialParams, Dumux::LayeredInjectionSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(LayeredInjectionSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    //typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}
/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
template<class TypeTag>
class LayeredInjectionSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    LayeredInjectionSpatialParams(const GridView& gridView)
    : ParentType(gridView)
    {
        barrierTopK_[0][0] = barrierTopK_[1][1] = 1e-19; //sqm
        barrierTopK_[0][1] = barrierTopK_[1][0] = 0.0; //sqm
        barrierMiddleK_[0][0] = barrierMiddleK_[1][1] = 1e-15; //sqm
        barrierMiddleK_[0][1] = barrierMiddleK_[1][0] = 0.0; //sqm
        reservoirK_[0][0] = reservoirK_[1][1] = 3.0*1.0e-14; //sqm
        reservoirK_[0][1] = reservoirK_[1][0] = 0.0; //sqm

        //Set the effective porosity of the layers
        barrierTopPorosity_ = 0.001;
        barrierMiddlePorosity_ = 0.05;
        reservoirPorosity_ = 0.2;

        // Same material parameters for every layer
        materialParams_.setSwr(0.0);
        materialParams_.setSwr(0.0);
        materialParams_.setLambda(2.0);
        materialParams_.setPe(1e4);
    }


    /*!
     * \brief Returns the scalar intrinsic permeability \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const DimWorldMatrix intrinsicPermeability(const Element &element,
                                       const FVElementGeometry &fvGeometry,
                                       int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().center();

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        double pi = 4.0*atan(1.0);
        Scalar val1 = 50 + 10.0*sin(pi*x/200);
        Scalar val2 = 60 + 10.0*sin(pi*x/200);

        if(y>val1  && y<val2) return barrierMiddleK_;
        else if(y>93) return barrierTopK_;
        else return reservoirK_;

    }



    const DimWorldMatrix intrinsicPermeability(const Element &element) const
    {
        //Get the global index of the element
        const GlobalPosition &globalPos = element.geometry().center();

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        double pi = 4.0*atan(1.0);
        Scalar val1 = 50 + 10.0*sin(pi*x/200);
        Scalar val2 = 60 + 10.0*sin(pi*x/200);

        if(y>val1  && y<val2) return barrierMiddleK_;
        else if(y>93) return barrierTopK_;
        else return reservoirK_;


//        if (paramIdx_[eIdx] == barrierTop_)
//            return barrierTopK_;
//        else if (paramIdx_[eIdx] == barrierMiddle_)
//            return barrierMiddleK_;
//        else
//            return reservoirK_;
    }

    DimWorldMatrix intrinsicPermeabilityAtPos(const GlobalPosition &globalPos) const
    {

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        double pi = 4.0*atan(1.0);
        Scalar val1 = 50 + 10.0*sin(pi*x/200);
        Scalar val2 = 60 + 10.0*sin(pi*x/200);

        if(y>val1  && y<val2) return barrierMiddleK_;
        else if(y>93) return barrierTopK_;
        else return reservoirK_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    {
        //Get the global index of the element
        const GlobalPosition &globalPos = element.geometry().center();

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        double pi = 4.0*atan(1.0);
        Scalar val1 = 50 + 10.0*sin(pi*x/200);
        Scalar val2 = 60 + 10.0*sin(pi*x/200);

        if(y>val1  && y<val2) return barrierMiddlePorosity_;
        else if(y>93) return barrierTopPorosity_;
        else return reservoirPorosity_;

//        if (paramIdx_[eIdx] == barrierTop_)
//            return barrierTopPorosity_;
//        else if (paramIdx_[eIdx] == barrierMiddle_)
//            return barrierMiddlePorosity_;
//        else
//            return reservoirPorosity_;
    }


    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
        return materialParams_;
    }



private:
    Scalar barrierTopPorosity_;
    Scalar barrierMiddlePorosity_;
    Scalar reservoirPorosity_;

    DimWorldMatrix barrierTopK_;
    DimWorldMatrix barrierMiddleK_;
    DimWorldMatrix reservoirK_;

    MaterialLawParams materialParams_;
};

} // end namespace
#endif

