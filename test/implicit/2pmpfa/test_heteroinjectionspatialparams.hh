// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
#ifndef DUMUX_HETEROINJECTION_SPATIAL_PARAMS_HH
#define DUMUX_HETEROINJECTION_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/implicit/2pmpfa/2pmodel.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class HeteroInjectionSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(HeteroInjectionSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(HeteroInjectionSpatialParams, SpatialParams, Dumux::HeteroInjectionSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(HeteroInjectionSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    //typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}
/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
template<class TypeTag>
class HeteroInjectionSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    HeteroInjectionSpatialParams(const GridView& gridView)
    : ParentType(gridView)
    {

        lensLowerLeft_[0]   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensLowerLeftX);
        lensLowerLeft_[1]   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensLowerLeftY);
        lensUpperRight_[0]  = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensUpperRightX);
        lensUpperRight_[1]  = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensUpperRightY);

        outerMaterialParams_.setSwr(0.0);
        outerMaterialParams_.setSnr(0.0);

        // parameters for the Brooks-Corey Law
        // entry pressures
        outerMaterialParams_.setPe(0);
        // Brooks-Corey shape parameters
        outerMaterialParams_.setLambda(2);

        lensMaterialParams_.setSwr(0.0);
        lensMaterialParams_.setSnr(0.0);

        // parameters for the Brooks-Corey Law
        // entry pressures
        lensMaterialParams_.setPe(0);
        // Brooks-Corey shape parameters
        lensMaterialParams_.setLambda(2);

        outerK_[0][0] = 1.0e-10;
        outerK_[1][1] = 1.0e-10;
        outerK_[0][1] = -5.0*1.0e-11;
        outerK_[1][0] = -5.0*1.0e-11;


        lensK_[0][0] = 1.0e-14;
        lensK_[1][1] = 1.0e-14;
        lensK_[0][1] = 0.0;
        lensK_[1][0] = 0.0;

//        lensK_ = 9.05e-12;
//        outerK_ = 4.6e-10;
    }

    /*!
     * \brief Returns the scalar intrinsic permeability \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    {
        const GlobalPosition& globalPos = fvGeometry.subContVol[scvIdx].global;

        if (isInLens_(globalPos))
            return lensK_;
        return outerK_;

    }

    DimWorldMatrix intrinsicPermeability(const Element &element) const
    {
        const GlobalPosition& globalPos = element.geometry().center();

        if (isInLens_(globalPos))
            return lensK_;
        return outerK_;

    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return 0.4; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
        const GlobalPosition& globalPos = fvGeometry.subContVol[scvIdx].global;

        if (isInLens_(globalPos))
            return lensMaterialParams_;
        return outerMaterialParams_;

        //return materialLawParams_;
    }


private:
    bool isInLens_(const GlobalPosition &globalPos) const
    {
        for (int i = 0; i < dimWorld; ++i) {
            if (globalPos[i] < lensLowerLeft_[i] || globalPos[i] > lensUpperRight_[i])
                return false;
        }
        return true;
    }

    GlobalPosition lensLowerLeft_;
    GlobalPosition lensUpperRight_;

    DimWorldMatrix lensK_;
    DimWorldMatrix outerK_;

    //DimWorldMatrix perm_;

    MaterialLawParams lensMaterialParams_;
    MaterialLawParams outerMaterialParams_;
   // MaterialLawParams materialLawParams_;
};

} // end namespace
#endif

