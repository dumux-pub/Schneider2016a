#ifndef BENCHMARK3_SPATIAL_PARAMETERS_HH
#define BENCHMARK3_SPATIAL_PARAMETERS_HH

#include <iostream>
#include <fstream>
#include <string>
#include <cstdio>

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include "regularizedcappedbrookscorey.hh"
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{
//forward declaration
template<class TypeTag>
class Benchmark3SpatialParameters;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(Benchmark3SpatialParameters);

// Set the spatial parameters
SET_TYPE_PROP(Benchmark3SpatialParameters, SpatialParams, Dumux::Benchmark3SpatialParameters<TypeTag>);

// Set the material Law
SET_PROP(Benchmark3SpatialParameters, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;

public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}

template<class TypeTag>
class Benchmark3SpatialParameters: public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename Grid::LevelGridView LevelGridView;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld,
    };

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;


    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;
    typedef std::vector<Tensor> PermeabilityType;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;

public:
    typedef EffToAbsLaw<EffMaterialLaw> MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    Benchmark3SpatialParameters(const GridView &gridView)
        : ParentType(gridView),
          gridView_(gridView)
    {
        // heat conductivity of granite
        lambdaSolid_ = 2.8;

        // residual saturations
        materialParams_.setSwr(0.2);
        materialParams_.setSnr(0.05);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(10000);
        materialParams_.setLambda(2.0);
    }

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element The current finite element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    const Tensor &intrinsicPermeability(const Element &element,
                                        const FVElementGeometry &fvElemGeom,
                                        int scvIdx) const
    {
        int eIdx = gridView_.indexSet().index(element);

        return permloc_[eIdx];
    }

    const Tensor &intrinsicPermeability(const Element &element) const
    {
        int eIdx = gridView_.indexSet().index(element);

        return permloc_[eIdx];
    }


    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     */
    double porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        int eIdx = gridView_.indexSet().index(element);

        return porosity_[eIdx];
    }

    double porosity(const Element &element) const
    {
        int eIdx = gridView_.indexSet().index(element);

        return porosity_[eIdx];
    }

    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvElemGeom,
                                               int scvIdx) const
    {
        return materialParams_;
    }

    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParams() const
    {
        return materialParams_;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar solidHeatCapacity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        int scvIdx) const
    {
        return 790; // solid heat capacity of granite
    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        return lambdaSolid_;
    }

    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        int scvIdx) const
    {
        return 2700; // density of granite [kg/m^3]

    }


    void setSpatialParametersProperties()
    {
        int numElements = gridView_.size(0);
        permloc_.resize(numElements);
        porosity_.resize(numElements);

        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdx = gridView_.indexSet().index(*eIt);

            int numVertices = eIt->template count<dim>();

            Scalar poro(0.);
            Scalar perm(0.);
            // start loop over all vertices of this element
            typedef typename GridView::template Codim<dim>::EntityPointer VertexPointer;

            for(int vertice = 0; vertice < numVertices; ++vertice)
            {
                const VertexPointer vPtr = eIt->template subEntity<dim>(vertice);

                // sum average of porosity
                poro += GridCreator::parameters(*vPtr)[0];

                // sum up reciprocal of permeability
                perm += 1 / (GridCreator::parameters(*vPtr)[1]*1e-15);
                //perm +=  GridCreator::parameters(*vPtr)[1]*1e-15);
            }
            // divide sum by number of vertices
            porosity_[eIdx] = (poro *= 1/double(numVertices));

            // prepare current perm
            permloc_[eIdx] = 0;
            perm *= 1 / double(numVertices);
            for(int i=0; i<dim; ++i)
            {
                permloc_[eIdx][i][i] = 1 / perm;
                //permloc_[eIdx][i][i] = perm;
            }
        }
    }

private:
    PermeabilityType permloc_;
    std::vector<double> porosity_;
    std::vector<bool> filled_;
    MaterialLawParams materialParams_;
    const GridView gridView_;
    Scalar lambdaSolid_;

};

} // end namespace

#endif
