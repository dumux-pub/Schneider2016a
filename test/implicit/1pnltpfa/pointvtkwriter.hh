// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief A vtk sequence writer for point sources
 */

#ifndef DUMUX_POINTVTKWRITER_HH
#define DUMUX_POINTVTKWRITER_HH

#include <iostream>
#include <fstream>

#include <dune/common/exceptions.hh>
#include <dune/common/iteratorfacades.hh>
#include <dune/grid/io/file/vtk/common.hh>
#include <dune/grid/io/file/vtk/vtuwriter.hh>
#include <dune/grid/io/file/vtk/dataarraywriter.hh>

namespace Dumux
{

//! A function containing vtk information and a vector
template <class Point>
class VTKFunction
{
    typedef typename std::vector<Point> PointVector;
    typedef typename std::vector<Point>::const_iterator PointVectorIterator;
    const PointVector& points_;
public:
    VTKFunction(const PointVector& v)
        : points_(v) {}

    std::size_t size()
    { return points_.size(); }

    PointVectorIterator begin()
    { return points_.begin(); }

    PointVectorIterator end()
    { return points_.end(); }
};

/*!
 * \ingroup IO
 * \brief A helper class writing a point cloud to vtk format
 */
template <class Point>
class PointVTKWriter
{
public:
    PointVTKWriter(const std::string& name, const std::string& path = "")
        : name_(name), path_(path) {}

    //! Write one timestep
    void write(const double& t)
    {
        // keep track of the time
        timesteps_.push_back(t);
        unsigned int count = timesteps_.size() - 1;

        // write the vtp file
        std::ofstream vtpFile;
        std::string vtpname = path_ + sequenceName_(count) + ".vtp";
        vtpFile.open(vtpname);
        writeVTPFile_(vtpFile);

        // write the pvd file
        std::ofstream pvdFile;
        pvdFile.exceptions(std::ios_base::badbit | std::ios_base::failbit |
                           std::ios_base::eofbit);
        std::string pvdname = path_ + name_ + ".pvd";
        pvdFile.open(pvdname.c_str());
        pvdFile << "<?xml version=\"1.0\"?> \n"
                << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"" << Dune::VTK::getEndiannessString() << "\"> \n"
                << "<Collection> \n";

        // write all timesteps
        for (unsigned int i = 0; i <= count; i++)
        {
            std::string fullname = sequenceName_(i) + ".vtp";
            pvdFile << "<DataSet timestep=\"" << timesteps_[i]
                    << "\" group=\"\" part=\"0\" name=\"\" file=\""
                    << fullname << "\"/> \n";
        }
        pvdFile << "</Collection> \n"
                << "</VTKFile> \n" << std::flush;
        pvdFile.close();
    }

    //! bind a vector of points
    void attachPointVector(const std::vector<Point>& points)
    {
        points_ = std::make_shared<VTKFunction<Point> >(points);
        numPoints_ = points_->size();
    }

private:

    //! write the vtp file
    void writeVTPFile_(std::ostream &stream)
    {
        // create writer, writes begin tag
        Dune::VTK::VTUWriter writer(stream, Dune::VTK::ascii, Dune::VTK::polyData);

        // write the main header
        writer.beginMain(0, numPoints_);
        writeData_(writer);
        writer.endMain();
    }

    //! write the body of the vtp
    void writeData_(Dune::VTK::VTUWriter& writer)
    {
        // dump point data
        // writer.beginPointData();
        // std::shared_ptr<Dune::VTK::DataArrayWriter<float> > arrayWriter (writer.template makeArrayWriter<float>("fieldname", /*field.ncomps=*/1, numPoints_));
        // for (auto&& point : *points_)
        //     arrayWriter->write(/*value*/);
        // writer.endPointData();

        // dump point coordinates
        writer.beginPoints();
        {
            std::shared_ptr<Dune::VTK::DataArrayWriter<float> > arrayWriter
                (writer.template makeArrayWriter<float>("Coordinates", 3, numPoints_));
            for (auto&& point : *points_)
            {
                for (unsigned int i = 0; i < point.size(); ++i)
                    arrayWriter->write(point[i]);
                // always has to be three dimensional coordintes, add padding
                for (unsigned int i = point.size(); i < 3; ++i)
                    arrayWriter->write(0.0);
            }
        }
        writer.endPoints();
    }

    //! create sequence name
    std::string sequenceName_(unsigned int count) const
    {
        std::stringstream n;
        n.fill('0');
        n << name_ << "-" << std::setw(5) << count;
        return n.str();
    }

    int numPoints_;
    std::string name_, path_;
    std::vector<double> timesteps_;
    std::shared_ptr<VTKFunction<Point> > points_;
};

} // end namespace Dumux

#endif
