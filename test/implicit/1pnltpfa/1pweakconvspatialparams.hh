// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_1P_WEAKCONV_SPATIALPARAMS_HH
#define DUMUX_1P_WEAKCONV_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams1p.hh>

namespace Dumux
{

template<class TypeTag>
class OnePWeakConvSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
{
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    OnePWeakConvSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {
        Scalar k1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, k1);
        Scalar k2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, k2);
        Scalar k3 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, k3);
        Scalar k4 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, SpatialParams, k4);

        bool useScalar = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, useScalar);

        DimWorldMatrix I(0);
        I[0][0] = 1;
        I[1][1] = 1;

        perm1_ = perm2_ = perm3_ = perm4_ = I;


        if(useScalar)
        {
            perm1_*= k1;
            perm2_*= k2;
            perm3_*= k3;
            perm4_*= k4;
        }else{
            perm1_*= k1;
            perm4_*= k4;

            perm2_[0][0] = k2;
            perm2_[1][1] = k3;
            perm2_[0][1] = perm2_[1][0] = 0.0;

            perm3_ = perm2_;
        }


        Scalar pi = 4.0*atan(1.0);
        beta_ = 7.0/16.0*pi;
    }

    /*!
     * \brief Return the intrinsic permeability for the current sub-control volume in [m^2].
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        const GlobalPosition globalPos = element.geometry().center();
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        int region = region_(globalPos);

        if(region == 1) return perm1_;
        else if(region == 2) return perm2_;
        else if(region == 3) return perm3_;

        return perm4_;

    }

    //The different permeability functions are needed for the different discretization schemes
    DimWorldMatrix intrinsicPermeability(const Element& element) const
    {
        const GlobalPosition globalPos = element.geometry().center();
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        int region = region_(globalPos);

        if(region == 1) return perm1_;
        else if(region == 2) return perm2_;
        else if(region == 3) return perm3_;

        return perm4_;

    }

    //The different permeability functions are needed for the different discretization schemes
    DimWorldMatrix intrinsicPermeabilityAtPos(GlobalPosition globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        int region = region_(globalPos);

        if(region == 1) return perm1_;
        else if(region == 2) return perm2_;
        else if(region == 3) return perm3_;

        return perm4_;

    }


    /*! \brief Define the porosity in [-].
   *
   * \param element The finite element
   * \param fvGeometry The finite volume geometry
   * \param scvIdx The local index of the sub-control volume where
   */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    { return 1.0; }

    int region_(const GlobalPosition& globalPos) const
    {
        int region = 1;

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        if((y - tan(beta_)*x)<=0 && y>=0) region = 1;
        else if((y - tan(beta_)*x)>0 && y>=0) region = 2;
        else if((y - tan(beta_)*x)>0 && y<0) region = 3;
        else region = 4;

        return region;
    }

private:
    DimWorldMatrix perm1_;
    DimWorldMatrix perm2_;
    DimWorldMatrix perm3_;
    DimWorldMatrix perm4_;
    Scalar beta_;
};
} // end namespace
#endif

