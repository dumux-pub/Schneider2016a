#ifndef DUMUX_CONORMALDECOMPOSITION_FACESTENCIL_HH
#define DUMUX_CONORMALDECOMPOSITION_FACESTENCIL_HH

#include <dumux/common/math.hh>

namespace Dumux{

template<class TypeTag>
class CoNormalDecompositionFaceStencil{

private:

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

public:

    CoNormalDecompositionFaceStencil(): stored_(false), faceFlux_(0)
    {
        numElements_ = dim;
    }

    //! Mark storage as completed
    void setStored()
    {
        stored_ = true;
    }

    //! Returns true if information has already been stored
    bool isStored() const
    {
        return stored_;
    }

//    void setCoNormal(DimVector coNormal)
//    {
//        coNormal_ = coNormal;
//    }
//
//    DimVector getCoNormal()
//    {
//        return coNormal_;
//    }

//    void setNormal(DimVector normal)
//    {
//        normal_ = normal;
//    }
//
//    DimVector getNormal()
//    {
//        return normal_;
//    }

    void setCoefficients(DimVector coefficients)
    {
        coefficients_ = coefficients;
    }

    const DimVector getCoefficients() const
    {
        return coefficients_;
    }

    void setCenterPoint(DimVector centerPoint)
    {
        centerPoint_ = centerPoint;
    }

    const DimVector getCenterPoint() const
    {
        return centerPoint_;
    }

    void setStencilCenterPoints(VectorFieldVector stencilCenterPoints)
    {
        stencilCenterPoints_ = stencilCenterPoints;
    }

    const VectorFieldVector getStencilCenterPoints() const
    {
        return stencilCenterPoints_;
    }

    void insertStencilCenterPoint(DimVector centerPoint){
        stencilCenterPoints_.push_back(centerPoint);
    }

    const std::vector<bool> getIsPointOnBoundary() const
    {
        return isPointOnBoundary_;
    }

    void insertIsPointOnBoundary(bool onBoundary){
        isPointOnBoundary_.push_back(onBoundary);
    }

    void setFaceArea(Scalar faceArea)
    {
        faceArea_ = faceArea;
    }

    const Scalar getFaceArea() const
    {
        return faceArea_;
    }

    const int getNumElements() const
    {
        return numElements_;
    }


    void setStencilElements(std::vector<ElementPointer> stencilElements)
    {
        stencilElementVector_ = stencilElements;
    }

    const std::vector<ElementPointer> getStencilElementVector() const
    {
        return stencilElementVector_;
    }

    void insertStencilElement(ElementPointer element){
        stencilElementVector_.push_back(element);
    }

    void insertStencilFaceIdx(int fIdx){
        stencilFaceIdx_.push_back(fIdx);
    }

    const int getStencilFaceIdx(int Idx) const
    {
        return stencilFaceIdx_[Idx];
    }

    const std::vector<int> getStencilFaceIdxVector() const
    {
        return stencilFaceIdx_;
    }

    void insertStencilGlobalIdx(std::pair<int,int> index){
        stencilGlobalIdx_.push_back(index);
    }

    const std::pair<int,int> getStencilGlobalIdx(int Idx) const
    {
        return stencilGlobalIdx_[Idx];
    }

    const std::vector< std::pair<int,int> > getStencilGlobalIdxVector() const
    {
        return stencilGlobalIdx_;
    }

//    void insertSpanVector(DimVector spanVector)
//    {
//      spanVectors_.push_back(spanVector);
//    }
//
//
//    const VectorFieldVector getSpanVectors() const
//    {
//        return spanVectors_;
//    }

    const std::vector<int> getGlobalIdxNeighbors() const
    {
       return globalIdxNeighbors_;
    }

    void insertGlobalIdxNeighbors(int globalIdx){
        globalIdxNeighbors_.push_back(globalIdx);
    }

    void setFaceFlux(Scalar faceFlux) const
    {
        faceFlux_ = faceFlux;
    }

    Scalar getFaceFlux() const
    {
        return faceFlux_;
    }

    //Cramer's rule for the solution of 2D or 3D system of equation
    bool calculateCoefficients(DimVector coNormal, DimVector t_1, DimVector t_2)
    {

        if(dim == 2){

            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            Scalar coNormalNorm = coNormal.two_norm();

            coNormal /= coNormalNorm;


            if(std::abs(t_1*coNormal - 1.0) < 1.0e-30){

                coefficients_[0] = 1.0;
                coefficients_[1] = 0;

                coefficients_[0] /= t_1_norm;

                coefficients_ *= coNormalNorm;

                return true;

            }else if(std::abs(t_2*coNormal - 1.0) < 1.0e-30){

                coefficients_[0] = 0.0;
                coefficients_[1] = 1.0;

                coefficients_[1] /= t_2_norm;

                coefficients_ *= coNormalNorm;

                return true;

            }

            Scalar A_f = t_1[0]*t_2[1] - t_1[1]*t_2[0];
            Scalar A_f_1 = coNormal[0]*t_2[1] - coNormal[1]*t_2[0];
            Scalar A_f_2 = t_1[0]*coNormal[1] - t_1[1]*coNormal[0];

            if(std::abs(A_f) < 1.0e-4) return false;

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;

            coefficients_ *= coNormalNorm;

            if(coefficients_[0] >= -1.0e-30 && coefficients_[1] >= -1.0e-30)
            {
                return true;
            }
            else
            {
                return false;
            }



        }else if(dim ==3){

            std::cout<<"ERROR: Wrong dimension!!!!!"<<std::endl;
            return false;

        }

        return false;

    }


    bool calculateCoefficients(DimVector coNormal, Dune::FieldVector<Scalar, 3> t_1, Dune::FieldVector<Scalar, 3> t_2, Dune::FieldVector<Scalar, 3> t_3)
	{

        if(dim == 2){


            std::cout<<"ERROR: Wrong dimension!!!!!"<<std::endl;


        }else if(dim ==3){

            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();
            Scalar t_3_norm = t_3.two_norm();
            Scalar coNormalNorm = coNormal.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            t_3 /= t_3_norm;

            //DimVector coNormal = coNormal_;
            coNormal /= coNormalNorm;

            if(std::abs(t_1*coNormal - 1.0) < 1.0e-30){

                coefficients_[0] = 1.0;
                coefficients_[1] = 0.0;
                coefficients_[2] = 0.0;

                coefficients_[0] /= t_1_norm ;

                coefficients_ *= coNormalNorm;

                return true;

            }else if(std::abs(t_2*coNormal - 1.0) < 1.0e-30){

                coefficients_[0] = 0.0;
                coefficients_[1] = 1.0;
                coefficients_[2] = 0.0;

                coefficients_[1] /= t_2_norm;

                coefficients_ *= coNormalNorm;

                return true;

            }else if(std::abs(t_3*coNormal - 1.0) < 1.0e-30){

                coefficients_[0] = 0.0;
                coefficients_[1] = 0.0;
                coefficients_[2] = 1.0;

                coefficients_[2] /= t_3_norm;

                coefficients_ *= coNormalNorm;

                return true;

            }


            Scalar A_f = t_1*crossProduct(t_2,t_3);
            Scalar A_f_1 = coNormal * crossProduct(t_2,t_3);
            Scalar A_f_2 = t_1 * crossProduct(coNormal,t_3);
            Scalar A_f_3 = t_1 * crossProduct(t_2,coNormal);


            if(std::abs(A_f) < 1.0e-4){
                return false;
            }

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;
            coefficients_[2] = A_f_3 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;
            coefficients_[2] /= t_3_norm ;

            coefficients_ *= coNormalNorm;

            if(coefficients_[0] >=-1.0e-30 &&  coefficients_[1] >=-1.0e-30 && coefficients_[2] >=-1.0e-30)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;

    }

    bool calculateCoefficients(DimVector coNormal, Dune::FieldVector<Scalar, 2> t_1, Dune::FieldVector<Scalar, 2> t_2, Dune::FieldVector<Scalar, 2> t_3)
    {
        std::cout<<"ERROR: Wrong dimension!!!!!"<<std::endl;
        return false;
    }

    bool checkForTwoPointFlux(DimVector coNormal, DimVector t_1)
    {

        Scalar t_1_norm = t_1.two_norm();

        t_1 /= t_1_norm;
        Scalar coNormalNorm = coNormal.two_norm();

        coNormal /= coNormalNorm;

        if(std::abs(t_1*coNormal - 1.0) < 1.0e-30){

            coefficients_[0] = 1.0;
            coefficients_[1] = 0.0;
            coefficients_[2] = 0.0;

            coefficients_[0] /= t_1_norm ;

            coefficients_ *= coNormalNorm;

            numElements_ = 1;

            return true;
        }

        return false;
    }

private:
    bool stored_;
    Scalar faceArea_;
    std::vector<ElementPointer> stencilElementVector_;
    std::vector< std::pair<int,int> > stencilGlobalIdx_;
    std::vector<int> stencilFaceIdx_;
    std::vector<int> globalIdxNeighbors_;
    //ElementPointer element_;
    //VectorFieldVector spanVectors_;
    //DimVector coNormal_;
    //DimVector normal_;
    DimVector coefficients_;
    int numElements_;
    DimVector centerPoint_;
    VectorFieldVector stencilCenterPoints_;
    std::vector<bool> isPointOnBoundary_;
    mutable Scalar faceFlux_;

};
}
#endif /* CoNormalDecompositionFaceStencil_HH_ */
