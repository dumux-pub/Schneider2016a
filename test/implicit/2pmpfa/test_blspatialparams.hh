// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
#ifndef DUMUX_BL_SPATIAL_PARAMS_HH
#define DUMUX_BL_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/implicit/2pmpfa/2pmodel.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class BLSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(BLSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(BLSpatialParams, SpatialParams, Dumux::BLSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(BLSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    //typedef RegularizedVanGenuchten<Scalar> EffectiveLaw;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}
/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief The spatial parameters for the LensProblem which uses the
 *        two-phase fully implicit model
 */
template<class TypeTag>
class BLSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    /*!
     * \brief The constructor
     *
     * \param gridView The grid view
     */
    BLSpatialParams(const GridView& gridView)
    : ParentType(gridView)
    {
//            lensLowerLeft_[0]   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensLowerLeftX);
//            lensLowerLeft_[1]   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensLowerLeftY);
//            lensUpperRight_[0]  = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensUpperRightX);
//            lensUpperRight_[1]  = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LensUpperRightY);
//
//        // residual saturations
//        lensMaterialParams_.setSwr(0.18);
//        lensMaterialParams_.setSnr(0.0);
//        outerMaterialParams_.setSwr(0.05);
//        outerMaterialParams_.setSnr(0.0);
//
//        // parameters for the Van Genuchten law
//        // alpha and n
//        lensMaterialParams_.setVgAlpha(0.00045);
//        lensMaterialParams_.setVgn(7.3);
//        outerMaterialParams_.setVgAlpha(0.0037);
//        outerMaterialParams_.setVgn(4.7);

        materialLawParams_.setSwr(0.0);
        materialLawParams_.setSnr(0.0);

        // parameters for the Brooks-Corey Law
        // entry pressures
        materialLawParams_.setPe(0);
        // Brooks-Corey shape parameters
        materialLawParams_.setLambda(2);

        perm_[0][0] = 1.0e-7;
        perm_[1][1] = 1.0e-7;

//        lensK_ = 9.05e-12;
//        outerK_ = 4.6e-10;
    }

    /*!
     * \brief Returns the scalar intrinsic permeability \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    DimWorldMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 int scvIdx) const
    {
        const GlobalPosition& globalPos = fvGeometry.subContVol[scvIdx].global;

//        if (isInLens_(globalPos))
//            return lensK_;
//        return outerK_;

        return perm_;
    }

    DimWorldMatrix intrinsicPermeability(const Element &element) const
    {
        const GlobalPosition& globalPos = element.geometry().center();

//        DimWorldMatrix perm(0);
//
//        for(int i=0; i< dimWorld; i++) perm[i][i]=1.0;
//
//        if (isInLens_(globalPos))
//        {
//            perm*=lensK_;
//            return perm;
//        }
//
//        perm *= outerK_;
//        return perm;
        return perm_;

    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    { return 0.2; }


    Scalar porosity(const Element &element) const
    { return 0.2; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                int scvIdx) const
    {
        const GlobalPosition& globalPos = fvGeometry.subContVol[scvIdx].global;

//        if (isInLens_(globalPos))
//            return lensMaterialParams_;
//        return outerMaterialParams_;

        return materialLawParams_;
    }

    const MaterialLawParams& materialLawParams(const Element &element) const
    {
        return materialLawParams_;
    }



private:
//    bool isInLens_(const GlobalPosition &globalPos) const
//    {
//        for (int i = 0; i < dimWorld; ++i) {
//            if (globalPos[i] < lensLowerLeft_[i] || globalPos[i] > lensUpperRight_[i])
//                return false;
//        }
//        return true;
//    }

//    GlobalPosition lensLowerLeft_;
//    GlobalPosition lensUpperRight_;
//
//    Scalar lensK_;
//    Scalar outerK_;

    DimWorldMatrix perm_;

    MaterialLawParams lensMaterialParams_;
    MaterialLawParams outerMaterialParams_;
    MaterialLawParams materialLawParams_;
};

} // end namespace
#endif

