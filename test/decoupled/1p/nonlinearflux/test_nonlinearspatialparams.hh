// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief spatial parameters for the test problem for diffusion models.
 */
#ifndef TEST_NONLINEAR_SPATIALPARAMS_HH
#define TEST_NONLINEAR_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/fv1p.hh>
#include <dumux/common/math.hh>

namespace Dumux
{

/*!
 * \ingroup IMPETtests
 * \brief spatial parameters for the test problem for 1-p diffusion models.
 */
template<class TypeTag>
class TestNonlinearSpatialParams: public FVSpatialParamsOneP<TypeTag>
{
    typedef FVSpatialParamsOneP<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::ScalarSolution ScalarSolution;


    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;
    typedef Dune::FieldVector<Scalar, dim> FieldVector;

public:

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        return permeability_[indexSet_.index(element)];
        //return permeability_;
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    void initialize(const double delta)
    {
        delta_ = delta;


        permeability_.resize(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            setPermeability_(permeability_[indexSet_.index(*eIt)], eIt->geometry().center());
        }


    }

    template<class Writer>
    void addOutputVtkFields(Writer& writer)
    {
        ScalarSolution *permXX = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permXY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permYY = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permZZ = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permXZ = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *permYZ = writer.allocateManagedBuffer(gridView_.size(0));
        ScalarSolution *volume = writer.allocateManagedBuffer(gridView_.size(0));

        ElementIterator eIt = gridView_.template begin<0>();
        ElementIterator eEndIt = gridView_.template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = indexSet_.index(*eIt);
            (*permXX)[eIdxGlobal][0] = permeability_[eIdxGlobal][0][0];
            (*permXY)[eIdxGlobal][0] = permeability_[eIdxGlobal][0][1];
            (*permYY)[eIdxGlobal][0] = permeability_[eIdxGlobal][1][1];
            (*permZZ)[eIdxGlobal][0] = permeability_[eIdxGlobal][2][2];
            (*permXZ)[eIdxGlobal][0] = permeability_[eIdxGlobal][0][2];
            (*permYZ)[eIdxGlobal][0] = permeability_[eIdxGlobal][1][2];
        }

        writer.attachCellData(*permXX, "permeability-X");
        writer.attachCellData(*permYY, "permeability-Y");
        writer.attachCellData(*permZZ, "permeability-Z");
        writer.attachCellData(*permXY, "permeability-XY");
        writer.attachCellData(*permXZ, "permeability-XZ");
        writer.attachCellData(*permYZ, "permeability-YZ");

        return;
    }

    TestNonlinearSpatialParams(const GridView& gridView)
    : ParentType(gridView), gridView_(gridView), indexSet_(gridView.indexSet()), permeability_(0)
    {

        double pi = 4.0*atan(1.0);

        thetaOne_ = pi/6.0;
        thetaTwo_ = -thetaOne_;
        thetaThree_ = -thetaOne_;
        thetaFour_ = thetaOne_;

        k1_ = 1000.0;
        k2_ = 1.0;
        k3_ = 1.0;

        delta_ = 1.0e-3;

    }

private:
    void setPermeability_(FieldMatrix& perm, const GlobalPosition& globalPos) const
    {

        if(dim == 3){
            double x = globalPos[0];
            double y = globalPos[1];
            double z = globalPos[2];

    //      perm[0][0] = 1.0 + y*y + z*z;
    //      perm[1][1] = 1.0 + x*x + z*z;
    //      perm[2][2] = 1.0 + x*x + y*y;
    //      perm[0][1] = -x*y;
    //      perm[1][0] = perm[0][1];
    //      perm[0][2] = -x*z;
    //      perm[2][0] = perm[0][2];
    //      perm[1][2] = -y*z;
    //      perm[2][1] = perm[1][2];



    //        permeability_[0][0] = permeability_[1][1] = permeability_[2][2] = 1.0;
    //        permeability_[0][1] = permeability_[1][0] = permeability_[1][2] = permeability_[2][1] = 0.5;

    //        double pi = 4.0*atan(1.0);
    //
    //        double theta = pi/4.0;
    //
    //        double cost = cos(theta);
    //        double sint = sin(theta);
    //
            double k1_ = 0.01;
            double k2_ = 0.01;
            double k3_ = 0.01;

            perm[0][0] = k1_;
            perm[1][1] = k2_;
            perm[2][2] = k3_;
    //
    //        if(y-x>= -0.02 && y-x <= 0.02){
    //          k1_ = 100.0;
    //            perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
    //            perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
    //            perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);
    //        }

    //        double cost = cos(thetaOne_);
    //        //double sint = sqrt(1.0 - cost*cost);
    //        double sint = sin(thetaOne_);
    //
    //        if(globalPos[0] <=0.5 && globalPos[1] <=0.5){
    //          cost = cos(thetaOne_);
    //          sint = sin(thetaOne_);
    //        }else if(globalPos[0] >0.5 && globalPos[1] <=0.5){
    //          cost = cos(thetaTwo_);
    //          sint = sin(thetaTwo_);
    //        }else if(globalPos[0] <=0.5 && globalPos[1] >0.5){
    //          cost = cos(thetaThree_);
    //          sint = sin(thetaThree_);
    //        }else if(globalPos[0] >0.5 && globalPos[1] >0.5){
    //          cost = cos(thetaFour_);
    //          sint = sin(thetaFour_);
    //        }
    //
    //        //permeability_[0][0] = cost*cost + delta_*sint*sint;
    //        //permeability_[1][1] = sint*sint + delta_*cost*cost;
    //        //permeability_[0][1] = permeability_[1][0] = cost*sint*(1.0 - delta_);
    //
    //        perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
    //        perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
    //        perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);
    //        perm[2][2] = k3_;




            if(y-x>= -0.05 && y-x <= 0.05 && z-x>= -0.05 && z-x <= 0.05 ){
                FieldVector mainDirection(0);

                mainDirection[0] = 1.0;
                mainDirection[1] = 1.0;
                mainDirection[2] = 1.0;

                FieldVector eigenValues(0);
                eigenValues[0] = 1000.0 * k1_;
                eigenValues[1] = k2_;
                eigenValues[2] = k3_;

                calculatePermeability(perm, mainDirection, eigenValues);

            }

        }else if(dim == 2){

            double x = globalPos[0];
            double y = globalPos[1];

            double k1_ = 0.01;
            double k2_ = 0.01;

            perm[0][0] = k1_;
            perm[1][1] = k2_;


            double pi = 4.0*atan(1.0);

            double theta = pi/4.0;

            double cost = cos(theta);
            double sint = sin(theta);

            if(y-x>= -0.02 && y-x <= 0.02){
                k1_ = 1000.0;
                perm[0][0] = cost*cost*k1_ + sint*sint*k2_;
                perm[1][1] = sint*sint*k1_ + cost*cost*k2_;
                perm[0][1] = perm[1][0] = cost*sint*(k1_ - k2_);
            }


        }

        //permeability_[0][0] = permeability_[1][1] = 1.0;
        //permeability_[0][1] = permeability_[1][0] = 0.5;
    }


    void calculatePermeability(FieldMatrix& perm, FieldVector principleDirection, FieldVector principleValues) const
    {

        Scalar k1 = principleValues[0];
        Scalar k2 = principleValues[1];
        Scalar k3 = principleValues[2];

        FieldMatrix diagValues(0);
        diagValues[0][0] = k1;
        diagValues[1][1] = k2;
        diagValues[2][2] = k3;

        FieldVector principleDirectionTwo(0);
        principleDirectionTwo[0] = principleDirection[1];
        principleDirectionTwo[1] = -principleDirection[0];

        FieldVector principleDirectionThree = crossProduct(principleDirection,principleDirectionTwo);

        Scalar norm1 = principleDirection.two_norm();
        Scalar norm2 = principleDirectionTwo.two_norm();
        Scalar norm3 = principleDirectionThree.two_norm();

        principleDirection/=norm1;
        principleDirectionTwo/=norm2;
        principleDirectionThree/=norm3;


        FieldMatrix principleVectors(0);
        principleVectors[0][0] = principleDirection[0];
        principleVectors[1][0] = principleDirection[1];
        principleVectors[2][0] = principleDirection[2];

        principleVectors[0][1] = principleDirectionTwo[0];
        principleVectors[1][1] = principleDirectionTwo[1];
        principleVectors[2][1] = principleDirectionTwo[2];

        principleVectors[0][2] = principleDirectionTwo[0];
        principleVectors[1][2] = principleDirectionTwo[1];
        principleVectors[2][2] = principleDirectionTwo[2];

        FieldMatrix principleVectorsTransposed(0);

        principleVectorsTransposed[0][0] = principleVectors[0][0];
        principleVectorsTransposed[1][1] = principleVectors[1][1];
        principleVectorsTransposed[2][2] = principleVectors[2][2];

        principleVectorsTransposed[0][1] = principleVectors[1][0];
        principleVectorsTransposed[0][2] = principleVectors[2][0];
        principleVectorsTransposed[1][0] = principleVectors[0][1];
        principleVectorsTransposed[1][2] = principleVectors[2][1];
        principleVectorsTransposed[2][0] = principleVectors[0][2];
        principleVectorsTransposed[2][1] = principleVectors[1][2];

        perm = diagValues;
        perm.rightmultiply(principleVectorsTransposed);
        perm.leftmultiply(principleVectors);

    }



    const GridView gridView_;
    const IndexSet& indexSet_;
    std::vector<FieldMatrix> permeability_;
    //FieldMatrix permeability_;
    double delta_;
    double thetaOne_;
    double thetaTwo_;
    double thetaThree_;
    double thetaFour_;
    double k1_;
    double k2_;
    double k3_;

};

} // end namespace
#endif
