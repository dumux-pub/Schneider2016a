// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Soil contamination problem where DNAPL infiltrates a fully
 *        water saturated medium.
 */

#ifndef DUMUX_WELLINJECTION_PROBLEM_HH
#define DUMUX_WELLINJECTION_PROBLEM_HH

#if HAVE_ALUGRID || HAVE_DUNE_ALUGRID || HAVE_UG

#if HAVE_ALUGRID
#include <dune/grid/alugrid/3d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#include <dune/grid/uggrid.hh>
#include <dumux/material/components/unit.hh>

//#if HAVE_UG
//#include <dune/grid/uggrid.hh>
//#endif

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/dnapl.hh>

#if PROBLEM == 1
#include <dumux/implicit/2pmpfa/2pmodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#include <dumux/implicit/ccmpfa/ccmpfapropertydefaults.hh>
#elif PROBLEM == 2
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/cellcentered/ccpropertydefaults.hh>
#elif PROBLEM == 3
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/box/boxpropertydefaults.hh>
#endif

#include <dumux/implicit/2pmpfa/2pgridadaptindicator.hh>
#include <dumux/implicit/adaptive/gridadaptinitializationindicator.hh>


//#include <dumux/linear/amgbackend.hh>

#include "test_wellinjectionspatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class WellInjectionProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_TYPE_TAG(WellInjectionProblem, INHERITS_FROM(TwoP, WellInjectionSpatialParams));
#if PROBLEM == 1
NEW_TYPE_TAG(WellInjectionCCMpfaProblem, INHERITS_FROM(CCMpfaModel, WellInjectionProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(WellInjectionCCProblem, INHERITS_FROM(CCModel, WellInjectionProblem));
#elif PROBLEM == 3
NEW_TYPE_TAG(WellInjectionBoxProblem, INHERITS_FROM(BoxModel, WellInjectionProblem));
#endif

// Set the grid type
// Set the grid type
#if HAVE_ALUGRID || HAVE_DUNE_ALUGRID
SET_TYPE_PROP(WellInjectionProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);
#else
SET_TYPE_PROP(WellInjectionProblem, Grid, Dune::UGGrid<3>);
#endif

//SET_TYPE_PROP(WellInjectionProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_TYPE_PROP(WellInjectionProblem, Problem, WellInjectionProblem<TypeTag>);

SET_PROP(WellInjectionProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};
// Set the non-wetting phase
// Set the non-wetting phase
SET_PROP(WellInjectionProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::DNAPL<Scalar> > type;
};

SET_BOOL_PROP(WellInjectionProblem, ProblemEnableGravity, true);

// Linear solver settings
//SET_TYPE_PROP(WellInjectionProblem, LinearSolver, Dumux::BoxBiCGStabILU0Solver<TypeTag> );
//SET_TYPE_PROP(WellInjectionProblem, LinearSolver, Dumux::AMGBackend<TypeTag>);
SET_TYPE_PROP(WellInjectionProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag> );

//SET_BOOL_PROP(WellInjectionProblem, AdaptiveGrid, true);
//SET_TYPE_PROP(WellInjectionProblem, AdaptationIndicator, TwoPImplicitGridAdaptIndicator<TypeTag>);
//SET_TYPE_PROP(WellInjectionProblem,  AdaptationInitializationIndicator, ImplicitGridAdaptInitializationIndicator<TypeTag>);


NEW_PROP_TAG(BaseProblem);

#if PROBLEM == 1
SET_TYPE_PROP(WellInjectionCCMpfaProblem, BaseProblem, ImplicitMpfaPorousMediaProblem<TypeTag>);
#elif PROBLEM == 2
SET_TYPE_PROP(WellInjectionCCProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
#elif PROBLEM == 3
SET_TYPE_PROP(WellInjectionBoxProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
#endif

}

/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief Soil contamination problem where DNAPL infiltrates a fully
 *        water saturated medium.
 *
 * The domain is sized 6m times 4m and features a rectangular lens
 * with low permeablility which spans from (1 m , 2 m) to (4 m, 3 m)
 * and is surrounded by a medium with higher permability. Note that
 * this problem is discretized using only two dimensions, so from the
 * point of view of the two-phase model, the depth of the domain
 * implicitly is 1 m everywhere.
 *
 * On the top and the bottom of the domain neumann boundary conditions
 * are used, while dirichlet conditions apply on the left and right
 * boundaries.
 *
 * DNAPL is injected at the top boundary from 3m to 4m at a rate of
 * 0.04 kg/(s m^2), the remaining neumann boundaries are no-flow
 * boundaries.
 *
 * The dirichlet boundaries on the left boundary is the hydrostatic
 * pressure scaled by a factor of 1.125, while on the right side it is
 * just the hydrostatic pressure. The DNAPL saturation on both sides
 * is zero.
 *
 * This problem uses the \ref TwoPModel.
 *
 * This problem should typically be simulated until \f$t_{\text{end}}
 * \approx 20\,000\;s\f$ is reached. A good choice for the initial time step
 * size is \f$t_{\text{inital}} = 250\;s\f$.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p -parameterFile test_box2p.input</tt> or
 * <tt>./test_cc2p -parameterFile test_cc2p.input</tt>
 */
template <class TypeTag >
class WellInjectionProblem : public GET_PROP_TYPE(TypeTag, BaseProblem)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseProblem) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonwettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    enum {

        // primary variable indices
        pwIdx = Indices::pwIdx,
        snIdx = Indices::snIdx,

        // equation indices
        contiNEqIdx = Indices::contiNEqIdx,
        contiWEqIdx = Indices::contiWEqIdx,

        // phase indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,


        // world dimension
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    WellInjectionProblem(TimeManager &timeManager,
                const GridView &gridView)
    : ParentType(timeManager, gridView)
    {

        //int globalRefine = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, GridAdapt, GlobalRefine);
        //GridCreator::grid().globalRefine(globalRefine);
        //this->setGrid(GridCreator::grid());

        eps_ = 3e-6;
        temperature_ = 273.15 + 20; // -> 20°C

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief User defined output after the time integration
     *
     * Will be called diretly after the time integration.
     */
    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0) {
            std::cout<<"Storage: " << storage << std::endl;
        }
    }

    /*!
     * \brief Returns the temperature \f$ K \f$
     *
     * This problem assumes a uniform temperature of 20 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Returns the source term
     *
     * \param values Stores the source values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} / (m^\textrm{dim} \cdot s )] \f$
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {

        Scalar eps = 1.0e-8;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar z = globalPos[2];

//      if(x > -15 + eps && x < 15 - eps && y > -15 + eps && y < 15 - eps && z < 7.5 -eps && z> -7.5 +eps)
//      {
//          values.setAllNeumann();
//      }
//      else{
//          values.setAllDirichlet();
//      }

        if(z >= 7.5 -eps || z <= -7.5 +eps)
        {
            values.setAllNeumann();
        }else
        if(x > -15 + eps && x < 15 - eps && y > -15 + eps && y < 15 - eps && z < 7.5 -eps && z> -7.5 +eps){
            values.setAllNeumann();
        }else{
            values.setAllDirichlet();
        }

    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        typename GET_PROP_TYPE(TypeTag, FluidState) fluidState;
        fluidState.setTemperature(temperature_);
        fluidState.setPressure(FluidSystem::wPhaseIdx, /*pressure=*/1e5);
        fluidState.setPressure(FluidSystem::nPhaseIdx, /*pressure=*/1e5);

        Scalar densityW = FluidSystem::density(fluidState, FluidSystem::wPhaseIdx);

        Scalar height = this->bBoxMax()[2] - this->bBoxMin()[2];
        Scalar depth = this->bBoxMax()[2] - globalPos[2];
//        Scalar alpha = 1 + 1.5/height;
//        Scalar width = this->bBoxMax()[0] - this->bBoxMin()[0];
//        Scalar factor = (width*alpha + (1.0 - alpha)*globalPos[0])/width;

//      // für die Dirichlet-RB (Druck) werden die exakten Werte der gegebenen Lösung eingesetzt
        Scalar eps = 1.0e-5;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar z = globalPos[2];
//
//      if(x <= -15 + eps || x >= 15 - eps || y <= -15 + eps || y >= 15 - eps)
//      {
//          values[pwIdx] = 1.0e5;
//          values[snIdx] = 0.0;
//      }
//      else
//      {
//          //std::cout<<"Dirichlet:" << 1 << std::endl;
//          values[pwIdx] = 1.0e6;
//          values[snIdx] = 0.0;
//      }

        values[pwIdx] = 1.0e5 - densityW*this->gravity()[2]*depth;
        values[snIdx] = 0.0;

//        if(x > -15 + eps && x < 15 - eps && y > -15 + eps && y < 15 - eps){
//            values[pwIdx] = 1.0e6 - densityW*this->gravity()[2]*depth;
//            values[snIdx] = 1.0;
//        }

//      if(x > -15 + eps && x < 15 - eps && y > -15 + eps && y < 15 - eps && z < 7.5 -eps && z> -7.5 +eps)
//      {
//          values[snIdx] = 1.0;
//          values[pwIdx] = 3.0e5- densityW*this->gravity()[2]*depth;
//      }

    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        Scalar eps = 1.0e-5;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar z = globalPos[2];

        values = 0.0;

        if(x > -15 + eps && x < 15 - eps && y > -15 + eps && y < 15 - eps && z < 7.5 -eps && z> -7.5 +eps)
        {
            values[contiNEqIdx] = -1.0e-1;
        }

    }
    // \}

    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        typename GET_PROP_TYPE(TypeTag, FluidState) fluidState;
        fluidState.setTemperature(temperature_);
        fluidState.setPressure(FluidSystem::wPhaseIdx, /*pressure=*/1e5);
        fluidState.setPressure(FluidSystem::nPhaseIdx, /*pressure=*/1e5);

        Scalar densityW = FluidSystem::density(fluidState, FluidSystem::wPhaseIdx);

        Scalar depth = this->bBoxMax()[1] - globalPos[1];

        // hydrostatic pressure
        //values[pwIdx] = exact(globalPos);
        values[pwIdx] = 1e5 - densityW*this->gravity()[2]*depth;
        //values[pwIdx] = 1e5;
        values[snIdx] = 0.0;
    }

private:

    Scalar exact (const GlobalPosition& globalPos) const
        {
            //! this routine computes a harmonic function (right hand side equal to 0, dirac measure along a tilted line, corresponding to the bench "Wellmesh"es )
            //! the symmetric permeability tensor is stored in lxx,lxy,lxz,lyy,lyz,lzz
            double alpha = 1.0;
            double mu0  = 1.85334252045523;
            double A11 = 0.122859140982213;
            double A12 = 0.0;
            double A13 = -1.68776357811111;
            double A21 = 0.0;
            double A22 = 0.76472449133173;
            double A23 = 0.0;
            double A31 = 0.754790818120945;
            double A32 = 0.0;
            double A33 = 0.274721390893459;
            double a2 =  0.000603774740915493;

            //!-------------------------------------------------
            //! Eval solution
            //!-------------------------------------------------
            double XX = A11*globalPos[0]+A12*globalPos[1]+A13*globalPos[2];
            double YY = A21*globalPos[0]+A22*globalPos[1]+A23*globalPos[2];
            double ZZ = A31*globalPos[0]+A32*globalPos[1]+A33*globalPos[2];

            //! computation of s2 = sh^2, solution to a2 S^2 + a1 S + a0 = 0

            double a1 = a2 - ZZ*ZZ - YY*YY;
            double a0 = - YY*YY;
            double s2 = (-a1+sqrt(a1*a1 - 4.0*a0*a2))/(2.0*a2);
            double sh = sqrt(s2);
            double frho = sh+sqrt(s2+1.0);
            double mu = log(frho);

            return alpha *(mu - mu0);
        }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->bBoxMax()[1] - eps_;
    }

    bool onInlet_(const GlobalPosition &globalPos) const
    {
        Scalar width = this->bBoxMax()[0] - this->bBoxMin()[0];
        Scalar lambda = (this->bBoxMax()[0] - globalPos[0])/width;
        return onUpperBoundary_(globalPos) && 0.5 < lambda && lambda < 2.0/3.0;
    }

    Scalar temperature_;
    Scalar eps_;
    std::string name_;
};
} //end namespace

#endif // HAVE_ALUGRID || HAVE_DUNE_ALUGRID || HAVE_UGGRID

#endif
