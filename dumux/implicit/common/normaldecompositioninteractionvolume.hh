#ifndef DUMUX_NORMALDECOMPOSITION_INTERACTIONVOLUME_HH
#define DUMUX_NORMALDECOMPOSITION_INTERACTIONVOLUME_HH

#include "conormaldecompositioninteractionvolume.hh"
#include <dumux/common/math.hh>

namespace Dumux{

template<class TypeTag>
class NormalDecompositionInteractionVolume: public CoNormalDecompositionInteractionVolume<TypeTag>
{
private:

    typedef CoNormalDecompositionInteractionVolume<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;


public:
    const CoNormalDecompositionFaceStencil<TypeTag>* getNormalFaceStencilVectorPlus(std::pair<int,int> index) const
    {
        return &faceNormalStencilVectorPlus_[this->getLocalIndex(index)];
    }

    const CoNormalDecompositionFaceStencil<TypeTag>* getNormalFaceStencilVectorMinus(std::pair<int,int> index) const
    {
        return &faceNormalStencilVectorMinus_[this->getLocalIndex(index)];
    }

    NormalDecompositionInteractionVolume(): ParentType()
    {

    }


public:
    std::vector<CoNormalDecompositionFaceStencil<TypeTag> > faceNormalStencilVectorPlus_;
    std::vector<CoNormalDecompositionFaceStencil<TypeTag> > faceNormalStencilVectorMinus_;
    VectorFieldVector faceProjectionPoints_;

};
}
#endif /* NormalDecompositionInteractionVolume_HH_ */
