// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_1PWEAKCONV_PROBLEM_HH
#define DUMUX_1PWEAKCONV_PROBLEM_HH


#if PROBLEM == 1
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/cornerpoint/cpelementvolumevariables.hh>
#include <dumux/implicit/cornerpoint/cpfvelementgeometry.hh>
#include <dumux/implicit/cornerpoint/cpdarcyfluxvariables.hh>
#include "resultevaluation2dtpfa.hh"
#elif PROBLEM == 2
#include <dumux/implicit/1pmpfa/1pmpfamodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#include "resultevaluationsingularity2d.hh"
#elif PROBLEM == 3
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#elif PROBLEM == 4
#include <dumux/implicit/1pMPFA/1pmodel.hh>
#include <dumux/implicit/mpfa/mpfaimplicitporousmediaproblem.hh>
#include "resultevaluation2dmpfa.hh"
#endif
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/unit.hh>

//#include <dumux/linear/amgbackend.hh>

#include "1pweakconvspatialparams.hh"

#include <dune/geometry/quadraturerules.hh>

namespace Dumux
{
template <class TypeTag>
class OnePWeakConvProblem;

namespace Properties
{

NEW_TYPE_TAG(OnePWeakConvProblem, INHERITS_FROM(OneP));
#if PROBLEM == 1
NEW_TYPE_TAG(OnePWeakConvCCProblem, INHERITS_FROM(CCModel, OnePWeakConvProblem));
#elif PROBLEM == 2
NEW_TYPE_TAG(OnePWeakConvCCMpfaProblem, INHERITS_FROM(CCMpfaModel, OnePWeakConvProblem));
#elif PROBLEM == 3
NEW_TYPE_TAG(OnePWeakConvBoxProblem, INHERITS_FROM(BoxModel, OnePWeakConvProblem));
#elif PROBLEM == 4
NEW_TYPE_TAG(OnePWeakConvMpfaProblem, INHERITS_FROM(MpfaModel, OnePWeakConvProblem));
SET_INT_PROP(OnePWeakConvMpfaProblem, MpfaMethod, Dumux::MpfaMethods::lMethod);
#endif

SET_PROP(OnePWeakConvProblem, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the grid type
SET_TYPE_PROP(OnePWeakConvProblem, Grid, Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>);

// Set the problem property
SET_TYPE_PROP(OnePWeakConvProblem, Problem, Dumux::OnePWeakConvProblem<TypeTag> );

// Set the spatial parameters
SET_TYPE_PROP(OnePWeakConvProblem, SpatialParams, Dumux::OnePWeakConvSpatialParams<TypeTag> );

// Linear solver settings
SET_TYPE_PROP(OnePWeakConvProblem, LinearSolver, Dumux::SuperLUBackend<TypeTag> );

// Enable gravity
SET_BOOL_PROP(OnePWeakConvProblem, ProblemEnableGravity, false);

NEW_PROP_TAG(BaseProblem);

#if PROBLEM == 1
SET_TYPE_PROP(OnePWeakConvCCProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
SET_TYPE_PROP(OnePWeakConvCCProblem, ElementVolumeVariables, CpElementVolumeVariables<TypeTag>);
SET_TYPE_PROP(OnePWeakConvCCProblem, FVElementGeometry, CpFVElementGeometry<TypeTag>);
SET_TYPE_PROP(OnePWeakConvCCProblem, FluxVariables, CpDarcyFluxVariables<TypeTag>);
#elif PROBLEM == 2
SET_TYPE_PROP(OnePWeakConvCCMpfaProblem, BaseProblem, ImplicitMpfaPorousMediaProblem<TypeTag>);
#elif PROBLEM == 3
SET_TYPE_PROP(OnePWeakConvCCMpfaProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
#elif PROBLEM == 4
SET_TYPE_PROP(OnePWeakConvMpfaProblem, BaseProblem, MpfaImplicitPorousMediaProblem<TypeTag>);
#endif

}

template <class TypeTag>
class OnePWeakConvProblem : public GET_PROP_TYPE(TypeTag, BaseProblem)
{

    typedef typename GET_PROP_TYPE(TypeTag, BaseProblem) ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };


    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;

    //quadrature Rule
    typedef Dune::QuadratureRule<Scalar, dim> Quad;
    typedef typename Quad::iterator QuadIterator;
    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::QuadratureRule<Scalar, dim-1> QuadFace;
    typedef typename QuadFace::iterator QuadIteratorFace;
    typedef typename Dune::ReferenceElements<Scalar, dim-1> ReferenceElementsFace;
    typedef typename Dune::ReferenceElement<Scalar, dim-1> ReferenceElementFace;

public:
    OnePWeakConvProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        pi_ = 4.0*atan(1.0);

        a_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, a1);
        a_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, a2);
        a_[2] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, a3);
        a_[3] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, a4);

        b_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, b1);
        b_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, b2);
        b_[2] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, b3);
        b_[3] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, b4);

        alpha_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, double, Problem, alpha);

        sourceTerm_.resize(this->gridView().size(0));

        beta_ = 7.0/16.0*pi_;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {

        DimWorldMatrix K = this->spatialParams().intrinsicPermeability(element);
        values = 0;

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int gIdx = this->elementMapper().index(element);
#else
            int gIdx = this->elementMapper().map(element);
#endif

        //get the Gaussian quadrature rule for intervals
        const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());
        const Quad &quad = Dune::QuadratureRules<Scalar, dim>::rule(referenceElement.type(), 4);
        //std::cout << "Using quadrature rule of order: " << quad.order() << std::endl;
        const QuadIterator qend = quad.end();

        QuadIterator qp = quad.begin();
        for(; qp != qend; ++qp)
        {
            //std::cout << qp->position() << std::endl;
            GlobalPosition globalPos = element.geometry().global(qp->position());

            Scalar x = globalPos[0];
            Scalar y = globalPos[1];

//            Scalar theta = theta_(globalPos);
//            int region = region_(globalPos);

//            Scalar factor_1 = x*x*(a_[region-1] * cos(alpha_*theta) + b_[region-1] * sin(alpha_*theta));
//            Scalar factor_2 = x*y*(-2.0 * b_[region-1] * cos(alpha_*theta) + 2.0 * a_[region-1] * sin(alpha_*theta));
//            Scalar factor_3 = y*y*(-a_[region-1] * cos(alpha_*theta) - b_[region-1] * sin(alpha_*theta));
//
//            Scalar factor = alpha_ * (alpha_-1)*std::pow((x*x + y*y),alpha_/2.0 -2) * (factor_1 + factor_2 + factor_3);

            GlobalPosition gradRad = exactGradRad(globalPos);
            GlobalPosition dr = dr_(globalPos);
            GlobalPosition dtheta = dtheta_(globalPos);
            GlobalPosition ddr = ddr_(globalPos);
            GlobalPosition ddtheta = ddtheta_(globalPos);
            Dune::FieldMatrix<Scalar,dim,dim> Hessian = HessianRad(globalPos);

            Scalar sxx = Hessian[0][0]*dr[0]*dr[0] + 2.0*Hessian[0][1]*dr[0]*dtheta[0] + Hessian[1][1]*dtheta[0]*dtheta[0]
                         + gradRad[0]*ddr[0] + gradRad[1]*ddtheta[0];
            Scalar syy = -sxx;

//            if(std::abs(sxx - factor) > 1.0e-12)
//                std::cout << "Source Check: " << sxx - factor << std::endl;

            Scalar integrationElement = element.geometry().integrationElement(qp->position());
            values += -(K[0][0]*sxx + K[1][1]*syy)*qp->weight()*integrationElement;
        }

        values /=element.geometry().volume();

		sourceTerm_[gIdx][0] = values;

    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        values.setAllDirichlet();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = exact(globalPos);
    }


    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        priVars[pressureIdx] = exact(element.geometry().center());
    }


    void resultEvaluation()
    {
#if PROBLEM == 1 || PROBLEM == 2 ||PROBLEM == 4
        result.evaluate(*this, true);

        std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
        std::cout.precision(2);
        std::cout
                << "\t pErrorL2 \t pErrorL2_absolute \t pInnerErrorL2 \t vErrorL2 \t vInnerErrorL2 \t faceFluxErrorL2 \t faceFluxBoundErrorL2 \t faceFluxInnerErrorL2 \t faceFluxErrorL2Absolute t\ faceFluxErrorL2_Singularity \t hMax"
                << std::endl;
        std::cout << "\t " << result.relativeL2Error << "\t " << result.absoluteL2Error << "\t\t " << result.relativeL2ErrorIn << "\t "
                << result.ervell2 << "\t " << result.ervell2In << "\t " << result.realtiveL2ErrorFaceFluxes << "\t\t " << result.realtiveL2ErrorFaceFluxesBound << "\t\t " << result.realtiveL2ErrorFaceFluxesInner << "\t\t " << result.absoluteL2ErrorFaceFluxes
                << "\t\t " << result.realtiveL2ErrorFaceFluxes_withoutSingularity<< "\t" << "\t\t " << result.hMax  << std::endl;
#endif
    }


    void addOutputVtkFields()
    {

        resultEvaluation();
        ScalarField *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarField *localRelativeFaceFluxError = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarField *sourceTerm = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarField *volume = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int gIdx = this->elementMapper().index(*eIt);
#else
            int gIdx = this->elementMapper().map(*eIt);
#endif

            (*exactPressure)[gIdx][0] = exact(eIt->geometry().center());
            (*volume)[gIdx][0] = eIt->geometry().volume();
        }
#if PROBLEM == 1 || PROBLEM == 2 ||PROBLEM == 4
        *localRelativeFaceFluxError = result.localRelativeFaceFluxError;
        this->resultWriter().attachCellData(*localRelativeFaceFluxError, "localFluxError");
#endif

        *sourceTerm = sourceTerm_;
        this->resultWriter().attachCellData(*sourceTerm, "source term");
        this->resultWriter().attachCellData(*exactPressure, "exact pressure");
        this->resultWriter().attachCellData(*volume, "volume");

        return;
    }

    //! exakte Lösung für Fehlerberechnung in resultevaluation3d.hh und das Setzen der RB
    Scalar exact (const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar theta = theta_(globalPos);
        Scalar r = r_(globalPos);

        int region = region_(globalPos);

        Scalar press = 10.0 + pow(r,alpha_)*(a_[region-1]*cos(alpha_*theta) + b_[region-1]*sin(alpha_*theta));

        return press;

    }

    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
    {

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar theta = theta_(globalPos);
        Scalar r = r_(globalPos);

        GlobalPosition grad(0);

        GlobalPosition gradRad = exactGradRad(globalPos);
        GlobalPosition dr = dr_(globalPos);
        GlobalPosition dtheta = dtheta_(globalPos);

        grad[0] =  gradRad[0]*dr[0] + gradRad[1]*dtheta[0];
        grad[1] =  gradRad[0]*dr[1] + gradRad[1]*dtheta[1];

        return grad;
    }


    Dune::FieldVector<Scalar,dim> exactGradRad(const GlobalPosition& globalPos) const
    {

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar theta = theta_(globalPos);
        Scalar r = r_(globalPos);

        int region = region_(globalPos);

        GlobalPosition grad(0);

        grad[0] =  alpha_*pow(r,(alpha_ - 1.0))*(a_[region-1]*cos(alpha_*theta) + b_[region-1]*sin(alpha_*theta));
        grad[1] =  pow(r,alpha_)*(alpha_*b_[region-1]*cos(alpha_*theta) - a_[region-1]*alpha_*sin(alpha_*theta));

        return grad;
    }

    Dune::FieldMatrix<Scalar,dim,dim> HessianRad(const GlobalPosition& globalPos) const
    {

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        Scalar theta = theta_(globalPos);
        Scalar r = r_(globalPos);

        int region = region_(globalPos);

        Dune::FieldMatrix<Scalar,dim,dim> Hessian(0);

        Hessian[0][0] = (alpha_ - 1.0)*alpha_*pow(r,(alpha_ - 2.0))*(a_[region-1]*cos(alpha_*theta) + b_[region-1]*sin(alpha_*theta));
        Hessian[0][1] = alpha_*alpha_*pow(r,(alpha_ - 1))*(b_[region-1]*cos(alpha_*theta) - a_[region-1]*sin(alpha_*theta));
        Hessian[1][0] = Hessian[0][1];
        Hessian[1][1] = -alpha_*alpha_*pow(r,alpha_)*(a_[region-1]*cos(alpha_*theta) + b_[region-1]*sin(alpha_*theta));

        return Hessian;
    }

    Scalar r_(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        return std::sqrt(x*x + y*y);
    }

    GlobalPosition dr_(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        GlobalPosition dr(0);

        dr[0] = x/r_(globalPos);
        dr[1] = y/r_(globalPos);

        return dr;
    }

    GlobalPosition ddr_(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        GlobalPosition ddr(0);

        ddr[0] = 1.0/r_(globalPos) - (x*x)/std::pow(r_(globalPos),3);
        ddr[1] = 1.0/r_(globalPos) - (y*y)/std::pow(r_(globalPos),3);

        return ddr;
    }

    Scalar theta_(const GlobalPosition& globalPos) const
    {
        Scalar theta = 0.0;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        int region = region_(globalPos);

        if(region == 1 || region == 2) theta = std::atan2(y,x);
        else if(region == 3 || region == 4) theta = 2*pi_ + std::atan2(y,x);

        return theta;
    }

    GlobalPosition dtheta_(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar r = r_(globalPos);

        GlobalPosition dtheta(0);

        dtheta[0] = -y/(std::pow(r,2.0));
        dtheta[1] =  x/(std::pow(r,2.0));

        return dtheta;
    }

    GlobalPosition ddtheta_(const GlobalPosition& globalPos) const
    {
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        Scalar r = r_(globalPos);

        GlobalPosition ddtheta(0);

        ddtheta[0] = 2.0*x*y/(std::pow(r,4.0));
        ddtheta[1] =  -2.0*x*y/(std::pow(r,4.0));

        return ddtheta;
    }


    int region_(const GlobalPosition& globalPos) const
    {
        int region = 1;

        Scalar x = globalPos[0];
        Scalar y = globalPos[1];

        if((y - tan(beta_)*x)<=0 && y>=0) region = 1;
        else if((y - tan(beta_)*x)>0 && y>=0) region = 2;
        else if((y - tan(beta_)*x)>0 && y<0) region = 3;
        else region = 4;

        return region;
    }

private:
    mutable ScalarField sourceTerm_;
    std::string name_;
    Dune::FieldVector<Scalar,4> a_;
    Dune::FieldVector<Scalar,4> b_;
    Scalar alpha_;
    Scalar beta_;
    Scalar pi_;
#if PROBLEM == 1 || PROBLEM == 2 || PROBLEM == 4
    Dumux::ResultEvaluation<TypeTag> result;
#endif

};
} //end namespace

#endif
