// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_FVCA6_TEST1_PROBLEM_HH
#define DUMUX_FVCA6_TEST1_PROBLEM_HH

#if HAVE_UG
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif
#if HAVE_ALUGRID
#include <dune/grid/alugrid/3d/alugrid.hh>
#include <dune/grid/io/file/dgfparser/dgfalu.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/gmshreader.hh>

#if PROBLEM == 1
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#elif PROBLEM == 2
#include <dumux/implicit/1pmpfa/1pmpfamodel.hh>
#include <dumux/implicit/commonmpfa/implicitmpfaporousmediaproblem.hh>
#elif PROBLEM == 3
#include <dumux/implicit/1p/1pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#endif
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/unit.hh>

//#include <dumux/linear/amgbackend.hh>

#include "test1_fvca6spatialparams.hh"

#include "../resultevaluation2d.hh"
#include <dune/geometry/quadraturerules.hh>

namespace Dumux
{
template <class TypeTag>
class Test1Problem;

namespace Properties
{

NEW_TYPE_TAG(Test1Problem, INHERITS_FROM(OneP));
#if PROBLEM == 1
NEW_TYPE_TAG(Test1CCProblem, INHERITS_FROM(CCModel, Test1Problem));
#elif PROBLEM == 2
NEW_TYPE_TAG(Test1CCMpfaProblem, INHERITS_FROM(CCMpfaModel, Test1Problem));
#elif PROBLEM == 3
NEW_TYPE_TAG(Test1BoxProblem, INHERITS_FROM(BoxModel, Test1Problem));
#endif

SET_PROP(Test1Problem, Fluid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::Unit<Scalar> > type;
};

// Set the grid type
//SET_TYPE_PROP(Test1Problem, Grid, Dune::YaspGrid<2>);
//SET_TYPE_PROP(Test1Problem, Grid, Dune::UGGrid<3>);
//SET_TYPE_PROP(Test1Problem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);

// Set the grid type
#if HAVE_ALUGRID || HAVE_DUNE_ALUGRID
SET_TYPE_PROP(Test1Problem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::nonconforming>);
#else
SET_TYPE_PROP(Test1Problem, Grid, Dune::UGGrid<3>);
#endif

// Set the problem property
SET_TYPE_PROP(Test1Problem, Problem, Dumux::Test1Problem<TypeTag> );

// Set the spatial parameters
SET_TYPE_PROP(Test1Problem, SpatialParams, Dumux::Test1SpatialParams<TypeTag> );

// Linear solver settings
#if HAVE_SUPERLU
SET_TYPE_PROP(Test1Problem, LinearSolver, Dumux::SuperLUBackend<TypeTag>);
//#else
//SET_TYPE_PROP(Test1Problem, LinearSolver, Dumux::ILUnRestartedGMResBackend<TypeTag>);
//SET_INT_PROP(Test1Problem, LinearSolverGMResRestart, 80);
//SET_INT_PROP(Test1Problem, LinearSolverMaxIterations, 1000);
//SET_SCALAR_PROP(Test1Problem, LinearSolverResidualReduction, 1e-8);
//SET_SCALAR_PROP(Test1Problem, LinearSolverPreconditionerIterations, 1);
#endif

//NEW_TYPE_TAG(Test1BoxProblemWithAMG, INHERITS_FROM(Test1BoxProblem));
//NEW_TYPE_TAG(Test1CCProblemWithAMG, INHERITS_FROM(Test1CCProblem));
//// Solver settings for the tests using AMG
//SET_TYPE_PROP(Test1BoxProblemWithAMG, LinearSolver, Dumux::AMGBackend<TypeTag> );
//SET_TYPE_PROP(Test1CCProblemWithAMG, LinearSolver, Dumux::AMGBackend<TypeTag> );


// Enable gravity
SET_BOOL_PROP(Test1Problem, ProblemEnableGravity, false);
}

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::YaspGrid<2> type;</tt> to
 * <tt>typedef Dune::YaspGrid<3> type;</tt> in the problem file
 * and use <tt>test_1p_3d.dgf</tt> in the parameter file.
 */

#if PROBLEM == 1
template <class TypeTag>
class Test1Problem : public ImplicitPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 2
template <class TypeTag>
class Test1Problem : public ImplicitMpfaPorousMediaProblem<TypeTag>
{
#elif PROBLEM == 3
template <class TypeTag>
class Test1Problem : public ImplicitPorousMediaProblem<TypeTag>
{
#endif

#if PROBLEM == 1
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 2
    typedef ImplicitMpfaPorousMediaProblem<TypeTag> ParentType;
#elif PROBLEM == 3
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
#endif



    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };


    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;

    //quadrature Rule
    typedef Dune::QuadratureRule<Scalar, dim> Quad;
    typedef typename Quad::iterator QuadIterator;
    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::QuadratureRule<Scalar, dim-1> QuadFace;
    typedef typename QuadFace::iterator QuadIteratorFace;
    typedef typename Dune::ReferenceElements<Scalar, dim-1> ReferenceElementsFace;
    typedef typename Dune::ReferenceElement<Scalar, dim-1> ReferenceElementFace;

public:
    Test1Problem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);

        int globalRefine = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             int,
                                             Problem,
                                             GlobalRefine);

        Grid& grid = GridCreator::grid();

        grid.globalRefine(globalRefine);
//      ElementIterator eIt = grid.leafGridView().template begin<0>();
//      ElementIterator eEndIt = grid.leafGridView().template end<0>();
//      for(;eIt != eEndIt; ++eIt)
//      {
//          GlobalPosition center = eIt->geometry().center();
//          //if(center[0]<0.5 && center[1]<0.5 && center[2]<0.5)
//          if(center[0]<0.75 && center[0]>0.25 && center[1]<0.75 && center[1]>0.25 )
//          {
//              grid.mark(1,*eIt);
//          }
//
//      }
//
//      grid.adapt();
//      // delete marks
//      grid.postAdapt();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param values Stores the source values, acts as return value
     * \param globalPos The global position
     */
//void sourceAtPos(PrimaryVariables &values,const GlobalPosition& globalPos) const
//    {
//        values = 0;
//
//        double pi = 4.0*atan(1.0);
//
//        values = -pi*pi*cos(pi*globalPos[0])*cos(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0))+
//                            3.0*pi*pi*sin(pi*globalPos[0])*sin(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0))-
//                            pi*pi*sin(pi*globalPos[0])*cos(pi*(globalPos[1]+1.0/2.0))*cos(pi*(globalPos[2]+1.0/3.0));
//    }

    void source(PrimaryVariables &values,
                const Element &element,
                  const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {

        DimWorldMatrix K = this->spatialParams().intrinsicPermeability();
        values = 0;

        //get the Gaussian quadrature rule for intervals
        const ReferenceElement& referenceElement = ReferenceElements::general(element.geometry().type());
        const Quad &quad = Dune::QuadratureRules<Scalar, dim>::rule(referenceElement.type(), 5);
        //std::cout << "Using quadrature rule of order: " << quad.order() << std::endl;
        const QuadIterator qend = quad.end();

        QuadIterator qp = quad.begin();
        for(; qp != qend; ++qp)
        {
            //std::cout << qp->position() << std::endl;
            double pi = 4.0*atan(1.0);
            GlobalPosition globalPos = element.geometry().global(qp->position());

            Scalar val = -pi*pi*cos(pi*globalPos[0])*cos(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0))+
                            3.0*pi*pi*sin(pi*globalPos[0])*sin(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0))-
                            pi*pi*sin(pi*globalPos[0])*cos(pi*(globalPos[1]+1.0/2.0))*cos(pi*(globalPos[2]+1.0/3.0));


            Scalar integrationElement = element.geometry().integrationElement(qp->position());
            values += val*qp->weight()*integrationElement;
        }

        values /=element.geometry().volume();

    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        //Scalar eps = 1.0e-10;

//        if(globalPos[0] < eps || globalPos[0] > 1 -eps)
//        {
            values.setAllDirichlet();
//        }
//        else{
//          values.setAllNeumann();
//        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = exact(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
//    using ParentType::neumann;
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        DimWorldMatrix K = this->spatialParams().intrinsicPermeability();
        values = 0;

        //get the Gaussian quadrature rule for intervals
        const ReferenceElementFace& referenceElementFace = ReferenceElementsFace::general(intersection.geometry().type());
        const QuadFace &quad = Dune::QuadratureRules<Scalar, dim-1>::rule(referenceElementFace.type(), 5);
        //std::cout << "Using quadrature rule of order: " << quad.order() << std::endl;
        const QuadIteratorFace qend = quad.end();

        QuadIteratorFace qp = quad.begin();
        for(; qp != qend; ++qp)
        {
            //std::cout << qp->position() << std::endl;
            GlobalPosition globalPos = intersection.geometry().global(qp->position());

            GlobalPosition grad(exactGrad(globalPos));
            GlobalPosition n(intersection.centerUnitOuterNormal());
            GlobalPosition d(dim);

            K.mv(n,d);

            Scalar integrationElement = intersection.geometry().integrationElement(qp->position());
            Scalar val = grad*d;
            values -= val*qp->weight()*integrationElement;
        }

        values /=intersection.geometry().volume();
    }

//    void neumannAtPos(PrimaryVariables &values,
//                      const GlobalPosition &globalPos) const
//    {
//      Scalar eps = 1.0e-5;
//      Scalar x = globalPos[0];
//      Scalar y = globalPos[1];
//
//      DimWorldMatrix K = this->spatialParams().intrinsicPermeability();
//
//      values = 0.0;
//
//      GlobalPosition grad(exactGrad(globalPos));
//      GlobalPosition n(dim);
//      GlobalPosition d(dim);
//
//      if(y < eps){
//
//          n[0] = 0;
//          n[1] = -1;
//      }
//      else
//      {
//          n[0] = 0;
//          n[1] = 1;
//      }
//
//      K.mv(n,d);
//
//      values -= grad*d;
//
//
//    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        priVars[pressureIdx] = exact(element.geometry().center());
    }

    void resultEvaluation()
    {
        result.evaluate(this->grid().leafGridView(), *this, true);

        std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
        std::cout.precision(2);
        std::cout
                << "\t pErrorL2 \t pInnerErrorL2 \t vErrorL2 \t vInnerErrorL2 \t faceFluxErrorL2 \t faceFluxBoundErrorL2 \t faceFluxInnerErrorL2 \t faceFluxInnerNoIsErrorL2 \t hMax \t\t pMin\t\t pMax\t\t pMinExact\t pMaxExact\t nGrad"
                << std::endl;
        std::cout << "\t " << result.relativeL2Error << "\t " << result.relativeL2ErrorIn << "\t "
                << result.ervell2 << "\t " << result.ervell2In << "\t " << result.realtiveL2ErrorFaceFluxes << "\t\t " << result.realtiveL2ErrorFaceFluxesBound << "\t\t " << result.realtiveL2ErrorFaceFluxesInner << "\t\t " << result.realtiveL2ErrorFaceFluxesInnerNoIntersect
                << "\t" << "\t\t " << result.hMax << "\t " << result.uMin << "\t " << result.uMax << "\t " << result.uMinExact << "\t " << result.uMaxExact << "\t "
                << result.ngrad << "\t " << std::endl;
    }

    void addOutputVtkFields()
    {

        resultEvaluation();
        ScalarField *exactPressure = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));
        ScalarField *localRelativeFaceFluxError = this->resultWriter().allocateManagedBuffer(this->gridView().size(0));

        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for(;eIt != eEndIt; ++eIt)
        {

#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int gIdx = this->elementMapper().index(*eIt);
#else
            int gIdx = this->elementMapper().map(*eIt);
#endif

            (*exactPressure)[gIdx][0] = exact(eIt->geometry().center());
        }

        *localRelativeFaceFluxError = result.localRelativeFaceFluxError;

        this->resultWriter().attachCellData(*exactPressure, "exact pressure");
        this->resultWriter().attachCellData(*localRelativeFaceFluxError, "localFluxError");

        return;
    }

    //! exakte Lösung für Fehlerberechnung in resultevaluation3d.hh und das Setzen der RB
    Scalar exact (const GlobalPosition& globalPos) const
    {
        //Scalar x = globalPos[0];
        //Scalar y = globalPos[1];
        //
        //Scalar press = 1.0 + 16.0*x*(1-x)*y*(1-y);
        //
        //return press;
        double pi = 4.0*atan(1.0);

        return (1.0+sin(pi*globalPos[0])*sin(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0)));

    }
    //! Berechnung des exakten Gradienten für Fehlerberechung in resultevaluation3d.hh
    Dune::FieldVector<Scalar,dim> exactGrad (const GlobalPosition& globalPos) const
    {

        Dune::FieldVector<Scalar,dim> grad(0);
        double pi = 4.0*atan(1.0);
        grad[0] = pi*cos(pi*globalPos[0])*sin(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0));
        grad[1] = pi*sin(pi*globalPos[0])*cos(pi*(globalPos[1]+1.0/2.0))*sin(pi*(globalPos[2]+1.0/3.0));
        grad[2] = pi*sin(pi*globalPos[0])*sin(pi*(globalPos[1]+1.0/2.0))*cos(pi*(globalPos[2]+1.0/3.0));

        return grad;
    }

    // \}

private:
    std::string name_;
    Dumux::ResultEvaluation<TypeTag> result;
};
} //end namespace

#endif
