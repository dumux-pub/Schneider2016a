// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef TEST_NONLINEAR_SPATIALPARAMS_TESTTHREE_HH
#define TEST_NONLINEAR_SPATIALPARAMS_TESTTHREE_HH

#include <dumux/material/spatialparams/fvspatialparams1p.hh>

namespace Dumux
{

template<class TypeTag>
class TestNonlinearSpatialParamsTestThree: public FVSpatialParamsOneP<TypeTag>
{
    typedef FVSpatialParamsOneP<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum
        {dim=Grid::dimension, dimWorld=Grid::dimensionworld};
    typedef typename Grid::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;

public:

    const FieldMatrix& intrinsicPermeability (const Element& element) const
    {
        return permeability_;
    }

    double porosity(const Element& element) const
    {
        return 1.0;
    }

    TestNonlinearSpatialParamsTestThree(const GridView& gridView)
    : ParentType(gridView)
    {
        double k1_ = 1.0;
        double k2_ = 1.0e-3;

        double pi = 4.0*atan(1.0);
        double theta = pi*40.0/180;
        double cost = cos(theta);
        double sint = sin(theta);

        permeability_[0][0] = cost*cost*k1_ + sint*sint*k2_;
        permeability_[1][1] = sint*sint*k1_ + cost*cost*k2_;
        permeability_[0][1] = permeability_[1][0] = cost*sint*(k1_ - k2_);
    }

private:
    FieldMatrix permeability_;
};

} // end namespace
#endif
