#ifndef DUMUX_MPFA_O_INTERACTIONVOLUMECONTAINER_HH
#define DUMUX_MPFA_O_INTERACTIONVOLUMECONTAINER_HH


#include <dumux/common/math.hh>
#include <dumux/implicit/mpfa/mpfaproperties.hh>

namespace Dumux{

template<class TypeTag>
class MpfaOInteractionVolumeContainer
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaInteractionVolume) InteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, MpfaInteractionVolumeManager) InteractionVolumeManager;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

public:

    MpfaOInteractionVolumeContainer(): problemPtr_(0)
    {}

    void init(Problem &problem)
    {
        problemPtr_ = &problem;
        interactionVolumes_.clear();
        storedVolumes_.clear();

        // For the Mpfa-O method we have one interaction region for each vertex
        int numVertices = problem.gridView().size(dim);
        interactionVolumes_.resize(numVertices);

        for(int i = 0; i < numVertices; i++)
        {
            InteractionVolume* iaVolume = new InteractionVolume;
            interactionVolumes_[i] = iaVolume;
        }

        ElementIterator eIt = problem.gridView().template begin<0>();
        ElementIterator eEndIt = problem.gridView().template end<0>();
        for(; eIt != eEndIt; ++eIt)
        {
            // Reference element will be needed later on
            const ReferenceElement& referenceElement = ReferenceElements::general(eIt->geometry().type());

            IntersectionIterator isIt = problem.gridView().ibegin(*eIt);
            IntersectionIterator isEnd = problem.gridView().iend(*eIt);
            for(; isIt != isEnd; ++isIt)
            {
                // loop over nodes of facet
                for(int nodeIndex = 0; nodeIndex < isIt->geometry().corners(); nodeIndex++)
                {
                    // get node index w.r.t element
                    int localVertCorner = referenceElement.subEntity(isIt->indexInInside(), 1, nodeIndex, dim);

                    if (hasVolumeBeenStored(*eIt, isIt->indexInInside(), localVertCorner))
                        continue;

                    int volumeIndex = getVolumeIndex(*eIt, localVertCorner);

                    InteractionVolumeManager::fillInteractionVolume(interactionVolumes_[volumeIndex], *eIt, problem, isIt->indexInInside(), localVertCorner, volumeIndex);

                    storedVolumes_.insert(std::pair < int, bool> (volumeIndex, true));
                }
            }
        }
    }

    bool isMpfaOVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        return true;
    }

    bool isMpfaOVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        return true;
    }

    InteractionVolume* getInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        return interactionVolumes_[globalVertIdx];
    }

    InteractionVolume* getInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        return interactionVolumes_[globalVertIdx];
    }

    InteractionVolume* getMpfaOInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx)
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        return interactionVolumes_[globalVertIdx];
    }

    InteractionVolume* getMpfaOInteractionVolume(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        return interactionVolumes_[globalVertIdx];
    }

    bool hasVolumeBeenStored(const Element &element, int facetIdxOnElement, int localVertIdx) const
    {
        // get global vertex index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        std::map<int, bool>::const_iterator it = storedVolumes_.find(globalVertIdx);
        if (it != storedVolumes_.end())
            return true;
        else
            return false;
    }

    int getVolumeIndex(const Element &element, int localVertIdx) const
    {
        // get global node index
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        int globalVertIdx = problem_().vertexMapper().index(*(element.template subEntity<dim>(localVertIdx)));
#else
        int globalVertIdx = problem_().vertexMapper().map(*(element.template subEntity<dim>(localVertIdx)));
#endif

        return globalVertIdx;
    }

protected:
    /*!
     * \brief A reference to the problem on which the model is applied.
     */
    Problem &problem_()
    { return *problemPtr_; }
    /*!
     * \copydoc problem_()
     */
    const Problem &problem_() const
    { return *problemPtr_; }

private:
    Problem *problemPtr_;
    std::vector<InteractionVolume*> interactionVolumes_;

    std::map<int, bool> storedVolumes_;

};
}
#endif /* DUMUX_MPFA_O_INTERACTIONVOLUMECONTAINER_HH */
