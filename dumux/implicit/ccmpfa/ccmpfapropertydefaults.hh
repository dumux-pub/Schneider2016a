// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup CCProperties
 * \ingroup CCModel
 * \file
 *
 * \brief Default properties for cell centered models
 */
#ifndef DUMUX_CC_MPFA_PROPERTY_DEFAULTS_HH
#define DUMUX_CC_MPFA_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/commonmpfa/implicitmpfapropertydefaults.hh>
#include "ccmpfaassembler.hh"
//#include "ccmpfafvelementgeometry.hh"
#include "ccmpfacompfvelementgeometry.hh"
#include "ccmpfaelementboundarytypes.hh"
#include "ccmpfalocalresidual.hh"
#include "ccmpfaelementvolumevariables.hh"
#include "ccmpfaproperties.hh"

namespace Dumux {

// forward declarations
//template<class TypeTag> class CCMpfaModel;
//template<class TypeTag> class CCMpfaLocalResidual;
//template<class TypeTag> class CCMpfaElementBoundaryTypes;
//template<class TypeTag> class CCMpfaElementVolumeVariables;
//template<class TypeTag> class CCMpfaFVElementGeometry;

namespace Properties {
//! Set the default for the FVElementGeometry
//SET_TYPE_PROP(CCMpfaModel, FVElementGeometry, Dumux::CCMpfaFVElementGeometry<TypeTag>);
SET_TYPE_PROP(CCMpfaModel, FVElementGeometry, Dumux::CCMpfaCompFVElementGeometry<TypeTag>);

//! Set the default for the ElementBoundaryTypes
SET_TYPE_PROP(CCMpfaModel, ElementBoundaryTypes, Dumux::CCMpfaElementBoundaryTypes<TypeTag>);

//! Mapper for the degrees of freedoms.
SET_TYPE_PROP(CCMpfaModel, DofMapper, typename GET_PROP_TYPE(TypeTag, ElementMapper));

//! Set the BaseLocalResidual to CCLocalResidual
SET_TYPE_PROP(CCMpfaModel, BaseLocalResidual, Dumux::CCMpfaLocalResidual<TypeTag>);

//! An array of secondary variable containers
SET_TYPE_PROP(CCMpfaModel, ElementVolumeVariables, Dumux::CCMpfaElementVolumeVariables<TypeTag>);

//! Assembler for the global jacobian matrix
SET_TYPE_PROP(CCMpfaModel, JacobianAssembler, Dumux::CCMpfaAssembler<TypeTag>);

//! indicate that this is no box discretization
SET_BOOL_PROP(CCMpfaModel, ImplicitIsBox, false);



} // namespace Properties
} // namespace Dumux

#endif
