// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FVNONLINEARPRESSURE1P_HH
#define DUMUX_FVNONLINEARPRESSURE1P_HH


// dumux environment
#include <dumux/decoupled/common/fv/fvpressure.hh>
#include <dumux/decoupled/1p/1pproperties.hh>
#include <dumux/decoupled/common/fv/nonlinearflux/fvnonlinearinteractionvolume.hh>
#include <dune/istl/io.hh>
#include <dumux/io/vtkmultiwriter.hh>

/**
 * \file
 * \brief  Single Phase Finite Volume Model
 */

namespace Dumux
{

template<class TypeTag> class FVNonlinearPressure1P: public FVPressure<TypeTag>
{
    typedef FVPressure<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;

    typedef Dumux::VtkMultiWriter<GridView>  VtkMultiWriter;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, CellData) CellData;
    typedef typename SolutionTypes::ScalarSolution ScalarSolutionType;
    typedef typename GET_PROP_TYPE(TypeTag, PressureSolutionVector) PressureSolution;
    typedef typename GET_PROP_TYPE(TypeTag, PressureCoefficientMatrix) Matrix;
    typedef typename GET_PROP_TYPE(TypeTag, PressureRHSVector) RHSVector;

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;

    enum
    {
        dim = GridView::dimension, dimWorld = GridView::dimensionworld
    };

    enum
    {
        pressEqIdx = Indices::pressureEqIdx // only one equation!
    };

    enum
    {
        rhs = ParentType::rhs, matrix = ParentType::matrix
    };

    enum FaceTypes
    {
        homogeneous = 1,
        heterogeneous = 2,
        boundary = 0
    };

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;
    typedef Dune::FieldVector<Scalar, 2> EntryType;
    typedef Dune::FieldVector<Scalar, 3> FluxCoefficientVector;

    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef Dumux::FVNonlinearInteractionVolume<TypeTag> NonlinearInteractionVolume;
    typedef Dumux::FVNonlinearFaceStencil<TypeTag> NonlinearFaceStencil;


public:
    // Function which calculates the source entry
    void getSource(EntryType&, const Element&, const CellData&, const bool);

    // Function which calculates the storage entry
    void getStorage(EntryType& entry, const Element& element,
                    const CellData& cellData, const bool first)
    {
        entry = 0;
    }

    // Function which calculates the flux entry
    void getFlux(EntryType&, const Intersection&, const CellData&, const bool);
    // Function which calculates the boundary flux entry
    void getFluxOnBoundary(EntryType&,
    const Intersection&, const CellData&, const bool);

    bool calculatePressureBoundary(const Intersection& intersection, const CellData& cellData, Scalar& pressBound, DimVector boundaryPoint);

    void assemble(bool first);

    void assembleLinear(bool first);

    void assembleTPFA(bool first);

    void initialize(bool solveTwice = true)
    {
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            ElementPointer ele = *eIt;
            NonlinearInteractionVolume interactionVolume(ele);
            interactionVolumes_.push_back(interactionVolume);
        }

        initializeInteractionVolume();

        ParentType::initialize();

        numIter_ = 0;
        bool calculateInitError = false;

        error_ = this->f_;
        if(linearInitialization_)
        {
            this->assembleTPFA(true);
            this->solve();
            storePressureSolution();
            error_ = this->f_;
            (this->A_).mmv(this->pressure(), error_);
            //pressDiff -= pressureNew;
            this->writeOutput();
        }

        if(!linear_){

            double error_0 = 1.0;

            if(calculateInitError){
                this->assemble(true);
                PressureSolution pressureInit= this->pressure();
                PressureSolution diffVec = this->rightHandSide();
                //Matrix A = this->globalMatrix();
                (this->A_).mmv(pressureInit, diffVec);
                error_0 = diffVec.two_norm();
                this->solve();
                numIter_++;
                std::cout<<"Iter: " << numIter_ << std::endl;
                std::cout<<"Error: " << error_0 << std::endl;
                this->writeOutput();
            }
            Scalar error = 1.0e5;
            Scalar tolerance = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Flux, Tolerance);
            //error_0 = std::max(1.0,error_0);
            error_0 = 1.0;
            while(numIter_ < maxIter_ && error > tolerance*error_0){
                PressureSolution pressureOld = this->pressure();
                this->assemble(true);
                this->solve();
                storePressureSolution();
                PressureSolution pressureNew = this->pressure();
                error_ = this->f_;
                (this->A_).mmv(pressureOld, error_);
                //pressDiff -= pressureNew;
                error = error_.two_norm();
                numIter_++;
                std::cout<<"Iter: " << numIter_ << std::endl;
                std::cout<<"Error: " << error << std::endl;
                this->writeOutput();
            }
        }else{
            this->assembleLinear(true);
            this->solve();
            storePressureSolution();
            this->writeOutput();
        }

        return;
    }

    /*! \brief Pressure update
     *
     * \copydetails FVPressure::update()
     *
     */
    void update()
    {
        ParentType::update();
        storePressureSolution();
    }

    /*! \brief Globally stores the pressure solution
     *
     */
    void storePressureSolution()
    {
        int size = problem_.gridView().size(0);
        for (int i = 0; i < size; i++)
        {
            CellData& cellData = problem_.variables().cellData(i);
            storePressureSolution(i, cellData);
            cellData.fluxData().resetVelocity();
        }
    }

    /*! \brief Stores the pressure solution of a cell
     *
     * \param globalIdx Global cell index
     * \param cellData A CellData object
     */
    void storePressureSolution(int globalIdx, CellData& cellData)
    {
            Scalar press = this->pressure()[globalIdx];

            cellData.setPressure(press);
    }


    void writeOutput(int verbose = true)
    {
        if (problem_.gridView().comm().rank() == 0)
            std::cout << "Writing result file for current time step\n";
        if (!resultWriter_)
            resultWriter_ = Dune::make_shared<VtkMultiWriter>( problem_.gridView(), problem_.name());
        resultWriter_->beginWrite( numIter_);
        addOutputVtkFields(*resultWriter_);
        problem_. addOutputVtkFields(*resultWriter_);
        //addOutputVtkFields();
        resultWriter_->endWrite();
    }

    /*! \brief Adds pressure output to the output file
     *
     * Adds pressures to the output.
     *
     * \tparam MultiWriter Class defining the output writer
     * \param writer The output writer (usually a <tt>VTKMultiWriter</tt> object)
     *
     */
    template<class MultiWriter>
    void addOutputVtkFields(MultiWriter &writer)
    {
        ScalarSolutionType *pressure = writer.allocateManagedBuffer (
                problem_.gridView().size(0));

        *pressure = this->pressure();
        writer.attachCellData(*pressure, "pressure");

        if(!linear_){
            ScalarSolutionType *error = writer.allocateManagedBuffer (
                            problem_.gridView().size(0));
            *error  = error_;
             writer.attachCellData(*error, "error");
        }

        return;
    }

    //!Initialize the global matrix of the system of equations to solve
    void initializeMatrix();
    void initializeMatrixRowSize();
    void initializeMatrixIndices();

    void initializeInteractionVolume(){

        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {

            int globalIdx = problem_.variables().index(*eIt);

            std::vector<int> indexItToLocal;
            int numFaces = 0;
            int numFacesMinus = 0;
            std::vector<int> indexItToLocalMinus;
            bool isOnBoundary = false;

            //std::vector<FVNonlinearFaceStencil<TypeTag> > faceStencilVector_;
            std::vector<ElementPointer> neighboringElements;
            std::vector<IntersectionIterator>  elementIntersections;
            std::vector<int> inverseIndexItToLocal;
            std::vector<int> globalIdxNeighbors;
            VectorFieldVector connectionVectors;
            VectorFieldVector coNormals;
            VectorFieldVector cellCenters;
            VectorFieldVector faceCenters;
            DimMatrix permeability;
            DimVector cellCenter;
            VectorFieldMatrix neighborPermeabilities;
            VectorFieldVector normalVectors;
            VectorFieldVector coNormalVectors;
            NonlinearInteractionVolume interactionVolume(*eIt);
            std::vector<int> faceTypes;

            permeability = problem_.spatialParams().intrinsicPermeability(*eIt);

            int volumeType = 1;

            cellCenter = eIt->geometry().center();

            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            IntersectionIterator isIt = problem_.gridView().ibegin(*eIt);

            for (; isIt != isEndIt; ++isIt){

                elementIntersections.push_back(isIt);

                if(isIt->neighbor()){
                    int faceIdx = isIt->indexInInside();
                    indexItToLocal.push_back(faceIdx);

                    ElementPointer neighboringElement = isIt->outside();
                    int globalIdxJ = problem_.variables().index(*neighboringElement);

                    neighboringElements.push_back(neighboringElement);
                    globalIdxNeighbors.push_back(globalIdxJ);

                    DimMatrix neighborPermeability = problem_.spatialParams().intrinsicPermeability(*neighboringElement);
                    neighborPermeabilities.push_back(neighborPermeability);

                    DimMatrix permeabilityDiff = neighborPermeability;
                    permeabilityDiff -= permeability;

                    double normPermeabilityDiff = permeabilityDiff.frobenius_norm();
                    bool isHomogeneous = true;

                    if(normPermeabilityDiff < 1.0e-20)
                    {
                        faceTypes.push_back(homogeneous);
                        indexItToLocalMinus.push_back(-1);
                        isHomogeneous = true;
                    }else{
                        faceTypes.push_back(heterogeneous);
                        //indexItToLocalMinus.push_back(numFacesMinus);
                        indexItToLocalMinus.push_back(-1);
                        //numFacesMinus++;
                        isHomogeneous = false;
                    }

                    DimVector connectionVector(0);
                    cellCenters.push_back(neighboringElement->geometry().center());
                    faceCenters.push_back(isIt->geometry().center());

                    if(isHomogeneous)
                    {
                        connectionVector = neighboringElement->geometry().center();
                        connectionVector -= cellCenter;
                    }
                    else
                    {
                        connectionVector = isIt->geometry().center();
                        connectionVector -= cellCenter;
                    }

                    connectionVectors.push_back(connectionVector);

                    DimVector unitNormalVector = isIt->centerUnitOuterNormal();
                    normalVectors.push_back(unitNormalVector);

                    DimVector coNormal(0);
                    permeability.mv(unitNormalVector, coNormal);
                    Scalar faceArea = isIt->geometry().volume();
                    coNormal *= faceArea;
                    coNormalVectors.push_back(coNormal);

                    ++numFaces;
                }else{
                    int faceIdx = isIt->indexInInside();
                    indexItToLocal.push_back(faceIdx);

                    isOnBoundary = true;
                    faceTypes.push_back(boundary);
                    indexItToLocalMinus.push_back(numFacesMinus);
                    numFacesMinus++;

                    volumeType = 0;
                    //ElementPointer neighboringElement = isIt->outside();
                    int globalIdxJ = -1;

                    DimVector connectionVector(0);
                    connectionVector = isIt->geometry().center();
                    connectionVector -= cellCenter;
                    connectionVectors.push_back(connectionVector);

                    cellCenters.push_back(isIt->geometry().center());
                    faceCenters.push_back(isIt->geometry().center());

                    neighboringElements.push_back(*eIt);
                    globalIdxNeighbors.push_back(globalIdxJ);
                    //FVNonlinearFaceStencil<TypeTag> faceStencil(faceIdx, element_, neighboringElement);

                    neighborPermeabilities.push_back(problem_.spatialParams().intrinsicPermeability(*eIt));
                    DimVector unitNormalVector = isIt->centerUnitOuterNormal();
                    normalVectors.push_back(unitNormalVector);

                    DimVector coNormal(0);
                    permeability.mv(unitNormalVector, coNormal);
                    Scalar faceArea = isIt->geometry().volume();
                    coNormal *= faceArea;
                    coNormalVectors.push_back(coNormal);

                    ++numFaces;
                }

            }

            interactionVolume.setPermeability(permeability);
            interactionVolume.setNeighboringPermeabilities(neighborPermeabilities);
            interactionVolume.setNeighboringElements(neighboringElements);
            interactionVolume.setElementIntersections(elementIntersections);
            interactionVolume.setNormalVectors(normalVectors);
            interactionVolume.setCoNormalVectors(coNormalVectors);
            interactionVolume.setConnectionVectors(connectionVectors);
            interactionVolume.setIndexItLocal(indexItToLocal);
            interactionVolume.setNumFaces(numFaces);
            interactionVolume.calculateInverseIndexItLocal();
            interactionVolume.setVolumeType(volumeType);
            interactionVolume.setGlobalIdxNeighboringElements(globalIdxNeighbors);
            interactionVolume.setFaceTypes(faceTypes);
            interactionVolume.setIndexItToLocalMinus(indexItToLocalMinus);
            interactionVolume.setCellCenter(cellCenter);
            interactionVolume.setCellCenters(cellCenters);
            interactionVolume.setFaceCenters(faceCenters);
            interactionVolume.setGlobalIdx(globalIdx);
            interactionVolume.setStored();

            isEndIt = problem_.gridView().iend(*eIt);
            isIt = problem_.gridView().ibegin(*eIt);

            interactionVolume.calculateFaceInterpolationPoints(isIt, isEndIt);

            isEndIt = problem_.gridView().iend(*eIt);
            isIt = problem_.gridView().ibegin(*eIt);

            interactionVolume.calculateStencilsPlus(isIt, isEndIt);

            if(isOnBoundary){
            isEndIt = problem_.gridView().iend(*eIt);
            isIt = problem_.gridView().ibegin(*eIt);

            interactionVolume.calculateStencilsMinus(isIt, isEndIt);
            }

            interactionVolumes_[globalIdx] = interactionVolume;
        }
    }

    void calculateVelocity(const Intersection& intersection, CellData& cellData)
    {
        ElementPointer elementI = intersection.inside();

        // get the global index of the cell
        int globalIdxI = this->problem_.variables().index(*elementI);

        EntryType entries(0.);

        Scalar faceFlux = 0.0;

        int faceIdx = intersection.indexInInside();
        int faceIdxNeigh = -1;

        /************* handle interior face *****************/
        bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

        if (intersection.neighbor())
        {
            int faceIdx = intersection.indexInInside();

            bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

            ElementPointer elementNeighbor = intersection.outside();
            int faceIdxNeigh = intersection.indexInOutside();
            int globalIdxNeigh = problem_.variables().index(*elementNeighbor);

            int faceType = interactionVolumes_[globalIdxI].getFaceType(faceIdx);

            if(faceType == homogeneous || faceType == heterogeneous)
            {

            NonlinearFaceStencil faceStencil = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
            int numElements = faceStencil.getNumElements();
            //std::vector<ElementPointer> stencilElementVector = faceStencil.getStencilElementVector();
            std::vector<int> globalIdxNeigbors = faceStencil.getGlobalIdxNeighbors();
            DimVector coefficients = faceStencil.getCoefficients();
            std::vector<int> stencilFaceIdxVector = faceStencil.getStencilFaceIdxVector();
            std::vector<DimVector> stencilCenterPoints = faceStencil.getStencilCenterPoints();

            Scalar d_plus = 0.0;
            Scalar d_minus = 0.0;

            Scalar M_plus = 0.0;
            Scalar M_minus = 0.0;
            Scalar M_plusminus = 0.0;
            Scalar M_minusplus = 0.0;

            Scalar f_plus = 0.0;
            Scalar f_minus = 0.0;

            Scalar mu_plus = 0.5;
            Scalar mu_minus = 0.5;

                for(int i=0 ; i < numElements ; i++){

                    //ElementPointer elementNeighbor = stencilElementVector[i];
                    int globalIdxJ = globalIdxNeigbors[i];
                    int localFaceIdx = stencilFaceIdxVector[i];


                    if(globalIdxJ > -1){

                        M_plus += coefficients[i];

                            int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                            if(localFaceType == homogeneous){
                                if(globalIdxJ == globalIdxNeigh){
                                    M_plusminus += coefficients[i];
                                }else{
                                    d_plus += coefficients[i]*this->pressure(globalIdxJ);
                                }
                            }
                            else if(localFaceType == heterogeneous)
                            {
                                Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                                Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                                if(globalIdxJ == globalIdxNeigh){
                                    M_plus -= coefficients[i]*harmonicCoeff;
                                    M_plusminus += coefficients[i]*neighHarmonicCoeff;
                                }else{
                                    d_plus += coefficients[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                                    M_plus -= coefficients[i]*harmonicCoeff;
                                }


                            }

                        //}

                    }else{
                        entries = 0;

                        Scalar pressBound = 0.0;

                        bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellData, pressBound, stencilCenterPoints[i]);

                        if(!isNeumann)
                        {
                            M_plus += coefficients[i];
                            d_plus += coefficients[i]*pressBound;
                        }
                        else
                        {
                            d_plus += coefficients[i]*pressBound;
                        }

                    }
                }
                    NonlinearFaceStencil faceStencilNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeigh);
                    int numElementsNeigh = faceStencilNeigh.getNumElements();
                    std::vector<int> globalIdxNeigborsNeigh = faceStencilNeigh.getGlobalIdxNeighbors();
                    DimVector coefficientsNeigh = faceStencilNeigh.getCoefficients();
                    std::vector<int> stencilFaceIdxVectorNeigh = faceStencilNeigh.getStencilFaceIdxVector();
                    std::vector<DimVector> stencilCenterPointsNeigh = faceStencilNeigh.getStencilCenterPoints();

                    for(int i=0 ; i < numElementsNeigh ; i++){

                        int globalIdxJNeigh = globalIdxNeigborsNeigh[i];
                        int localFaceIdxNeigh = stencilFaceIdxVectorNeigh[i];


                        if(globalIdxJNeigh > -1){

                            M_minus += coefficientsNeigh[i];

                            int localFaceType = interactionVolumes_[globalIdxNeigh].getFaceType(localFaceIdxNeigh);

                            if(localFaceType == homogeneous){
                                if(globalIdxJNeigh == globalIdxI){
                                    M_minusplus += coefficientsNeigh[i];
                                }else{
                                    d_minus += coefficientsNeigh[i]*this->pressure(globalIdxJNeigh);
                                }
                            }
                            else if(localFaceType == heterogeneous)
                            {
                                Scalar harmonicCoeff = interactionVolumes_[globalIdxNeigh].getHarmonicAveragingCoefficient(localFaceIdxNeigh);
                                Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxNeigh].getNeighboringHarmonicAveragingCoefficient(localFaceIdxNeigh);

                                if(globalIdxJNeigh == globalIdxI){
                                    M_minus -= coefficientsNeigh[i]*harmonicCoeff;
                                    M_minusplus += coefficientsNeigh[i]*neighHarmonicCoeff;
                                }else{
                                    d_minus += coefficientsNeigh[i]*neighHarmonicCoeff*this->pressure(globalIdxJNeigh);
                                    M_minus -= coefficientsNeigh[i]*harmonicCoeff;
                                }


                            }
                        }else{
                            entries = 0;
                            Scalar pressBound = 0.0;

                            bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxNeigh].getElementIntersection(localFaceIdxNeigh)), cellData, pressBound,stencilCenterPointsNeigh[i]);

                            if(!isNeumann)
                            {
                                M_minus += coefficientsNeigh[i];
                                d_minus += coefficientsNeigh[i]*pressBound;
                            }
                            else
                            {
                                d_minus += coefficientsNeigh[i]*pressBound;
                            }

                        }

                }

                    mu_plus = 0.5;
                    mu_minus = 0.5;

                    if(d_plus > 1.0e-12 || d_minus > 1.0e-12 ){
                        mu_plus = d_minus/(d_minus + d_plus);
                        mu_minus = d_plus/(d_minus + d_plus);
                    }

                    M_plus *= mu_plus;
                    M_plus += mu_minus*M_minusplus;

                    M_minus *= mu_minus;
                    M_minus += mu_plus*M_plusminus;

                    faceFlux = M_plus*this->pressure(globalIdxI);
                    faceFlux -= M_minus*this->pressure(globalIdxNeigh);


            }
            else{
                DUNE_THROW(Dune::NotImplemented, "Unknown face type!!!");
            }
        }
        /************* boundary face ************************/

        else
        {

            BoundaryTypes bcType;
            problem_.boundaryTypes(bcType, intersection);

            if (bcType.isNeumann(pressEqIdx))
            {
                entries = 0;
                getFluxOnBoundary(entries, intersection, cellData, false);

                faceFlux = entries[matrix]*this->pressure(globalIdxI);
                faceFlux -= entries[rhs];

            }
            else{

                int faceIdx = intersection.indexInInside();

                bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

                //ElementPointer elementNeighbor = intersection.outside();
                //int faceIdxNeigh = intersection.indexInOutside();
                //int globalIdxNeigh = problem_.variables().index(*elementNeighbor);

                //if(!isOnBoundary){

                NonlinearFaceStencil faceStencilPlus = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
                int numElements = faceStencilPlus.getNumElements();
                //std::vector<ElementPointer> stencilElementVector = faceStencil.getStencilElementVector();
                std::vector<int> globalIdxNeigborsPlus = faceStencilPlus.getGlobalIdxNeighbors();
                DimVector coefficientsPlus = faceStencilPlus.getCoefficients();
                std::vector<int> stencilFaceIdxVectorPlus = faceStencilPlus.getStencilFaceIdxVector();
                std::vector<DimVector> stencilCenterPointsPlus = faceStencilPlus.getStencilCenterPoints();

                Scalar d_plus = 0.0;
                Scalar d_minus = 0.0;

                Scalar M_plus = 0.0;
                Scalar M_minus = 0.0;
                Scalar M_plusminus = 0.0;
                Scalar M_minusplus = 0.0;

                Scalar f_plus = 0.0;
                Scalar f_minus = 0.0;

                Scalar mu_plus = 0.5;
                Scalar mu_minus = 0.5;

                for(int i=0 ; i < numElements ; i++){

                   //ElementPointer elementNeighbor = stencilElementVector[i];
                   int globalIdxJ = globalIdxNeigborsPlus[i];
                   int localFaceIdx = stencilFaceIdxVectorPlus[i];


                   if(globalIdxJ > -1){

                    M_plus += coefficientsPlus[i];

                    int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                    if(localFaceType == homogeneous){
                        d_plus += coefficientsPlus[i]*this->pressure(globalIdxJ);
                    }
                    else if(localFaceType == heterogeneous){
                        Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                        Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                        d_plus += coefficientsPlus[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                        M_plus -= coefficientsPlus[i]*harmonicCoeff;
                    }


                   }else{
                       entries = 0;
                       Scalar pressBound = 0.0;

                       bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellData, pressBound, stencilCenterPointsPlus[i]);

                       if(!isNeumann)
                       {
                           M_plus += coefficientsPlus[i];

                           if(localFaceIdx == faceIdx) M_plusminus += coefficientsPlus[i];
                           else d_plus += coefficientsPlus[i]*pressBound;
                       }
                       else
                       {
                           d_plus += coefficientsPlus[i]*pressBound;
                       }

                   }
                }

                   NonlinearFaceStencil faceStencilMinus = interactionVolumes_[globalIdxI].getFaceStencilVectorMinus(faceIdx);
                   int numElementsMinus = faceStencilMinus.getNumElements();
                   std::vector<int> globalIdxNeigborsMinus = faceStencilMinus.getGlobalIdxNeighbors();
                   DimVector coefficientsMinus = faceStencilMinus.getCoefficients();
                   std::vector<int> stencilFaceIdxVectorMinus = faceStencilMinus.getStencilFaceIdxVector();
                   std::vector<DimVector> stencilCenterPointsMinus= faceStencilMinus.getStencilCenterPoints();

                   for(int i=0 ; i < numElementsMinus ; i++){

                       //ElementPointer elementNeighbor = stencilElementVector[i];
                       int globalIdxJ = globalIdxNeigborsMinus[i];
                       int localFaceIdx = stencilFaceIdxVectorMinus[i];


                       if(globalIdxJ > -1){

                        M_minus += coefficientsMinus[i];

                        if(globalIdxJ == globalIdxI){
                            M_minusplus += coefficientsMinus[i];
                        }
                        else {

                            int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                            if(localFaceType == homogeneous){
                                d_minus += coefficientsMinus[i]*this->pressure(globalIdxJ);
                            }
                            else if(localFaceType == heterogeneous){
                                Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                                Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                                d_minus += coefficientsMinus[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                                M_minus -= coefficientsMinus[i]*harmonicCoeff;
                            }
                        }

                    }else{
                        entries = 0;
                        Scalar pressBound = 0.0;
                        bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellData, pressBound,stencilCenterPointsMinus[i]);

                        if(!isNeumann)
                        {
                            M_minus += coefficientsMinus[i];
                            d_minus += coefficientsMinus[i]*pressBound;
                        }
                        else
                        {
                           d_minus += coefficientsMinus[i]*pressBound;
                        }
                    }

                }

               mu_plus = 0.5;
               mu_minus = 0.5;

               if(d_plus > 1.0e-12 || d_minus > 1.0e-12 ){
                mu_plus = d_minus/(d_minus + d_plus);
                mu_minus = d_plus/(d_minus + d_plus);
               }

               Scalar pressBound = 0.0;
               DimVector boundaryPoint =  faceStencilMinus.getCenterPoint();
               bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(faceIdx)), cellData, pressBound, boundaryPoint);

                M_plus *= mu_plus;
                M_plus += mu_minus*M_minusplus;

                M_minus *= mu_minus;
                M_minus += mu_plus*M_plusminus;

                faceFlux = M_plus*this->pressure(globalIdxI);
                faceFlux -= M_minus*pressBound;
            }
        }

            DimVector velocity = intersection.centerUnitOuterNormal();
            velocity *= faceFlux;
            velocity /= intersection.geometry().volume();

            cellData.fluxData().setVelocity(faceIdx, velocity);
            cellData.fluxData().setVelocityMarker(faceIdx);

        return;
    }

    FVNonlinearPressure1P(Problem& problem) :
        ParentType(problem), problem_(problem),
        gravity_(
        problem.gravity()),
        error_(0)
    {
        ElementIterator element = problem_.gridView().template begin<0> ();
        Scalar temperature = problem_.temperature(*element);
        Scalar referencePress = problem_.referencePressure(*element);

        density_ = Fluid::density(temperature, referencePress);
        viscosity_ = Fluid::viscosity(temperature, referencePress);

        linear_ = false;
        linearInitialization_ = false;
        maxIter_ = 100;

        try
        {
            linear_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Flux , Linear);
            linearInitialization_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Flux , LinearInitialization);
            maxIter_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Flux , MaxIter);
            //GridCreator::grid().globalRefine(numRefine);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

    }

private:
    Problem& problem_;
    const GlobalPosition& gravity_; //!< vector including the gravity constant
    Scalar density_;
    Scalar viscosity_;
    bool linear_;
    bool linearInitialization_;
    int maxIter_;
    std::vector<NonlinearInteractionVolume> interactionVolumes_;
    Dune::shared_ptr<VtkMultiWriter> resultWriter_;
    int numIter_;
    PressureSolution error_;
};

template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::getSource(EntryType& entry, const Element& element
        , const CellData& cellData, const bool first)
{
    // cell volume, assume linear map here
    Scalar volume = element.geometry().volume();

    // get sources from problem
    PrimaryVariables sourcePhase(0.0);
    problem_.source(sourcePhase, element);
        sourcePhase /= density_;

    entry[rhs] = volume * sourcePhase;

    return;
}

template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::getFlux(EntryType& entry, const Intersection& intersection
        , const CellData& cellData, const bool first)
{
    ElementPointer elementI = intersection.inside();
    ElementPointer elementJ = intersection.outside();

    // get global coordinates of cell centers
    const GlobalPosition& globalPosI = elementI->geometry().center();
    const GlobalPosition& globalPosJ = elementJ->geometry().center();

    //get face normal
    const Dune::FieldVector<Scalar, dim>& unitOuterNormal = intersection.centerUnitOuterNormal();

    // get face area
    Scalar faceArea = intersection.geometry().volume();

    // distance vector between barycenters
    GlobalPosition distVec = globalPosJ - globalPosI;

    // compute distance between cell centers
    Scalar dist = distVec.two_norm();

    // compute vectorized permeabilities
    DimMatrix meanPermeability(0);

    problem_.spatialParams().meanK(meanPermeability, problem_.spatialParams().intrinsicPermeability(*elementI),
            problem_.spatialParams().intrinsicPermeability(*elementJ));

    Dune::FieldVector<Scalar, dim> permeability(0);
    meanPermeability.mv(unitOuterNormal, permeability);

    permeability/=viscosity_;

    //calculate current matrix entry
    entry[matrix] = ((permeability * unitOuterNormal) / dist) * faceArea;

    //calculate right hand side
    entry[rhs] = density_ * (permeability * gravity_) * faceArea;

    return;
}

template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::getFluxOnBoundary(EntryType& entry,
const Intersection& intersection, const CellData& cellData, const bool first)
{
    ElementPointer element = intersection.inside();

    // get global coordinates of cell centers
    const GlobalPosition& globalPosI = element->geometry().center();

    // center of face in global coordinates
    const GlobalPosition& globalPosJ = intersection.geometry().center();

    //get face normal
    const Dune::FieldVector<Scalar, dim>& unitOuterNormal = intersection.centerUnitOuterNormal();

    // get face area
    Scalar faceArea = intersection.geometry().volume();

    // distance vector between barycenters
    GlobalPosition distVec = globalPosJ - globalPosI;

    // compute distance between cell centers
    Scalar dist = distVec.two_norm();

    BoundaryTypes bcType;
    problem_.boundaryTypes(bcType, intersection);
    PrimaryVariables boundValues(0.0);

    if (bcType.isDirichlet(pressEqIdx))
    {
        problem_.dirichlet(boundValues, intersection);

        //permeability vector at boundary
        // compute vectorized permeabilities
        DimMatrix meanPermeability(0);

        problem_.spatialParams().meanK(meanPermeability,
                problem_.spatialParams().intrinsicPermeability(*element));

        Dune::FieldVector<Scalar, dim> permeability(0);
        meanPermeability.mv(unitOuterNormal, permeability);

        permeability/= viscosity_;

        //get dirichlet pressure boundary condition
        Scalar pressBound = boundValues;

        //calculate current matrix entry
        entry[matrix] = ((permeability * unitOuterNormal) / dist) * faceArea;
        entry[rhs] = entry[matrix] * pressBound;

        //calculate right hand side
        entry[rhs] -= density_ * (permeability * gravity_) * faceArea;

    }
    //set neumann boundary condition
    else if (bcType.isNeumann(pressEqIdx))
    {
        problem_.neumann(boundValues, intersection);
        Scalar J = boundValues /= density_;

        entry[rhs] = -(J * faceArea);
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "No valid boundary condition type defined for pressure equation!");
    }

    return;
}



template<class TypeTag>
bool FVNonlinearPressure1P<TypeTag>::calculatePressureBoundary(const Intersection& intersection, const CellData& cellData, Scalar& pressBound, DimVector boundaryPoint)
{
    ElementPointer element = intersection.inside();

    // get global coordinates of cell centers
    const GlobalPosition& globalPosI = element->geometry().center();

    // center of face in global coordinates
    const GlobalPosition& globalPosJ = intersection.geometry().center();

    //get face normal
    const Dune::FieldVector<Scalar, dim>& unitOuterNormal = intersection.centerUnitOuterNormal();

    // get face area
    Scalar faceArea = intersection.geometry().volume();

    // distance vector between barycenters
    GlobalPosition distVec = globalPosJ - globalPosI;

    // compute distance between cell centers
    Scalar dist = distVec.two_norm();

    BoundaryTypes bcType;
    problem_.boundaryTypes(bcType, intersection);
    PrimaryVariables boundValues(0.0);

    if (bcType.isDirichlet(pressEqIdx))
    {
        problem_.dirichletAtPos(boundValues, boundaryPoint);
        //problem_.dirichlet(boundValues, intersection);

        //get dirichlet pressure boundary condition
        pressBound = boundValues;

        return false;
    }
    //set neumann boundary condition
    else if (bcType.isNeumann(pressEqIdx))
    {
        problem_.neumann(boundValues, intersection);

        //permeability vector at boundary
        // compute vectorized permeabilities
        DimMatrix meanPermeability(0);

        problem_.spatialParams().meanK(meanPermeability,
                problem_.spatialParams().intrinsicPermeability(*element));

        Dune::FieldVector<Scalar, dim> permeability(0);
        meanPermeability.mv(unitOuterNormal, permeability);

        //permeability/= viscosity_;

        //get dirichlet pressure boundary condition
        Scalar pressBound = boundValues * faceArea;
        pressBound *= -1;

        Scalar coeff = ((permeability * unitOuterNormal) / dist) * faceArea;

        pressBound /= coeff;

        return true;
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "No valid boundary condition type defined for pressure equation!");
    }

    return false;
}


template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::assembleLinear(bool first)
{

    std::cout<<"Assemble Nonlinear Flux approximation" <<std::endl;

    this->A_ = 0;
    this->f_ = 0;

    double mu_plus = 0.5;
    double mu_minus = 0.5;

    ElementIterator eEndIt = problem_.gridView().template end<0>();
    for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
    {
        // get the global index of the cell
        int globalIdxI = problem_.variables().index(*eIt);

        // assemble interior element contributions
        if (eIt->partitionType() == Dune::InteriorEntity)
        {
            // get the cell data
            CellData& cellDataI = problem_.variables().cellData(globalIdxI);

            EntryType entries(0.);

            /*****  source term ***********/
            getSource(entries, *eIt, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];

            /*****  flux term ***********/
            // iterate over all faces of the cell
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                /************* handle interior face *****************/
                if (isIt->neighbor())
                {
                    int faceIdx = isIt->indexInInside();

                    //bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

                    ElementPointer elementNeighbor = isIt->outside();
                    int faceIdxNeigh = isIt->indexInOutside();
                    int globalIdxNeigh = problem_.variables().index(*elementNeighbor);

                    //if(!isOnBoundary){

                    NonlinearFaceStencil faceStencil = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
                    int numElements = faceStencil.getNumElements();
                    //std::vector<ElementPointer> stencilElementVector = faceStencil.getStencilElementVector();
                    std::vector<int> globalIdxNeigbors = faceStencil.getGlobalIdxNeighbors();
                    DimVector coefficients = faceStencil.getCoefficients();
                    std::vector<int> stencilFaceIdxVector = faceStencil.getStencilFaceIdxVector();
                    std::vector<DimVector> stencilCenterPoints = faceStencil.getStencilCenterPoints();

                        for(int i=0 ; i < numElements ; i++){

                            //ElementPointer elementNeighbor = stencilElementVector[i];
                            int globalIdxJ = globalIdxNeigbors[i];
                            int localFaceIdx = stencilFaceIdxVector[i];


                            if(globalIdxJ > -1){
                                // set diagonal entry
                                this->A_[globalIdxI][globalIdxI] += mu_plus*coefficients[i];

                                // set off-diagonal entry
                                this->A_[globalIdxI][globalIdxJ] -= mu_plus*coefficients[i];
                            }else{
                                entries = 0;

                                Scalar pressBound = 0.0;
                                bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellDataI, pressBound,stencilCenterPoints[i]);

                                //set right hand side
                                this->f_[globalIdxI] += mu_plus*coefficients[i]*pressBound;

                                // set diagonal entry
                                this->A_[globalIdxI][globalIdxI] += mu_plus*coefficients[i];
                            }
                        }
                            NonlinearFaceStencil faceStencilNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeigh);
                            int numElementsNeigh = faceStencilNeigh.getNumElements();
                            std::vector<int> globalIdxNeigborsNeigh = faceStencilNeigh.getGlobalIdxNeighbors();
                            DimVector coefficientsNeigh = faceStencilNeigh.getCoefficients();
                            std::vector<int> stencilFaceIdxVectorNeigh = faceStencilNeigh.getStencilFaceIdxVector();
                            std::vector<DimVector> stencilCenterPointsNeigh = faceStencilNeigh.getStencilCenterPoints();

                            for(int i=0 ; i < numElementsNeigh ; i++){

                                //ElementPointer elementNeighbor = stencilElementVector[i];
                                int globalIdxJNeigh = globalIdxNeigborsNeigh[i];
                                int localFaceIdxNeigh = stencilFaceIdxVectorNeigh[i];


                                if(globalIdxJNeigh > -1){
                                    // set diagonal entry
                                    this->A_[globalIdxI][globalIdxNeigh] -= mu_minus*coefficientsNeigh[i];

                                    // set off-diagonal entry
                                    this->A_[globalIdxI][globalIdxJNeigh] += mu_minus*coefficientsNeigh[i];
                                }else{
                                    entries = 0;

                                    Scalar pressBound = 0.0;
                                    bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxNeigh].getElementIntersection(localFaceIdxNeigh)), cellDataI, pressBound,stencilCenterPointsNeigh[i]);

                                    //set right hand side
                                    this->f_[globalIdxI] -= mu_minus*coefficientsNeigh[i]*pressBound;

                                    // set diagonal entry
                                    this->A_[globalIdxI][globalIdxNeigh] -= mu_minus*coefficientsNeigh[i];
                                }

                        }
                    }
                /************* boundary face ************************/

                else
                {
                    int faceIdx = isIt->indexInInside();

                    //bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

                    NonlinearFaceStencil faceStencilPlus = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
                    int numElements = faceStencilPlus.getNumElements();
                    //std::vector<ElementPointer> stencilElementVector = faceStencil.getStencilElementVector();
                    std::vector<int> globalIdxNeigborsPlus = faceStencilPlus.getGlobalIdxNeighbors();
                    DimVector coefficientsPlus = faceStencilPlus.getCoefficients();
                    std::vector<int> stencilFaceIdxVectorPlus = faceStencilPlus.getStencilFaceIdxVector();
                    std::vector<DimVector> stencilCenterPointsPlus = faceStencilPlus.getStencilCenterPoints();

                    for(int i=0 ; i < numElements ; i++){

                       int globalIdxJ = globalIdxNeigborsPlus[i];
                       int localFaceIdx = stencilFaceIdxVectorPlus[i];

                       if(globalIdxJ > -1){

                        // set diagonal entry
                        this->A_[globalIdxI][globalIdxI] += mu_plus*coefficientsPlus[i];

                        // set off-diagonal entry
                        this->A_[globalIdxI][globalIdxJ] -= mu_plus*coefficientsPlus[i];


                       }else{
                           entries = 0;
                           Scalar pressBound = 0.0;
                           bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellDataI, pressBound, stencilCenterPointsPlus[i]);

                           this->A_[globalIdxI][globalIdxI] += mu_plus*coefficientsPlus[i];

                           this->f_[globalIdxI] += mu_plus*coefficientsPlus[i]*pressBound;

                       }
                    }

                       NonlinearFaceStencil faceStencilMinus = interactionVolumes_[globalIdxI].getFaceStencilVectorMinus(faceIdx);
                       int numElementsMinus = faceStencilMinus.getNumElements();
                       std::vector<int> globalIdxNeigborsMinus = faceStencilMinus.getGlobalIdxNeighbors();
                       DimVector coefficientsMinus = faceStencilMinus.getCoefficients();
                       std::vector<int> stencilFaceIdxVectorMinus = faceStencilMinus.getStencilFaceIdxVector();
                       std::vector<DimVector> stencilCenterPointsMinus= faceStencilMinus.getStencilCenterPoints();

                       Scalar pressBoundCenter = 0.0;
                       DimVector boundaryPoint =  faceStencilMinus.getCenterPoint();
                       bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(faceIdx)), cellDataI, pressBoundCenter, boundaryPoint);

                       for(int i=0 ; i < numElementsMinus ; i++){

                           //ElementPointer elementNeighbor = stencilElementVector[i];
                           int globalIdxJMinus = globalIdxNeigborsMinus[i];
                           int localFaceIdxMinus = stencilFaceIdxVectorMinus[i];


                           if(globalIdxJMinus > -1){

                            // set diagonal entry

                            this->f_[globalIdxI] += mu_minus*coefficientsMinus[i]*pressBoundCenter;

                            this->A_[globalIdxI][globalIdxJMinus] += mu_minus*coefficientsMinus[i];


                        }else{
                            entries = 0;
                            Scalar pressBound = 0.0;
                            bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdxMinus)), cellDataI, pressBound,stencilCenterPointsMinus[i]);

                            //set right hand side
                            this->f_[globalIdxI] += mu_minus*coefficientsMinus[i]*pressBoundCenter;
                            this->f_[globalIdxI] -= mu_minus*coefficientsMinus[i]*pressBound;
                        }

                    }

                }

            } //end interfaces loop
    //        printmatrix(std::cout, A_, "global stiffness matrix", "row", 11, 3);

            /*****  storage term ***********/
            entries = 0;
            getStorage(entries, *eIt, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];
    //         set diagonal entry
            this->A_[globalIdxI][globalIdxI] += entries[matrix];
        }
        // assemble overlap and ghost element contributions
        else
        {
            this->A_[globalIdxI] = 0.0;
            this->A_[globalIdxI][globalIdxI] = 1.0;
            this->f_[globalIdxI] = this->pressure(globalIdxI);
        }
    } // end grid traversal
    //printmatrix(std::cout, this->A_, "global stiffness matrix after assempling", "row", 11,3);
    //printvector(std::cout, this->f_, "right hand side", "row", 10);
    return;

}

template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::assembleTPFA(bool first)
{
    // initialization: set matrix A_ to zero
    this->A_ = 0;
    this->f_ = 0;

    ElementIterator eEndIt = problem_.gridView().template end<0>();
    for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
    {
        // get the global index of the cell
        int globalIdxI = problem_.variables().index(*eIt);

        // assemble interior element contributions
        if (eIt->partitionType() == Dune::InteriorEntity)
        {
            // get the cell data
            CellData& cellDataI = problem_.variables().cellData(globalIdxI);

            EntryType entries(0.);

            /*****  source term ***********/
            getSource(entries, *eIt, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];

            /*****  flux term ***********/
            // iterate over all faces of the cell
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                /************* handle interior face *****************/
                if (isIt->neighbor())
                {
                    ElementPointer elementNeighbor = isIt->outside();

                    int globalIdxJ = problem_.variables().index(*elementNeighbor);
                    entries = 0;
                    getFlux(entries, *isIt, cellDataI, first);

                    //set right hand side
                    this->f_[globalIdxI] -= entries[rhs];

                    // set diagonal entry
                    this->A_[globalIdxI][globalIdxI] += entries[matrix];

                    // set off-diagonal entry
                    this->A_[globalIdxI][globalIdxJ] -= entries[matrix];
                } // end neighbor

                /************* boundary face ************************/
                else
                {
                    entries = 0;
                    getFluxOnBoundary(entries, *isIt, cellDataI, first);

                    //set right hand side
                    this->f_[globalIdxI] += entries[rhs];
                    // set diagonal entry
                    this->A_[globalIdxI][globalIdxI] += entries[matrix];
                }
            } //end interfaces loop
    //        printmatrix(std::cout, A_, "global stiffness matrix", "row", 11, 3);

            /*****  storage term ***********/
            entries = 0;
            getStorage(entries, *eIt, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];
    //         set diagonal entry
            this->A_[globalIdxI][globalIdxI] += entries[matrix];
        }
        // assemble overlap and ghost element contributions
        else
        {
            this->A_[globalIdxI] = 0.0;
            this->A_[globalIdxI][globalIdxI] = 1.0;
            this->f_[globalIdxI] = this->pressure(globalIdxI);
        }
    } // end grid traversal
    //printmatrix(std::cout, this->A_, "global stiffness matrix after assempling", "row", 11,3);
    //printvector(std::cout, this->f_, "right hand side", "row", 10);
    return;
}



template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::assemble(bool first)
{

    std::cout<<"Assemble Nonlinear Flux approximation" <<std::endl;

    this->A_ = 0;
    this->f_ = 0;

    ElementIterator eEndIt = problem_.gridView().template end<0>();
    for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
    {
        // get the global index of the cell
        int globalIdxI = problem_.variables().index(*eIt);

        // assemble interior element contributions
        if (eIt->partitionType() == Dune::InteriorEntity)
        {
            // get the cell data
            CellData& cellDataI = problem_.variables().cellData(globalIdxI);

            EntryType entries(0.);

            /*****  source term ***********/
            getSource(entries, *eIt, cellDataI, first);
            this->f_[globalIdxI] += entries[rhs];

            /*****  flux term ***********/
            // iterate over all faces of the cell
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                /************* handle interior face *****************/
                if (isIt->neighbor())
                {
                    int faceIdx = isIt->indexInInside();

                    //bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

                    ElementPointer elementNeighbor = isIt->outside();
                    int faceIdxNeigh = isIt->indexInOutside();
                    int globalIdxNeigh = problem_.variables().index(*elementNeighbor);

                    int faceType = interactionVolumes_[globalIdxI].getFaceType(faceIdx);

                    if(faceType == homogeneous || faceType == heterogeneous)
                    {

                    NonlinearFaceStencil faceStencil = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
                    int numElements = faceStencil.getNumElements();
                    std::vector<int> globalIdxNeigbors = faceStencil.getGlobalIdxNeighbors();
                    DimVector coefficients = faceStencil.getCoefficients();
                    std::vector<int> stencilFaceIdxVector = faceStencil.getStencilFaceIdxVector();
                    std::vector<DimVector> stencilCenterPoints = faceStencil.getStencilCenterPoints();

                    Scalar d_plus = 0.0;
                    Scalar d_minus = 0.0;

                    Scalar M_plus = 0.0;
                    Scalar M_minus = 0.0;
                    Scalar M_plusminus = 0.0;
                    Scalar M_minusplus = 0.0;

                    Scalar f_plus = 0.0;
                    Scalar f_minus = 0.0;

                    Scalar mu_plus = 0.5;
                    Scalar mu_minus = 0.5;

                        for(int i=0 ; i < numElements ; i++){

                            //ElementPointer elementNeighbor = stencilElementVector[i];
                            int globalIdxJ = globalIdxNeigbors[i];
                            int localFaceIdx = stencilFaceIdxVector[i];


                            if(globalIdxJ > -1){


                                M_plus += coefficients[i];
                                    int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                                    if(localFaceType == homogeneous){
                                        if(globalIdxJ == globalIdxNeigh){
                                            M_plusminus += coefficients[i];
                                        }else{
                                            d_plus += coefficients[i]*this->pressure(globalIdxJ);
                                        }
                                    }
                                    else if(localFaceType == heterogeneous)
                                    {
                                        Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                                        Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                                        if(globalIdxJ == globalIdxNeigh){
                                            M_plus -= coefficients[i]*harmonicCoeff;
                                            M_plusminus += coefficients[i]*neighHarmonicCoeff;
                                        }else{
                                            d_plus += coefficients[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                                            M_plus -= coefficients[i]*harmonicCoeff;
                                        }


                                    }

                            }else{
                                entries = 0;
                                Scalar pressBound = 0.0;

                                bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellDataI, pressBound, stencilCenterPoints[i]);

                                if(!isNeumann)
                                {
                                    M_plus += coefficients[i];
                                    d_plus += coefficients[i]*pressBound;
                                }
                                else
                                {
                                    d_plus += coefficients[i]*pressBound;
                                }
                            }
                        }
                            NonlinearFaceStencil faceStencilNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeigh);
                            int numElementsNeigh = faceStencilNeigh.getNumElements();
                            std::vector<int> globalIdxNeigborsNeigh = faceStencilNeigh.getGlobalIdxNeighbors();
                            DimVector coefficientsNeigh = faceStencilNeigh.getCoefficients();
                            std::vector<int> stencilFaceIdxVectorNeigh = faceStencilNeigh.getStencilFaceIdxVector();
                            std::vector<DimVector> stencilCenterPointsNeigh = faceStencilNeigh.getStencilCenterPoints();

                            for(int i=0 ; i < numElementsNeigh ; i++){

                                //ElementPointer elementNeighbor = stencilElementVector[i];
                                int globalIdxJNeigh = globalIdxNeigborsNeigh[i];
                                int localFaceIdxNeigh = stencilFaceIdxVectorNeigh[i];


                                if(globalIdxJNeigh > -1){
                                    M_minus += coefficientsNeigh[i];
                                    int localFaceType = interactionVolumes_[globalIdxNeigh].getFaceType(localFaceIdxNeigh);

                                    if(localFaceType == homogeneous){
                                        if(globalIdxJNeigh == globalIdxI){
                                            M_minusplus += coefficientsNeigh[i];
                                        }else{
                                            d_minus += coefficientsNeigh[i]*this->pressure(globalIdxJNeigh);
                                        }
                                    }
                                    else if(localFaceType == heterogeneous)
                                    {
                                        Scalar harmonicCoeff = interactionVolumes_[globalIdxNeigh].getHarmonicAveragingCoefficient(localFaceIdxNeigh);
                                        Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxNeigh].getNeighboringHarmonicAveragingCoefficient(localFaceIdxNeigh);

                                        if(globalIdxJNeigh == globalIdxI){
                                            M_minus -= coefficientsNeigh[i]*harmonicCoeff;
                                            M_minusplus += coefficientsNeigh[i]*neighHarmonicCoeff;
                                        }else{
                                            d_minus += coefficientsNeigh[i]*neighHarmonicCoeff*this->pressure(globalIdxJNeigh);
                                            M_minus -= coefficientsNeigh[i]*harmonicCoeff;
                                        }


                                    }

                                }else{
                                    entries = 0;
                                    Scalar pressBound = 0.0;

                                    bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxNeigh].getElementIntersection(localFaceIdxNeigh)), cellDataI, pressBound,stencilCenterPointsNeigh[i]);

                                    if(!isNeumann)
                                    {
                                        M_minus += coefficientsNeigh[i];
                                        d_minus += coefficientsNeigh[i]*pressBound;
                                    }
                                    else
                                    {
                                        d_minus += coefficientsNeigh[i]*pressBound;
                                    }
                                }
                        }

                            mu_plus = 0.5;
                            mu_minus = 0.5;

                            if(std::abs(d_plus) > 1.0e-12 || std::abs(d_minus) > 1.0e-12 ){
                                mu_plus = d_minus/(d_minus + d_plus);
                                mu_minus = d_plus/(d_minus + d_plus);
                            }

                            this->f_[globalIdxI] += mu_plus*f_plus + mu_minus*f_minus;

                            this->A_[globalIdxI][globalIdxI] += mu_plus*M_plus;
                            this->A_[globalIdxI][globalIdxI] += mu_minus*M_minusplus;
                            this->A_[globalIdxI][globalIdxNeigh] -= mu_minus*M_minus;
                            this->A_[globalIdxI][globalIdxNeigh] -= mu_plus*M_plusminus;


                    }

                }
                /************* boundary face ************************/

                else
                {

                    BoundaryTypes bcType;
                    problem_.boundaryTypes(bcType, *isIt);

                    if (bcType.isNeumann(pressEqIdx))
                    {
                        entries = 0;
                        getFluxOnBoundary(entries, *isIt, cellDataI, first);

                        //set right hand side
                        this->f_[globalIdxI] += entries[rhs];
                        // set diagonal entry
                        this->A_[globalIdxI][globalIdxI] += entries[matrix];
                    }
                    else
                    {

                        int faceIdx = isIt->indexInInside();

                        //bool isOnBoundary = interactionVolumes_[globalIdxI].isOnBounday();

                        NonlinearFaceStencil faceStencilPlus = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx);
                        int numElements = faceStencilPlus.getNumElements();
                        //std::vector<ElementPointer> stencilElementVector = faceStencil.getStencilElementVector();
                        std::vector<int> globalIdxNeigborsPlus = faceStencilPlus.getGlobalIdxNeighbors();
                        DimVector coefficientsPlus = faceStencilPlus.getCoefficients();
                        std::vector<int> stencilFaceIdxVectorPlus = faceStencilPlus.getStencilFaceIdxVector();
                        std::vector<DimVector> stencilCenterPointsPlus = faceStencilPlus.getStencilCenterPoints();

                        Scalar d_plus = 0.0;
                        Scalar d_minus = 0.0;

                        Scalar M_plus = 0.0;
                        Scalar M_minus = 0.0;
                        Scalar M_plusminus = 0.0;
                        Scalar M_minusplus = 0.0;

                        Scalar f_plus = 0.0;
                        Scalar f_minus = 0.0;

                        Scalar mu_plus = 0.5;
                        Scalar mu_minus = 0.5;

                        for(int i=0 ; i < numElements ; i++){

                           //ElementPointer elementNeighbor = stencilElementVector[i];
                           int globalIdxJ = globalIdxNeigborsPlus[i];
                           int localFaceIdx = stencilFaceIdxVectorPlus[i];


                           if(globalIdxJ > -1){

                            M_plus += coefficientsPlus[i];
                            int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                            if(localFaceType == homogeneous){
                                d_plus += coefficientsPlus[i]*this->pressure(globalIdxJ);
                            }
                            else if(localFaceType == heterogeneous){
                                Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                                Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                                d_plus += coefficientsPlus[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                                M_plus -= coefficientsPlus[i]*harmonicCoeff;
                            }


                           }else{
                               entries = 0;
                               Scalar pressBound = 0.0;

                               bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellDataI, pressBound, stencilCenterPointsPlus[i]);

                               if(!isNeumann)
                               {
                                   M_plus += coefficientsPlus[i];

                                   if(localFaceIdx == faceIdx) M_plusminus += coefficientsPlus[i];
                                   else d_plus += coefficientsPlus[i]*pressBound;
                               }
                               else
                               {
                                   d_plus += coefficientsPlus[i]*pressBound;
                               }

                           }
                        }

                           NonlinearFaceStencil faceStencilMinus = interactionVolumes_[globalIdxI].getFaceStencilVectorMinus(faceIdx);
                           int numElementsMinus = faceStencilMinus.getNumElements();
                           std::vector<int> globalIdxNeigborsMinus = faceStencilMinus.getGlobalIdxNeighbors();
                           DimVector coefficientsMinus = faceStencilMinus.getCoefficients();
                           std::vector<int> stencilFaceIdxVectorMinus = faceStencilMinus.getStencilFaceIdxVector();
                           std::vector<DimVector> stencilCenterPointsMinus= faceStencilMinus.getStencilCenterPoints();

                           for(int i=0 ; i < numElementsMinus ; i++){
                               int globalIdxJ = globalIdxNeigborsMinus[i];
                               int localFaceIdx = stencilFaceIdxVectorMinus[i];


                               if(globalIdxJ > -1){

                                M_minus += coefficientsMinus[i];

                                if(globalIdxJ == globalIdxI){
                                    M_minusplus += coefficientsMinus[i];
                                }
                                else {

                                    int localFaceType = interactionVolumes_[globalIdxI].getFaceType(localFaceIdx);

                                    if(localFaceType == homogeneous){
                                        d_minus += coefficientsMinus[i]*this->pressure(globalIdxJ);
                                    }
                                    else if(localFaceType == heterogeneous){
                                        Scalar harmonicCoeff = interactionVolumes_[globalIdxI].getHarmonicAveragingCoefficient(localFaceIdx);
                                        Scalar neighHarmonicCoeff = interactionVolumes_[globalIdxI].getNeighboringHarmonicAveragingCoefficient(localFaceIdx);

                                        d_minus += coefficientsMinus[i]*neighHarmonicCoeff*this->pressure(globalIdxJ);
                                        M_minus -= coefficientsMinus[i]*harmonicCoeff;
                                    }
                                }

                            }else{
                                entries = 0;

                                Scalar pressBound = 0.0;
                                bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(localFaceIdx)), cellDataI, pressBound,stencilCenterPointsMinus[i]);

                                if(!isNeumann)
                                {
                                    M_minus += coefficientsMinus[i];
                                    d_minus += coefficientsMinus[i]*pressBound;
                                }
                                else
                                {
                                   d_minus += coefficientsMinus[i]*pressBound;
                                }

                            }

                        }

                           mu_plus = 0.5;
                           mu_minus = 0.5;

                           if(d_plus > 1.0e-12 || d_minus > 1.0e-12 ){
                            mu_plus = d_minus/(d_minus + d_plus);
                            mu_minus = d_plus/(d_minus + d_plus);
                           }

                           this->f_[globalIdxI] += mu_plus*f_plus + mu_minus*f_minus;

                           this->A_[globalIdxI][globalIdxI] += mu_plus*M_plus;
                           this->A_[globalIdxI][globalIdxI] += mu_minus*M_minusplus;

                           Scalar pressBound = 0.0;
                           DimVector boundaryPoint =  faceStencilMinus.getCenterPoint();
                           bool isNeumann = calculatePressureBoundary(*(interactionVolumes_[globalIdxI].getElementIntersection(faceIdx)), cellDataI, pressBound, boundaryPoint);

                           this->f_[globalIdxI] += mu_minus*M_minus*pressBound;
                           this->f_[globalIdxI] += mu_plus*M_plusminus*pressBound;

                           //this->A_[globalIdxI][globalIdxNeigh] -= mu_minus*M_minus;
                           //this->A_[globalIdxI][globalIdxNeigh] -= mu_plus*M_plusminus;
                }
                }

            } //end interfaces loop
        }
        // assemble overlap and ghost element contributions
        else
        {
            this->A_[globalIdxI] = 0.0;
            this->A_[globalIdxI][globalIdxI] = 1.0;
            this->f_[globalIdxI] = this->pressure(globalIdxI);
        }
    } // end grid traversal
    return;

}

//!Initialize the global matrix of the system of equations to solve
template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::initializeMatrix()
{
    initializeMatrixRowSize();
    this->A_.endrowsizes();
    initializeMatrixIndices();
    this->A_.endindices();
}

//!Initialize the global matrix of the system of equations to solve
template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::initializeMatrixRowSize()
{
    // determine matrix row sizes
    if(linear_){
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            // cell index
            int globalIdxI = problem_.variables().index(*eIt);

            // initialize row size
            int rowSize = 0;

            std::set<int> indices;
            indices.insert(globalIdxI);

            // run through all intersections with neighbors
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {

                if (isIt->neighbor())
                {
                    ElementPointer elementNeighbor = isIt->outside();
                    int globalIdxNeigh = problem_.variables().index(*elementNeighbor);
                    int faceIdx = isIt->indexInInside();
                    int faceIdxNeig = isIt->indexInOutside();

                    indices.insert(globalIdxNeigh);

                    std::vector<int> globalIdxNeigbors = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx).getGlobalIdxNeighbors();
                    std::vector<int> globalIdxNeigborsNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeig).getGlobalIdxNeighbors();
                    int numNeigh = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx).getNumElements();
                    int numNeighNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeig).getNumElements();

                    for(int i = 0; i<numNeigh ; i++){
                        indices.insert(globalIdxNeigbors[i]);
                    }

                    for(int i = 0; i< numNeighNeigh; i++){
                        indices.insert(globalIdxNeigborsNeigh[i]);
                    }

                }else
                {

                }
            }

            indices.erase(-1);

            rowSize += indices.size();

            this->A_.setrowsize(globalIdxI, rowSize);
        }
    }else
    {
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            // cell index
            int globalIdxI = problem_.variables().index(*eIt);

            // initialize row size
            int rowSize = 1;

            // run through all intersections with neighbors
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                if (isIt->neighbor())
                    rowSize++;
            }
            this->A_.setrowsize(globalIdxI, rowSize);
        }

    }

    return;
}

//!Initialize the global matrix of the system of equations to solve
template<class TypeTag>
void FVNonlinearPressure1P<TypeTag>::initializeMatrixIndices()
{

    if(linear_){
        // determine matrix row sizes
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            // cell index
            int globalIdxI = problem_.variables().index(*eIt);

            std::set<int> indices;
            indices.insert(globalIdxI);

            // run through all intersections with neighbors
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
            {

                if (isIt->neighbor())
                {
                    ElementPointer elementNeighbor = isIt->outside();
                    int globalIdxNeigh = problem_.variables().index(*elementNeighbor);
                    int faceIdx = isIt->indexInInside();
                    int faceIdxNeig = isIt->indexInOutside();

                    indices.insert(globalIdxNeigh);

                    std::vector<int> globalIdxNeigbors = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx).getGlobalIdxNeighbors();
                    std::vector<int> globalIdxNeigborsNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeig).getGlobalIdxNeighbors();
                    int numNeigh = interactionVolumes_[globalIdxI].getFaceStencilVectorPlus(faceIdx).getNumElements();
                    int numNeighNeigh = interactionVolumes_[globalIdxNeigh].getFaceStencilVectorPlus(faceIdxNeig).getNumElements();

                    for(int i = 0; i<numNeigh ; i++){
                        indices.insert(globalIdxNeigbors[i]);
                    }

                    for(int i = 0; i< numNeighNeigh; i++){
                        indices.insert(globalIdxNeigborsNeigh[i]);
                    }

                }else
                {

                }
            }

            indices.erase(-1);

            std::set<int>::iterator itSet = indices.begin();

            for(;itSet != indices.end() ; itSet++)
            {
                int globalIdxJ = *itSet;
                this->A_.addindex(globalIdxI, globalIdxJ);
            }

        }
    }else{
        // determine position of matrix entries
        ElementIterator eEndIt = problem_.gridView().template end<0>();
        for (ElementIterator eIt = problem_.gridView().template begin<0>(); eIt != eEndIt; ++eIt)
        {
            // cell index
            int globalIdxI = problem_.variables().index(*eIt);

            // add diagonal index
            this->A_.addindex(globalIdxI, globalIdxI);

            // run through all intersections with neighbors
            IntersectionIterator isEndIt = problem_.gridView().iend(*eIt);
            for (IntersectionIterator isIt = problem_.gridView().ibegin(*eIt); isIt != isEndIt; ++isIt)
                if (isIt->neighbor())
                {
                    // access neighbor
                    ElementPointer outside = isIt->outside();
                    int globalIdxJ = problem_.variables().index(*outside);

                    // add off diagonal index
                    this->A_.addindex(globalIdxI, globalIdxJ);
                }
        }
    }

    return;

}


}
#endif
