#ifndef DUMUX_MPFAO2DFLUXCALCULATOR_HH
#define DUMUX_MPFAO2DFLUXCALCULATOR_HH

#include <dumux/common/math.hh>
#include <Dense>

namespace Dumux{

template<class TypeTag>
class MpfaO2DFluxCalculator
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MpfaOInteractionVolume) InteractionVolume;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename InteractionVolume::DynamicMatrix TransMatrix;
    typedef typename InteractionVolume::DynamicVector FluxVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::EntityPointer VertexPointer;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Intersection Intersection;

    typedef typename GridView::Grid::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

public:

    static Scalar calculateScvFaceFlux(const Problem &problem, const Element &element, int localIsIndex,
                                const InteractionVolume &interactionVolume, const ElementVolumeVariables &elemVolVars,
                                                const FVElementGeometry &fvGeometry, int phaseIdx, int &regionFaceIdx)
    {
        const TransMatrix &T = interactionVolume.getTransMatrix();
        // Set up vector with the primary variable and boundary condition values
        int noOfFaces = interactionVolume.getNumberOfSubFaces();
        int noOfSubVols = interactionVolume.getNumberOfSubVols();
        int NoOfHeads = noOfSubVols + interactionVolume.getNumberOfBoundaryFaces();
        FluxVector piezoHeads = FluxVector::Zero(NoOfHeads);
        FluxVector fluxes = FluxVector::Zero(noOfFaces);

        // find the face index of the facet in the interaction region
        // and determine if the flux has to be multiplied by -1
        bool switchSign;
        int faceIndexInRegion = interactionVolume.getFaceIndexInRegion(problem, element, localIsIndex, switchSign, regionFaceIdx);

        // set up piezometric head vector entries of the sub volumes
        for (int head = 0; head < noOfSubVols; head++)
        {
            const typename InteractionVolume::SubVolume& subVolume = interactionVolume.getSubVolume(head);
            int idxInStencil = fvGeometry.findElementInNeighbors(subVolume.globalIdx);
            Scalar pressure = elemVolVars[idxInStencil].fluidState().pressure(phaseIdx);

            // turn pressure into piezometric head
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                // ask for the gravitational acceleration at the given point
                const GlobalPosition subVolCenter = subVolume.element->geometry().center();
                GlobalPosition g( problem.gravityAtPos(subVolCenter) );
                const Scalar density = elemVolVars[idxInStencil].fluidState().density(phaseIdx);
                // make gravity acceleration a force
                Scalar f = g * subVolCenter;
                f *= density;
                // calculate the final potential
                pressure -= f;
            }
            // set value in the piezometric heads vector
            piezoHeads(head) = pressure;
        }

        // set the entries in piezo head vector for the Dirichlet boundary conditions
        int dirichletFaceCounter = 0;
        const std::set<int> &dirichletFaceIndexSet = interactionVolume.getDirichletFaceIndexSet();
        std::set<int>::iterator it = dirichletFaceIndexSet.begin();
        for(; it != dirichletFaceIndexSet.end(); ++it)
        {
            const typename InteractionVolume::SubVolumeFace& subVolFace = interactionVolume.getSubVolumeFace(*it);

            Scalar pressure; PrimaryVariables values; typename VolumeVariables::FluidState fluidState;
            if (subVolFace.faceType == InteractionVolume::FaceTypes::DirichletFace)
            {
                problem.dirichletAtPos(values, subVolFace.center);
                VolumeVariables::completeFluidState(values, problem, element, fvGeometry, 0, fluidState);
                pressure = fluidState.pressure(phaseIdx);
            }
            else if ((subVolFace.faceType == InteractionVolume::FaceTypes::InternalDirichletFace))
            {
                problem.internalDirichletAtPos(values, subVolFace.center);
                VolumeVariables::completeFluidState(values, problem, element, fvGeometry, 0, fluidState);
                pressure = fluidState.pressure(phaseIdx);
            }
            else
                DUNE_THROW(Dune::NotImplemented, "Dirichlet face has neither internal nor external dirichlet flag assigned to!");

            // evaluate gravity at the position of the continutiy point on the face
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                // ask for the gravitational acceleration at the given point
                const typename InteractionVolume::SubVolume& positiveSubVolume = interactionVolume.getSubVolume(subVolFace.positiveSubVolIndex);
                const GlobalPosition contiPoint = positiveSubVolume.contiPoints[subVolFace.localIdxOnPositive];
                GlobalPosition g( problem.gravityAtPos(contiPoint) );
                const Scalar density = fluidState.density(phaseIdx);
                // make gravity acceleration a force
                Scalar f = g * contiPoint;
                f *= density;
                // calculate the final potential
                pressure -= f;
            }
            piezoHeads(noOfSubVols + dirichletFaceCounter) = pressure;
            dirichletFaceCounter++;
        }

        if (T.cols() != piezoHeads.size())
            DUNE_THROW(Dune::NotImplemented, "Error in mpfao2dfluxcalculator.hh, l. 123");
        if (T.rows() != fluxes.size())
            DUNE_THROW(Dune::NotImplemented, "T.rows() != fluxes.size()");

        fluxes = T * piezoHeads;

        if (switchSign)
            return -fluxes(faceIndexInRegion);
        else
            return fluxes(faceIndexInRegion);
    }
};
}
#endif
