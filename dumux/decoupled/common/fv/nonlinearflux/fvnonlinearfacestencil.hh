#ifndef DUMUX_FVNONLINEARFACESTENCIL_HH
#define DUMUX_FVNONLINEARFACESTENCIL_HH

#include <dumux/common/math.hh>
#include <dumux/decoupled/1p/1pproperties.hh>

namespace Dumux{

template<class TypeTag>
class FVNonlinearFaceStencil{

private:

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum
    {
        dim = GridView::dimension
    };

    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;

    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP(TypeTag, SolutionTypes) SolutionTypes;
    typedef typename SolutionTypes::PrimaryVariables PrimaryVariables;

    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldVector<DimVector, dim> FieldVectorVector;
    typedef std::vector<DimVector> VectorFieldVector;
    typedef std::vector<DimMatrix> VectorFieldMatrix;

public:

    FVNonlinearFaceStencil(int faceIdx, ElementPointer ele): stored_(false), element_(ele)
    {
        faceIdx_ = faceIdx;
        numElements_ = dim;
    }

    void setFaceIdx(int faceIdx)
    {
        faceIdx_ = faceIdx;
    }

    int getFaceIdx(){
        return faceIdx_;
    }

    //! Mark storage as completed
    void setStored()
    {
        stored_ = true;
    }

    //! Returns true if information has already been stored
    bool isStored() const
    {
        return stored_;
    }

    void setSamePermeability(bool samePerm)
    {
        samePermeability_ = samePerm;
    }

    bool isSamePermeability()
    {
        return samePermeability_;
    }

    void setPermeability(DimMatrix permeability)
    {
        permeability_ = permeability;
    }

    DimMatrix getPermeability()
    {
        return permeability_;
    }

    void setCoNormal(DimVector coNormal)
    {
        coNormal_ = coNormal;
    }

    DimVector getCoNormal()
    {
        return coNormal_;
    }

    void setNormal(DimVector normal)
    {
        normal_ = normal;
    }

    DimVector getNormal()
    {
        return normal_;
    }

    void setCoefficients(DimVector coefficients)
    {
        coefficients_ = coefficients;
    }

    DimVector getCoefficients()
    {
        return coefficients_;
    }

    void setCenterPoint(DimVector centerPoint)
    {
        centerPoint_ = centerPoint;
    }

    DimVector getCenterPoint()
    {
        return centerPoint_;
    }

    void setStencilCenterPoints(VectorFieldVector stencilCenterPoints)
    {
        stencilCenterPoints_ = stencilCenterPoints;
    }

    VectorFieldVector getStencilCenterPoints()
    {
        return stencilCenterPoints_;
    }

    void insertStencilCenterPoint(DimVector centerPoint){
        stencilCenterPoints_.push_back(centerPoint);
    }

    std::vector<bool> getIsPointOnBoundary()
    {
        return isPointOnBoundary_;
    }

    void insertIsPointOnBoundary(bool onBoundary){
        isPointOnBoundary_.push_back(onBoundary);
    }

    void setFaceArea(Scalar faceArea)
    {
        faceArea_ = faceArea;
    }

    Scalar getFaceArea()
    {
        return faceArea_;
    }

    int getNumElements()
    {
        return numElements_;
    }


    void setStencilElements(std::vector<ElementPointer> stencilElements)
    {
        stencilElementVector_ = stencilElements;
    }

    std::vector<ElementPointer> getStencilElementVector(){
        return stencilElementVector_;
    }

    void insertStencilElement(ElementPointer element){
        stencilElementVector_.push_back(element);
    }


    void insertStencilFaceIdx(int faceIdx){
        stencilFaceIdx_.push_back(faceIdx);
    }

    int getStencilFaceIdx(int Idx)
    {
        return stencilFaceIdx_[Idx];
    }

    std::vector<int> getStencilFaceIdxVector(){
        return stencilFaceIdx_;
    }

    void insertSpanVector(DimVector spanVector)
    {
        spanVectors_.push_back(spanVector);
    }

    VectorFieldVector getSpanVectors(){
        return spanVectors_;
    }

    std::vector<int> getGlobalIdxNeighbors(){
        return globalIdxNeighbors_;
    }

    void insertGlobalIdxNeighbors(int globalIdx){
        globalIdxNeighbors_.push_back(globalIdx);
    }

    bool checkLinearIndependence(DimVector vector)
    {
        int numInsertedVectors = spanVectors_.size();

        for(int k = 0 ; k<numInsertedVectors; k++){
            if(dim == 3){
                DimVector crossProductVector = crossProduct(vector,spanVectors_[k]);
                double crossProductVector_norm = crossProductVector.two_norm();
                if(crossProductVector_norm < 1.0e-10) return false;
            }else if(dim == 2){
                DimVector spanVector = spanVectors_[k];
                double determinant = spanVector[0]*vector[1] - spanVector[1]*vector[0];
                if(std::abs(determinant)< 1.0e-10) return false;
            }
        }

        return true;
    }

    //Cramer's rule for the solution of 2D or 3D system of equation
    void calculateCoefficients()
    {

        if(dim == 2){

            DimVector t_1 = spanVectors_[0];
            DimVector t_2 = spanVectors_[1];
            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            Scalar coNormalNorm = coNormal_.two_norm();

            DimVector coNormal = coNormal_;
            coNormal /= coNormalNorm;

//            if(std::abs(t_1*coNormal -1.0)<1.0e-12){
//
//                coefficients_[0] = 1.0 ;
//                coefficients_[1] = 0 ;
//
//                coefficients_[0] /= t_1_norm ;
//                //coefficients_[1] /= t_2_norm ;
//
//                coefficients_ *= coNormalNorm;
//
//                return;
//            }

            Scalar A_f = t_1[0]*t_2[1] - t_1[1]*t_2[0];
            Scalar A_f_1 = coNormal[0]*t_2[1] - coNormal[1]*t_2[0];
            Scalar A_f_2 = t_1[0]*coNormal[1] - t_1[1]*coNormal[0];

            if(std::abs(A_f) < 1.0e-4) std::cout<<"Error: can't calculate span coefficients" << std::endl;

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;

            coefficients_ *= coNormalNorm;

        }else if(dim ==3){

            DimVector t_1 = spanVectors_[0];
            DimVector t_2 = spanVectors_[1];
            DimVector t_3 = spanVectors_[2];
            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();
            Scalar t_3_norm = t_3.two_norm();
            Scalar coNormalNorm = coNormal_.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            t_3 /= t_3_norm;

            DimVector coNormal = coNormal_;
            coNormal /= coNormalNorm;

            //            if(std::abs(t_1*coNormal -1.0)<1.0e-12){
            //
            //                coefficients_[0] = 1.0 ;
            //                coefficients_[1] = 0 ;
            //
            //
            //                coefficients_[0] /= t_1_norm ;
            //                //coefficients_[1] /= t_2_norm ;
            //
            //                coefficients_ *= coNormalNorm;
            //
            //                return;
            //            }


            Scalar A_f = t_1*crossProduct(t_2,t_3);
            Scalar A_f_1 = coNormal * crossProduct(t_2,t_3);
            Scalar A_f_2 = t_1 * crossProduct(coNormal,t_3);
            Scalar A_f_3 = t_1 * crossProduct(t_2,coNormal);

            if(std::abs(A_f) < 1.0e-4){
                std::cout<<"Error: can't calculate span coefficients" << std::endl;
            }

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;
            coefficients_[2] = A_f_3 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;
            coefficients_[2] /= t_3_norm ;

            coefficients_ *= coNormalNorm;

        }

    }

    bool calculateCoefficients(DimVector coNormal, DimVector t_1, DimVector t_2)
    {

        if(dim == 2){

            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            Scalar coNormalNorm = coNormal.two_norm();

            coNormal /= coNormalNorm;

            //if(std::abs(t_1*coNormal -1)<1.0e-12) return true;

            Scalar A_f = t_1[0]*t_2[1] - t_1[1]*t_2[0];
            Scalar A_f_1 = coNormal[0]*t_2[1] - coNormal[1]*t_2[0];
            Scalar A_f_2 = t_1[0]*coNormal[1] - t_1[1]*coNormal[0];

            if(std::abs(A_f) < 1.0e-4) return false;

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;

            coefficients_ *= coNormalNorm;

            if(coefficients_[0] >= -1.0e-10 && coefficients_[1] >= -1.0e-10){
                return true;
            }
            else return false;



        }else if(dim ==3){
            std::cout<<"ERROR: Wrong dimension!!!!!"<<std::endl;
            return false;
        }

        return false;

    }

    bool calculateCoefficients(DimVector coNormal, DimVector t_1, DimVector t_2, DimVector t_3)
    {

        if(dim == 2){
            std::cout<<"ERROR: Wrong dimension!!!!!"<<std::endl;
        }else if(dim ==3)
        {
            Scalar t_1_norm = t_1.two_norm();
            Scalar t_2_norm = t_2.two_norm();
            Scalar t_3_norm = t_3.two_norm();
            Scalar coNormalNorm = coNormal.two_norm();

            t_1 /= t_1_norm;
            t_2 /= t_2_norm;
            t_3 /= t_3_norm;

            DimVector coNormal = coNormal_;
            coNormal /= coNormalNorm;

            //if(std::abs(t_1*coNormal -1)<1.0e-12) return true;

            Scalar A_f = t_1*crossProduct(t_2,t_3);
            Scalar A_f_1 = coNormal * crossProduct(t_2,t_3);
            Scalar A_f_2 = t_1 * crossProduct(coNormal,t_3);
            Scalar A_f_3 = t_1 * crossProduct(t_2,coNormal);


            if(std::abs(A_f) < 1.0e-4){
                return false;
            }

            coefficients_[0] = A_f_1 / A_f ;
            coefficients_[1] = A_f_2 / A_f ;
            coefficients_[2] = A_f_3 / A_f ;

            coefficients_[0] /= t_1_norm ;
            coefficients_[1] /= t_2_norm ;
            coefficients_[2] /= t_3_norm ;

            coefficients_ *= coNormalNorm;

            if(coefficients_[0] >=-1.0e-10 &&  coefficients_[1] >=-1.0e-10 && coefficients_[2] >=-1.0e-10){
                return true;
            }
            else return false;

        }

        return false;
    }

private:
    int faceIdx_;
    bool stored_;
    bool samePermeability_;
    Scalar faceArea_;
    std::vector<ElementPointer> stencilElementVector_;
    std::vector<int> stencilFaceIdx_;
    std::vector<int> inverseStencilFaceIdx_;
    std::vector<int> globalIdxNeighbors_;
    ElementPointer element_;
    VectorFieldVector spanVectors_;
    DimVector coNormal_;
    DimVector normal_;
    DimVector coefficients_;
    DimMatrix permeability_;
    int numElements_;
    DimVector centerPoint_;
    VectorFieldVector stencilCenterPoints_;
    std::vector<bool> isPointOnBoundary_;
};
}
#endif /* FVNONLINEARFACESTENCIL_HH_ */
